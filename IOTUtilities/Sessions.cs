﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IOTBusinessEntities;

namespace IOTUtilities
{
    public class Sessions
    {
        public Sessions()
        {
            

        }

      
        public SessionEntity GetSessionEntity()
        {
            var session = HttpContext.Current.Session;
            if (session != null)
                return (SessionEntity)session["SessionEntity"];
            else
                return null;
        }

        public void SetSessionEntity(SessionEntity sessionEntity)
        {
            var session = HttpContext.Current.Session;
            session.Add("SessionEntity", sessionEntity);
        }

    
        public void SetSession(string key, string value)
        {
            var session = HttpContext.Current.Session;
            session.Add(key, value);
        }

        public string GetSession(string key)
        {
            var session = HttpContext.Current.Session;
            return Convert.ToString(session[key]);
        }

        public string CheckSessionExpired()
        {
            var session = HttpContext.Current.Session;
            if (session != null)
            {
                if (GetSessionEntity() != null)
                    //if (GetDomainMasterEntity() != null)
                    return "";
            }
            return "Session Expired.";
        }

        public void ClearSession()
        {
            HttpContext.Current.Session.Clear();
        }
    }
}