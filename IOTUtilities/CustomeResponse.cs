﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IOTBusinessEntities;

namespace IOTUtilities
{
    public class CustomeResponse
    {

        public static ResponseEntity getSystemError(Exception ex)
        {
            ResponseEntity objResponseEntity = new ResponseEntity();
            objResponseEntity.IsComplete = false;
            objResponseEntity.Description = "system error";
            objResponseEntity.data = ex.Message;
           //objResponseEntity.message = MessageCode.MsgInJson(MsgCode.ok);

            return objResponseEntity;
        }

        public static ResponseEntity getOkResponse(object data)
        {
            ResponseEntity objResponseEntity = new ResponseEntity();
            objResponseEntity.IsComplete = true;
            objResponseEntity.Description = "success";
            objResponseEntity.data = data;
            objResponseEntity.message = MessageCode.MsgInJson(MsgCode.ok);

            return objResponseEntity;
        }

        public static ResponseEntity getOkResponse()
        {
            ResponseEntity objResponseEntity = new ResponseEntity();
            objResponseEntity.IsComplete = true;
            objResponseEntity.Description = "success";
            objResponseEntity.data = null;
            objResponseEntity.message = MessageCode.MsgInJson(MsgCode.ok);

            return objResponseEntity;
        }

        public static ResponseEntity getMsgResponse(string Description)
        {
            ResponseEntity objResponseEntity = new ResponseEntity();
            objResponseEntity.IsComplete = true;
            objResponseEntity.Description = Description;
            objResponseEntity.data = null;
            objResponseEntity.message = MessageCode.MsgInJson(MsgCode.ok);

            return objResponseEntity;
        }

        //public static ResponseEntity getSaveSuccessResponse()
        //{
        //    ResponseEntity objResponseEntity = new ResponseEntity();
        //    objResponseEntity.IsComplete = true;
        //    objResponseEntity.Description = "saved successfully.";
        //    objResponseEntity.data = null;

        //    return objResponseEntity;
        //}

        public static ResponseEntity getFailResponse(string Description)
        {
            ResponseEntity objResponseEntity = new ResponseEntity();
            objResponseEntity.IsComplete = false;
            objResponseEntity.Description = Description;
            objResponseEntity.data = null;

            return objResponseEntity;
        }


    }
}