﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace HashAndSalt
{
    public class HashComputer
    {
        public string GetPasswordHashAndSalt(string message)
        {   
            // Let us use SHA256 algorithm to 
            // generate the hash from this salted password
           ////  SHA256 md5 = new SHA256CryptoServiceProvider();
          //  byte[] dataBytes = Utility.GetBytes(message);
          //  byte[] resultBytes = md5.ComputeHash(dataBytes);

          //  // return the hash string to the caller
          //  return Utility.GetString(resultBytes);

            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] dataBytes = Utility.GetBytes(message);
            byte[] resultBytes = md5.ComputeHash(dataBytes);

            // return the hash string to the caller
            return Utility.GetString(resultBytes);


          
           
        }
    }
}
