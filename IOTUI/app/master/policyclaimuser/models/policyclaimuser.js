﻿"use strict"

angular.module('app.policyclaimuser').factory('policyclaimFactory', function ($http, $q, APP_CONFIG) {

    var getPolicyClaimList = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/PolicyClaim/getPolicyClaimList";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
   
    var getPolicyClaimById = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/PolicyClaim/getPolicyClaimById";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    
    var SavePolicyClaim = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/PolicyClaim/CreatePolicyClaim";
        var deferred = $q.defer();
        $http.post(apiUrl, param, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }


    var SendClaimInsuranceDoc = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/PolicyClaim/SendClaimInsurance";
        var deferred = $q.defer();
        $http.post(apiUrl, param, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var UpdatePolicyClaim = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/PolicyClaim/UpdatePolicyClaim";
        var deferred = $q.defer();
        $http.post(apiUrl, param, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    
    var DeletePolicyClaim = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/PolicyClaim/DeletePolicyClaim";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }


    return {

        getPolicyClaimList: getPolicyClaimList,
        getPolicyClaimById: getPolicyClaimById,
        SavePolicyClaim: SavePolicyClaim,
        UpdatePolicyClaim: UpdatePolicyClaim,
        DeletePolicyClaim: DeletePolicyClaim,
        SendClaimInsuranceDoc: SendClaimInsuranceDoc
    };


});