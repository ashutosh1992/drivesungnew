﻿"use strict"

angular.module('app.usercreation').factory('usercreationFactory', function ($http, $q, APP_CONFIG) {

    var getUserList = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/UserProfile/GetUserList";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var Saveregistration = function (fd) {


        var apiUrl
        //if (fd.action == "New") {
        apiUrl = APP_CONFIG.apiUrl + "/api/UserProfile/CreateUserProfile";
        //}
        //else if (fd.action == "Edit") {
        //    apiUrl = APP_CONFIG.apiUrl + "/api/ConfigSetting/UpdateConfigSetting";
        //}

        var deferred = $q.defer();

        $http.post(apiUrl, fd, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var deleteUser = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/UserProfile/DeleteUserProfile";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var GetRoleList = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/UserProfile/GetRoleList";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var GetOrganizationList = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Organization/GetOrganizationList";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    

    


    return {

        getUserList: getUserList,
        Saveregistration: Saveregistration,
        deleteUser: deleteUser,
        GetRoleList: GetRoleList,
        GetOrganizationList: GetOrganizationList
       
    };



});
