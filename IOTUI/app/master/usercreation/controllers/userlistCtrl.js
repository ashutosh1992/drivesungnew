﻿'use strict';

angular.module('app.usercreation').controller('userlistCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, usercreationFactory) {

    function loadInitData() {
       
        var param = {
            "s_user_id": $rootScope.userInfo.user_id,
            "s_role_Id": 1
        };


        usercreationFactory.getUserList(param).then(function (result) {
           showData(result.data.data);

        }, function (error) {
            console.log(error);
        });
    }

   function showData(ListData) {
        showUserList(ListData);
    }

   function showUserList(ListData) {
        var IsDelete = true;
        angular.forEach(ListData, function (data, index) {
            data.Edit = "";
            data.View = "";
            data.Delete = "";
            data.fullname = data.first_name + " " + data.last_name;
           
        });

        var tableOptions = {
            "data": ListData,
            columns: [
                { data: "user_id" },
                { data: "fullname" },
                { data: "email_id1" },
                { data: "address1" },
                { data: "mobileno" },
                { data: "role_desc" },
                { data: "Edit" },
                { data: "View" },
                { data: "Delete" },
              
            ],
            "order": [[0, 'desc']],
            destroy: true,
            "fnCreatedRow": function (nRow, aData, iDataIndex) {

                $('td:eq(' + 5 + ')', nRow).html("<button class='btn btn-default' ng-click='editRow(" + aData.user_id + ")'><i class='fa fa-edit'></i></button>");
                $('td:eq(' + 6 + ')', nRow).html("<button class='btn btn-default'  ng-click='viewRow(" + aData.user_id + ")'><i class='fa fa-reorder'></i></button>");
                $('td:eq(' + 7 + ')', nRow).html("<button class='btn btn-default' ng-click='deleteRow(" + aData.user_id + ")'><i class='fa fa-times'></i></button>");
              
            }
            , "aoColumnDefs": [

            //     //{ "bVisible": IsDelete, "aTargets": [-1] }
                 { "bVisible": false, "aTargets": [0] }

            ]

        };

        $scope.tablebind(tableOptions, $scope);
    }

    $scope.newRow = function () {
      
        var params = { 'action': 'New', 'user_id': 0 };
        $window.sessionStorage["createuser"] = JSON.stringify(params);
        $state.go('app.userdetails');
    }

    $scope.editRow = function (user_id) {
        debugger;
        var params = { 'action': 'Edit', 'user_id': user_id };
        $window.sessionStorage["createuser"] = JSON.stringify(params);
        $state.go('app.userdetails');
    }

    $scope.viewRow = function (user_id) {
        var params = { 'action': 'View', 'user_id': user_id };
        $window.sessionStorage["createuser"] = JSON.stringify(params);
        $state.go('app.userdetails');
    }

    $scope.deleteRow = function (user_id) {
        var params = { 'action': 'Edit', 'user_id': user_id };
        $window.sessionStorage["createuser"] = JSON.stringify(params);
        $.SmartMessageBox({
            title: "User Details",
            content: "Are you sure want to delete Record",
            buttons: '[Cancel],[Ok]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Ok") {
                deleteUserRecord(user_id);
            }
        });
    }

   
    function deleteUserRecord(user_id) {
       var param = {
           'user_id': user_id,
           's_user_id': $rootScope.userInfo.user_id,
            's_role_Id': 2

        };

        usercreationFactory.deleteUser(param)
             .then(function (result) {

                 var hexcolor = "#F00";
                 if (result.data.isComplete == false)
                     hexcolor = "#FF0000";

                 $.smallBox({
                     title: "User Delete succesfully",
                     content: "<i class='fa fa-clock-o'></i> <i>" + result.data.description + "</i>",
                     color: hexcolor,
                     iconSmall: "fa fa-check bounce animated",
                     timeout: 4000
                 });

                 //if (result.data.isComplete == true)
                     loadInitData();

             },
              function (error) {
                 console.log(error);
             });

    }

   

    function init() {
       
        loadInitData();
    }

    init();
});
