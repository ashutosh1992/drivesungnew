﻿'use strict'

angular.module('app.usercreation').controller('userdetailsCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, usercreationFactory, updateprofileFactory, $rootScope) {

    var actionCS;
    function loadInitData() {
      
        GetRoleList();
        GetOrganizationList();
        actionCS = JSON.parse($window.sessionStorage["createuser"]);
        $scope.action = actionCS.action;
        $scope.CSMasterIndex = 0;
        if (actionCS.action == "New") {
            $scope.lblheading = "New User";
            $scope.lblheading1 = 'Details';
            $scope.editMode = true;
            $scope.deleteMode = false;
            $scope.btnSaveText = "Submit";
            $scope.AuthcheckboxModel = {
                value: true,
            };
            $scope.user_id = 0;
            $scope.vehicle_color = "0";
            $scope.vehicle_type = "0";
            $('#cmbdate_of_birth').combodate({
                value: $scope.date_of_birth,
                minYear: '1960',
                maxYear: moment().format('YYYY')
            });
            //$scope.role_Id = "Select User Type";
            //$scope.o_id = "Select Organization";

        }
        else {
            //$scope.options = [{ Text: "public", value: "public" }, { Text: "private", value: "private" }];
            if (actionCS.action == "Edit")
                $scope.lblheading1 = 'Update';
            $scope.lblheading1 = 'View';
            //$scope.btnSaveText="Update"
            $scope.refer = {

                user_id: actionCS.user_id,
                s_user_id: $rootScope.userInfo.user_id,
                s_role_Id: 1,
            }

            updateprofileFactory.Showregistration($scope.refer).then(function (result) {
                var dt = new Date();
                var resultData = result.data.data;
                $scope.role_Id = resultData[0].role_id,
                $scope.o_id = resultData[0].o_id
                $scope.user_name = resultData[0].user_name;
                $scope.email_id1 = resultData[0].email_id1;
                $scope.user_pwd = resultData[0].user_pwd;
                $scope.passwordConfirm = resultData[0].passwordConfirm;
                $scope.first_name = resultData[0].first_name;
                $scope.middle_name = resultData[0].middle_name;
                $scope.last_name = resultData[0].last_name;
                $scope.mobileno = resultData[0].mobileno;
                //$scope.date_of_birth = resultData[0].date_of_birth;
                $scope.date_of_birth = resultData[0].wdb;
                $scope.gender = resultData[0].gender;
                $scope.user_id = resultData[0].user_id;
                $scope.profile_picture_uri = resultData[0].profile_picture_uri;
                $scope.address1 = resultData[0].address1;
                $scope.deviceno = resultData[0].deviceno;
                $scope.vehicle_no = resultData[0].vehicle_no;
                $scope.license_no = resultData[0].license_no;
                //$scope.license_expiry_date = resultData[0].license_expiry_date;
                $scope.license_expiry_date = resultData[0].wled;
                $scope.vehicle_color = resultData[0].vehicle_color == "" ? "0" : resultData[0].vehicle_color;
                $scope.vehicle_type = resultData[0].vehicle_type == "" ? "0" : resultData[0].vehicle_type;
                $scope.country = resultData[0].country;
                $scope.pincode = resultData[0].pincode;
                $scope.region = resultData[0].region;
                $scope.city = resultData[0].city;
                $scope.state = resultData[0].state;
                $scope.insurance_mapping_key = resultData[0].insurance_mapping_key;
                //$scope.registration_no = resultData[0].registration_no;
                $scope.vehiclengine_type = resultData[0].vehiclengine_type
                $scope.insurance_provider = resultData[0].insurance_provider,
                $scope.insurer_emailid = resultData[0].insurer_emailid,
               


                $('#cmbdate_of_birth').combodate({
                    value: $scope.date_of_birth,
                    minYear: '1960',
                    maxYear: moment().format('YYYY')
                });
                $('#cmbexpirydate').combodate({
                    value: $scope.license_expiry_date,
                    minYear: '1960',
                    maxYear: moment().format('YYYY')
                });




            }, function (error) {
                console.log(error);
            });
        }

    }

   

    $scope.finalCancelCompetitionRecord = function () {

        $.SmartMessageBox({
            title: "User Details",
            content: "Are you sure want to Cancel",
            buttons: '[Cancel],[Ok]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Ok") {
                $state.go('app.userlist');
            }
        });
        return;

    }

   


    $scope.SaveCompetitionRecord = function () {

        var registerData = {
            'user_name': $scope.email_id1,
            'role_Id': $scope.role_Id,
            'email_id1': $scope.email_id1,
            'user_pwd': $scope.user_pwd,
            'passwordConfirm': $scope.passwordConfirm,
            'first_name': $scope.first_name,
            'middle_name': $scope.middle_name,
            'last_name': $scope.last_name,
            'mobileno': $scope.mobileno,
            'date_of_birth': $scope.date_of_birth,
            'gender': $scope.gender,
            'address1': $scope.address1,
            'deviceno': $scope.deviceno,
            'vehicle_no': $scope.vehicle_no,
            'license_no': $scope.license_no,
            'license_expiry_date': $scope.license_expiry_date,
            'vehicle_color': $scope.vehicle_color,
            'vehicle_type': $scope.vehicle_type,
            'country': $scope.country,
            'pincode': $scope.pincode,
            'region': $scope.region,
            'city': $scope.city,
            'state': $scope.state,
            's_role_Id': 1,
            'user_id': $scope.user_id,
            's_user_id': $scope.user_id,
            'o_id': $scope.o_id,
            'insurance_mapping_key': $scope.insurance_mapping_key,
            //'registration_no': $scope.registration_no,
            'vehiclengine_type': $scope.vehiclengine_type,
            'insurance_provider': $scope.insurance_provider,
            'insurer_emailid': $scope.insurer_emailid,


        };

        var fd = new FormData();
        fd.append('file', $scope.profile_picture_uri);
        fd.append('data', JSON.stringify(registerData));

        usercreationFactory.Saveregistration(fd).then(function (result) {

            $.smallBox({
                title: "User register succesfully.",
                content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                color: "#5F895F",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
            $state.go('app.userlist');


        }, function (error) {
            console.log(error);
        });


    }

    $scope.UpdateCompetitionRecord = function () {
        var registerData = {
            'user_name': $scope.user_name,
            'email_id1': $scope.email_id1,
            'role_Id': $scope.role_Id,
            'user_pwd': $scope.user_pwd,
            'passwordConfirm': $scope.passwordConfirm,
            'first_name': $scope.first_name,
            'middle_name': $scope.middle_name,
            'last_name': $scope.last_name,
            'mobileno': $scope.mobileno,
            'date_of_birth': $scope.date_of_birth,
            'gender': $scope.gender,
            'address1': $scope.address1,
            'deviceno': $scope.deviceno,
            'vehicle_no': $scope.vehicle_no,
            'license_no': $scope.license_no,
            'license_expiry_date': $scope.license_expiry_date,
            'vehicle_color': $scope.vehicle_color,
            'vehicle_type': $scope.vehicle_type,
            'country': $scope.country,
            'pincode': $scope.pincode,
            'region': $scope.region,
            'city': $scope.city,
            'state': $scope.state,
            's_role_Id': 2,
            'user_id': $scope.user_id,
            's_user_id': $scope.user_id,
            'insurance_mapping_key': $scope.insurance_mapping_key,
            //'registration_no': $scope.registration_no,
            'vehiclengine_type': $scope.vehiclengine_type,
            'insurance_provider': $scope.insurance_provider,
            'insurer_emailid': $scope.insurer_emailid,


        };

        var fd = new FormData();
        fd.append('file', $scope.profile_picture_uri);
        fd.append('data', JSON.stringify(registerData));

        updateprofileFactory.Updateregistration(fd).then(function (result) {

            $.smallBox({
                title: "User register succesfully.",
                content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                color: "#5F895F",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
           // init();


        }, function (error) {
            console.log(error);
        });

    }

    function GetRoleList() {
        var filters = {
            //VechileNo: VechileNo,
            //DeviceNo: DeviceNo,
            //fromdate: fromdate,
            //todate: todate,
            //fromtime: fromtime,
            //totime: totime,
            //NoOfRecords: NoOfRecords,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        usercreationFactory.GetRoleList(filters)
        .then(function (result) {
            fillRollList(JSON.stringify(result.data.data));
        }, function (error) {
            console.log(error);
        });
    }

    function fillRollList(allRoll) {
        $scope.allRoll = JSON.parse(allRoll);
    }

    function GetOrganizationList() {
        var filters = {
            
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        usercreationFactory.GetOrganizationList(filters)
        .then(function (result) {
            fillOrganizationList(JSON.stringify(result.data.data));
        }, function (error) {
            console.log(error);
        });
    }


    function fillOrganizationList(allOrg) {
        $scope.allOrg = JSON.parse(allOrg);
    }

    function init() {
        loadInitData();
    }

    init();
});