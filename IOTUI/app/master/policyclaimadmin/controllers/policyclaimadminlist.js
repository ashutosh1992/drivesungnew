﻿'use strict';
angular.module('app.policyclaimadmin').controller('policyclaimadminlistCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, policyclaimadminFactory) {

    function loadInitData() {

        var param = {
            "s_user_id": $rootScope.userInfo.user_id,
            "s_role_Id": 2
        };


        policyclaimadminFactory.getPolicyClaimList(param).then(function (result) {
            showData(result.data.data);

        }, function (error) {
            console.log(error);
        });
    }


    function showData(ListData) {
        showPolicyClaimList(ListData);
    }

    function showPolicyClaimList(ListData) {
        var IsDelete = true;
        angular.forEach(ListData, function (data, index) {
            data.Action = "";

            data.period_from = $filter('date')(data.period_from, "dd-MM-yyyy");
            data.period_to = $filter('date')(data.period_to, "dd-MM-yyyy");
            data.date_accident_or_loss = $filter('date')(data.date_accident_or_loss, "dd-MM-yyyy");
            //data.fullname = data.first_name + " " + data.last_name;
            if(data.status =="p")
            {
                data.status = "Pending";
            }
            if (data.status == "a") {
                data.status = "Approved";
            }
            if (data.status == "r") {
                data.status = "Reject";
            }
            
            

        });

        var tableOptions = {
            "data": ListData,
            columns: [
                { data: "policy_no" },
                { data: "period_from" },
                { data: "period_to" },
                { data: "name_of_driver" },
                { data: "date_accident_or_loss" },
                { data: "status" },
                { data: "Action" },


            ],
            "order": [[0, 'desc']],
            destroy: true,
            "fnCreatedRow": function (nRow, aData, iDataIndex) {

                $('td:eq(' + 6 + ')', nRow).html("<button class='btn btn-default'  ng-click='Action(" + aData.claim_id + ")'><i class='fa fa-edit'></i></button>");


            },
            //, "aoColumnDefs": [

            ////     //{ "bVisible": IsDelete, "aTargets": [-1] }
            //     { "bVisible": false, "aTargets": [0] }

            //]

        };

        $scope.tablebind(tableOptions, $scope);
    }



    $scope.Action = function (claim_id) {
        debugger;
        var params = { 'action': 'Edit', 'claim_id': claim_id };
        $window.sessionStorage["policyclaimadmin"] = JSON.stringify(params);
        $state.go('app.policyclaimadmindetails');
    }


    function init() {
        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
        loadInitData();
    }

    init();
});