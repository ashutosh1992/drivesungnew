﻿"use strict";

angular.module('app.policyclaimadmin', ['ui.router'])

angular.module('app.policyclaimadmin').config(function ($stateProvider) {

    $stateProvider
        .state('app.policyclaimadmin', {
            abstract: true,
            data: {
                title: ''
            }
        })

        .state('app.policyclaimadminlist', {
            url: '/PolicyClaimAdminList',
            data: {
                title: ''
            },
            views: {
                "content@app": {
                    templateUrl: 'app/master/policyclaimadmin/views/policyclaimadminlist.html',
                    controller: 'policyclaimadminlistCtrl'
                }
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js',
                        'build/vendor.datatables.js'
                    ]);
                }
            }
        })

    .state('app.policyclaimadmindetails', {
        url: '/PolicyClaimAdmindetails',
        data: {
            title: ''
        },
        views: {
            "content@app": {
                templateUrl: 'app/master/policyclaimadmin/views/policyclaimadmindetail.html',
                controller: 'policyclaimadmindetailsCtrl'
            }
        },
        resolve: {
            scripts: function (lazyScript) {
                return lazyScript.register([
                    'build/vendor.ui.js',
                    'build/vendor.datatables.js'
                ]);
            }
        }

    })
});
