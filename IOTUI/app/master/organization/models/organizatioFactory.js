﻿"use strict"

angular.module('app.organization').factory('organizationFactory', function ($http, $q, APP_CONFIG) {

    var getOrganizationList = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Organization/GetOrganizationList";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var getOrganizationById = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Organization/GetOrganizationById";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var SaveOrganization = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Organization/CreateOrganization";
        var deferred = $q.defer();
        $http.post(apiUrl, param, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    var UpdateOrganization = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Organization/UpdateOrganization";
        var deferred = $q.defer();
        $http.post(apiUrl, param, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var DeleteOrganization = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Organization/DeleteOrganization";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }


    return {

        getOrganizationList: getOrganizationList,
        getOrganizationById: getOrganizationById,
        SaveOrganization: SaveOrganization,
        UpdateOrganization: UpdateOrganization,
        DeleteOrganization: DeleteOrganization
      

    };


});