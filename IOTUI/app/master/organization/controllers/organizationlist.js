﻿'use strict';
angular.module('app.organization').controller('organizationlistCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, organizationFactory) {

    function loadInitData() {

        var param = {
            "s_user_id": $rootScope.userInfo.user_id,
            "s_role_Id": 1
        };


        organizationFactory.getOrganizationList(param).then(function (result) {
            showData(result.data.data);

        }, function (error) {
            console.log(error);
        });
    }


    function showData(ListData) {
        showOrganizationList(ListData);
    }

    function showOrganizationList(ListData) {
        var IsDelete = true;
        angular.forEach(ListData, function (data, index) {
            data.Edit = "";
            data.View = "";
            data.Delete = "";
            //data.fullname = data.first_name + " " + data.last_name;

        });

        var tableOptions = {
            "data": ListData,
            columns: [
                { data: "o_code" },
                { data: "o_name" },
                { data: "o_type" },
                { data: "address1" },
                { data: "email_id1" },
                { data: "contact_no1" },
                { data: "Edit" },
                { data: "View" },
                { data: "Delete" },

            ],
            "order": [[0, 'desc']],
            destroy: true,
            "fnCreatedRow": function (nRow, aData, iDataIndex) {

                $('td:eq(' + 6 + ')', nRow).html("<button class='btn btn-default' ng-click='editRow(" + aData.o_id + ")'><i class='fa fa-edit'></i></button>");
                $('td:eq(' + 7 + ')', nRow).html("<button class='btn btn-default'  ng-click='viewRow(" + aData.o_id + ")'><i class='fa fa-reorder'></i></button>");
                $('td:eq(' + 8 + ')', nRow).html("<button class='btn btn-default' ng-click='deleteRow(" + aData.o_id + ")'><i class='fa fa-times'></i></button>");

            }
            ,
            //"aoColumnDefs": [

            ////     //{ "bVisible": IsDelete, "aTargets": [-1] }
            //     { "bVisible": false, "aTargets": [0] }

            //]

        };

        $scope.tablebind(tableOptions, $scope);
    }

    $scope.newRow = function () {

        var params = { 'action': 'New', 'o_id': 0 };
        $window.sessionStorage["organization"] = JSON.stringify(params);
        $state.go('app.organizationdetails');
    }

    $scope.editRow = function (o_id) {
        debugger;
        var params = { 'action': 'Edit', 'o_id': o_id };
        $window.sessionStorage["organization"] = JSON.stringify(params);
        $state.go('app.organizationdetails');
    }

    $scope.viewRow = function (o_id) {
        var params = { 'action': 'View', 'o_id': o_id };
        $window.sessionStorage["organization"] = JSON.stringify(params);
        $state.go('app.organizationdetails');
    }

    $scope.deleteRow = function (o_id) {
        var params = { 'action': 'Edit', 'o_id': o_id };
        $window.sessionStorage["organization"] = JSON.stringify(params);
        $.SmartMessageBox({
            title: "Organization Details",
            content: "Are you sure want to delete Record",
            buttons: '[Cancel],[Ok]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Ok") {
                deleteOrganizationRecord(o_id);
            }
        });
    }


    function deleteOrganizationRecord(o_id) {
        var param = {
            'o_id': o_id,
            's_user_id': $rootScope.userInfo.user_id,
            's_role_Id': 2

        };

        organizationFactory.DeleteOrganization(param)
             .then(function (result) {

                 var hexcolor = "#F00";
                 if (result.data.isComplete == false)
                     hexcolor = "#FF0000";

                 $.smallBox({
                     title: "Organization Delete succesfully",
                     content: "<i class='fa fa-clock-o'></i> <i>" + result.data.description + "</i>",
                     color: hexcolor,
                     iconSmall: "fa fa-check bounce animated",
                     timeout: 4000
                 });

                 //if (result.data.isComplete == true)
                 loadInitData();

             },
              function (error) {
                  console.log(error);
              });

    }

    function init() {
        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
        loadInitData();
    }

    init();
});