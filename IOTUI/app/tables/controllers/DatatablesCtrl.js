/**
 * Created by griga on 2/9/16.
 */


angular.module('app.tables').controller('DatatablesCtrl', function ($q,DTOptionsBuilder, DTColumnBuilder) {

    var getTableData = function () {
        var deferred = $q.defer();
        deferred.resolve([
  {
      "id": "1",
      "name": "Jennifer",
      "phone": "1-342-463-8341",
      "company": "Et Rutrum Non Associates",
      "zip": "35728",
      "city": "Fogo",
      "date": "03/04/14"
  },
  {
      "id": "2",
      "name": "Clark",
      "phone": "1-516-859-1120",
      "company": "Nam Ac Inc.",
      "zip": "7162",
      "city": "Machelen",
      "date": "03/23/13"
  }
        ]
);
        return deferred.promise;
    };

    this.standardOptions = DTOptionsBuilder
        .fromFnPromise(getTableData)
         //Add Bootstrap compatibility
        .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
        .withBootstrap();
    this.standardColumns = [
        DTColumnBuilder.newColumn('id').withClass('text-danger'),
        DTColumnBuilder.newColumn('name'),
        DTColumnBuilder.newColumn('phone'),
        DTColumnBuilder.newColumn('company'),
        DTColumnBuilder.newColumn('zip'),
        DTColumnBuilder.newColumn('city'),
        DTColumnBuilder.newColumn('date')
    ];


});