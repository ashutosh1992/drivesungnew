﻿'use strict';

angular.module('app.IotDailyReport', [
    'ui.router',
    'ngResource',
    'datatables',
    'datatables.bootstrap',
    'uiGmapgoogle-maps',
    'easypiechart',
    'angularjs-gauge'
])
 

.config(function ($stateProvider) {
    $stateProvider
        .state('app.IotDailyReport', {
            url: '/IotDailyReport',
            views: {
                "content@app": {
                    //controller: 'IotDailyReportCtrl',
                    templateUrl: 'app/IotDailyReport/views/IotDailyReport.html'
                }
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                         'build/vendor.ui.js',
                        'build/vendor.datatables.js',
                        'build/vendor.graphs.js'
                    ]);
                }
            },


                    });
});
