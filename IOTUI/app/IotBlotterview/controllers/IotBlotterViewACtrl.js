'use strict';

angular.module('app.IotBlotterView').controller('IotBlotterViewACtrl', function ($q, $scope, $filter, $interval, $rootScope, $window, CalendarEvent, SmartMapStyle, IotBlotterViewFactory, uiGmapGoogleMapApi, DTOptionsBuilder, DTColumnBuilder, APP_CONFIG) {

    function loadInitData() {
        $rootScope.Timer = null;
        // $scope.statusfilter;

        SetCountzero();
        //$scope.OverSpeedChecked = false,
        //$scope.IgnitionOnChecked = false,
        //$scope.MovingChecked = false,
        //$scope.ParkedChecked = false,
        //$scope.UnReachableChecked = false,
        //$scope.HardAccelsChecked = false,
        //$scope.HardBreakingChecked = false,
        //$scope.TamperAlertChecked = false
        $scope.circle = []
        $scope.$on('$destroy', function () {
            StopAutoRefresh();
        });

        //if ($window.sessionStorage["userInfo"]) {
        //    $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        //}
    }

    function SetCount(hdata) {

        $scope.HB = hdata[0].hb;
        $scope.HA = hdata[0].ha;
        $scope.HC = hdata[0].cor;
        $scope.OS = hdata[0].os;
        $scope.CT = hdata[0].mcall;

    }
    function SetCountzero() {

        $scope.HB = 0;
        $scope.HA = 0;
        $scope.HC = 0;
        $scope.OS = 0;
        $scope.CT = 0;



    }

    function SetPieChartData(hdata) {
        var chart = AmCharts.makeChart("chartdivAge", {
            "type": "pie",
            "theme": "light",
            "dataProvider": [{
                "title": "Age Below 25 Years",
                "value": hdata[0].ageL25,
                "color": "#ec6224"
            },
                             {
                                 "title": "Between 26-55 Years",
                                 "value": hdata[0].ageI26_55,
                                 "color": "#1c95ba"
                             }, {
                                 "title": "Between 55-75 Years",
                                 "value": hdata[0].agegI55_75,
                                 "color": "#0a8f5d"
                             },
                            {
                                "title": "Above 75 Years",
                                "value": hdata[0].agegI55_75,
                                "color": "#676735"
                            },
                              {
                                  "title": "Not Avaliable",
                                  "value": hdata[0].na,
                                  "color": "#00bcd4"
                              }
            ],
            "titleField": "title",
            "colorField": "color",
            "valueField": "value",
            "labelRadius": 5,
            "radius": "42%",
            "innerRadius": "30%",
            "labelText": "[[title]]",
            "export": {
                "enabled": false
            }
        });

        var chart = AmCharts.makeChart("chartdivGender", {
            "type": "pie",
            "theme": "light",
            "dataProvider": [{
                "title": "Male",
                "value": hdata[0].male,
                "color": "#5381f1"
            },
                             {
                                 "title": "Female",
                                 "value": hdata[0].female,
                                 "color": "#ffbad2"
                             }, {
                                 "title": "Not Specified",
                                 "value": hdata[0].other,
                                 "color": "#00bcd4"
                             }],
            "titleField": "title",
            "colorField": "color",
            "valueField": "value",
            "labelRadius": 5,

            "radius": "42%",
            "innerRadius": "0%",
            "labelText": "[[title]]",
            "export": {
                "enabled": false
            }
        });



        var chart = AmCharts.makeChart("chartdivColor", {
            "type": "pie",
            "theme": "light",
            "dataProvider": [
                 {
                     "title": "Red",
                     "value": hdata[0].car_red,
                     "color": "#cc0001"
                 },
                {
                    "title": "White",
                    "value": hdata[0].car_white,
                    "color": "#ccc"
                },
                  {
                      "title": "Black",
                      "value": hdata[0].car_black,
                      "color": "#000"
                  },

                //{
                //    "title": "Other Light",
                //    "value": hdata[0].car_otherlight,
                //    "color": "#ff0000"
                //},
                {
                    "title": "Metallic Light",
                    "value": hdata[0].car_metalliclight,
                    "color": "#727272"
                },
                {
                    "title": "Metallic Dark",
                    "value": hdata[0].car_metallicdark,
                    "color": "#171717"
                },
                //{
                //    "title": "Other Dark",
                //    "value": hdata[0].car_otherdark,
                //    "color": "#010101"
                //},

                //{
                //    "title": "Other",
                //    "value": hdata[0].car_colorother,
                //    "color": "#193b6a"
                //},
                {
                    "title": "Not Avaliable",
                    "value": hdata[0].car_color_na,
                    "color": "#9400d4"
                }
            ],
            "titleField": "title",
            "colorField": "color",
            "valueField": "value",
            "labelRadius": 5,

            "radius": "42%",
            "innerRadius": "60%",
            "labelText": "[[title]]",
            "export": {
                "enabled": false
            }
        });
    }

    function setVehicleData(alldata, hdata) {
        var alldata = JSON.parse(alldata);
        var hdata = JSON.parse(hdata);
        SetCount(hdata);
        SetPieChartData(hdata);
        fillVehicleDataGrid(alldata);
        if (alldata.length > 0) {
            SetMapData(alldata);
        }
        else {
            loadDefaultMap();
        }

    }

    function SetMapData(alldata) {
        var markersr = [];
        var cureentlat;
        var currentlng;
        var prvlat;
        var prvlng;
        var options;
        var alertorstatus;
        var address;
        angular.forEach(alldata, function (data, index) {
            cureentlat = data.latitude;
            currentlng = data.longitude;

            if (data.status == 'disconnect') {
                options = { icon: 'styles/img/markersicon/disconnect.png' };
                alertorstatus = "Disconnect";
            }

            if (data.status == 'stop') {
                options = { icon: 'styles/img/markersicon/parked.png' };
                alertorstatus = "stop";
            }

            if (data.status == 'moving') {
                options = { icon: 'styles/img/markersicon/moving.png' };
                alertorstatus = "Moving";
            }

            if (data.hardbraking != null) {
                if (data.hardbraking) {
                    options = { icon: 'styles/img/markersicon/hardbreaking.png' };
                    alertorstatus = "Harsh Breaking";
                }
            }

            if (data.overspeedstarted != null) {
                if (data.overspeedstarted) {
                    options = { icon: 'styles/img/markersicon/overspeed.png' };
                    alertorstatus = "Over Speed";
                }
            }

            if (data.cornerpacket != null) {
                if (data.cornerpacket) {
                    options = { icon: 'styles/img/markersicon/cornerpacket.png' };
                    alertorstatus = "Cornering";
                }
            }

            if (data.mcall != null) {
                if (data.mcall) {
                    options = { icon: 'styles/img/markersicon/mcall.png' };
                    alertorstatus = "Calls Taken";
                }
            }

            if (data.hardacceleration != null) {
                if (data.hardacceleration) {
                    options = { icon: 'styles/img/markersicon/hardaccels.png' };
                    alertorstatus = "Harsh Acceleration";
                }
            }

            var vehicledetails = {
                vehicle_no: data.deviceno,
                speed: data.speed,
                drivername: data.first_name + ' ' + data.middle_name + ' ' + data.last_name,
                derivermobileno: data.mobileno,
                emailid: data.email_id1,
                recorddatetime: data.recorddatetime,
                latitude: data.latitude,
                longitude: data.longitude,
                address: data.address1,
                alert: alertorstatus
            }

            markersr.push({
                id: index,
                latitude: cureentlat,
                longitude: currentlng,
                options: options,
                vehicledetails: vehicledetails
            });


        });
        var zoom = $scope.map.zoom;
        $scope.map = {
            center: { latitude: cureentlat, longitude: currentlng }, zoom: zoom, bounds: {},
            control: {},
            markersr: markersr
        };


    }

    function fillVehicleDataGrid(alldata) {
        angular.forEach(alldata, function (data, index) {
            //data.location = '';
            data.Map = "Map";

            if (data.status == 'disconnect') {
                data.alertorstatus = "Disconnect";
            }

            if (data.status == 'stop') {
                data.alertorstatus = "stop";
            }

            if (data.status == 'moving') {
                data.alertorstatus = "Moving";
            }

            if (data.hardbraking != null) {
                if (data.hardbraking) {
                    data.alertorstatus = "Harsh Breaking";
                }
            }

            if (data.overspeedstarted != null) {
                if (data.overspeedstarted) {
                    data.alertorstatus = "Over Speed";
                }
            }

            if (data.cornerpacket != null) {
                if (data.cornerpacket) {
                    data.alertorstatus = "Cornering";
                }
            }

            if (data.mcall != null) {
                if (data.mcall) {
                    data.alertorstatus = "Calls Taken";
                }
            }

            if (data.hardacceleration != null) {
                if (data.hardacceleration) {
                    data.alertorstatus = "Harsh Acceleration";
                }
            }

        });

        var tableOptions = {
            "data": alldata,
            //"iDisplayLength": 10,
            columns: [
                //{ data: "type" },

                { data: "vehicle_no" },
                { data: "recorddatetime" },
               //{ data: "location" },
                //{ data: "latitude" },
                //{ data: "longitude" },
                { data: "speed" },
                //{ data: "distance" },
                //{ data: "distancecan" },
                //{ data: "rpm" },
                { data: "alertorstatus" },
                 { data: "Map" }
                //{ data: "fuellevel" },

            ],
            "order": [[1, 'desc']],
            //"mColumns": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
            "mColumns": [0, 1, 2, 3, 4],
            destroy: true,
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                var locationdata = {
                    id: iDataIndex,
                    lat: aData.latitude,
                    lng: aData.longitude
                };
                $('td:eq(1)', nRow).html("<i class='fa fa-calendar' style='color:red'  title = 'Date & Time'></i> &nbsp" + aData.recorddatetime + "<br><a id='#viewlocation" + iDataIndex + "' ng-click='locationinfo(" + JSON.stringify(locationdata) + ")' class='fa fa-location-arrow'style='color:green' title = 'click here for location details' ></a><div id='container" + iDataIndex + "'></div>");
                $('td:eq(4)', nRow).html("<a ng-click='viewRow(" + aData.latitude + "," + aData.longitude + ")' class='fa fa-map-marker fa-2x' aria-hidden='true' style='color:#075e07' title = 'View on Map'></a>");
            }

        };

        $scope.dashTablebind(tableOptions, $scope);
    }

     $scope.locationinfo = function (locationdata) {
        var geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(locationdata.lat, locationdata.lng);
        var location;

        geocoder.geocode({ 'latLng': latlng }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    location = results[1].formatted_address;;
                }
                else {
                    location = 'lat-' + locationdata.lat + ', lng-' + locationdata.lng;
                }
                document.getElementById("container" + locationdata.id + "").innerHTML = '</small>' + location + '</small>';
            }
        })

    }


    function loadDefaultMap() {
        $scope.map = {
            center: { latitude: 18.50891, longitude: 73.92603 }, zoom: 5, bounds: {},
            control: {},
            markersr: []
        };
    }

    $scope.getVehicleData = function () {

        loadInitData();
        loadDefaultMap();

        var filters = {
            VechileNo: "",
            DeviceNo: "",
            fromdate: "",
            todate: "",
            fromtime: "",
            totime: "",
            NoOfRecords: "",
            OverSpeed: $scope.OSChecked,
            HA: $scope.HAChecked,
            HB: $scope.HBChecked,
            HC: $scope.HCChecked,
            CT: $scope.CTChecked,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        //call api using factory
        IotBlotterViewFactory.getVehicleData(filters)
        .then(function (result) {
            //format result
            setVehicleData(JSON.stringify(result.data.data.all), JSON.stringify(result.data.data.count));
        }, function (error) {
            console.log(error);
        });
    }

    function fillVehicleList() {
        $scope.dtVehicleList = JSON.parse($scope.alldata);
    }

    $scope.viewRow = function (latitude, longitude) {
        //var zoom = $scope.map.zoom;
        //$scope.map = {
        //    center: { latitude: latitude, longitude: longitude },
        //    zoom: 20,
        //};
        $scope.map.center.latitude = latitude;
        $scope.map.center.longitude = longitude;
        $scope.map.zoom = 16;
        $scope.circle = [];

        var newObj = {
            latitude: latitude,
            longitude: longitude
        };
        $scope.circle.push(newObj);

    }


    $scope.AutoOnOff = function () {

        if ($scope.OnOffStatus) {

            StartAutoRefresh();
        }
        else {
            StopAutoRefresh();
        }
    }

    $scope.windowOptions = {
        visible: false
    };

    $scope.onClick = function () {
        $scope.windowOptions.visible = !$scope.windowOptions.visible;
    };

    $scope.closeClick = function () {
        $scope.windowOptions.visible = false;
    };

    //$scope.title = "Window Title!";

    //start Auto referesh
    function StartAutoRefresh() {
        $rootScope.Timer = $interval(function () {
            $scope.getVehicleData();
        }, 30000);
    };

    //Stop Auto referesh
    function StopAutoRefresh() {
        if (angular.isDefined($rootScope.Timer)) {
            $interval.cancel($rootScope.Timer);
        }
    };


    $scope.toggleActiveOS = function () {
        $scope.isActiveOS = !$scope.isActiveOS;
        $scope.OSChecked = !$scope.isActiveOS
        $scope.getVehicleData();
    };

    $scope.toggleActiveHA = function () {
        $scope.isActiveHA = !$scope.isActiveHA;
        $scope.HAChecked = !$scope.isActiveHA
        $scope.getVehicleData();
    };

    $scope.toggleActiveCOR = function () {
        $scope.isActiveCOR = !$scope.isActiveCOR;
        $scope.HCChecked = !$scope.isActiveCOR
        $scope.getVehicleData();
    };

    $scope.toggleActiveCT = function () {
        $scope.isActiveCT = !$scope.isActiveCT;
        $scope.CTChecked = !$scope.isActiveCT
        $scope.getVehicleData();
    };

    $scope.toggleActiveHB = function () {
        $scope.isActiveHB = !$scope.isActiveHB;
        $scope.HBChecked = !$scope.isActiveHB
        $scope.getVehicleData();
    };

    $scope.toggleActivePAR = function () {
        $scope.isActivePAR = !$scope.isActivePAR;
    };

    function init() {
        //$scope.statusfilter = "Status"
        $scope.isActiveOS = true;
        $scope.isActiveHA = true;
        $scope.isActiveCOR = true;
        $scope.isActiveCT = true;
        $scope.isActiveHB = true;
        $scope.isActivePAR = true;

        $scope.OSChecked = null;
        $scope.HAChecked = null;
        $scope.HBChecked = null;
        $scope.HCChecked = null;
        $scope.CTChecked = null;

        $scope.getVehicleData();
        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
    }

    init();

});