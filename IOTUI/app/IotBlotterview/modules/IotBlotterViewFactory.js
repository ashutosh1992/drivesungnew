﻿"use strict";

angular.module('app.IotBlotterView').factory('IotBlotterViewFactory', function ($http, $q, APP_CONFIG) {

    var getVehicleData = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Dashboard/GetBlotterViewData";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    //var getVehicleData = function (filters) {
    //    var apiUrl = APP_CONFIG.apiUrl + "/api/Dashboard/GetBlotterViewUserData";
    //    var deferred = $q.defer();
    //    $http.post(apiUrl, filters).then(function (result) {
    //        deferred.resolve(result);
    //    }, function (error) {
    //        deferred.reject(error);
    //    });
    //    return deferred.promise;
    //}

    return {
        getVehicleData: getVehicleData
    };

});