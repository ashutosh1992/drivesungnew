﻿"use strict"

angular.module('app.IotGeofence').factory('IotGeofenceFactory', function ($http, $q, APP_CONFIG) {

    var getGeofenceList = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Geofence/GetGeofenceList";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var getAssignUnassignGeofenceList = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Geofence/GetAssignUnassignGeofenceList";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    
    var deleteGeofence = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Geofence/DeleteGeofence";
        var deferred = $q.defer();

        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var getGeofenceById = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Geofence/GetGeofenceById";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }


    var saveGeofence = function (param) {
        var apiUrl
        if (param.actionEntity.action == "New") {
            apiUrl = APP_CONFIG.apiUrl + "/api/Geofence/CreateGeofence";
        }
        else if (param.actionEntity.action == "Edit") {
            apiUrl = APP_CONFIG.apiUrl + "/api/Geofence/UpdateGeofence";
        }
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var getUserList = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Common/GetUserList";
        var deferred = $q.defer();

        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var updateGeofenceUserMapping = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Geofence/UpdateGeofenceUserMapping";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }



    return {
        getGeofenceList: getGeofenceList,
        getAssignUnassignGeofenceList:getAssignUnassignGeofenceList,
        deleteGeofence: deleteGeofence,
        getGeofenceById: getGeofenceById,
        saveGeofence: saveGeofence,
        getUserList: getUserList,
        updateGeofenceUserMapping: updateGeofenceUserMapping
    };



});
