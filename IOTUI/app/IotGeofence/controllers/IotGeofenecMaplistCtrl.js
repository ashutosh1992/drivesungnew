﻿'use strict';

angular.module('app.IotGeofence').controller('IotGeofenecMaplistCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, IotGeofenceFactory) {

    function loadInitData() {
        $scope.selection = [];
        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }

        var actionPara = JSON.parse($window.sessionStorage["Geofenceaction"]);
        $scope.user_id = actionPara.user_id
        $scope.user_name = actionPara.user_name
        $scope.deviceno = actionPara.deviceno
        var param = {
            "user_id": $scope.user_id,
            "s_user_id": $rootScope.userInfo.user_id,
            "s_role_Id": $rootScope.userInfo.role_Id
        };

        getGeofenceList(param);

        IotGeofenceFactory.getAssignUnassignGeofenceList(param).then(function (result) {
            showGeofenceList(result.data.data);

        }, function (error) {
            console.log(error);
        });
    }


    function getGeofenceList(filters) {
        IotGeofenceFactory.getGeofenceList(filters).then(function (result) {
            fillMasterGeofenceList(JSON.stringify(result.data.data));
        }, function (error) {
            console.log(error);
        });
    }

    function fillMasterGeofenceList(allGeofence) {
        $scope.dtGeofenceList = JSON.parse(allGeofence);
    }

    function showGeofenceList(ListData) {
        var IsDelete = true;
        angular.forEach(ListData, function (data, index) {
            if (data.chkforall == true) {
                $scope.selection.push(data.gc_id);
            }
            if (data.chkformaster == true) {
                $scope.m_geofenceId = data.gc_id;
            }
        });
        var tableOptions = {
            "data": ListData,
            columns: [
                { data: "chkforall" },
                { data: "gc_id" },
                { data: "geofence_name" },
                { data: "geofenec_desc" },
                { data: "created_at" },
                //{ data: "updated_at" }
            ]
            ,
            "order": [[0, 'desc']],
            "mColumns": [0, 1, 2, 3, 4],
            destroy: true,
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                $('td:eq(' + 0 + ')', nRow).html("<input id=" + aData.gc_id + " type='checkbox' value=" + aData.gc_id + " ng-checked='selection.indexOf(" + aData.gc_id + ") > -1' ng-click='SelectedChk(" + aData.gc_id + ")'/>");

            }
        };
        $scope.tablebind(tableOptions, $scope);

    }

    $scope.SelectedChk = function (gc_id) {
        var idx = $scope.selection.indexOf(gc_id);
        // is currently selected
        if (idx > -1) {
            $scope.selection.splice(idx, 1);
        }
            // is newly selected
        else {
            $scope.selection.push(gc_id);
        }

    }

    $scope.Cancel = function () {
        $state.go('app.IotGeofence.IotGeofenecUserList');
    }

    $scope.SaveRecord = function () {
        var GeofenceUserMappingEntity = [];
        angular.forEach($scope.selection, function (data, index) {
            if (data == $scope.m_geofenceId) {
                GeofenceUserMappingEntity.push({ 'user_id': $scope.user_id, 'gc_id': data, 'blnmaster': true })
            }
            else {
                GeofenceUserMappingEntity.push({ 'user_id': $scope.user_id, 'gc_id': data, 'blnmaster': false })
            }
        });
        var param = {
            "user_id":$scope.user_id,
            "s_user_id": $rootScope.userInfo.user_id,
            "s_role_Id": $rootScope.userInfo.role_Id,
            "created_by": $rootScope.userInfo.user_id,
            "geofenceUserMappingEntities": GeofenceUserMappingEntity
        }
        IotGeofenceFactory.updateGeofenceUserMapping(param)
             .then(function (result) {
                 var hexcolor = "#5F895F";
                 if (result.data.isComplete == false)
                     hexcolor = "#FF0000";
                 $.smallBox({
                     title: "Geofence Assign",
                     content: "<i class='fa fa-clock-o'></i> <i>" + result.data.description + "</i>",
                     color: hexcolor,
                     iconSmall: "fa fa-check bounce animated",
                     timeout: 4000
                 });

                 if (result.data.isComplete == true)
                     $state.go('app.IotGeofence.IotGeofenecUserList');

             }, function (error) {
                 console.log(error);
             });

    }

    function init() {
        loadInitData();
    }

    init();
});
