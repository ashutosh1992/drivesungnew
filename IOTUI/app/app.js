'use strict';

/**
 * @ngdoc overview
 * @name app [smartadminApp]
 * @description
 * # app [smartadminApp]
 *
 * Main module of the application.
 */


angular.module('app', [
    'ngSanitize',
    'ngAnimate',
    'restangular',
    'ui.router',
    'ui.bootstrap',

    // Smartadmin Angular Common Module
    'SmartAdmin',
    // App
    'app.auth',
    'app.layout',
    'app.chat',
    'app.dashboard',
    'app.calendar',
    'app.inbox',
    'app.graphs',
    'app.tables',
    'app.forms',
    'app.ui',
    'app.widgets',
    'app.maps',
    'app.appViews',
    'app.misc',
    'app.smartAdmin',
    'app.eCommerce',
    'app.dashboardtelematics',
    'app.home',
    'app.tripplanning',
    'app.claimform',
   'app.tripplayback',
   'app.configsetting',
    'app.usercompetition',
    'app.competition',
    'app.Iotdashboardlist',
    'app.iotlivetracking',
    'app.Iottripplayback',
    'app.Iotmapallvehicles',
    'app.IotGraphicalReport',
    'app.Iotobd',
    'app.IotDailyReport',
    'socialLogin', // for social login
    'app.Iotreports',
    'app.IotBlotterView',
    'app.updateprofile',
    'angularjs-datetime-picker',
    'app.IotGeofence',
     'app.usercreation',
    'app.organization',
    'app.policyclaimuser',
    'app.policyclaimadmin',

])
.config(function ($provide, $httpProvider, $windowProvider, RestangularProvider) {

    // Intercept http calls.
    $provide.factory('ErrorHttpInterceptor', function ($q, $window, $location, $rootScope) {
        var errorCounter = 0;
        function notifyError(rejection) {
            console.log(rejection);
            $.bigBox({
                title: rejection.status + ' ' + rejection.statusText,
                content: rejection.data != null ? rejection.data.message : rejection.data,
                color: "#C46A69",
                icon: "fa fa-warning shake animated",
                number: ++errorCounter,
                timeout: 6000
            });
        }

        return {
            // optional method
            'request': function (config) {
                // debugger;
                $rootScope.$broadcast("$viewHTTPContentLoadedStart");

                // do something on success
                if (config.url.indexOf("/token") != -1) {
                    delete $httpProvider.defaults.headers.common['Authorization'];
                    $httpProvider.defaults.withCredentials = false;
                    $window.sessionStorage["TokenInfo"] = null;

                    delete config.headers['Authorization'];
                }
                else if (config.url.indexOf("http://quotes.rest") != -1) {
                    //$httpProvider.defaults.withCredentials = false;
                    delete config.headers['Authorization'];
                    delete config.headers['Accept'];
                }

                if ($window.sessionStorage["TokenInfo"] == null)
                    $rootScope.$broadcast("$sessionExpiredMsg");

                $rootScope.sessionEndTime = new Date();
                $rootScope.sessionEndTime.setMinutes($rootScope.sessionEndTime.getMinutes() + 30);

                return config;
            },

            // On request failure
            requestError: function (rejection) {
                // show notification
                notifyError(rejection);

                // Return the promise rejection.
                return $q.reject(rejection);
            },

            // optional method
            'response': function (response) {
                $rootScope.$broadcast("$viewHTTPContentLoadedEnd");
                // do something on success
                //401 -- Unauthorized error
                //400 -- Exception
                //-1 -- Server not found
                if (response.status == 401 || response.status == -1) {
                    $location.path('/error');
                    //return $q.reject(response);
                }
                else if (response.status == 400) {
                    $rootScope.$broadcast("$sessionExpiredMsg");
                }
                return response;
            },

            // On response failure
            responseError: function (rejection) {
                $rootScope.$broadcast("$viewHTTPContentLoadedEnd");
                // show notification
                notifyError(rejection);
                if (rejection.status == 401 || rejection.status == 400 || rejection.status == -1) {
                    delete $httpProvider.defaults.headers.common['Authorization'];
                    $httpProvider.defaults.withCredentials = false;
                    $window.sessionStorage["TokenInfo"] = null;

                    if (rejection.status == 401 || rejection.status == -1) {
                        $location.path('/error');
                    }
                    else if (rejection.status == 400) {
                        $rootScope.$broadcast("$sessionExpiredMsg");
                    }

                    //$location.path('/login');
                }
                // Return the promise rejection.
                return $q.reject(rejection);
            }
        };

    });

    var $window = $windowProvider.$get();

    if ($window.sessionStorage["TokenInfo"] != null && JSON.parse($window.sessionStorage["TokenInfo"])) {
        var tokenInfo = JSON.parse($window.sessionStorage["TokenInfo"]);
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $httpProvider.defaults.headers.common['Authorization'] = 'Bearer ' + tokenInfo.accessToken;
        $httpProvider.defaults.withCredentials = true;
    }
    else {
        $httpProvider.defaults.withCredentials = false;
    }

    $httpProvider.interceptors.push('ErrorHttpInterceptor');

    RestangularProvider.setBaseUrl(location.pathname.replace(/[^\/]+?$/, ''));

})
.constant('APP_CONFIG', window.appConfig)

.run(function ($rootScope
    , $state, $stateParams, $window, $filter, APP_CONFIG
    ) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    // editableOptions.theme = 'bs3';

    if ($window.sessionStorage["userInfo"]) {
        $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        //  $rootScope.userInfo.lastLoginDate = $filter('date')($rootScope.userInfo.lastLoginDate, APP_CONFIG.dateFormat + ' hh:mm:ss a');
    }
    $rootScope.dateFormat = APP_CONFIG.dateFormat;
    $rootScope.stringPattern = "^[^|='+*<>]*$";
    // $rootScope.apiRootUrl = APP_CONFIG.apiUrlForDoc.replace('/api', '');

    $rootScope.validationMsg = {
        positiveNum: "Positive number only",
        required: "Mandatory",
        maxLength: function (maxlen) { return "Max length is " + maxlen },
        minLength: function (minlen) { return "Min length is " + minlen },
        resSpecialChar: "Special characters not allowed",
        numOnly: "Only number allowed",
        invalidDate: "Invalid date"
    };

    //$rootScope.buttonText = {
    //    addNew: 'Add New',
    //    save: 'Save',
    //    update: 'Update',
    //    saveAndNext: 'Save & Next',
    //    updateAndNext: 'Update & Next',
    //    cancel: 'Cancel',
    //    clear: 'Clear',
    //    viewAll: 'Back',
    //    remove: 'Remove',
    //    submit: 'Submit'
    //};





    //$rootScope.toDos = {
    //    'isShowQuery': false,
    //    'isShowPolling': false,
    //    'isShowReimbLTA': false,
    //    'isShowLoan': false,
    //    'isShowReimbExpense': false
    //};

});


