﻿'use strict';

angular.module('app.Iotreports').controller('hbhaRptCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, IotreportsFactory) {

    function loadInitData() {
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth() + 1;
        if (month < 10) {
            month = "0" + month;
        };
        var nowday = d.getDate();
        var predate = d.setDate(d.getDate() - 30)
        var preday = d.getDate();
        if (preday < 10) {
            preday = "0" + preday;

        };
        var premonth = d.getMonth() + 1;
        if (premonth < 10) {
            premonth = "0" + premonth;
        };
        if (nowday < 10) {
            nowday = "0" + nowday;

        };
        $scope.fromdate = preday + "-" + premonth + "-" + year;
        $scope.todate = nowday + "-" + month + "-" + year;
    }

    $scope.display = function () {

        var date = new Date($scope.todate)
        var param = {
            DeviceNo: $scope.vehicleRefId,
            sfDate: $scope.fromdate,
            stDate: $scope.todate,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id
        }

        IotreportsFactory.getHA_HB_HCRptList(param).then(function (result) {

            setInitialData(result.data.data);
            showData(result.data.data);
            SetChartData(result.data.data);
        


        }, function (error) {
            console.log(error);
        });
    }

    function setInitialData(ListData) {

        $scope.stoppageDataModel = ListData;
    }

    function showData(ListData) {
        showStoppageDataList(ListData);
    }

    function showStoppageDataList(ListData) {
        var IsDelete = true;
        //angular.forEach(ListData, function (data, index) {
        //    data.Edit = "";
        //    data.View = "";
        //    data.Delete = "";
        //});

        var tableOptions = {
            "data": ListData,
            columns: [
                //{ data: "deviceno" },
	            { data: "gridrecorddate" },
                { data: "hardacceleration" },
                { data: "hardbraking" },
                { data: "cornerpacket" },
                { data: "overspeedstarted" },
                { data: "mcall" }

            ],
            "order": [[0, 'desc']],
            destroy: true
            //, "aoColumnDefs": [

            //     //{ "bVisible": IsDelete, "aTargets": [-1] }
            //     { "bVisible": false, "aTargets": [0] }

            //]

        };

        $scope.tablebind(tableOptions, $scope);
    }

    function fillVehicleList(allVehicle) {

        $scope.dtVehicleList = JSON.parse(allVehicle);
    }

    function getVehicleList() {
        var filters = {
            //VechileNo: VechileNo,
            //DeviceNo: DeviceNo,
            //fromdate: fromdate,
            //todate: todate,
            //fromtime: fromtime,
            //totime: totime,
            //NoOfRecords: NoOfRecords,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };

        IotreportsFactory.getVehicleList(filters)
        .then(function (result) {
            fillVehicleList(JSON.stringify(result.data.all));
        }, function (error) {
            console.log(error);
        });
    }

    function generateChartData(alldata) {
        debugger;
        var chartData = [];
        var recorddate;
        var newDate;
        angular.forEach(alldata, function (data, index) {
            //recorddate = Date.parse(data.recorddate);
            //newDate = new Date(recorddate);

            var cdatetime = data.recorddatetime;
            var hardacceleration = data.hardacceleration;
            var hardbraking = data.hardbraking;
            var cornerpacket = data.cornerpacket;
            var overspeedstarted = data.overspeedstarted;
            var mcall = data.mcall;



            chartData.push({
                date: cdatetime,
                BreakingEvents: hardbraking,
                AccelarationEvents: hardacceleration,
                Cornering: cornerpacket,
                OverspeedstartedEvents: overspeedstarted,
                MobilecallEvents: mcall
            });

        });


        return chartData;
    }

    //function SetChartData(alldata) {

    //    var chartData = generateChartData(alldata);
    //    var chart = AmCharts.makeChart("chartharsbreakingdiv", {
    //        "theme": "light",
    //        "type": "serial",
    //        "legend": {
    //            "horizontalGap": 10,
    //            "maxColumns": 3,
    //            "position": "bottom",
    //            "useGraphSettings": true,
    //            "markerSize": 10,
    //            "title": "Harsh Event"
    //        },
    //        "dataProvider": chartData,
    //        "valueAxes": [{
    //            "position": "left",
    //            "title": "No of Events",
    //        }],
    //        "startDuration": 1,
    //        "graphs": [{
    //            "balloonText": "Date:[[category]], Harsh Breaking : <b>[[value]]</b>",
    //            "fillAlphas": 0.9,
    //            "lineAlpha": 0.2,
    //            "title": "Breaking Count",
    //            "type": "column",
    //            "valueField": "BreakingEvents"
    //        }, {
    //            "balloonText": "Date:[[category]], Harsh Accelaration : <b>[[value]]</b>",
    //            "fillAlphas": 0.9,
    //            "lineAlpha": 0.2,
    //            "title": "Accelaration Count",
    //            "type": "column",
    //            "clustered": false,
    //            "columnWidth": 0.5,
    //            "valueField": "AccelarationEvents"
    //        }, {
    //            "balloonText": "Date:[[category]], Harsh Cornering : <b>[[value]]</b>",
    //            "fillAlphas": 0.9,
    //            "lineAlpha": 0.2,
    //            "title": "Cornering Count",
    //            "type": "column",
    //            "clustered": false,
    //            "columnWidth": 0.5,
    //            "valueField": "Cornering"
    //        }],
    //        "plotAreaFillAlphas": 0.1,
    //        "categoryField": "date",
    //        "categoryAxis": {
    //            "gridPosition": "start",
    //            "title": "Date",
    //        },
    //        "colors": ["#863ba2", "#28628e", "#298a52"],
    //        "export": {
    //            "enabled": true
    //        }
    //    });

    //}

    function SetChartData(alldata) {
        //var chartkey = [];
        //$.each(alldata[0], function (key, value) {
        //    chartkey.push(key);
        //});

        var chartData = generateChartData(alldata);
        var chart = AmCharts.makeChart("chartharsbreakingdiv", {
            "type": "serial",
            "theme": "light",
            "legend": {
                "horizontalGap": 10,
                "maxColumns": 5,
                "position": "bottom",
                "useGraphSettings": true,
                "markerSize": 10,
                "title": "Harsh Event "

            },
            "dataProvider": chartData,
            "valueAxes": [{
                "stackType": "regular",
                "axisAlpha": 0.3,
                "gridAlpha": 0,
                "title": "No of Events"
            }],
            "graphs": [{
                "balloonText": "Date:[[category]], Harsh Breaking : <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Harsh Breaking",
                "type": "column",
                "color": "#863ba2",
                "valueField": "BreakingEvents"
            }, {
                "balloonText": "Date:[[category]], Harsh Accelaration : <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Harsh Accelaration",
                "type": "column",
                "color": "#28628e",
                "valueField": "AccelarationEvents"
            }, {
                "balloonText": "Date:[[category]], Harsh Cornering : <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Harsh Cornering",
                "type": "column",
                "color": "#298a52",
                "valueField": "Cornering"
            },{
                    "balloonText": "Date:[[category]],Over Speed: <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Over Speed",
                "type": "column",
                "color": "#c53c2e",
                "valueField": "OverspeedstartedEvents"
                }, {
                    "balloonText": "Date:[[category]], Phone Call : <b>[[value]]</b>",
                    "fillAlphas": 0.8,
                    "labelText": "[[value]]",
                    "lineAlpha": 0.3,
                    "title": "Phone Call",
                    "type": "column",
                    "color": "#5c5c5c",
                    "valueField": "MobilecallEvents"
                }
            ],
            "categoryField": "date",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 1,
                "gridAlpha": 0,
                "position": "left"
            },
            "colors": ["#863ba2", "#28628e", "#298a52", "#c53c2e", "#5c5c5c"],
            "export": {
                "enabled": true
            }
        });


    }

    function init() {
        getVehicleList();
        loadInitData();
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
    }

    init();
});
