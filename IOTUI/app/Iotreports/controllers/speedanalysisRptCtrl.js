﻿'use strict';

angular.module('app.Iotreports').controller('speedanalysisRptCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, IotreportsFactory) {

    function loadInitData() {
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth() + 1;
        if (month < 10) {
            month = "0" + month;
        };
        var nowday = d.getDate();
        var predate = d.setDate(d.getDate() - 30)
        var preday = d.getDate();
        if (preday < 10) {
            preday = "0" + preday;

        };
        var premonth = d.getMonth() + 1;
        if (premonth < 10) {
            premonth = "0" + premonth;
        };
        if (nowday < 10) {
            nowday = "0" + nowday;

        };
        $scope.fromdate = preday + "-" + premonth + "-" + year;
        $scope.todate = nowday + "-" + month + "-" + year;
    }

    $scope.display = function () {

        var date = new Date($scope.todate)
        var param = {
            DeviceNo: $scope.vehicleRefId,
            sfDate: $scope.fromdate,
            stDate: $scope.todate,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id
        }

        IotreportsFactory.getspeedList(param).then(function (result) {
            showData(result.data.data);
            SetChartSpeedAnalysisData(result.data.data.speeddata);
            SetChartAggregateData(result.data.data.aggregate);
        }, function (error) {
            console.log(error);
        });
    }

    function showData(ListData) {
        var tableOptions = {
            "data": ListData.aggregate,
            columns: [

	            { data: "gridrecorddate" },
                { data: "avgspeed" },
                { data: "maxspeed" },

            ],
            "order": [[0, 'desc']],
            destroy: true
        };

        $scope.tablebind(tableOptions, $scope);

        var tableOptions = {
            "data": ListData.speeddata,
            columns: [
                { data: "recorddatetime" },
                { data: "0-10" },
                { data: "11-20" },
                { data: "21-30" },
                { data: "31-40" },
                { data: "41-50" },
                { data: "51-60" },
                { data: "61-70" },
                { data: "71-80" },
                { data: "81-90" },
                { data: "91-100" },
                { data: "101-120" },
                { data: "121-140" },
                { data: "141-500" }

            ],
            "order": [[0, 'desc']],
            destroy: true
        };

        $scope.tablebind1(tableOptions, $scope);
    }

    function fillVehicleList(allVehicle) {

        $scope.dtVehicleList = JSON.parse(allVehicle);
    }

    function getVehicleList() {
        var filters = {
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };

        IotreportsFactory.getVehicleList(filters)
        .then(function (result) {
            fillVehicleList(JSON.stringify(result.data.all));
        }, function (error) {
            console.log(error);
        });
    }

    function generateChartData(alldata) {
        var chartData = [];
        var recorddate;
        var newDate;
        angular.forEach(alldata, function (data, index) {
            chartData.push({
                date: data.recorddatetime,
                avgspeed: data.avgspeed,
                maxspeed: data.maxspeed
            });

        });

        return chartData;
    }

    function SetChartSpeedAnalysisData(alldata) {
        var chartkey = [];
        $.each(alldata[0], function (key, value) {
            chartkey.push(key);
        });


        var chart = AmCharts.makeChart("chartSpeedDiv", {
            "type": "serial",
            "theme": "light",
            "legend": {
                "horizontalGap": 5,
                "maxColumns": 7,
                "position": "bottom",
                "useGraphSettings": true,
                "markerSize": 8,
                "title": "Speed Range In km/hr "

            },
            "dataProvider": alldata,
            "valueAxes": [{
                "stackType": "regular",
                "axisAlpha": 0.3,
                "gridAlpha": 0,
                "title": "Driving Percentage(%)"
            }],
            "graphs": [{
                "balloonText": "Date:<b>[[recorddatetime]]</b>,<br>Speed Range:<b>[[title]] km/hr , </b> Percentage : <b>[[value]] % </b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": chartkey[1],
                "type": "column",
                "color": "#000000",
                "valueField": chartkey[1]
            }, {
                "balloonText": "Date:<b>[[recorddatetime]]</b>,<br>Speed Range:<b>[[title]] km/hr , </b> Percentage : <b>[[value]] % </b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": chartkey[2],
                "type": "column",
                "color": "#000000",
                "valueField": chartkey[2]
            }, {
                "balloonText": "Date:<b>[[recorddatetime]]</b>,<br>Speed Range:<b>[[title]] km/hr , </b> Percentage : <b>[[value]] % </b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": chartkey[3],
                "type": "column",
                "color": "#000000",
                "valueField": chartkey[3]
            }, {
                "balloonText": "Date:<b>[[recorddatetime]]</b>,<br>Speed Range:<b>[[title]] km/hr , </b> Percentage : <b>[[value]] % </b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": chartkey[4],
                "type": "column",
                "color": "#000000",
                "valueField": chartkey[4]
            }, {
                "balloonText": "Date:<b>[[recorddatetime]]</b>,<br>Speed Range:<b>[[title]] km/hr , </b> Percentage : <b>[[value]] % </b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": chartkey[5],
                "type": "column",
                "color": "#000000",
                "valueField": chartkey[5]
            },
            {
                "balloonText": "Date:<b>[[recorddatetime]]</b>,<br>Speed Range:<b>[[title]] km/hr , </b> Percentage : <b>[[value]] % </b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": chartkey[6],
                "type": "column",
                "color": "#000000",
                "valueField": chartkey[6]
            }, {
                "balloonText": "Date:<b>[[recorddatetime]]</b>,<br>Speed Range:<b>[[title]] km/hr , </b> Percentage : <b>[[value]] % </b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": chartkey[7],
                "type": "column",
                "color": "#000000",
                "valueField": chartkey[7]
            }, {
                "balloonText": "Date:<b>[[recorddatetime]]</b>,<br>Speed Range:<b>[[title]] km/hr , </b> Percentage : <b>[[value]] % </b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": chartkey[8],
                "type": "column",
                "color": "#000000",
                "valueField": chartkey[8]
            }, {
                "balloonText": "Date:<b>[[recorddatetime]]</b>,<br>Speed Range:<b>[[title]] km/hr , </b> Percentage : <b>[[value]] % </b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": chartkey[9],
                "type": "column",
                "color": "#000000",
                "valueField": chartkey[9]
            }, {
                "balloonText": "Date:<b>[[recorddatetime]]</b>,<br>Speed Range:<b>[[title]] km/hr , </b> Percentage : <b>[[value]] % </b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": chartkey[10],
                "type": "column",
                "color": "#000000",
                "valueField": chartkey[10]
            }, {
                "balloonText": "Date:<b>[[recorddatetime]]</b>,<br>Speed Range:<b>[[title]] km/hr , </b> Percentage : <b>[[value]] % </b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": chartkey[11],
                "type": "column",
                "color": "#000000",
                "valueField": chartkey[11]
            }, {
                "balloonText": "Date:<b>[[recorddatetime]]</b>,<br>Speed Range:<b>[[title]] km/hr , </b> Percentage : <b>[[value]] % </b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": chartkey[12],
                "type": "column",
                "color": "#000000",
                "valueField": chartkey[12]
            }

            ],
            "categoryField": "recorddatetime",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 1,
                "gridAlpha": 0,
                "position": "left"
            },
            "export": {
                "enabled": true
            }
        });


    }

    function SetChartAggregateData(alldata) {

        var chartData = generateChartData(alldata);
        var chart = AmCharts.makeChart("speedanalysisdiv", {
            "type": "serial",
            "theme": "light",
            "legend": {
                "useGraphSettings": true
            },
            "dataProvider": chartData,
            "valueAxes": [{
                "integersOnly": true,
                "maximum": 200,
                "minimum": 0,
                "reversed": false,
                "axisAlpha": 1,
                "dashLength": 5,
                "gridCount": 10,
                "position": "left",
                "title": "Speed (km/hr)"
            }],
            "startDuration": 0.5,
            "graphs": [{
                "balloonText": "Date: [[category]],Avg Speed : [[value]] km/hr",
                "bullet": "diamond",
                "title": "Avg Speed",
                "valueField": "avgspeed",
                "fillAlphas": 0
            }, {
                "balloonText": "Date: [[category]],Max Speed : [[value]] km/hr",
                "bullet": "round",
                "title": "Max Speed",
                "valueField": "maxspeed",
                "fillAlphas": 0

            }],
            "chartCursor": {
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "date",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 1,
                "fillAlpha": 0.05,
                "fillColor": "#000000",
                "gridAlpha": 0,
                "position": "bottom"
            },
            "colors": ["#01A9DB", "#d28924"],
            "export": {
                "enabled": true,
                "position": "bottom-right"
            }
        });


    }

    function init() {
        getVehicleList();
        loadInitData();
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
    }

    init();
});
