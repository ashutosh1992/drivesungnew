﻿'use strict';

angular.module('app.Iotreports').controller('tripdetailsanalysisRptCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, IotreportsFactory) {

    function loadInitData() {
      
        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }

        var param = {
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id
        };

        if ($rootScope.userInfo.role_Id != 2)
        {
            IotreportsFactory.ActiveTrip(param).then(function (result) {
                if (result.data.data[0] != null)
                {
                    $scope.starttrip = true;
                    $scope.endtrip = false;
                    $scope.activetrip_id = result.data.data[0].trip_id;
                }
                else
                {
                    $scope.starttrip = false;
                    $scope.endtrip = true;
                }
        
          
            }, function (error) {
                console.log(error);
            });

        }
        else {
            $scope.starttrip = true;
            $scope.endtrip = true;
        }


        IotreportsFactory.gettripsList(param).then(function (result) {

            setInitialData(result.data.data);
            showData(result.data.data);

        }, function (error) {
            console.log(error);
        });
    }

    function setInitialData(ListData) {

        $scope.stoppageDataModel = ListData;
    }

    function showData(ListData) {
        showStoppageDataList(ListData);
    }

    function showStoppageDataList(ListData) {
        var IsDelete = true;
        angular.forEach(ListData, function (data, index) {
            //data.Edit = "";
            //data.View = "";
            data.playtrip = " ";
            data.Edittripname = " ";
            data.playtripvideo = " ";
        });

        var tableOptions = {
            "data": ListData,
            columns: [
                { data: "trip_id" },
                { data: "vehicle_no" },
	            { data: "trip_name" },
                //{ data: "trip_from" },
                //{ data: "trip_to" },
                { data: "trip_start_time" },
                { data: "trip_end_time" },
                { data: "rating" },
                { data: "distance" },
                { data: "Edittripname" },
                { data: "playtrip" },
                { data: "playtripvideo" }


                //{ data: "sysdatetime" },
                //{ data: "noofrecords" }
            ],
            "order": [[0, 'desc']],
            destroy: true,

            "fnCreatedRow": function (nRow, aData, iDataIndex) {
               
                if (aData.rating > 0) {
                   // $('td:eq(7)', nRow).html('<span style="background:linear-gradient(to bottom,#1175c1 70%,#81a4bf 100%); display: inline-block; max-width: 40px; padding: 11px 9px; font-size: 16px; font-weight: 700;color: #fff; line-height: 1;vertical-align: middle;white-space: nowrap;text-align: center;background-color: #999;border-radius: 50px; margin-left:30px;"><span style="color:#fff; font-size:16px; font-weight:500;">' + aData.rating + '</span>');
                    $('td:eq(5)', nRow).html('<span style="background:linear-gradient(to bottom,#1175c1 70%,#81a4bf 100%); width: 100px !important; display: inline-block; max-width: 40px; padding: 11px 7px; font-size: 16px; font-weight: 700;color: #fff; line-height: 1;vertical-align: middle;white-space: nowrap;text-align: center;background-color: #999;border-radius: 50px; margin-left:30px;"><span style="color:#fff; font-size:16px; font-weight:500;">' + aData.rating + '</span>');
                }
                else {
                    $('td:eq(5)', nRow).html=''
                    //$('td:eq(7)', nRow).html('<span style="background:linear-gradient(to bottom,#1175c1 70%,#81a4bf 100%); display: inline-block; max-width: 40px; padding: 11px 9px; font-size: 16px; font-weight: 700;color: #fff; line-height: 1;vertical-align: middle;white-space: nowrap;text-align: center;background-color: #999;border-radius: 50px; margin-left:30px;"><span style="color:#fff; font-size:16px; font-weight:500;">NA</span>');
                    $('td:eq(5)', nRow).html('<span style="background:linear-gradient(to bottom,#1175c1 70%,#81a4bf 100%); display: inline-block; max-width: 44px; padding: 13px 13px; font-size: 15px; font-weight: 600;color: #fff; line-height: 1;vertical-align: middle;white-space: nowrap;text-align: center;background-color: #999;border-radius: 50px; margin-left:28px;"><span style="color:#fff; font-size:15px; font-weight:500; margin-left:-1px ">NA</span>');
                }
                var data = { 'deviceno': aData.deviceno, 'trip_start_time': aData.trip_start_time, 'trip_end_time': aData.trip_end_time };
                var videodata = { 'uploadfile': aData.uploadfile };
                $('td:eq(' + 7 + ')', nRow).html('<button class="btn btn-default" data-toggle="modal" data-target="#myModal" ng-click="tripiddetails(' +aData.trip_id + ')" ><i class="fa fa-edit"></i></button>');
                if (aData.trip_end_time != null) $('td:eq(8)', nRow).html("<button class='btn btn-primary btn-sm' ng-click='viewtripplayback(" + JSON.stringify(data) + ")'> <i class='fa fa-play' aria-hidden='true' title='Play Trip'></i> Play</button>");
                if (aData.trip_end_time != null && aData.uploadfile != "") $('td:eq(9)', nRow).html("<button class='btn btn-primary btn-sm' data-toggle='modal' data-target='#myvideo' ng-click='tripvideo(" + JSON.stringify(videodata) + ")' > <i class='fa fa-play' aria-hidden='true' title='Play Trip'></i> Video</button>");
            }

            //, "aoColumnDefs": [

            //     //{ "bVisible": IsDelete, "aTargets": [-1] }
            //     { "bVisible": false, "aTargets": [0] }

            //]

        };

        $scope.tablebind(tableOptions, $scope);
    }

    $scope.viewtripplayback = function (data) {
        var params = { 'action': 'trip', 'deviceno': data.deviceno, 'trip_from': data.trip_start_time, 'trip_to': data.trip_end_time };
        $window.sessionStorage["dashaction"] = JSON.stringify(params);
        $state.go('app.Iottripplayback');
    }

    $scope.tripvideo = function (videodata)
    {       
        $scope.videourl = videodata.uploadfile;
        var myVideo = document.getElementById('playVideo');
        myVideo.src = $scope.videourl;
        myVideo.load();
        myVideo.play();
    }


    $scope.startNewtrip = function () {
       
        var dt = new Date();
        var param = {
            trip_name: $filter('date')(new Date(dt.getFullYear(), dt.getMonth(), dt.getDate(), dt.getHours(), dt.getMinutes()), "yyyy-MM-dd HH:mm:ss"),
            trip_start_time: $filter('date')(new Date(dt.getFullYear(), dt.getMonth(), dt.getDate(), dt.getHours(), dt.getMinutes()), "yyyy-MM-dd HH:mm:ss"),
            //trip_from: $scope.trip_from,
            //trip_to: $scope.trip_to,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id
        };

        IotreportsFactory.StartTrip(param).then(function (result) {
             $scope.starttrip = true;
             $scope.endtrip = false;
             $scope.activetrip_id = result.data.data[0].trip_id;
                $.smallBox({
                    title: "Start Trip Successfully ",
                    content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                    color: "#5F895F",
                    iconSmall: "fa fa-check bounce animated",
                    timeout: 4000
                });

        }, function (error) {
            console.log(error);
        });

    }

    $scope.EndTrip = function () {
      var dt = new Date();
        var data = {
            trip_id: $scope.activetrip_id,
            trip_end_time: $filter('date')(new Date(dt.getFullYear(), dt.getMonth(), dt.getDate(), dt.getHours(), dt.getMinutes()), "yyyy-MM-dd HH:mm:ss"),
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id
        };

        //var fd = new FormData();
        //fd.append('file', null);
        //fd.append('data', JSON.stringify(data));

        IotreportsFactory.EndTrip(data).then(function (result) {
            $scope.starttrip = false;
            $scope.endtrip = true;
           
            $.smallBox({
                title: "End Trip Successfully ",
                content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                color: "#5F895F",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
            loadInitData();

        }, function (error) {
            console.log(error);
        });

    }
    $scope.tripiddetails=function (id){
        $scope.update_tripid=id;
    }

    $scope.Updatetripname = function () {

        //var dt = new Date();
        var param = {
            //trip_name: $filter('date')(new Date(dt.getFullYear(), dt.getMonth(), dt.getDate(), dt.getHours(), dt.getMinutes()), "yyyy-MM-dd HH:mm:ss"),
            //trip_start_time: $filter('date')(new Date(dt.getFullYear(), dt.getMonth(), dt.getDate(), dt.getHours(), dt.getMinutes()), "yyyy-MM-dd HH:mm:ss"),
            trip_name: $scope.trip_name,
            trip_id:$scope.update_tripid,
            //trip_to: $scope.trip_to,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id
        };

        IotreportsFactory.UpdateTrip(param).then(function (result) {
           
            $.smallBox({
                title: "Trip Updated Successfully ",
                content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                color: "#5F895F",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
            init();

        }, function (error) {
            console.log(error);
        });

    }


    function init() {
        $scope.videourl = "http://45.35.14.40/APIV4/UploadFile//tripvideodata/343_1305_01-10-2017_10-48-14.mp4";
        loadInitData();
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
    }

    init();
});
