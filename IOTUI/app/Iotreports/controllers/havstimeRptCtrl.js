﻿'use strict';

angular.module('app.Iotreports').controller('havstimeRptCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, IotreportsFactory) {

    function loadInitData() {
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth() + 1;
        if (month < 10) {
            month = "0" + month;
        };
        var nowday = d.getDate();
        var predate = d.setDate(d.getDate() - 30)
        var preday = d.getDate();
        if (preday < 10) {
            preday = "0" + preday;
        };
        var premonth = d.getMonth() + 1;
        if (premonth < 10) {
            premonth = "0" + premonth;
        };

        if (nowday < 10) {
            nowday = "0" + nowday;
        };

        $scope.fromdate = preday + "-" + premonth + "-" + year;
        $scope.todate = nowday + "-" + month + "-" + year;
    }

    $scope.display = function () {

        var date = new Date($scope.todate)
        var param = {
            DeviceNo: $scope.vehicleRefId,
            sfDate: $scope.fromdate,
            stDate: $scope.todate,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id
        }

        IotreportsFactory.getHA_vs_timeRptList(param).then(function (result) {
            showData(result.data.data);
            SetChartData(result.data.data);

        }, function (error) {
            console.log(error);
        });
    }


    function showData(ListData) {
        var tableOptions = {
            "data": ListData,
            columns: [
                { data: "recorddatetime" },
                { data: "hardacceleration" },
                { data: "hardbraking" },
                { data: "cornerpacket" },
                { data: "mcall" },
                { data: "overspeedstarted" }

            ],
            "order": [[0, 'desc']],
            destroy: true

        };

        $scope.tablebind(tableOptions, $scope);
    }

    function fillVehicleList(allVehicle) {
        $scope.dtVehicleList = JSON.parse(allVehicle);
    }

    function getVehicleList() {
        var filters = {
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };

        IotreportsFactory.getVehicleList(filters)
        .then(function (result) {
            fillVehicleList(JSON.stringify(result.data.all));
        }, function (error) {
            console.log(error);
        });
    }

    function generateChartData(alldata) {
        var chartData = [];
        var recorddate;
        var newDate;
        angular.forEach(alldata, function (data, index) {
            chartData.push({
                date: data.recorddatetime,
                hardbraking: data.hardbraking,
                hardacceleration: data.hardacceleration,
                cornerpacket: data.cornerpacket,
                mcall: data.mcall,
                overspeedstarted: data.overspeedstarted
            });
        });
        return chartData;
    }

    function SetChartData(alldata) {

        var chartData = generateChartData(alldata);
        var chart = AmCharts.makeChart("chartdiv", {
            "type": "serial",
            "theme": "light",
            "legend": {
                "useGraphSettings": true
            },
            "dataProvider": chartData,
            "synchronizeGrid": true,
            "valueAxes": [{
                "id": "v1",
                "axisColor": "#22958a",
                "axisThickness": 2,
                "axisAlpha": 1,
                "position": "left",
                "title": "Event 1- Yes, 0-No",
            }],
            "graphs": [
                 {
                     "valueAxis": "v1",
                     "lineColor": "#28628e",
                     "bullet": "round",
                     "bulletBorderThickness": 1,
                     "hideBulletsCount": 30,
                     "title": "Harsh Acceleration",
                     "valueField": "hardacceleration",
                     "fillAlphas": 0
                 },
                 {
                     "valueAxis": "v1",
                     "lineColor": "#863ba2",
                     "bullet": "round",
                     "bulletBorderThickness": 1,
                     "hideBulletsCount": 30,
                     "title": "Harsh Braking",
                     "valueField": "hardbraking",
                     "fillAlphas": 0
                 },
                 {
                     "valueAxis": "v1",
                     "lineColor": "#298a52",
                     "bullet": "round",
                     "bulletBorderThickness": 1,
                     "hideBulletsCount": 30,
                     "title": "Harsh Cornering",
                     "valueField": "cornerpacket",
                     "fillAlphas": 0
                 },
                {
                    "valueAxis": "v1",
                    "lineColor": "#5c5c5c",
                    "bullet": "round",
                    "bulletBorderThickness": 1,
                    "hideBulletsCount": 30,
                    "title": "Call Taken",
                    "valueField": "mcall",
                    "fillAlphas": 0
                },

                {
                    "valueAxis": "v1",
                    "lineColor": "#c53c2e",
                    "bullet": "round",
                    "bulletBorderThickness": 1,
                    "hideBulletsCount": 30,
                    "title": "Over Speed",
                    "valueField": "overspeedstarted",
                    "fillAlphas": 0
                }],
            "chartScrollbar": {},
            "chartCursor": {
                "cursorPosition": "mouse"
            },
            "categoryField": "date",
            "categoryAxis": {
                //"parseDates": true,
                "axisColor": "#DADADA",
                //"minPeriod": "hh:mm:ss",
                "minorGridEnabled": true,

            },
            "export": {
                "enabled": true,
                "position": "bottom-right"
            }
        });
        // chart.dataDateFormat = "dd:mm:yy";
        chart.addListener("dataUpdated", zoomChart);
        zoomChart(chart);

    }

    function zoomChart(chart) {
        chart.zoomToIndexes(chart.dataProvider.length - 20, chart.dataProvider.length - 1);
    }

    function init() {
        getVehicleList();
        loadInitData();
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
    }

    init();
});
