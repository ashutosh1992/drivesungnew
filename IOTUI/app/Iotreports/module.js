﻿"use strict";

angular.module('app.Iotreports', ['ui.router',
    'ngResource',
    'datatables',
    'datatables.bootstrap',
    'uiGmapgoogle-maps',
    'easypiechart',
    'angularjs-gauge'])

angular.module('app.Iotreports').config(function ($stateProvider) {
    $stateProvider
            .state('app.Iotreports', {
                abstract: true,
                
            })


            .state('app.Iotreports.aggregatedata', {
                url: '/IotDailyReport',
                views: {
                    "content@app": {
                        controller: 'aggregatedataCtrl',
                        templateUrl: 'app/Iotreports/views/aggregatedata.html'
                    }
                },
                resolve: {
                    scripts: function (lazyScript) {
                        return lazyScript.register([
                             'build/vendor.ui.js',
                            'build/vendor.datatables.js',
                            'build/vendor.graphs.js'
                        ]);
                    }
                },


                
            })


         .state('app.Iotreports.driverRatingData', {
             url: '/IotDailyReport',
             views: {
                 "content@app": {
                     templateUrl: 'app/Iotreports/views/driverratingdata.html',
                     controller: 'driverratingdataCtrl'
                 }
             },
             resolve: {
                 scripts: function (lazyScript) {
                     return lazyScript.register([
                          'build/vendor.ui.js',
                         'build/vendor.datatables.js',
                         'build/vendor.graphs.js'
                     ]);
                 }
             },


            
         })
    .state('app.Iotreports.stoppage', {
        url: '/stoppagedata',

        views: {
            "content@app": {
                templateUrl: 'app/Iotreports/views/stoppagedata.html',
                controller: 'stoppageCtrl'
            }
        },
        resolve: {
            scripts: function (lazyScript) {
                return lazyScript.register([
                     'build/vendor.ui.js',
                    'build/vendor.datatables.js',
                    'build/vendor.graphs.js'
                ]);
            }
        },
        
    })

    .state('app.Iotreports.distancedata', {
          url: '/distancedata',

          views: {
              "content@app": {
                  templateUrl: 'app/Iotreports/views/distanceData.html',
                  controller: 'distanceCtrl'
              }
          },
          resolve: {
              scripts: function (lazyScript) {
                  return lazyScript.register([
                       'build/vendor.ui.js',
                      'build/vendor.datatables.js',
                      'build/vendor.graphs.js'
                  ]);
              }
          },
         
      })

    .state('app.Iotreports.stoppageanalysis', {
        url: '/stoppageanalysis',

        views: {
            "content@app": {
                templateUrl: 'app/Iotreports/views/stoppageanalysis.html',
                controller: 'stoppageCtrl'
            }
        },
        resolve: {
            scripts: function (lazyScript) {
                return lazyScript.register([
                     'build/vendor.ui.js',
                    'build/vendor.datatables.js',
                    'build/vendor.graphs.js'
                ]);
            }
        },
        
    })

      .state('app.Iotreports.driverperformance', {
          url: '/driverperformance',

          views: {
              "content@app": {
                  templateUrl: 'app/Iotreports/views/driverperformance.html',
                  controller: 'driverperformanceRptCtrl'
              }
          },
          resolve: {
              scripts: function (lazyScript) {
                  return lazyScript.register([
                       'build/vendor.ui.js',
                      'build/vendor.datatables.js',
                      'build/vendor.graphs.js'
                  ]);
              }
          },
         
      })

      .state('app.Iotreports.speedanalysis', {
          url: '/speedanalysis',

          views: {
              "content@app": {
                  templateUrl: 'app/Iotreports/views/speedanalysis.html',
                  controller: 'speedanalysisRptCtrl'
              }
          },
          resolve: {
              scripts: function (lazyScript) {
                  return lazyScript.register([
                       'build/vendor.ui.js',
                      'build/vendor.datatables.js',
                      'build/vendor.graphs.js'
                  ]);
              }
          },
         
      })
    

      .state('app.Iotreports.hbhadata', {
          url: '/hbhahcanalysis',

          views: {
              "content@app": {
                  templateUrl: 'app/Iotreports/views/hbhadata.html',
                  controller: 'hbhaRptCtrl'
              }
          },
          resolve: {
              scripts: function (lazyScript) {
                  return lazyScript.register([
                       'build/vendor.ui.js',
                      'build/vendor.datatables.js',
                      'build/vendor.graphs.js'
                  ]);
              }
          },
         
      })

     .state('app.Iotreports.tempanalysis', {
         url: '/tempanalysis',

         views: {
             "content@app": {
                 templateUrl: 'app/Iotreports/views/tempanalysis.html',
                 controller: 'tempanalysisRptCtrl'
             }
         },
         resolve: {
             scripts: function (lazyScript) {
                 return lazyScript.register([
                      'build/vendor.ui.js',
                     'build/vendor.datatables.js',
                     'build/vendor.graphs.js'
                 ]);
             }
         },
        
     })
    
     .state('app.Iotreports.tripdetailsanalysis', {
         url: '/tripdetailsanalysis',

         views: {
             "content@app": {
                 templateUrl: 'app/Iotreports/views/tripdetailsanalysis.html',
                 controller: 'tripdetailsanalysisRptCtrl'
             }
         },
         resolve: {
             scripts: function (lazyScript) {
                 return lazyScript.register([
                      'build/vendor.ui.js',
                     'build/vendor.datatables.js',
                     'build/vendor.graphs.js'
                 ]);
             }
         },
         
     })


            .state('app.Iotreports.havstimedata', {
                url: '/IothavstimeReport',
                views: {
                    "content@app": {
                        controller: 'havstimeRptCtrl',
                        templateUrl: 'app/Iotreports/views/havstimeanalysis.html'
                    }
                },
                resolve: {
                    scripts: function (lazyScript) {
                        return lazyScript.register([
                             'build/vendor.ui.js',
                            'build/vendor.datatables.js',
                            'build/vendor.graphs.js'
                        ]);
                    }
                },

            })
    
      .state('app.Iotreports.dayvsnighthrsdata', {
          url: '/IotdayvsnightHrsReport',
          views: {
              "content@app": {
                  controller: 'dayvsnight_hrsCtrl',
                  templateUrl: 'app/Iotreports/views/dayvsnighthrsanalysis.html'
              }
          },
          resolve: {
              scripts: function (lazyScript) {
                  return lazyScript.register([
                       'build/vendor.ui.js',
                      'build/vendor.datatables.js',
                      'build/vendor.graphs.js'
                  ]);
              }
          },

      })

});


