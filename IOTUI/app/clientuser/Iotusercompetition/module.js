﻿"use strict";

angular.module('app.usercompetition', ['ui.router'])

angular.module('app.usercompetition').config(function ($stateProvider) {

    $stateProvider
        .state('app.usercompetition', {
            abstract: true,
            data: {
                title: ''
            }
        })

     .state('app.usercompetition.createcompetition', {
         url: '/createcompetition',
         data: {
             title: ''
         },
         views: {
             "content@app": {
                 templateUrl: 'app/clientuser/Iotusercompetition/views/createcompetition.html',
                 controller: 'createcompetitionCtrl'
             }
         }

     })

        .state('app.usercompetition.mycompetition', {
            url: '/mycompetition',
            data: {
                title: ''
            },
            views: {
                "content@app": {
                    templateUrl: 'app/clientuser/Iotusercompetition/views/mycompetition.html',
                    controller: 'mycompetitionCtrl'
                }
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js',
                       'build/vendor.datatables.js',
                       'build/vendor.graphs.js'
                    ]);
                }
            }
        })

             .state('app.usercompetition.userratingandranking', {
                 url: '/userating',
                 data: {
                     title: ''
                 },
                 views: {
                     "content@app": {
                         templateUrl: 'app/clientuser/Iotusercompetition/views/userratingandranking.html',
                         controller: 'userratingCtrl'
                     }
                 },
                 resolve: {
                     scripts: function (lazyScript) {
                         return lazyScript.register([
                             'build/vendor.ui.js',
                            'build/vendor.datatables.js',
                            'build/vendor.graphs.js'
                         ]);
                     }
                 }

             })

          .state('app.usercompetition.viewcompetition', {
              url: '/viewcompetition',
              data: {
                  title: ''
              },
              views: {
                  "content@app": {
                      templateUrl: 'app/clientuser/Iotusercompetition/views/viewcompetition.html',
                      controller: 'viewcompetitionCtrl'
                  },
                  resolve: {
                      scripts: function (lazyScript) {
                          return lazyScript.register([
                              'build/vendor.ui.js',
                             'build/vendor.datatables.js',
                             'build/vendor.graphs.js'
                          ]);
                      }
                  }

              }
          })

    //.state('app.tables.jqgrid', {
    //    url: '/tables/jqgrid',
    //    data: {
    //        title: 'Jquery Grid'
    //    },
    //    views: {
    //        "content@app": {
    //            controller: 'JqGridCtrl',
    //            templateUrl: "app/tables/views/jqgrid.html"
    //        }
    //    },
    //    resolve: {
    //        scripts: function (lazyScript) {
    //            return lazyScript.register([
    //                'smartadmin-plugin/legacy/jqgrid/js/minified/jquery.jqGrid.min.js',
    //                'smartadmin-plugin/legacy/jqgrid/js/i18n/grid.locale-en.js'
    //            ])

    //        }
    //    }
    //})
});