﻿"use strict"

angular.module('app.usercompetition').factory('usercompetitionFactory', function ($http, $q, APP_CONFIG) {

    var getViewCompetitionList = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Compitition/viewcmpt";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }


    var saveCompetitionRecord = function (fd) {
        debugger;
        var apiUrl
        //if (fd.action == "New") {
        apiUrl = APP_CONFIG.apiUrl + "/api/Compitition/CreateCompitition";
        //}
        //else if (fd.action == "Edit") {
        //    apiUrl = APP_CONFIG.apiUrl + "/api/ConfigSetting/UpdateConfigSetting";
        //}


        var deferred = $q.defer();

        $http.post(apiUrl, fd, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var joinCompetitionRecord = function (param) {

        var apiUrl = APP_CONFIG.apiUrl + "/api/Compitition/JoinCmpt";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var getMyCompetitionList = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Compitition/GetMyCompition";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var getRatingandRankingCompetitionList = function (param) {

        var apiUrl = APP_CONFIG.apiUrl + "/api/Compitition/GetTop10Player";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;

    }

    var sendInvite = function (param) {

        var apiUrl = APP_CONFIG.apiUrl + "/api/Compitition/SendInvitation";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }



 


    return {
        getViewCompetitionList: getViewCompetitionList,
        saveCompetitionRecord: saveCompetitionRecord,
        joinCompetitionRecord: joinCompetitionRecord,
        getMyCompetitionList: getMyCompetitionList,
        getRatingandRankingCompetitionList: getRatingandRankingCompetitionList,
        sendInvite: sendInvite
      
    };



});
