﻿'use strict';

angular.module('app.usercompetition').controller('userratingCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, usercompetitionFactory) {

    function loadInitData() {
        var actionCS = JSON.parse($window.sessionStorage["userratingandranking"]);
        $scope.prize_desc1 = actionCS.prize_desc1;
        $scope.prize_desc2 = actionCS.prize_desc2;
        $scope.prize_desc3 = actionCS.prize_desc3;

        var param = {
            "cmpt_id": actionCS.cmpt_id,
            "s_user_id": $rootScope.userInfo.user_id,
            "s_role_Id": 1
        };

        usercompetitionFactory.getRatingandRankingCompetitionList(param).then(function (result) {
            debugger;

            if (result.data.data.private.length > 0) {
                showData(result.data.data.private);
            }
            else {
                showData(result.data.data.public);
            }

        }, function (error) {
            console.log(error);
        });
    }

    function showData(ListData) {
        showRatingAndRakingList(ListData);

    }

    function showRatingAndRakingList(ListData) {
        var dt = new Date();
        var recorddatetime = "";
        var currenttime = $filter('date')(dt, "yyyy-MM-dd");
        angular.forEach(ListData, function (data, index) {
            data.pic = "";
            var a = moment(currenttime, 'YYYY/M/D');
            var b = moment($filter('date')(data.joining_date, "yyyy-MM-dd"), 'YYYY/M/D');
           if(currenttime>data.end_date)
            {
                a = moment($filter('date')(data.end_date, "yyyy-MM-dd"), 'YYYY/M/D');
            }
            if(b<data.start_date)
            {
                b = moment($filter('date')(data.start_date, "yyyy-MM-dd"), 'YYYY/M/D');
            }
            var diffDays = a.diff(b, 'days');
            //alert(diffDays);
            if (diffDays > 0)
            {
                data.day = diffDays;
            }
            else {
                data.day = "0";
            }
               
           
            //data.day = "5";

        });

        var tableOptions = {
            "data": ListData,
            columns: [
                { data: "pic" },
                { data: "first_name" },
	            { data: "score" },
                { data: "distance_travel" },
                { data: "day" }

            ],

            destroy: true,
            "fnCreatedRow": function (nRow, aData, iDataIndex) {

               
                //$('td:eq(' + 4 + ')', nRow).html('<button type="button" class="btn btn-primary" data-toggle="modal" ng-click="joincompetitionvalue(' + aData.cmpt_id + ')" data-target="#myModal">Join</button>');
              
                if (aData.profile_picture_uri != null) {
                    $('td:eq(' + 0 + ')', nRow).html(' <img src="' + aData.profile_picture_uri + '" alt="img" class="compcar">');
                }
                else {
                    $('td:eq(' + 0 + ')', nRow).html(' <a> <i class="glyphicon glyphicon-user" style=" font-size: 30px"></i></a>');
                }
            }
            //, "aoColumnDefs": [

            //     //{ "bVisible": IsDelete, "aTargets": [-1] }
            //     { "bVisible": false, "aTargets": [0] }

            //]

        };

        $scope.tablebind(tableOptions, $scope);
    }


    function init() {
        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
        loadInitData();
    }

    init();
});
