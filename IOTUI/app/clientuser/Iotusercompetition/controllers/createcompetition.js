﻿'use strict'

angular.module('app.usercompetition').controller('createcompetitionCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, usercompetitionFactory) {

    function loadInitData() {
    }

    $scope.SaveCompetitionRecord = function () {
        var date = new Date($scope.start_date);

        //$scope.start_date = $filter('date')(date, 'dd/MM/yyyy')
        //alert($scope.start_date);

        var data = {
            //'action': actionCS.action,
            'cmpt_name': $scope.cmpt_name,
            'cmpt_type': "private",
            's_date': $scope.start_date,
            'e_date': $scope.end_date,
            'terms_conditions': $scope.terms_conditions,
            'min_km_travel': $scope.min_km_travel,
            'total_km_travel':$scope.total_km_travel,
            //'ec_agefrom': $scope.ec_agefrom,
            //'ec_ageto': $scope.ec_ageto,
            'prize1_desc': 0,
            'prize2_desc':0,
            'prize3_desc': 0,
            //'gender': "male",
            //'authentication_required': $scope.AuthcheckboxModel.value,
            's_user_id': $rootScope.userInfo.user_id,
            's_role_Id': 1
        }
        var fd = new FormData();
        fd.append('file', $scope.cmpt_profile_picture_path);
        fd.append('data', JSON.stringify(data));
        
        usercompetitionFactory.saveCompetitionRecord(fd).then(function (result) {

            $.smallBox({
                title: "Competition Saved Successfully.",
                content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                color: "#5F895F",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
            $state.go('app.usercompetition.viewcompetition');

        }, function (error) {
            console.log(error);
        });



    }

    function init() {
        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
            loadInitData();
        }

        init();
    
})