'use strict';

angular.module('app.tripplayback').controller('tripplaybackCtrl', function ($q, $scope, $filter, $interval, $rootScope, CalendarEvent, SmartMapStyle, tripplaybackFactory, uiGmapGoogleMapApi, DTOptionsBuilder, DTColumnBuilder, APP_CONFIG) {

    function loadInitData() {
        $scope.address = ""
        $scope.GridData = false;
        $scope.playtrip = {
            newplay: true,
            play: false,
            pause: false,
            stop: false
        };
        var dt = new Date();

       

        $scope.$on('$destroy', function () {
            StopAutoRefresh();
        });

        $scope.grirdPlayTripdt = [];
        $scope.playtripmodel = {
            playSpeed: 100,
            chartModel: {
                Speed: {
                    SpeedPercent: 0,
                    Speed: 0,
                    maxvalue: 255,
                    Options: {
                        animate: {
                            duration: 0,
                            enabled: false
                        },
                        barColor: '#014476',
                        scaleColor: false,
                        lineWidth: 0,
                        lineCap: 'circle'
                    }
                },
                Rpm: {
                    RpmPercent: 0,
                    Rpm: 0,
                    maxvalue: 255,
                    Options: {
                        animate: {
                            duration: 0,
                            enabled: false
                        },
                        barColor: '#383c3f',
                        scaleColor: false,
                        lineWidth: 0,
                        lineCap: 'circle'
                    }
                },
                CoolantTemp: {
                    CoolantTempPercent: 0,
                    CoolantTemp: 0,
                    maxvalue: 255,
                    Options: {
                        animate: {
                            duration: 0,
                            enabled: false
                        },
                        barColor: '#2C3E50',
                        scaleColor: false,
                        lineWidth: 0,
                        lineCap: 'circle'
                    }
                },
                Fuellevel: {
                    FuellevelPercent: 0,
                    Fuellevel: 0,
                    maxvalue: 255,
                    Options: {
                        animate: {
                            duration: 0,
                            enabled: false
                        },
                        barColor: '#2C3E50',
                        scaleColor: false,
                        lineWidth: 0,
                        lineCap: 'circle'
                    }
                }
            },
            date: new Date(),
            Timer: null,
            windowOptions: {
                visible: false
            },
            fromdate: new Date(dt.getFullYear(), dt.getMonth(), dt.getDate(), 0,0 ),
            todate: new Date(dt.getFullYear(), dt.getMonth(), dt.getDate(), dt.getHours(), dt.getMinutes()),
             

        };
    };

    function loadDefaultMapAndChart() {
        $scope.playtripmodel.map = {
            center: { latitude: 18.50891, longitude: 73.92603 }, zoom: 10, bounds: {},
            control: {},
            markersr: [{
                id: 101,
                latitude: 0,
                longitude: 0
            }]
        };

        uiGmapGoogleMapApi.then(function (maps) {
            $scope.playtripmodel.polylines = [];
        });

        $scope.playtripmodel.chartModel = {
            Speed: {
                SpeedPercent: 0,
                Speed: 0,
                Options: {
                    animate: {
                        duration: 0,
                        enabled: false
                    },
                    barColor: '#014476',
                    scaleColor: false,
                    lineWidth: 20,
                    lineCap: 'circle'
                }
            },
            Rpm: {
                RpmPercent: 0,
                Rpm: 0,
                Options: {
                    animate: {
                        duration: 0,
                        enabled: false
                    },
                    barColor: '#383c3f',
                    scaleColor: false,
                    lineWidth: 20,
                    lineCap: 'circle'
                }
            },
            CoolantTemp: {
                CoolantTempPercent: 0,
                CoolantTemp: 0,
                Options: {
                    animate: {
                        duration: 0,
                        enabled: false
                    },
                    barColor: '#2C3E50',
                    scaleColor: false,
                    lineWidth: 20,
                    lineCap: 'circle'
                }
            },
            Fuellevel: {
                FuellevelPercent: 0,
                Fuellevel: 0,
                Options: {
                    animate: {
                        duration: 5,
                        enabled: true
                    },
                    barColor: '#E67E22',
                    scaleColor: false,
                    lineWidth: 3,
                    lineCap: 'butt'
                }
            }
        };

    }

    function fillVehicleList(allVehicle) {
        $scope.playtripmodel.dtVehicleList = JSON.parse(allVehicle);
    }

    function getVehicleList() {
        var filters = {
            //VechileNo: VechileNo,
            //DeviceNo: DeviceNo,
            //fromdate: fromdate,
            //todate: todate,
            //fromtime: fromtime,
            //totime: totime,
            //NoOfRecords: NoOfRecords,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        tripplaybackFactory.getVehicleList(filters)
        .then(function (result) {
            fillVehicleList(JSON.stringify(result.data.all));
        }, function (error) {
            console.log(error);
        });
    }

    function SetPlayTripData(alldata) {
        $scope.datalength = 0
        var alldata = JSON.parse(alldata);
        if (alldata.length > 0) {
            $scope.playtripmodel.alldata = alldata;
            $scope.datalength = alldata.length;
        }
        else {
            $scope.playtripmodel.alldata = null;
            loadDefaultMapAndChart();
            fillVehicleDataGrid(alldata);
        }

    }

    function SetMapData() {

        var pl = []
        if ($scope.polylines != null) {
            pl = $scope.polylines;
        }

        var cureentlat;
        var currentlng;
        var prvlat;
        var prvlng;
        var index = $scope.count;
        var data = $scope.playtripmodel.alldata[index];

        if ($scope.count > 0) {
            cureentlat = data.latitude;
            currentlng = data.longitude;
            prvlat = $scope.playtripmodel.alldata[$scope.count - 1].latitude;
            prvlng = $scope.playtripmodel.alldata[$scope.count - 1].longitude;
            if (data.speed >= 80) {
                pl.push({
                    id: index,
                    path:
                        [{ "latitude": prvlat, "longitude": prvlng }, { "latitude": cureentlat, "longitude": currentlng }],
                    stroke: {
                        color: '#f91517',
                        weight: 3
                    },
                    icons: [{
                        icon: {
                            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
                        },
                        offset: '25px',
                        repeat: '50px'
                    }
                    ]
                })
            }
            else if (data.speed > 60 && data.speed <= 79) {
                pl.push({
                    id: index,
                    path:
                        [{ "latitude": prvlat, "longitude": prvlng }, { "latitude": cureentlat, "longitude": currentlng }],
                    stroke: {
                        color: '#16872f',
                        weight: 3
                    },
                    icons: [{
                        icon: {
                            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
                        },
                        offset: '25px',
                        repeat: '50px'
                    }
                    ]
                })
            }
            else {

                pl.push({
                    id: index,
                    path:
                        [{ "latitude": prvlat, "longitude": prvlng }, { "latitude": cureentlat, "longitude": currentlng }],
                    stroke: {
                        color: '#1451f9',
                        weight: 3
                    },
                    icons: [{
                        icon: {
                            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
                        },
                        offset: '25px',
                        repeat: '50px'
                    }
                    ]
                })
            }

        }

        $scope.polylines = pl;

        var zoom = $scope.playtripmodel.map.zoom;
        $scope.playtripmodel.map = {
            center: { latitude: cureentlat, longitude: currentlng }, bounds: {},
            control: {},
            markersr: [{
                id: 101,
                latitude: cureentlat,
                longitude: currentlng
            }]
        };

        //var geocoder = new google.maps.Geocoder;
        //var latlng = { lat: parseFloat(cureentlat), lng: parseFloat(currentlng) };
        //geocoder.geocode({ 'location': latlng }, function (results, status) {
        //    if (status === 'OK') {
        //        if (results[1]) {
        //            $scope.address = results[1].formatted_address
        //        } else {
        //            $scope.address = latlng + " : " + 'No results found';
        //        }
        //    } else {
        //        $scope.address = latlng + " : " + 'Geocoder failed due to: ' + status;
        //    }
        //});

        //uiGmapGoogleMapApi.then(function (maps) {
        //    $scope.polylines = pl;
        //});

        //   $scope.$watch('map.markersr[0].latitude', loadDefaultMap1);
        //        $scope.$applyAsync();

    }

    function SetChartData() {
        var index = $scope.count;
        var livedata = $scope.playtripmodel.alldata[index];
        //Speed
        $scope.playtripmodel.date = livedata.recorddatetime;
        var CalSpeedPercentage = 0;
        var Speed = 0;
        if (livedata.speed != undefined) {
            CalSpeedPercentage = ((100 * livedata.speed) / 255);
            Speed = livedata.speed;
        }
        var CalRpmPercentage = 0;
        var Rpm = 0;
        if (livedata.rpm != undefined) {
            CalRpmPercentage = ((100 * livedata.rpm) / 16383);
            Rpm = livedata.rpm;
        }
        var CalCoolantTempPercentage = 0
        var CoolantTemp = 0;
        if (livedata.coolanttemperature != undefined) {
            CalCoolantTempPercentage = ((100 * livedata.coolanttemperature) / 215);
            CoolantTemp = livedata.coolanttemperature;
        }
        var CalFuellevelPercent = 0;
        var Fuellevel = 0;
        if (livedata.fuellevel != undefined) {
            CalFuellevelPercent = ((100 * livedata.fuellevel) / 100);
            Fuellevel = livedata.fuellevel;
        }

        $scope.playtripmodel.chartModel = {
            Speed: {
                SpeedPercent: CalSpeedPercentage,
                Speed: Speed,
                maxvalue: 255,
                Options: {
                    animate: {
                        duration: 0,
                        enabled: false
                    },
                    barColor: '#014476',
                    scaleColor: false,
                    lineWidth: 0,
                    lineCap: 'circle'
                }
            },
            Rpm: {
                RpmPercent: CalRpmPercentage,
                Rpm: Rpm,
                maxvalue: 16383.75,
                Options: {
                    animate: {
                        duration: 0,
                        enabled: false
                    },
                    barColor: '#383c3f',
                    scaleColor: false,
                    lineWidth: 0,
                    lineCap: 'circle'
                }
            },
            CoolantTemp: {
                CoolantTempPercent: CalCoolantTempPercentage,
                CoolantTemp: CoolantTemp,
                maxvalue: 255,
                Options: {
                    animate: {
                        duration: 0,
                        enabled: false
                    },
                    barColor: '#2C3E50',
                    scaleColor: false,
                    lineWidth: 0,
                    lineCap: 'circle'
                }
            }
            ,
            Fuellevel: {
                FuellevelPercent: CalFuellevelPercent,
                Fuellevel: Fuellevel,
                maxvalue: 215,
                Options: {
                    animate: {
                        duration: 0,
                        enabled: false
                    },
                    barColor: '#2C3E50',
                    scaleColor: false,
                    lineWidth: 0,
                    lineCap: 'circle'
                }
            }
        };


    }

    function fillVehicleDataGrid() {
        if ($scope.playtripmodel.alldata != null) {
            var index = $scope.count;
            var data = $scope.playtripmodel.alldata[index];
           // data.recorddatetime = $filter('date')(data.recorddatetime, APP_CONFIG.dateTimeFormat);
            data.type = "";
            data.location = "Location Details";
            data.distance = "";
            $scope.grirdPlayTripdt.push(data);
            //angular.forEach($scope.grirdPlayTripdt, function (data, index) {
            //  //  data.recorddatetime = $filter('date')(data.recorddatetime, APP_CONFIG.dateTimeFormat);
            //    data.type = "";
            //    data.location = "Location Details";
            //    data.distance = "";
            //});
            var tableOptions = {
                "data": $scope.grirdPlayTripdt,
                //"iDisplayLength": 10,
                columns: [
                    { data: "type" },
                    { data: "deviceno" },
                    { data: "recorddatetime" },
                    { data: "location" },
                    { data: "latitude" },
                    { data: "longitude" },
                    { data: "speed" },
                    { data: "distance" },
                    { data: "rpm" },
                    { data: "coolanttemperature" },
                    { data: "fuellevel" },

                ],
                "order": [[2, 'desc']],
                "mColumns": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                destroy: true

            };

            $scope.dashTablebind(tableOptions, $scope);
        }
    }

    $scope.getPlayTripData = function () {
        var VechileNo = $scope.playtripmodel.vehicleRefId;
        var DeviceNo = $scope.playtripmodel.vehicleRefId;
        var fromdate;
        var todate;
        var fromtime;
        var totime;
        var NoOfRecords = null;

        var filters = {
            VechileNo: VechileNo,
            DeviceNo: DeviceNo,
            fromdate: $scope.playtripmodel.fromdate,
            todate: $scope.playtripmodel.todate,
            fromtime: fromtime,
            totime: totime,
            NoOfRecords: 2000,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        //call api using factory
        tripplaybackFactory.getPlayTripData(filters)
        .then(function (result) {
            //format result
            SetPlayTripData(JSON.stringify(result.data.data.all));
            StartAutoRefresh();
        }, function (error) {
            console.log(error);
        });
    }

    //Stop Auto referesh
    function StopAutoRefresh() {
        //$scope.Message = "Timer stopped.";
        if (angular.isDefined($rootScope.TimerPlayTrip)) {
            $interval.cancel($rootScope.TimerPlayTrip);
            $scope.playtripmodel.playSpeed = 0;
        }
    };

    function StartAutoRefresh() {
        //$scope.Message = "Timer started. ";
        $rootScope.TimerPlayTrip = $interval(function () {
            if ($scope.datalength > 0) {
                $scope.count++;
                if ($scope.count < $scope.datalength) {
                    SetMapData();
                    SetChartData();
                    if ($scope.GridData) {
                        fillVehicleDataGrid();
                    }
                }
                else {
                    StopAutoRefresh();
                    $scope.playtrip = {
                        newplay: true,
                        play: false,
                        pause: false,
                        stop: false,
                    };
                }
            }
            else {
                StopAutoRefresh();
            }

            //$Scope.playtripmodel.playSpeed
        }, $scope.playtripmodel.playSpeed);
    };

    $scope.newplayOn = function () {

        $scope.count = 1;
        $scope.polylines = null;
        $scope.grirdPlayTripdt = [];
        $scope.playtripmodel.playSpeed = 100;
        $scope.getPlayTripData();
        $scope.playtrip = {
            newplay: false,
            play: false,
            pause: true,
            stop: true,
        };

    }
    $scope.playOn = function () {
        StartAutoRefresh();
        $scope.playtrip = {
            newplay: false,
            play: false,
            pause: true,
            stop: true,
        };
    }

    $scope.pauseOn = function () {
        StopAutoRefresh();
        $scope.playtrip = {
            newplay: false,
            play: true,
            pause: false,
            stop: true,
        };
    }

    $scope.stopOn = function () {
        StopAutoRefresh();
        $scope.playtrip = {
            newplay: true,
            play: false,
            pause: false,
            stop: false,
        };
        $scope.playtripmodel.alldata = undefined;
        $scope.datalength = 0;
        $scope.count = 1;
        $scope.polylines = null;
        $scope.grirdPlayTripdt = [];
        $scope.playtripmodel.playSpeed = 100;
        loadDefaultMapAndChart();
    }

    $scope.onClick = function () {
        $scope.playtripmodel.windowOptions.visible = !$scope.playtripmodel.windowOptions.visible;
    };

    $scope.closeClick = function () {
        $scope.playtripmodel.windowOptions.visible = false;
    };





    function init() {
        loadInitData();
        loadDefaultMapAndChart();
        getVehicleList()

    }

    init();

});