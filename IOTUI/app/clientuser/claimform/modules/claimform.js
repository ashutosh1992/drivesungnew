﻿"use strict"

angular.module('app.claimform').factory('claimformFactory', function ($http, $q,  APP_CONFIG) {
    //commonFactory
    var GetGeneralLedgerMasterList = function () {
        var apiUrl = APP_CONFIG.apiUrl + "/api/GeneralLedger/GetGeneralLedgerMasterList";
        var deferred = $q.defer();
        $http.post(apiUrl).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    //var getGeneralLedgerDetails = function (param) {
    //    var apiUrl = APP_CONFIG.apiUrl + "/api/GeneralLedger/getGLMasterDetails";
    //    var deferred = $q.defer();
    //    $http.post(apiUrl, param).then(function (result) {
    //        deferred.resolve(result);
    //    }, function (error) {
    //        deferred.reject(error);
    //    });
    //    return deferred.promise;
    //}

    //var deleteGeneralLedgerMasterRecord = function (param) {
    //    var apiUrl = APP_CONFIG.apiUrl + "/api/GeneralLedger/deleteGeneralLedgerMasterRecord";
    //    var deferred = $q.defer();

    //    $http.post(apiUrl, param).then(function (result) {
    //        deferred.resolve(result);
    //    },
    //    function (error) {
    //        deferred.reject(error);
    //    });
    //    return deferred.promise;
    //}

    //var saveGeneralLedgerMasterRecord = function (param) {
    //    var apiUrl = APP_CONFIG.apiUrl + "/api/GeneralLedger/saveGeneralLedgerMasterRecord";
    //    var deferred = $q.defer();

    //    $http.post(apiUrl, param).then(function (result) {
    //        deferred.resolve(result);
    //    }, function (error) {
    //        deferred.reject(error);
    //    });
    //    return deferred.promise;
    //}


    return {
        GetGeneralLedgerMasterList: GetGeneralLedgerMasterList,
        //getGeneralLedgerDetails: getGeneralLedgerDetails,
        //saveGeneralLedgerMasterRecord: saveGeneralLedgerMasterRecord,
        //deleteGeneralLedgerMasterRecord: deleteGeneralLedgerMasterRecord
    };

   

});
