﻿"use strict";

angular.module('app.claimform', ['ui.router'])

angular.module('app.claimform').config(function ($stateProvider) {

    $stateProvider
         .state('app.claimform', {
             abstract: true,
             data: {
                 title: ''
             }
         })
         .state('app.claimform.detail', {
             url: '/claimform',
            data: {
                title: 'Claim Form'
            },
            views: {
                "content@app": {
                    templateUrl: 'app/clientuser/claimform/views/claimformdetails.html',
                    controller: 'claimformCtrl'
                }
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js',
                        'build/vendor.datatables.js'
                    ]);
                }
            }
        })

});
