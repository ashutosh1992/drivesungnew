﻿angular.module('app.dashboardtelematics').controller('amchart',
[
    '$scope', 'googleMapAPIUrl', 'utilService',
    function ($scope, googleMapAPIUrl, utilService) {
        /* Scope Init */
        $scope.googleMapAPIUrl = googleMapAPIUrl;
        $scope.map = null;

        /* jQuery Init */
        angular.element(document).ready(function () {
            utilService.initDatePicker($("#trip_play_start_date"));
            utilService.initDatePicker($("#trip_play_end_date"));
            utilService.initDataTable($("#trip_table"));

            /*Gauge Speed Ometer Chart*/
            /*Gauge Speed Ometer Chart*/
            var ratingmeter = AmCharts.makeChart("chartratingmeterdiv", {
                "type": "gauge",
                "theme": "light",
                "axes": [{
                    "axisThickness": 1,
                    "axisAlpha": 0.2,
                    "tickAlpha": 0.2,
                    "valueInterval": 20,
                    "bands": [{
                        "color": "#84b761",
                        "endValue": 90,
                        "startValue": 0
                    }, {
                        "color": "#fdd400",
                        "endValue": 130,
                        "startValue": 90
                    }, {
                        "color": "#cc4748",
                        "endValue": 220,
                        "innerRadius": "95%",
                        "startValue": 130
                    }],
                    "bottomText": "",
                    "bottomTextYOffset": -20,
                    "endValue": 220
                }],
                "arrows": [{}],
                "export": {
                    "enabled": true
                }
            });

            setInterval(randomValuenew, 2000);
            // set random value
            function randomValuenew() {
                var value = Math.round(Math.random() * 200);
                if (ratingmeter) {
                    if (ratingmeter.arrows) {
                        if (ratingmeter.arrows[0]) {
                            if (ratingmeter.arrows[0].setValue) {
                                ratingmeter.arrows[0].setValue(120);
                                ratingmeter.axes[0].setBottomText("120");
                            }
                        }
                    }
                }
            }

            /*Gauge Speed Meter Chart*/
            var gaugespeedmeter = AmCharts.makeChart("chartspeedmeterdiv", {
                "type": "gauge",
                "theme": "light",
                "axes": [{
                    "axisThickness": 1,
                    "axisAlpha": 0.2,
                    "tickAlpha": 0.2,
                    "valueInterval": 20,
                    "bands": [{
                        "color": "#84b761",
                        "endValue": 90,
                        "startValue": 0
                    }, {
                        "color": "#fdd400",
                        "endValue": 130,
                        "startValue": 90
                    }, {
                        "color": "#cc4748",
                        "endValue": 220,
                        "innerRadius": "95%",
                        "startValue": 130
                    }],
                    "bottomText": "0 RPM",
                    "bottomTextYOffset": -20,
                    "endValue": 220
                }],
                "arrows": [{}],
                "export": {
                    "enabled": true
                }
            });

            setInterval(randomValue, 2000);
            // set random value
            function randomValue() {
                var value = Math.round(Math.random() * 200);
                if (gaugespeedmeter) {
                    if (gaugespeedmeter.arrows) {
                        if (gaugespeedmeter.arrows[0]) {
                            if (gaugespeedmeter.arrows[0].setValue) {
                                gaugespeedmeter.arrows[0].setValue(value);
                                gaugespeedmeter.axes[0].setBottomText(value + " RPM");
                            }
                        }
                    }
                }
            }

            /*Gauge Speed Ometer Chart*/
            var gaugeometer = AmCharts.makeChart("chartspeedometerdiv", {
                "type": "gauge",
                "theme": "light",
                "axes": [{
                    "axisThickness": 1,
                    "axisAlpha": 0.2,
                    "tickAlpha": 0.2,
                    "valueInterval": 20,
                    "bands": [{
                        "color": "#84b761",
                        "endValue": 90,
                        "startValue": 0
                    }, {
                        "color": "#fdd400",
                        "endValue": 130,
                        "startValue": 90
                    }, {
                        "color": "#cc4748",
                        "endValue": 220,
                        "innerRadius": "95%",
                        "startValue": 130
                    }],
                    "bottomText": "0\tkm/hr",
                    "bottomTextYOffset": -20,
                    "endValue": 220
                }],
                "arrows": [{}],
                "export": {
                    "enabled": true
                }
            });

            setInterval(randomValue1, 2000);
            // set random value
            function randomValue1() {
                var value = Math.round(Math.random() * 200);
                if (gaugeometer) {
                    if (gaugeometer.arrows) {
                        if (gaugeometer.arrows[0]) {
                            if (gaugeometer.arrows[0].setValue) {
                                gaugeometer.arrows[0].setValue(value);
                                gaugeometer.axes[0].setBottomText(value + "\tkm/hr");
                            }
                        }
                    }
                }
            }

            /*Gauge Speed accelerometer Chart*/
            var gaugeaccelerometer = AmCharts.makeChart("chartacceleratordiv", {
                "type": "gauge",
                "theme": "light",
                "axes": [{
                    "axisThickness": 1,
                    "axisAlpha": 0.2,
                    "tickAlpha": 0.2,
                    "valueInterval": 20,
                    "bands": [{
                        "color": "#84b761",
                        "endValue": 90,
                        "startValue": 0
                    }, {
                        "color": "#fdd400",
                        "endValue": 130,
                        "startValue": 90
                    }, {
                        "color": "#cc4748",
                        "endValue": 220,
                        "innerRadius": "95%",
                        "startValue": 130
                    }],
                    "bottomText": "0\nCoolent Temprature",
                    "bottomTextYOffset": -20,
                    "endValue": 220
                }],
                "arrows": [{}],
                "export": {
                    "enabled": true
                }
            });

            setInterval(randomValue2, 2000);
            // set random value
            function randomValue2() {
                var value = Math.round(Math.random() * 200);
                if (gaugeaccelerometer) {
                    if (gaugeaccelerometer.arrows) {
                        if (gaugeaccelerometer.arrows[0]) {
                            if (gaugeaccelerometer.arrows[0].setValue) {
                                gaugeaccelerometer.arrows[0].setValue(value);
                                gaugeaccelerometer.axes[0].setBottomText(value + "\nCoolent Temprature");
                            }
                        }
                    }
                }
            }

            AmCharts.makeChart("tripPlaybackdiv", {
                "type": "serial",
                "theme": "light",
                "legend": {
                    "useGraphSettings": true
                },
                "dataProvider": [{
                    "Day": 'Day 1',
                    "AvgSpeed": 20,
                    "MaxSpeed": 40,
                    "MinSpeed": 20,

                }, {
                    "Day": 'Day 2',
                    "AvgSpeed": 30,
                    "MaxSpeed": 60,
                    "MinSpeed": 30,

                }, {
                    "Day": 'Day 3',
                    "AvgSpeed": 25,
                    "MaxSpeed": 50,
                    "MinSpeed": 60,

                }, {
                    "Day": 'Day 4',
                    "AvgSpeed": 60,
                    "MaxSpeed": 120,
                    "MinSpeed": 10,

                }, {
                    "Day": 'Day 5',
                    "AvgSpeed": 30,
                    "MaxSpeed": 60,
                    "MinSpeed": 80,

                }, {
                    "Day": 'Day 6',
                    "AvgSpeed": 50,
                    "MaxSpeed": 100,
                    "MinSpeed": 30,

                }, {
                    "Day": 'Day 7',
                    "AvgSpeed": 20,
                    "MaxSpeed": 40,
                    "MinSpeed": 80,

                }, {
                    "Day": 'Day 8',
                    "AvgSpeed": 45,
                    "MaxSpeed": 90,
                    "MinSpeed": 40,

                }, {
                    "Day": 'Day 9',
                    "AvgSpeed": 50,
                    "MaxSpeed": 120,
                    "MinSpeed": 50,

                }, {

                    "Day": 'Day 10',
                    "AvgSpeed": 40,
                    "MaxSpeed": 80,
                    "MinSpeed": 20,

                }],
                "valueAxes": [{
                    "integersOnly": true,
                    "maximum": 200,
                    "minimum": 0,
                    "reversed": false,
                    "axisAlpha": 1,
                    "dashLength": 5,
                    "gridCount": 10,
                    "position": "left",
                    "title": "Speed(in km)"
                }],
                "startDuration": 0.5,
                "graphs": [{
                    "balloonText": "line1",
                    "bullet": "diamond",
                    "title": "line1",
                    "valueField": "AvgSpeed",
                    "fillAlphas": 0
                }, {
                    "balloonText": "line2",
                    "bullet": "diamond",
                    "title": "line2",
                    "valueField": "MinSpeed",
                    "fillAlphas": 0
                }, {
                    "balloonText": "line3",
                    "bullet": "round",
                    "title": "line3",
                    "valueField": "MaxSpeed",
                    "fillAlphas": 0

                }],
                "chartCursor": {
                    "cursorAlpha": 0,
                    "zoomable": false
                },
                "categoryField": "Day",
                "categoryAxis": {
                    "gridPosition": "start",
                    "axisAlpha": 1,
                    "fillAlpha": 0.05,
                    "fillColor": "#000000",
                    "gridAlpha": 0,
                    "position": "bottom"
                },
                "export": {
                    "enabled": true,
                    "position": "bottom-right"
                }
            });
        });

    }
]);