﻿"use strict";

angular.module('app.competition', ['ui.router'])

angular.module('app.competition').config(function ($stateProvider) {

    $stateProvider
        .state('app.competition', {
            abstract: true,
            data: {
                title: ''
            }
        })

        .state('app.competition.list', {
            url: '/competitionlist',
            data: {
                title: ''
            },
            views: {
                "content@app": {
                    templateUrl: 'app/clientadmin/competition/views/competitionlist.html',
                    controller: 'competitionlistCtrl'
                }
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js',
                        'build/vendor.datatables.js'
                    ]);
                }
            }
        })

        .state('app.competition.detail', {
            url: '/competition',
            data: {
                title: ''
            },
            views: {
                "content@app": {
                    templateUrl: 'app/clientadmin/competition/views/competitiondetails.html',
                    controller: 'competitionAddEditCtrl'
                }
            }
            //resolve: {
            //    srcipts: function (lazyScript) {
            //        return lazyScript.register([
            //               'build/vendor.ui.js',
            //            'build/vendor.datatables.js'

            //        ])
            //    }
            //}
        })
});
