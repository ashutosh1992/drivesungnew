"use strict";

angular.module('app.auth').directive('loginInfo', function (User, $window, $rootScope) {

    return {
        restrict: 'A',
        templateUrl: 'app/auth/directives/login-info.tpl.html',
        link: function (scope, element) {
           // debugger;
            if ($window.sessionStorage["userInfo"]) {

                $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
                //$rootScope.userInfo.lastLoginDate = $filter('date')($rootScope.userInfo.lastLoginDate, APP_CONFIG.dateFormat + ' hh:mm:ss a');
            }

            //User.initialized.then(function () {
            //    scope.user = User
            //});
            // alert($rootScope.userInfo.first_name);
            scope.first_name = $rootScope.userInfo.first_name;
            //  alert($rootScope.userInfo.profile_picture_uri);
            scope.profile_picture_uri = $rootScope.userInfo.profile_picture_uri;
        }
    }
})
