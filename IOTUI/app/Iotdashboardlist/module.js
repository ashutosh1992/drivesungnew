'use strict';



angular.module('app.Iotdashboardlist', [
    'ui.router',
    'ngResource',
    'datatables',
    'datatables.bootstrap',
    'uiGmapgoogle-maps',
    'easypiechart',
    'angularjs-gauge'
])
  .config(function (uiGmapGoogleMapApiProvider) {
      uiGmapGoogleMapApiProvider.configure({
          key: 'AIzaSyA7bGFFmYR_pRQeU84EYuPVmv__bpSbqdo',
          v: '3.0', //defaults to latest 3.X anyhow
          libraries: 'weather,geometry,visualization,drawing'
      });
  })


.config(function ($stateProvider) {
    $stateProvider
        .state('app.Iotdashboardlist', {
            url: '/Iotdashboardlist',
            views: {
                "content@app": {
                    controller: 'IotdashboardlistCtrl',
                    templateUrl: 'app/iotdashboardlist/views/iotdashboardlist.html'
                }
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                         'build/vendor.ui.js',
                        'build/vendor.datatables.js',
                        'build/vendor.graphs.js'
                    ]);
                }
            },

           
            
        })

    //.state('app.dashboardtelematics.livetracking', {
    //    url: '/LiveTracking',
    //    views: {
    //        "content@app": {
    //            controller: 'dashboardtelematics',
    //            templateUrl: 'app/clientuser/dashboard-telematics/LiveTracking.html'
    //        }
    //    },
    //    resolve: {
    //        scripts: function (lazyScript) {
    //            return lazyScript.register([
    //                 'build/vendor.ui.js',
    //                'build/vendor.datatables.js',
    //                'build/vendor.graphs.js'
    //            ]);
    //        }
    //    },


    //    data: {
    //        title: 'Dashboard Telematics'
    //    }
    //})

    //   .state('app.dashboardtelematics.mapallvechile', {
    //       url: '/MapAllVechile',
    //       views: {
    //           "content@app": {
    //               controller: 'dashboardtelematics',
    //               templateUrl: 'app/clientuser/dashboard-telematics/mapAllVechile.html'
    //           }
    //       },
    //       resolve: {
    //           scripts: function (lazyScript) {
    //               return lazyScript.register([
    //                    'build/vendor.ui.js',
    //                   'build/vendor.datatables.js',
    //                   'build/vendor.graphs.js'
    //               ]);
    //           }
    //       },


    //       data: {
    //           title: 'Dashboard Telematics'
    //       }
    //   })


    //   .state('app.dashboardtelematics.Tripplayback', {
    //       url: '/TripPlayBack',
    //       views: {
    //           "content@app": {
    //               controller: 'dashboardtelematics',
    //               templateUrl: 'app/clientuser/dashboard-telematics/Tripplayback.html'
    //           }
    //       },
    //       resolve: {
    //           scripts: function (lazyScript) {
    //               return lazyScript.register([
    //                    'build/vendor.ui.js',
    //                   'build/vendor.datatables.js',
    //                   'build/vendor.graphs.js'
    //               ]);
    //           }
    //       },


    //       data: {
    //           title: 'Dashboard Telematics'
    //       }
    //   })

    //.state('app.dashboardtelematics.springBoard', {
    //    url: '/springBoard',
    //    views: {
    //        "content@app": {
    //            controller: 'dashboardtelematics',
    //            templateUrl: 'app/clientuser/dashboard-telematics/springboard.html'
    //        }
    //    },
    //    resolve: {
    //        scripts: function (lazyScript) {
    //            return lazyScript.register([
    //                 'build/vendor.ui.js',
    //                'build/vendor.datatables.js',
    //                'build/vendor.graphs.js'
    //            ]);
    //        }
    //    },


    //    data: {
    //        title: 'Dashboard Telematics'
    //    }
    //});
});
