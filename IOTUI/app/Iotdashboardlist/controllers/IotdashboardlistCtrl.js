'use strict';

angular.module('app.Iotdashboardlist').controller('IotdashboardlistCtrl', function ($q, $scope, $filter, $interval, $rootScope, $window, $timeout, $state, CalendarEvent, SmartMapStyle, dashboardFactory, uiGmapGoogleMapApi, DTOptionsBuilder, DTColumnBuilder, APP_CONFIG, Excel) {

    function loadInitData() {
        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }

        $scope.$on('$destroy', function () {
            StopAutoRefresh();
        });
        //  $scope.user_name = $rootScope.userInfo.user_name

    }

    function fillDashBoardLiveData(alldata) {
        var alldata = JSON.parse(alldata);
        if (alldata.length > 0) {
            $scope.alldata = alldata;
            fillVehicleDataGrid(alldata);

        }
        else {

            fillVehicleDataGrid(alldata);
        }

    }

    function fillVehicleDataGrid(alldata) {

        angular.forEach(alldata, function (data, index) {
            var loc;
            if (data.mobileno == null) {
                data.mobileno = "";
            }
            if (data.obdavaliable) {
                data.obdfilter = "obd yes";
            }
            else {
                data.obdfilter = "obd no";
            }
            data.timelocation = "Location: <br> Date: " + data.recorddatetime + "<br> lat:" + data.latitude + "<br> long:" + data.longitude;
            data.userdeatail = "";
            data.Graph = " ";
            data.Map = "Map";
            data.obd = " ";


        });
        var tableOptions = {
            "data": alldata,

            columns: [

                { data: "status" },
                { data: "userdeatail" },
                { data: "timelocation" },
                { data: "user_category" },
                //{ data: "rpm" },
                { data: "alert" },
                { data: "Graph" },
                { data: "Map" },
                { data: "obd" },
                { data: "rating" },
                { data: "deviceno" },
                { data: "vehicle_no" },
                { data: "first_name" },
                { data: "user_name" },
                { data: "status" },
                { data: "obdfilter" },
            ],
            "order": [[1, 'desc']],
            // "mColumns": [0, 1, 2, 3, 4, 5, 6,7,8,9,10,],

            "destroy": true,
            "aoColumnDefs": [{ "bVisible": false, "aTargets": [9] }, { "bVisible": false, "aTargets": [10] }, { "bVisible": false, "aTargets": [11] }, { "bVisible": false, "aTargets": [12] }, { "bVisible": false, "aTargets": [13] }, { "bVisible": false, "aTargets": [14] }],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {

                $('td:eq(0)', nRow).html('');
                if (aData.status == 'disconnect') {
                    //$('td:eq(0)', nRow).append('<i class="fa fa-car fa-3x" aria-hidden="true" style="color: #4169e1" title = "disconnect">');
                    $('td:eq(0)', nRow).append('  <img src="./styles/img/Disconnect_Vehicle.png" alt="" height="40" width="40" style="align-content: center;" title = "Disconnect" >');
                }
                if (aData.status == 'stop') {
                    //$('td:eq(0)', nRow).append('<i class="fa fa-car fa-3x" aria-hidden="true" style="color: #ccc" title = "stop">');
                    $('td:eq(0)', nRow).append('  <img src="./styles/img/Parking.png" alt="" height="40" width="40" style="align-content: center; vertical-align:middle; margin-top:10px" title = "Stop" >');
                }
                if (aData.status == 'moving') {
                    //$('td:eq(0)', nRow).append('<i class="fa fa-car fa-3x" aria-hidden="true" style="color: #9db56a" title = "moving">');
                    $('td:eq(0)', nRow).append('  <img src="./styles/img/Moving_Vehicle.png" alt="" height="40" width="40" style="align-content: center; vertical-align:middle; margin-top:10px" title = "moving" >');
                }

                //$('td:eq(0)', nRow).html('');
                //if (aData.status == 'disconnect') {
                //    $('td:eq(0)', nRow).append('  <img src="./styles/img/dis.png" alt="" height="30" width="30" style="align-content: center;" title = "Disconnect" >');
                //}
                //if (aData.status == 'stop') {
                //    $('td:eq(0)', nRow).append('  <img src="./styles/img/par.png" alt="" height="30" width="30" style="align-content: center;" title = "Stop" >');
                //}
                //if (aData.status == 'moving') {
                //    $('td:eq(0)', nRow).append('  <img src="./styles/img/mov.png" alt="" height="30" width="30" style="align-content: center;" title = "Moving" >');
                //}


                var DriverName = aData.first_name != null && aData.first_name != '' ? aData.first_name : aData.user_name;
                var VehicleNo = aData.vehicle_no != null && aData.vehicle_no != '' ? aData.vehicle_no : aData.deviceno;

                $('td:eq(1)', nRow).html("<i class='fa fa-user' aria-hidden='true' style='color:blue'></i> : " + DriverName + "<br><a  title = 'Trip playback' ng-click='viewIottripplayback(" + aData.deviceno + ")' > <i class='fa fa-car' aria-hidden='true' style='color:#da6918'></i> : " + VehicleNo + "</a><br> <i class='fa fa-phone' aria-hidden='true' style='color:#bd1616'></i> : " + aData.mobileno);
                //$('td:eq(2)', nRow).html("<i class='fa fa-calendar' style='color:red'  title = 'Date & Time'></i> &nbsp" + aData.recorddatetime + "<br><i class='fa fa-map-marker'style='color:green' title = 'Lat. Long.'></i>:&nbsp" + aData.latitude + ", &nbsp" + aData.longitude);

                var locationdata = {
                    id: iDataIndex,
                    lat: aData.latitude,
                    lng: aData.longitude
                };
                $('td:eq(2)', nRow).html("<i class='fa fa-calendar' style='color:red'  title = 'Date & Time'></i> &nbsp" + aData.recorddatetime + "<br><a id='#viewlocation" + iDataIndex + "' ng-click='locationinfo(" + JSON.stringify(locationdata) + ")' class='fa fa-location-arrow'style='color:green' title = 'click here for location details' ></a><div id='container" + iDataIndex + "'></div>");
                // $('td:eq(2)', nRow).html("<i class='fa fa-calendar' style='color:red'  title = 'Date & Time'></i> &nbsp" + aData.recorddatetime) ;
                //$('td:eq(3)', nRow).html('');
                //$('td:eq(3)', nRow).html("<style='color:red; align-content:center!important; margin-left:30px';  title = 'Speed/hr'> &nbsp&nbsp " + aData.speed + "");
                //$('td:eq(3)', nRow).html("<style='color:red; align-content:center!important; margin-left:30px';  title = 'Speed/hr'> &nbsp&nbsp " + aData.speed + "");
                $('td:eq(3)', nRow).html('');
                $('td:eq(3)', nRow).html("<style='color:red; align-content:center!important; margin-left:30px';> &nbsp&nbsp " + aData.user_category + "");

                $('td:eq(4)', nRow).html('');
                if (aData.alert == 'HB' || aData.alert == 'HC' || aData.alert == 'HA' || aData.alert == 'OS' || aData.alert == 'CT') {

                    $('td:eq(4)', nRow).append('<i class="fa fa-bell fa-2x" style="color:red; align-content:center!important; margin-left:10px;" title = "' + aData.alert + '"></i>');
                }
                else {

                    $('td:eq(4)', nRow).append('<i class="fa fa-bell-o fa-2x" style="color:#ccc; align-content:center!important;  margin-left:10px;" title = "' + 'No alert' + '"></i>');
                }
                $('td:eq(5)', nRow).append("<a ng-click='viewGraphicalReport(" + aData.deviceno + ")'><i class='fa fa-bar-chart fa-2x' style='color:#9db56a; margin-left:20px;' title = 'Graphical Reports'></i></a>");
                $('td:eq(6)', nRow).html("<a ng-click='viewIotlivetracking(" + aData.deviceno + ")' class='fa fa-map-marker fa-2x' aria-hidden='true' style='color:#075e07; margin-left:20px;' title = 'Live Tracking'></a>");
                if (aData.obdavaliable) {
                    $('td:eq(7)', nRow).html("<a ng-click='viewIotOBDdata(" + aData.deviceno + ")'><img src='styles/img/OBD_Icon.png' height='40px' width='40px' alt='img' class='margin-bottom-5 margin-top-0' style='display: inline-block;vertical-align: middle;white-space: nowrap;text-align: center;margin-left:10px;' title = 'OBD Data'></a>");
                }
                else {
                    $('td:eq(7)', nRow).html("<a ng-click='viewIotOBDdataNotAvaliable(" + VehicleNo + ")'><img src='styles/img/No_OBD_Icon.png' height='40px' width='40px' alt='img' class='margin-bottom-5 margin-top-0' style='display: inline-block;vertical-align: middle;white-space: nowrap;text-align: center;margin-left:10px;' title = 'OBD Data'></a>");
                }

                // $('td:eq(8)', nRow).html('</br>');
                //$('td:eq(8)', nRow).html('<span style="background:linear-gradient(to bottom,#1175c1 70%,#81a4bf 100%); display: inline-block; max-width: 40px; padding: 11px 9px; font-size: 16px; font-weight: 700;color: #fff; line-height: 1;vertical-align: middle;white-space: nowrap;text-align: center;background-color: #999;border-radius: 50px; margin-left:30px;"><span style="color:#fff; font-size:16px; font-weight:500;">' + aData.rating + '</span>');
                //$('td:eq(8)', nRow).html('<span style="background:linear-gradient(to bottom,#1175c1 70%,#81a4bf 100%); display: inline-block; max-width: 40px; padding: 11px 9px; font-size: 15px; font-weight: 600;color: #fff; line-height: 1;vertical-align: middle;white-space: nowrap;text-align: center;background-color: #999;border-radius: 50px; margin-left:28px;"><span style="color:#fff; font-size:15px; font-weight:500; margin-left:-1px ">' + aData.rating + '</span>');
                $('td:eq(8)', nRow).html('<span style="background:linear-gradient(to bottom,#1175c1 70%,#81a4bf 100%); width: 100px !important; display: inline-block; max-width: 40px; padding: 11px 7px; font-size: 16px; font-weight: 700;color: #fff; line-height: 1;vertical-align: middle;white-space: nowrap;text-align: center;background-color: #999;border-radius: 50px; margin-left:30px;"><span style="color:#fff; font-size:16px; font-weight:500;">' + aData.rating + '</span>');
            }
        };

        $scope.dashTablebind(tableOptions, $scope);
    }


    $scope.viewGraphicalReport = function (deviceno) {
        var params = { 'action': 'graph', 'deviceno': deviceno };
        $window.sessionStorage["dashaction"] = JSON.stringify(params);

        $state.go('app.IotGraphicalReport');
    }


    $scope.viewIotlivetracking = function (deviceno) {
        var params = { 'action': 'live', 'deviceno': deviceno };
        $window.sessionStorage["dashaction"] = JSON.stringify(params);

        $state.go('app.iotlivetracking');
    }

    $scope.viewIottripplayback = function (deviceno) {
        var params = { 'action': 'history', 'deviceno': deviceno };
        $window.sessionStorage["dashaction"] = JSON.stringify(params);

        $state.go('app.Iottripplayback');
    }

    $scope.viewIotOBDdata = function (deviceno) {
        var params = { 'action': 'obd', 'deviceno': deviceno };
        $window.sessionStorage["dashaction"] = JSON.stringify(params);

        $state.go('app.Iotobd');
    }

    $scope.viewIotOBDdataNotAvaliable = function (deviceno) {
        var hexcolor = "#5F895F";
        $.smallBox({
            title: "OBD",
            content: "<i class='fa fa-clock-o'></i> <i> OBD not connected for vehicle no = " + deviceno + "</i>",
            color: hexcolor,
            iconSmall: "fa fa-check bounce animated",
            timeout: 4000
        });
    }

    $scope.locationinfo = function (locationdata) {
        var geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(locationdata.lat, locationdata.lng);
        var location;

        geocoder.geocode({ 'latLng': latlng }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    location = results[1].formatted_address;;
                }
                else {
                    location = 'lat-' + locationdata.lat + ', lng-' + locationdata.lng;
                }
                document.getElementById("container" + locationdata.id + "").innerHTML = '</small>' + location + '</small>';
            }
        })

    }

    function getDashBoardLiveData() {
        var VechileNo = $scope.vehicleRefId;
        var DeviceNo = $scope.vehicleRefId;
        var fromdate;
        var todate;
        var fromtime;
        var totime;
        var NoOfRecords = $scope.NoOfRecords;

        var filters = {
            VechileNo: VechileNo,
            DeviceNo: DeviceNo,
            fromdate: fromdate,
            todate: todate,
            fromtime: fromtime,
            totime: totime,
            NoOfRecords: NoOfRecords,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        //call api using factory
        dashboardFactory.getDashBoardLiveData(filters)
        .then(function (result) {
            //format result
            fillDashBoardLiveData(JSON.stringify(result.data.data.dashboardlist));
            //   fillSubHeaderData(JSON.stringify(result.data.data.count))
        }, function (error) {
            console.log(error);
        });
    }

    function fillVehicleList(allVehicle) {
        $scope.dtVehicleList = JSON.parse(allVehicle);

    }

    function getVehicleList() {
        var filters = {
            //VechileNo: VechileNo,
            //DeviceNo: DeviceNo,
            //fromdate: fromdate,
            //todate: todate,
            //fromtime: fromtime,
            //totime: totime,
            //NoOfRecords: NoOfRecords,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        dashboardFactory.getVehicleList(filters)
        .then(function (result) {
            fillVehicleList(JSON.stringify(result.data.all));
        }, function (error) {
            console.log(error);
        });
    }

    $scope.AutoOnOff = function () {
        if ($scope.OnOffStatus) {
            StartAutoRefresh();
        }
        else {
            StopAutoRefresh();
        }
    }

    $scope.ManualRefresh = function () {
        getDashBoardLiveData();
    }


    function StartAutoRefresh() {
        $rootScope.Timer = $interval(function () {
            getDashBoardLiveData();
        }, 30000);
    };

    //Stop Auto referesh
    function StopAutoRefresh() {
        //$scope.Message = "Timer stopped.";
        if (angular.isDefined($rootScope.Timer)) {
            $interval.cancel($rootScope.Timer);
        }
    };
    //$scope.exportToExcel = function (tableId) { // ex: '#my-table'
    //    var exportHref = Excel.tableToExcel(tableId, 'WireWorkbenchDataExport');
    //    $timeout(function () { location.href = exportHref; }, 100); // trigger download
    //}

    function init() {

        loadInitData();
        //  getVehicleList();
        getDashBoardLiveData();
        // showchart();



    }

    init();

});