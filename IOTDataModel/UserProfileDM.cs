﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Npgsql;
using IOTBusinessEntities;
using IOTUtilities;
using HashAndSalt;
using System.IO;
using System.Web;
using System.Net.Mail;
using System.Web.Http;

namespace IOTDataModel
{
    public class UserProfileDM : IDisposable
    {
        private BaseDAL objDAL;
        private List<NpgsqlParameter> Parameters;
        private String strSqlQry;
        private Boolean blnResult;
        private DataTable dtData;
        private Int32 _user_id;
        private ResponseEntity objResponseEntity;
        private PasswordManager pwdManager;
        private string salt;
        private string password_string;
        private string passwordHash;
        private DataTable dtuserprofile;
        private DataSet dsData;
        private string Url = "";

        public UserProfileDM()
        {
            objDAL = new BaseDAL();
            objResponseEntity = new ResponseEntity();
            pwdManager = new PasswordManager();
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing)
            {
                // Free any other managed objects here.
                objDAL.Dispose();
                objResponseEntity = null;
                pwdManager.Dispose();
                Parameters = null;
                strSqlQry = null;
                dtData = null;
                salt = null;
                password_string = null;
                passwordHash = null;
            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~UserProfileDM()
        {
            Dispose(false);
        }

        #endregion

        //        public bool CreateUserProfile(UserEntity userEntity)
        //        {
        //            try
        //            {
        //                objDAL.CreateConnection();
        //                objDAL.BeginTransaction();



        //                strSqlQry = @"INSERT INTO user_master(role_id, user_name, first_name, middle_name, last_name, date_of_birth, gender, address1, address2, address3, mobileno, email_id1, email_id2, profile_picture_uri, deviceno, vehicle_no, license_no, license_expiry_date, registration_no, vehicle_manufacturer, vehicle_color, vehicle_type, user_login_type, insurance_mapping_key, o_id, created_at,   isactive)
        //	                                VALUES (@role_id, @user_name, @first_name, @middle_name, @last_name, @date_of_birth, @gender, @address1, @address2, @address3, @mobileno, @email_id1, @email_id2, @profile_picture_uri, @deviceno, @vehicle_no, @license_no, @license_expiry_date, @registration_no, @vehicle_manufacturer, @vehicle_color, @vehicle_type, @user_login_type, @insurance_mapping_key, @o_id, @created_at, @isactive) RETURNING user_id;";

        //                Parameters = new List<NpgsqlParameter>();
        //                //Parameters.Add(new NpgsqlParameter("@user_id", _user_id));
        //                Parameters.Add(new NpgsqlParameter("@role_id", CommonUtils.ConvertToInt(userEntity.role_Id)));
        //                Parameters.Add(new NpgsqlParameter("@user_name", CommonUtils.ConvertToString(userEntity.user_name)));
        //                Parameters.Add(new NpgsqlParameter("@first_name", CommonUtils.ConvertToString(userEntity.first_name)));
        //                Parameters.Add(new NpgsqlParameter("@middle_name", CommonUtils.ConvertToString(userEntity.middle_name)));
        //                Parameters.Add(new NpgsqlParameter("@last_name", CommonUtils.ConvertToString(userEntity.last_name)));
        //                if (CommonUtils.ConvertToDateTime(userEntity.date_of_birth) == null)
        //                {
        //                    Parameters.Add(new NpgsqlParameter("@date_of_birth", DBNull.Value));
        //                }
        //                else
        //                {
        //                    Parameters.Add(new NpgsqlParameter("@date_of_birth", CommonUtils.ConvertToDateTime(userEntity.date_of_birth)));
        //                }

        //                Parameters.Add(new NpgsqlParameter("@gender", CommonUtils.ConvertToString(userEntity.gender)));

        //                Parameters.Add(new NpgsqlParameter("@address1", CommonUtils.ConvertToString(userEntity.address1)));
        //                Parameters.Add(new NpgsqlParameter("@address2", CommonUtils.ConvertToString(userEntity.address2)));
        //                Parameters.Add(new NpgsqlParameter("@address3", CommonUtils.ConvertToString(userEntity.address3)));
        //                Parameters.Add(new NpgsqlParameter("@mobileno", CommonUtils.ConvertToString(userEntity.mobileno)));
        //                Parameters.Add(new NpgsqlParameter("@email_id1", CommonUtils.ConvertToString(userEntity.email_id1)));
        //                Parameters.Add(new NpgsqlParameter("@email_id2", CommonUtils.ConvertToString(userEntity.email_id2)));
        //                Parameters.Add(new NpgsqlParameter("@profile_picture_uri", CommonUtils.ConvertToString(userEntity.profile_picture_uri)));
        //                if (userEntity.deviceno == null)
        //                {
        //                    Parameters.Add(new NpgsqlParameter("@deviceno", CommonUtils.ConvertToInt64(userEntity.mobileno)));
        //                }
        //                else
        //                {
        //                    Parameters.Add(new NpgsqlParameter("@deviceno", CommonUtils.ConvertToInt64(userEntity.deviceno)));
        //                }
        //                Parameters.Add(new NpgsqlParameter("@vehicle_no", CommonUtils.ConvertToString(userEntity.vehicle_no)));
        //                Parameters.Add(new NpgsqlParameter("@license_no", CommonUtils.ConvertToString(userEntity.license_no)));
        //                if (CommonUtils.ConvertToDateTime(userEntity.license_expiry_date) == null)
        //                {
        //                    Parameters.Add(new NpgsqlParameter("@license_expiry_date", DBNull.Value));
        //                }
        //                else
        //                {
        //                    Parameters.Add(new NpgsqlParameter("@license_expiry_date", CommonUtils.ConvertToDateTime(userEntity.license_expiry_date)));
        //                }

        //                Parameters.Add(new NpgsqlParameter("@registration_no", CommonUtils.ConvertToString(userEntity.registration_no)));
        //                Parameters.Add(new NpgsqlParameter("@vehicle_manufacturer", CommonUtils.ConvertToString(userEntity.vehicle_manufacturer)));
        //                Parameters.Add(new NpgsqlParameter("@vehicle_color", CommonUtils.ConvertToString(userEntity.vehicle_color)));
        //                Parameters.Add(new NpgsqlParameter("@vehicle_type", CommonUtils.ConvertToString(userEntity.vehicle_type)));
        //                Parameters.Add(new NpgsqlParameter("@user_login_type", CommonUtils.ConvertToString(userEntity.user_login_type)));
        //                Parameters.Add(new NpgsqlParameter("@insurance_mapping_key", CommonUtils.ConvertToString(userEntity.insurance_mapping_key)));
        //                Parameters.Add(new NpgsqlParameter("@o_id", 1));
        //                Parameters.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
        //                Parameters.Add(new NpgsqlParameter("@isactive", true));

        //                object objuser_id = objDAL.ExecutParameterizedQryAndGetID(strSqlQry, Parameters);

        //                if (objuser_id != null)
        //                {
        //                    _user_id = CommonUtils.ConvertToInt(objuser_id.ToString());
        //                    passwordHash = pwdManager.GeneratePasswordHash(CommonUtils.ConvertToString(userEntity.user_pwd), out salt);
        //                    password_string = CommonUtils.ConvertToString(userEntity.user_pwd) + salt;

        //                    strSqlQry = @"INSERT INTO user_pwd(user_id,user_pwd, random_salt, password_string, pwdhash, pwd_type, pwd_expeiry_date, created_at, isactive)
        //	                       VALUES (@user_id, @user_pwd, @random_salt, @password_string, @pwdhash, @pwd_type, @pwd_expeiry_date, @created_at, @isactive) ";
        //                    Parameters = new List<NpgsqlParameter>();
        //                    Parameters.Add(new NpgsqlParameter("@user_id", _user_id));
        //                    Parameters.Add(new NpgsqlParameter("@user_pwd", CommonUtils.ConvertToString(userEntity.user_pwd)));
        //                    Parameters.Add(new NpgsqlParameter("@random_salt", salt));
        //                    Parameters.Add(new NpgsqlParameter("@password_string", password_string));
        //                    Parameters.Add(new NpgsqlParameter("@pwdhash", passwordHash));
        //                    Parameters.Add(new NpgsqlParameter("@pwd_type", "registartaion"));
        //                    Parameters.Add(new NpgsqlParameter("@pwd_expeiry_date", DBNull.Value));
        //                    Parameters.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
        //                    Parameters.Add(new NpgsqlParameter("@isactive", true));

        //                    blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);
        //                }

        //                objDAL.CommitTransaction();
        //                objDAL.CloseConnection();

        //                return true;

        //            }
        //            catch (Exception ex)
        //            {
        //                objDAL.RollBackTransaction();
        //                objDAL.CloseConnection();
        //                throw ex;
        //            }

        //        }



        public Int32 CreateUserProfile(UserEntity userEntity)
        {
            try
            {
                objDAL.CreateConnection();
                objDAL.BeginTransaction();



              // strSqlQry = @"INSERT INTO user_master(role_id, user_name, first_name, middle_name, last_name, date_of_birth, gender, address1, address2, address3, mobileno, email_id1, email_id2,  deviceno, vehicle_no, license_no, license_expiry_date, registration_no, vehicle_manufacturer, vehicle_color, vehicle_type, user_login_type, insurance_mapping_key, o_id, created_at,isactive,social_id,country,state,pincode,city,region)
            //	 VALUES (@role_id, @user_name, @first_name, @middle_name, @last_name, @date_of_birth, @gender, @address1, @address2, @address3, @mobileno, @email_id1, @email_id2, @deviceno, @vehicle_no, @license_no, @license_expiry_date, @registration_no, @vehicle_manufacturer, @vehicle_color, @vehicle_type, @user_login_type, @insurance_mapping_key, @o_id, @created_at, @isactive,@social_id,@country,@state,@pincode,@city,@region) RETURNING user_id;";
                strSqlQry = @"INSERT INTO user_master(role_id, user_name, first_name, middle_name, last_name, date_of_birth, gender, address1, address2, address3, mobileno, email_id1, email_id2,   vehicle_no, license_no, license_expiry_date, registration_no, vehicle_manufacturer, vehicle_color, vehicle_type, user_login_type, insurance_mapping_key, o_id, created_at,isactive,social_id,country,state,pincode,city,region,vin_no,insurance_provider,insurer_emailid,vehiclengine_type,engine_capacity)
	                         VALUES (@role_id, @user_name, @first_name, @middle_name, @last_name, @date_of_birth, @gender, @address1, @address2, @address3, @mobileno,LOWER( @email_id1),LOWER( @email_id2), @vehicle_no, @license_no, @license_expiry_date, @registration_no, @vehicle_manufacturer, @vehicle_color, @vehicle_type, @user_login_type, @insurance_mapping_key, @o_id, @created_at, @isactive,@social_id,@country,@state,@pincode,@city,@region,@vin_no,@insurance_provider,@insurer_emailid,@vehiclengine_type,@engine_capacity) RETURNING user_id;";


                Parameters = new List<NpgsqlParameter>();
                //Parameters.Add(new NpgsqlParameter("@user_id", _user_id));
                Parameters.Add(new NpgsqlParameter("@role_id", CommonUtils.ConvertToInt(userEntity.role_Id)));
                Parameters.Add(new NpgsqlParameter("@user_name", CommonUtils.ConvertToString(userEntity.user_name)));
                Parameters.Add(new NpgsqlParameter("@first_name", CommonUtils.ConvertToString(userEntity.first_name)));
                Parameters.Add(new NpgsqlParameter("@middle_name", CommonUtils.ConvertToString(userEntity.middle_name)));
                Parameters.Add(new NpgsqlParameter("@last_name", CommonUtils.ConvertToString(userEntity.last_name)));
                if (CommonUtils.ConvertToDateTime(userEntity.date_of_birth) == null)
                {
                    Parameters.Add(new NpgsqlParameter("@date_of_birth", DBNull.Value));
                }
                else
                {
                    Parameters.Add(new NpgsqlParameter("@date_of_birth", CommonUtils.ConvertToDateTime(userEntity.date_of_birth)));
                }

                Parameters.Add(new NpgsqlParameter("@gender", CommonUtils.ConvertToString(userEntity.gender)));

                Parameters.Add(new NpgsqlParameter("@address1", CommonUtils.ConvertToString(userEntity.address1)));
                Parameters.Add(new NpgsqlParameter("@address2", CommonUtils.ConvertToString(userEntity.address2)));
                Parameters.Add(new NpgsqlParameter("@address3", CommonUtils.ConvertToString(userEntity.address3)));
                Parameters.Add(new NpgsqlParameter("@mobileno", CommonUtils.ConvertToString(userEntity.mobileno)));
                Parameters.Add(new NpgsqlParameter("@email_id1", CommonUtils.ConvertToString(userEntity.email_id1)));
                Parameters.Add(new NpgsqlParameter("@email_id2", CommonUtils.ConvertToString(userEntity.email_id2)));
                //Parameters.Add(new NpgsqlParameter("@profile_picture_uri", CommonUtils.ConvertToString(userEntity.profile_picture_uri)));
                //if (userEntity.deviceno == 0)
                //{
                //    Parameters.Add(new NpgsqlParameter("@deviceno", CommonUtils.ConvertToInt64(userEntity.mobileno)));
                //}
                //else
                //{
                //    Parameters.Add(new NpgsqlParameter("@deviceno", CommonUtils.ConvertToInt64(userEntity.deviceno)));
                //}
                Parameters.Add(new NpgsqlParameter("@vehicle_no", CommonUtils.ConvertToString(userEntity.vehicle_no)));
                Parameters.Add(new NpgsqlParameter("@license_no", CommonUtils.ConvertToString(userEntity.license_no)));
                if (CommonUtils.ConvertToDateTime(userEntity.license_expiry_date) == null)
                {
                    Parameters.Add(new NpgsqlParameter("@license_expiry_date", DBNull.Value));
                }
                else
                {
                    Parameters.Add(new NpgsqlParameter("@license_expiry_date", CommonUtils.ConvertToDateTime(userEntity.license_expiry_date)));
                }

                Parameters.Add(new NpgsqlParameter("@registration_no", CommonUtils.ConvertToString(userEntity.registration_no)));
                Parameters.Add(new NpgsqlParameter("@vehicle_manufacturer", CommonUtils.ConvertToString(userEntity.vehicle_manufacturer)));
                Parameters.Add(new NpgsqlParameter("@vehicle_color", CommonUtils.ConvertToString(userEntity.vehicle_color)));
                Parameters.Add(new NpgsqlParameter("@vehicle_type", CommonUtils.ConvertToString(userEntity.vehicle_type)));
                Parameters.Add(new NpgsqlParameter("@user_login_type", CommonUtils.ConvertToString(userEntity.user_login_type)));
                Parameters.Add(new NpgsqlParameter("@insurance_mapping_key", CommonUtils.ConvertToString(userEntity.insurance_mapping_key)));
                Parameters.Add(new NpgsqlParameter("@o_id", CommonUtils.ConvertToInt(userEntity.o_id)));
                Parameters.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                Parameters.Add(new NpgsqlParameter("@isactive", true));
                Parameters.Add(new NpgsqlParameter("@social_id", CommonUtils.ConvertToString(userEntity.social_id)));
                Parameters.Add(new NpgsqlParameter("@country", CommonUtils.ConvertToString(userEntity.country)));
                Parameters.Add(new NpgsqlParameter("@state", CommonUtils.ConvertToString(userEntity.state)));
                Parameters.Add(new NpgsqlParameter("@city", CommonUtils.ConvertToString(userEntity.city)));
                Parameters.Add(new NpgsqlParameter("@pincode", CommonUtils.ConvertToString(userEntity.pincode)));
                Parameters.Add(new NpgsqlParameter("@region", CommonUtils.ConvertToString(userEntity.region)));
                Parameters.Add(new NpgsqlParameter("@vin_no", CommonUtils.ConvertToInt(userEntity.vin_no)));
                Parameters.Add(new NpgsqlParameter("@insurance_provider", CommonUtils.ConvertToString(userEntity.insurance_provider)));
                Parameters.Add(new NpgsqlParameter("@insurer_emailid", CommonUtils.ConvertToString(userEntity.insurer_emailid)));
                Parameters.Add(new NpgsqlParameter("@vehiclengine_type", CommonUtils.ConvertToString(userEntity.vehiclengine_type)));
                Parameters.Add(new NpgsqlParameter("@engine_capacity", CommonUtils.ConvertToString(userEntity.engine_capacity)));
                
                //Parameters.Add(new NpgsqlParameter("@social_type", CommonUtils.ConvertToString(userEntity.social_type)));

                object objuser_id = objDAL.ExecutParameterizedQryAndGetID(strSqlQry, Parameters);

               if (objuser_id != null)
                {
                  
                    _user_id = CommonUtils.ConvertToInt(objuser_id.ToString());

                    //--- for update device no as user id as per discussion 12/08/2017
                    strSqlQry = @"Update user_master set deviceno=@user_id where user_id=@user_id";
                    Parameters = new List<NpgsqlParameter>();
                    Parameters.Add(new NpgsqlParameter("@user_id", _user_id));
                    objDAL.ExecutParameterizedQry(strSqlQry, Parameters);
                    //----------------------------------------------
                    
                    // Insert or update Device Info for per login as per swapnil sir 
                    updateDeviceInfo(_user_id, userEntity);
                    // ---
                  
                    strSqlQry = @"INSERT INTO user_pwd(user_id,user_pwd, random_salt, password_string, pwdhash, pwd_type, pwd_expeiry_date, created_at, isactive)
	                       VALUES (@user_id, @user_pwd, @random_salt, @password_string, @pwdhash, @pwd_type, @pwd_expeiry_date, @created_at, @isactive) ";


                    Parameters = new List<NpgsqlParameter>();
                    Parameters.Add(new NpgsqlParameter("@user_id", _user_id));
                    if (userEntity.social_id !=null && userEntity.social_id !="" )
                    {
                        Parameters.Add(new NpgsqlParameter("@user_pwd", CommonUtils.ConvertToString(userEntity.user_name)));
                        userEntity.user_pwd = userEntity.user_name;
                       
                    }
                    else
                    {
                        Parameters.Add(new NpgsqlParameter("@user_pwd", CommonUtils.ConvertToString(userEntity.user_pwd)));
                       

                    }
                    passwordHash = pwdManager.GeneratePasswordHash(CommonUtils.ConvertToString(userEntity.user_pwd), out salt);
                    password_string = CommonUtils.ConvertToString(userEntity.user_pwd) + salt;

                  
                    Parameters.Add(new NpgsqlParameter("@random_salt", salt));
                    Parameters.Add(new NpgsqlParameter("@password_string", password_string));
                    Parameters.Add(new NpgsqlParameter("@pwdhash", passwordHash));
                    Parameters.Add(new NpgsqlParameter("@pwd_type", "registartaion"));
                    Parameters.Add(new NpgsqlParameter("@pwd_expeiry_date", DBNull.Value));
                    Parameters.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                    Parameters.Add(new NpgsqlParameter("@isactive", true));

                    blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);
                }

               
                objDAL.CommitTransaction();
                objDAL.CloseConnection();

                return CommonUtils.ConvertToInt(objuser_id.ToString());

            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public DataTable GetUserList(FilterEntity filterEntity)
        {
            try
            {
               

                if (filterEntity.s_role_Id == 2)
                {
                    strSqlQry = @"SELECT user_master.*, role_master.role_code, role_master.role_desc, user_pwd.pwd_expeiry_date
                        FROM            role_master INNER JOIN
                         user_master INNER JOIN
                         user_pwd ON user_master.user_id = user_pwd.user_id ON role_master.role_Id = user_master.role_Id
                         WHERE       (coalesce(user_master.deleted,false) = '0') and  o_id = (SELECT o_id FROM user_master where user_id =" + filterEntity.s_user_id + ") ";
                }
                else
                {
                    strSqlQry = @"SELECT user_master.*, role_master.role_code, role_master.role_desc, user_pwd.pwd_expeiry_date
                        FROM            role_master INNER JOIN
                         user_master INNER JOIN
                         user_pwd ON user_master.user_id = user_pwd.user_id ON role_master.role_Id = user_master.role_Id
                         WHERE       (coalesce(user_master.deleted,false) = '0')";
                }

                objDAL.CreateConnection();
                DataTable dtUser = objDAL.FetchRecords(strSqlQry);
                objDAL.CloseConnection();

                return dtUser;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
            finally
            {
                objResponseEntity = null;
            }
        }

        public DataTable GetUserById(FilterEntity filterEntity)
        {
            try
            {
                string strSqlQry = @"SELECT user_master.user_id , user_master.role_id, user_name, first_name, middle_name, last_name,to_char( date_of_birth, 'dd-MM-yyyy') as date_of_birth,to_char( date_of_birth, 'yyyy-MM-dd') as wdb , gender, address1, address2, address3, mobileno, email_id1, email_id2, profile_picture_uri, deviceno, vehicle_no, license_no, to_char( license_expiry_date, 'dd-MM-yyyy') as license_expiry_date,to_char( license_expiry_date, 'yyyy-MM-dd') as wled , registration_no, vehicle_manufacturer, vehicle_color, vehicle_type, user_login_type, insurance_mapping_key, o_id, social_id, social_type, country, state,vehiclengine_type, pincode, region, city, vin_no,insurance_provider,insurer_emailid,engine_capacity,insurance_comp_contact,sos_contact,family_contact, role_master.role_code, role_master.role_desc, user_pwd.pwd_expeiry_date
                        FROM   role_master INNER JOIN
                         user_master INNER JOIN
                         user_pwd ON user_master.user_id = user_pwd.user_id ON role_master.role_Id = user_master.role_Id
                         WHERE   coalesce(user_master.deleted,false) = '0' and user_master.user_id=" + CommonUtils.ConvertToInt(filterEntity.user_id);

                objDAL.CreateConnection();
                DataTable dtUser = objDAL.FetchRecords(strSqlQry);
                objDAL.CloseConnection();

                return dtUser;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objResponseEntity = null;
            }
        }

        public DataTable UpdateUserProfile(UserEntity userEntity)
        {
            try
            {
                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                _user_id = CommonUtils.ConvertToInt(userEntity.user_id);

                strSqlQry = @"UPDATE user_master
                            SET  first_name=@first_name, middle_name=@middle_name, last_name=@last_name, date_of_birth=@date_of_birth, gender=@gender,
                            address1=@address1, address2=@address2, address3=@address3, mobileno=@mobileno, email_id1=@email_id1, email_id2=@email_id2, 
                            vehicle_no=@vehicle_no, license_no=@license_no, license_expiry_date=@license_expiry_date, registration_no=@registration_no, 
                            vehicle_manufacturer=@vehicle_manufacturer, vehicle_color=@vehicle_color, vehicle_type=@vehicle_type, insurance_mapping_key=@insurance_mapping_key,
                            o_id=@o_id, country=@country, state=@state, pincode=@pincode, region=@region, city=@city,vin_no=@vin_no,insurance_provider=@insurance_provider,insurer_emailid=@insurer_emailid, update_at=@update_at,vehiclengine_type=@vehiclengine_type ,engine_capacity=@engine_capacity,insurance_comp_contact=@insurance_comp_contact,sos_contact=@sos_contact, family_contact=@family_contact WHERE (user_id = @user_id)";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@user_id", _user_id));
                Parameters.Add(new NpgsqlParameter("@first_name", CommonUtils.ConvertToString(userEntity.first_name)));
                Parameters.Add(new NpgsqlParameter("@middle_name", CommonUtils.ConvertToString(userEntity.middle_name)));
                Parameters.Add(new NpgsqlParameter("@last_name", CommonUtils.ConvertToString(userEntity.last_name)));
                if (CommonUtils.ConvertToDateTime(userEntity.date_of_birth) == null)
                {
                    Parameters.Add(new NpgsqlParameter("@date_of_birth", DBNull.Value));
                }
                else
                {
                    Parameters.Add(new NpgsqlParameter("@date_of_birth", CommonUtils.ConvertToDateTime(userEntity.date_of_birth)));
                }

                Parameters.Add(new NpgsqlParameter("@gender", CommonUtils.ConvertToString(userEntity.gender)));

                Parameters.Add(new NpgsqlParameter("@address1", CommonUtils.ConvertToString(userEntity.address1)));
                Parameters.Add(new NpgsqlParameter("@address2", CommonUtils.ConvertToString(userEntity.address2)));
                Parameters.Add(new NpgsqlParameter("@address3", CommonUtils.ConvertToString(userEntity.address3)));
                Parameters.Add(new NpgsqlParameter("@mobileno", CommonUtils.ConvertToString(userEntity.mobileno)));
                Parameters.Add(new NpgsqlParameter("@email_id1", CommonUtils.ConvertToString(userEntity.email_id1)));
                Parameters.Add(new NpgsqlParameter("@email_id2", CommonUtils.ConvertToString(userEntity.email_id2)));
                //     Parameters.Add(new NpgsqlParameter("@profile_picture_uri", CommonUtils.ConvertToString(userEntity.profile_picture_uri)));
                //if (userEntity.deviceno == null)
                //{
                //    Parameters.Add(new NpgsqlParameter("@deviceno", CommonUtils.ConvertToInt64(userEntity.mobileno)));
                //}
                //else
                //{
                //    Parameters.Add(new NpgsqlParameter("@deviceno", CommonUtils.ConvertToInt64(userEntity.deviceno)));
                //}
                Parameters.Add(new NpgsqlParameter("@vehicle_no", CommonUtils.ConvertToString(userEntity.vehicle_no)));
                Parameters.Add(new NpgsqlParameter("@license_no", CommonUtils.ConvertToString(userEntity.license_no)));
                if (CommonUtils.ConvertToDateTime(userEntity.license_expiry_date) == null)
                {
                    Parameters.Add(new NpgsqlParameter("@license_expiry_date", DBNull.Value));
                }
                else
                {
                    Parameters.Add(new NpgsqlParameter("@license_expiry_date", CommonUtils.ConvertToDateTime(userEntity.license_expiry_date)));
                }

                Parameters.Add(new NpgsqlParameter("@registration_no", CommonUtils.ConvertToString(userEntity.registration_no)));
                Parameters.Add(new NpgsqlParameter("@vehicle_manufacturer", CommonUtils.ConvertToString(userEntity.vehicle_manufacturer)));
                Parameters.Add(new NpgsqlParameter("@vehicle_color", CommonUtils.ConvertToString(userEntity.vehicle_color)));
                Parameters.Add(new NpgsqlParameter("@vehicle_type", CommonUtils.ConvertToString(userEntity.vehicle_type)));
                //Parameters.Add(new NpgsqlParameter("@user_login_type", CommonUtils.ConvertToString(userEntity.user_login_type)));
                Parameters.Add(new NpgsqlParameter("@insurance_mapping_key", CommonUtils.ConvertToString(userEntity.insurance_mapping_key)));
                Parameters.Add(new NpgsqlParameter("@o_id", CommonUtils.ConvertToInt(userEntity.o_id)));
                Parameters.Add(new NpgsqlParameter("@update_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                Parameters.Add(new NpgsqlParameter("@country", CommonUtils.ConvertToString(userEntity.country)));
                Parameters.Add(new NpgsqlParameter("@state", CommonUtils.ConvertToString(userEntity.state)));
                Parameters.Add(new NpgsqlParameter("@city", CommonUtils.ConvertToString(userEntity.city)));
                Parameters.Add(new NpgsqlParameter("@pincode", CommonUtils.ConvertToString(userEntity.pincode)));
                Parameters.Add(new NpgsqlParameter("@region", CommonUtils.ConvertToString(userEntity.region)));
                Parameters.Add(new NpgsqlParameter("@vehiclengine_type", CommonUtils.ConvertToString(userEntity.vehiclengine_type)));
                Parameters.Add(new NpgsqlParameter("@vin_no", CommonUtils.ConvertToInt(userEntity.vin_no)));
                Parameters.Add(new NpgsqlParameter("@insurance_provider", CommonUtils.ConvertToString(userEntity.insurance_provider)));
                Parameters.Add(new NpgsqlParameter("@insurer_emailid", CommonUtils.ConvertToString(userEntity.insurer_emailid)));
                Parameters.Add(new NpgsqlParameter("@engine_capacity", CommonUtils.ConvertToString(userEntity.engine_capacity)));
                Parameters.Add(new NpgsqlParameter("@insurance_comp_contact", CommonUtils.ConvertToString(userEntity.insurance_comp_contact)));
                Parameters.Add(new NpgsqlParameter("@sos_contact", CommonUtils.ConvertToString(userEntity.sos_contact)));
                Parameters.Add(new NpgsqlParameter("@family_contact", CommonUtils.ConvertToString(userEntity.family_contact)));
                
               //    Parameters.Add(new NpgsqlParameter("@isactive", CommonUtils.ConvertToBoolean(userEntity.isactive)));

                blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                string strSqlQry2 = @"SELECT  user_master.*, role_master.role_code, role_master.role_desc, user_pwd.pwd_expeiry_date
                        FROM   role_master INNER JOIN
                         user_master INNER JOIN
                         user_pwd ON user_master.user_id = user_pwd.user_id ON role_master.role_Id = user_master.role_Id
                         WHERE   coalesce(user_master.deleted,false) = '0' and user_master.user_id=" + _user_id;
                objDAL.CreateConnection();
                dtuserprofile = objDAL.FetchRecords(strSqlQry2);

                objDAL.CommitTransaction();
                objDAL.CloseConnection();

                return dtuserprofile;

            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool UpdateUserProfilePath(UserEntity userEntity)
        {
            try
            {
                objDAL.CreateConnection();
                _user_id = CommonUtils.ConvertToInt(userEntity.user_id);

                strSqlQry = @"UPDATE user_master  SET  profile_picture_uri=@profile_picture_uri  WHERE (user_id = @user_id)";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@user_id", _user_id));
                Parameters.Add(new NpgsqlParameter("@profile_picture_uri", CommonUtils.ConvertToString(userEntity.profile_picture_uri)));

                blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                objDAL.CloseConnection();

                return blnResult;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool DeleteUserProfile(UserEntity userEntity)
        {
            try
            {
                _user_id = CommonUtils.ConvertToInt(CommonUtils.ConvertToInt(userEntity.user_id));

                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                strSqlQry = @"update user_master set deleted=true where user_id=@user_id ";
                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@user_id", _user_id));

                blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                objDAL.CommitTransaction();
                objDAL.CloseConnection();


                return blnResult;

            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public DataTable GetRoleList(FilterEntity filterEntity)
        {
            try
            {
                string strSqlQry = @"SELECT role_Id, role_code, role_desc, isactive FROM role_master WHERE  (isactive = '1')";

                objDAL.CreateConnection();
                DataTable dtrole = objDAL.FetchRecords(strSqlQry);
                objDAL.CloseConnection();

                return dtrole;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
            finally
            {
                objResponseEntity = null;
            }
        }

        public bool ChangeUserPwd(UserEntity userEntity)
        {
            try
            {
                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                _user_id = CommonUtils.ConvertToInt(userEntity.user_id);


                string salt = null;
                string password_string = null;
                string passwordHash = pwdManager.GeneratePasswordHash(CommonUtils.ConvertToString(userEntity.user_pwd), out salt);
                password_string = CommonUtils.ConvertToString(userEntity.user_pwd) + salt;


                strSqlQry = @"Update user_pwd set isactive=false where user_id=@user_id";
                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@user_id", _user_id));

                blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);
                if (blnResult)
                {
                    strSqlQry = @"INSERT INTO user_pwd(	user_id, user_pwd, random_salt, password_string, pwdhash, pwd_type, pwd_expeiry_date, created_at, isactive)
	                       VALUES (@user_id, @user_pwd, @random_salt, @password_string, @pwdhash, @pwd_type, @pwd_expeiry_date, @created_at, @isactive)";
                    Parameters = new List<NpgsqlParameter>();
                    Parameters.Add(new NpgsqlParameter("@user_id", _user_id));
                    Parameters.Add(new NpgsqlParameter("@user_pwd", CommonUtils.ConvertToString(userEntity.user_pwd)));
                    Parameters.Add(new NpgsqlParameter("@random_salt", salt));
                    Parameters.Add(new NpgsqlParameter("@password_string", password_string));
                    Parameters.Add(new NpgsqlParameter("@pwdhash", passwordHash));
                    Parameters.Add(new NpgsqlParameter("@pwd_type", "registartaion"));
                    Parameters.Add(new NpgsqlParameter("@pwd_expeiry_date", DBNull.Value));
                    Parameters.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                    Parameters.Add(new NpgsqlParameter("@isactive", true));

                    blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);
                }

                objDAL.CommitTransaction();
                objDAL.CloseConnection();

                return true;

            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool CheckUserAvailability(UserEntity userEntity)
        {
            try
            {
                string SQLQry = @"select user_id  from user_master where user_name=@user_name and mobileno=@mobileno  and coalesce(user_master.deleted,false) = false";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@user_name", CommonUtils.ConvertToString(userEntity.user_name)));
                Parameters.Add(new NpgsqlParameter("@mobileno", CommonUtils.ConvertToString(userEntity.mobileno)));

                objDAL.CreateConnection();
                dtData = objDAL.ExecutParameterizedQrywithDataTable(SQLQry, Parameters);
                objDAL.CloseConnection();

                if (dtData != null && dtData.Rows.Count > 0)
                {
                    objDAL.CloseConnection();
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool CheckEmailAvailability(UserEntity userEntity)
        {
            try
            {
                string SQLQry = @"select user_id  from user_master where (LOWER(email_id1)=LOWER(@email_id1) or LOWER(email_id2)=LOWER(@email_id2) ) and coalesce(user_master.deleted,false) = false";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@email_id1", CommonUtils.ConvertToString(userEntity.email_id1)));
                Parameters.Add(new NpgsqlParameter("@email_id2", CommonUtils.ConvertToString(userEntity.email_id1)));


                objDAL.CreateConnection();
                dtData = objDAL.ExecutParameterizedQrywithDataTable(SQLQry, Parameters);
                objDAL.CloseConnection();

                if (dtData != null && dtData.Rows.Count > 0)
                {
                    objDAL.CloseConnection();
                    return true;
                }

                return false;
            }

            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }
        public bool CheckMobileAvailability(UserEntity userEntity)
        {
            try
            {
                string SQLQry = @"select user_id  from user_master where mobileno=@mobileno  and coalesce(user_master.deleted,false) = false";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@mobileno", CommonUtils.ConvertToString(userEntity.mobileno)));



                objDAL.CreateConnection();
                dtData = objDAL.ExecutParameterizedQrywithDataTable(SQLQry, Parameters);
                objDAL.CloseConnection();

                if (dtData != null && dtData.Rows.Count > 0)
                {
                    objDAL.CloseConnection();
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
        }

        public bool CheckMobileAvailabilityforupdate(UserEntity userEntity)
        {
            try
            {
                string SQLQry = @"select user_id  from user_master where mobileno=@mobileno and user_id <> @user_id  and coalesce(user_master.deleted,false) = false";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@mobileno", CommonUtils.ConvertToString(userEntity.mobileno)));
                Parameters.Add(new NpgsqlParameter("@user_id", CommonUtils.ConvertToInt64(userEntity.user_id)));



                objDAL.CreateConnection();
                dtData = objDAL.ExecutParameterizedQrywithDataTable(SQLQry, Parameters);
                objDAL.CloseConnection();

                if (dtData != null && dtData.Rows.Count > 0)
                {
                    objDAL.CloseConnection();
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
        }


        public DataSet GetOverallRating(UserEntity userEntity)
        {
            try
            {
                string _strcnt;
                dsData = new DataSet();

                string date = string.Format("{0:yyyy-MM-dd}", userEntity.devicedate);

                string strSqlQry = @" SELECT user_id,user_name,dr_overall_rating.deviceno,round(rating) as rating FROM user_master LEFT OUTER JOIN  dr_overall_rating ON user_master.deviceno = dr_overall_rating.deviceno where  user_master.user_id=" + CommonUtils.ConvertToInt64(userEntity.user_id);

                objDAL.CreateConnection();

                DataTable dtRating = objDAL.FetchRecords(strSqlQry);

                string strsqlsumQry = @" select sum(hardacceleration) as ha ,sum(hardbraking) as hb,sum( cornerpacket) as hc ,round (avg(avgspeed)) as avgspeed ,round( sum(distance),1) as distance,round( sum(distance_day),1) as distance_d,round( sum(distance_night),1) as distance_n,sum( overspeedstarted) as overspeedstarted ,sum(mcall) as mcall  from ( select hardacceleration,hardbraking, cornerpacket,avgspeed ,distance,distance_day,distance_night,overspeedstarted,mcall from rpt_daily_agg_parameter where 
                                   deviceno= " + CommonUtils.ConvertToInt64(userEntity.user_id) +" and distance>0 order by recorddatetime  desc limit 30) as foo";
               DataTable dtmontlysum = objDAL.FetchRecords(strsqlsumQry);

                dtRating.Columns.Add("ha", typeof(int));
                dtRating.Columns.Add("hb", typeof(int));
                dtRating.Columns.Add("hc", typeof(int));
                dtRating.Columns.Add("avg_speed", typeof(decimal));
                dtRating.Columns.Add("distance", typeof(decimal));
                dtRating.Columns.Add("distance_d", typeof(decimal));
                dtRating.Columns.Add("distance_n", typeof(decimal));
                dtRating.Columns.Add("over_speed", typeof(int));
                dtRating.Columns.Add("calls_taken", typeof(int));
               

                //for create row
                if (dtRating.Rows.Count > 0)
                {
                    //---for hardbraking 
                    //string strhardbraking = @"SELECT  coalesce(count(*),0) as hardbraking FROM  iot_hub  where deviceno in(SELECT deviceno FROM user_master where isactive=True and user_id =" + userEntity.user_id + ") and coalesce(hardbraking,0)=1 and  to_char (recorddatetime, 'yyyy-MM-dd') = '" + date + "';";
                    //_strcnt = objDAL.FetchSingleRecord(strhardbraking);
                    dtRating.Rows[0]["hb"] = CommonUtils.ConvertToInt(dtmontlysum.Rows[0]["hb"]);
                    //strhardbraking = null;

                    //---for hardacceleration 
                    //string strhardacceleration = @"SELECT  coalesce(count(*),0) as hardacceleration FROM  iot_hub  where deviceno in(SELECT deviceno FROM user_master where isactive=True and user_id =" + userEntity.user_id + ") and coalesce(hardacceleration,0)=1 and  to_char (recorddatetime, 'yyyy-MM-dd') = '" + date + "';";
                    //_strcnt = objDAL.FetchSingleRecord(strhardacceleration);
                    dtRating.Rows[0]["ha"] = CommonUtils.ConvertToInt(dtmontlysum.Rows[0]["ha"]);
                    //strhardacceleration = null;

                    //---for cornerpacket 
                    //string strcornerpacket = @"SELECT  coalesce(count(*),0) as cornerpacket FROM  iot_hub  where deviceno in(SELECT deviceno FROM user_master where isactive=True and user_id =" + userEntity.user_id + ") and coalesce(cornerpacket,0)=1 and  to_char (recorddatetime, 'yyyy-MM-dd') = '" + date + "';";
                    //_strcnt = objDAL.FetchSingleRecord(strcornerpacket);
                    dtRating.Rows[0]["hc"] = CommonUtils.ConvertToInt(dtmontlysum.Rows[0]["hc"]);
                    //strcornerpacket = null;

                    // for avg_speed
                    //string stravg_speed = @"SELECT  Avg(coalesce(speed,0)) as avgspeed FROM iot_hub where deviceno in(SELECT deviceno FROM user_master where isactive=True and user_id =" + userEntity.user_id + ") and  coalesce(speed,0)>0  and  to_char (recorddatetime, 'yyyy-MM-dd') = '" + date + "';";
                    //_strcnt = objDAL.FetchSingleRecord(stravg_speed);
                    dtRating.Rows[0]["avg_speed"] = CommonUtils.ConvertToDecimal(dtmontlysum.Rows[0]["avgspeed"]);
                    //stravg_speed = null;

                    // for distance
                    //string strdistance = @"SELECT  sum(coalesce(distance,0)) as distance FROM iot_hub where deviceno in(SELECT deviceno FROM user_master where  isactive=True and user_id =" + userEntity.user_id + ") and coalesce(distance,0)>0 and  to_char (recorddatetime, 'yyyy-MM-dd') = '" + date + "';";
                    //_strcnt = objDAL.FetchSingleRecord(strdistance);
                    dtRating.Rows[0]["distance"] = CommonUtils.ConvertToDecimal(dtmontlysum.Rows[0]["distance"]);
                    dtRating.Rows[0]["distance_d"] = CommonUtils.ConvertToDecimal(dtmontlysum.Rows[0]["distance_d"]);
                    dtRating.Rows[0]["distance_n"] = CommonUtils.ConvertToDecimal(dtmontlysum.Rows[0]["distance_n"]);
                    //strdistance = null;

                    //---for overspeed 
                    //string stroverspeedstarted = @"SELECT   coalesce(count(*),0) as overspeedstarted FROM  iot_hub  where deviceno in(SELECT deviceno FROM user_master where isactive=True and user_id =" + userEntity.user_id + ") and coalesce(overspeedstarted,0)=1 and  to_char (recorddatetime, 'yyyy-MM-dd') = '" + date + "';";
                    //_strcnt = objDAL.FetchSingleRecord(stroverspeedstarted);
                    dtRating.Rows[0]["over_speed"] = CommonUtils.ConvertToInt(dtmontlysum.Rows[0]["overspeedstarted"]);
                    //stroverspeedstarted = null;

                    //---for mcall 
                    //string strmcall = @"SELECT   coalesce(count(*),0) as mcall FROM  iot_hub  where deviceno in(SELECT deviceno FROM user_master where isactive=True and user_id =" + userEntity.user_id + ") and coalesce(mcall,0)=1  and  to_char (recorddatetime, 'yyyy-MM-dd') = '" + date + "';";
                    //_strcnt = objDAL.FetchSingleRecord(strmcall);
                    dtRating.Rows[0]["calls_taken"] = CommonUtils.ConvertToInt(dtmontlysum.Rows[0]["mcall"]);
                    //strmcall = null;

                    dtmontlysum = null;

                }

                dsData.Tables.Add(dtRating);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "rating_count";

                strSqlQry = @"select  to_char(recorddate, 'yyyy-MM-dd') as recorddate , round(distance,1) as distance, round(distance_d,1) as distance_d, round(distance_n,1) as distance_n,round(final_score) as rating  FROM public.dr_daily_parameter_wise_rating where deviceno=(select deviceno from user_master where  user_id =" + userEntity.user_id + " ) order by recorddate  desc limit 30";

                DataTable dtmonthlyRating = objDAL.FetchRecords(strSqlQry);
                dsData.Tables.Add(dtmonthlyRating);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "dailyrating";

                strSqlQry = @"select to_char(recorddatetime, 'yyyy-MM-dd') as recorddate,hardacceleration,hardacceleration_day,hardacceleration_night,hardbraking,hardbraking_day,hardbraking_night, cornerpacket,
                                    cornerpacket_day,cornerpacket_night,overspeedstarted,overspeedstarted_day,overspeedstarted_night,mcall,mcall_day,mcall_night from rpt_daily_agg_parameter where 
                                   deviceno= " + CommonUtils.ConvertToInt64(userEntity.user_id) + " and distance>0 order by recorddatetime  desc limit 30";

                DataTable dtHarshEvent = objDAL.FetchRecords(strSqlQry);
                dsData.Tables.Add(dtHarshEvent);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "harshevent";



                objDAL.CloseConnection();
                return dsData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objResponseEntity = null;
                
            }
        }

        public bool UserlougOutLog(UserEntity userEntity)
        {
            try
            {
                objDAL.CreateConnection();

                _user_id = CommonUtils.ConvertToInt(userEntity.user_id);

                strSqlQry = @"INSERT INTO user_login_log(user_id, logindatetime, action)
	                       VALUES (@user_id, @logindatetime, @action)";
                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@user_id", _user_id));
                Parameters.Add(new NpgsqlParameter("@logindatetime", CommonUtils.ConvertToDateTime(DateTime.Now)));
                Parameters.Add(new NpgsqlParameter("@action", "lougout"));
                objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                objDAL.CloseConnection();

                return true;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool ForgotPassword_email(UserEntity userEntity)
        {
            try
            {
                string strSqlQry = @"SELECT a.user_name as username, b.user_pwd as password FROM user_master a
                INNER JOIN  user_pwd b ON a.user_id = b.user_id  where (LOWER(a.email_id1)=LOWER(@email_id1) or LOWER(a.email_id2)=LOWER(@email_id2)) and coalesce(a.deleted,false) = false";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@email_id1", CommonUtils.ConvertToString(userEntity.email_id1)));
                Parameters.Add(new NpgsqlParameter("@email_id2", CommonUtils.ConvertToString(userEntity.email_id1)));

                objDAL.CreateConnection();
                dtData = objDAL.ExecutParameterizedQrywithDataTable(strSqlQry, Parameters);
                objDAL.CloseConnection();


                StreamReader reader = new StreamReader(System.Configuration.ConfigurationManager.AppSettings["mailpath"]);
                string readFile = reader.ReadToEnd();
                string StrContent = "";
                StrContent = readFile;
                StrContent = StrContent.Replace("[uname]", " Username =" + dtData.Rows[0]["username"] + "");
                StrContent = StrContent.Replace("[password]", "Password=" + dtData.Rows[0]["password"] + "");
                MailMessage msg = new MailMessage();
                string mailfrom = System.Configuration.ConfigurationManager.AppSettings["Mailfrom"];
                msg.From = new MailAddress(mailfrom, mailfrom);
                msg.To.Add(userEntity.email_id1);
                msg.Subject = "Your Password Has Been Changed!";
                msg.Body = StrContent;
                msg.IsBodyHtml = true;
                msg.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                msg.Priority = MailPriority.High;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = System.Configuration.ConfigurationManager.AppSettings["Hostname"];
                //smtp.EnableSsl = true;
                smtp.Port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Portno"]);
                smtp.UseDefaultCredentials = false;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["smtpusername"], System.Configuration.ConfigurationManager.AppSettings["smtppassword"]);
                try
                {
                    smtp.Send(msg);
                    return true;
                }
                catch
                {
                    return false;
                }

                //return dtUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objResponseEntity = null;
            }
        }


        public UserEntity CheckSocialUserAvailability(UserEntity userEntity)
        {
            try
            {
                string SQLQry = @"SELECT user_id, role_id, user_name, first_name, middle_name, last_name,to_char( date_of_birth, 'yyyy-MM-dd') as  date_of_birth, gender, address1, address2, address3, mobileno, email_id1, email_id2, profile_picture_uri, deviceno, vehicle_no, license_no,to_char( license_expiry_date, 'yyyy-MM-dd HH:mm:ss') as   license_expiry_date, registration_no, vehicle_manufacturer, vehicle_color, vehicle_type, user_login_type, insurance_mapping_key, o_id,to_char( created_at, 'yyyy-MM-dd HH:mm:ss') as   created_at, to_char( update_at, 'yyyy-MM-dd HH:mm:ss') as   update_at , deleted, isactive, social_id
	           FROM public.user_master where user_name=@user_name and user_login_type=@user_login_type and (coalesce(user_master.deleted,false) = false )";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@user_name", CommonUtils.ConvertToString(userEntity.user_name)));
                Parameters.Add(new NpgsqlParameter("@user_login_type", CommonUtils.ConvertToString(userEntity.user_login_type)));

                objDAL.CreateConnection();
                dtData = objDAL.ExecutParameterizedQrywithDataTable(SQLQry, Parameters);
            

                if (dtData != null && dtData.Rows.Count > 0)
                {

                    userEntity.city = CommonUtils.ConvertToString(dtData.Rows[0]["social_id"]);
                    userEntity.user_id = CommonUtils.ConvertToInt(dtData.Rows[0]["user_id"]);
                    userEntity.role_Id = CommonUtils.ConvertToInt(dtData.Rows[0]["role_Id"]);
                    userEntity.user_name = CommonUtils.ConvertToString(dtData.Rows[0]["user_name"]);
                    //  userEntity.user_role = CommonUtils.ConvertToString(dtData.Rows[0]["user_role"]);
                    userEntity.first_name = CommonUtils.ConvertToString(dtData.Rows[0]["first_name"]);
                    userEntity.middle_name = CommonUtils.ConvertToString(dtData.Rows[0]["middle_name"]);
                    userEntity.last_name = CommonUtils.ConvertToString(dtData.Rows[0]["last_name"]);
                    userEntity.date_of_birth = CommonUtils.ConvertToDateTime(dtData.Rows[0]["date_of_birth"]);
                    userEntity.gender = CommonUtils.ConvertToString(dtData.Rows[0]["gender"]);
                    userEntity.address1 = CommonUtils.ConvertToString(dtData.Rows[0]["address1"]);
                    userEntity.address2 = CommonUtils.ConvertToString(dtData.Rows[0]["address2"]);
                    userEntity.address3 = CommonUtils.ConvertToString(dtData.Rows[0]["address3"]);
                    userEntity.mobileno = CommonUtils.ConvertToString(dtData.Rows[0]["mobileno"]);
                    userEntity.email_id1 = CommonUtils.ConvertToString(dtData.Rows[0]["email_id1"]);
                    userEntity.email_id2 = CommonUtils.ConvertToString(dtData.Rows[0]["email_id2"]);
                    if ((dtData.Rows[0]["social_id"] == null || dtData.Rows[0]["social_id"] == ""))
                    {
                        userEntity.profile_picture_uri = CommonUtils.getUrlPath(CommonUtils.ConvertToString(dtData.Rows[0]["profile_picture_uri"]), "userprofile", out Url);
                    }
                    else
                    {
                        userEntity.profile_picture_uri = CommonUtils.ConvertToString(dtData.Rows[0]["profile_picture_uri"]);
                    }
                    //userEntity.profile_picture_uri = CommonUtils.ConvertToString(dtData.Rows[0]["profile_picture_uri"]);
                    userEntity.deviceno = CommonUtils.ConvertToInt(dtData.Rows[0]["deviceno"]);
                    userEntity.vehicle_no = CommonUtils.ConvertToString(dtData.Rows[0]["vehicle_no"]);
                    userEntity.license_no = CommonUtils.ConvertToString(dtData.Rows[0]["license_no"]);
                    userEntity.license_expiry_date = CommonUtils.ConvertToDateTime(dtData.Rows[0]["license_expiry_date"]);
                    userEntity.registration_no = CommonUtils.ConvertToString(dtData.Rows[0]["registration_no"]);
                    userEntity.vehicle_manufacturer = CommonUtils.ConvertToString(dtData.Rows[0]["vehicle_manufacturer"]);
                    userEntity.vehicle_color = CommonUtils.ConvertToString(dtData.Rows[0]["vehicle_color"]);
                    userEntity.vehicle_type = CommonUtils.ConvertToString(dtData.Rows[0]["vehicle_type"]);
                    userEntity.user_login_type = CommonUtils.ConvertToString(dtData.Rows[0]["user_login_type"]);
                    userEntity.insurance_mapping_key = CommonUtils.ConvertToString(dtData.Rows[0]["insurance_mapping_key"]);
                    userEntity.o_id = CommonUtils.ConvertToInt(dtData.Rows[0]["o_id"]);
                    userEntity.created_at = CommonUtils.ConvertToDateTime(dtData.Rows[0]["created_at"]);
                    userEntity.update_at = CommonUtils.ConvertToDateTime(dtData.Rows[0]["update_at"]);

                    // Insert or update Device Info for per login as per swapnil sir 
                    updateDeviceInfo(CommonUtils.ConvertToInt(dtData.Rows[0]["user_id"]), userEntity);
                    // ---
                }
                else
                {
                    userEntity = null;
                }

                objDAL.CloseConnection();
                return userEntity;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }


        public bool updateDeviceInfo(Int64 _uid, UserEntity userEntity)
        {
            //-------------store device info as per Swapnil sir requirement
            
                try
                {
                      if (userEntity != null && userEntity.userDeviceInfoEntities != null)
                     {

                        string SQLQry = @" update user_device_info set isactive=false where  user_id=@user_id;";
                        Parameters = new List<NpgsqlParameter>();
                        Parameters.Add(new NpgsqlParameter("@user_id", _uid));
                        objDAL.ExecutParameterizedQry(SQLQry, Parameters);

                            foreach (UserDeviceInfoEntity UserDeviceInfoEntity in userEntity.userDeviceInfoEntities)
                            {
                                SQLQry = @"INSERT INTO public.user_device_info(device_number, device_type, device_name, device_status, os_version, hw_id, user_token, user_id, created_at, isactive)
	                                             VALUES (@device_number, @device_type, @device_name, @device_status, @os_version, @hw_id, @user_token, @user_id, @created_at, @isactive);";
                                Parameters = new List<NpgsqlParameter>();
                                Parameters.Add(new NpgsqlParameter("@device_number", CommonUtils.ConvertToString(UserDeviceInfoEntity.device_number)));
                                Parameters.Add(new NpgsqlParameter("@device_type", CommonUtils.ConvertToString(UserDeviceInfoEntity.device_type)));
                                Parameters.Add(new NpgsqlParameter("@device_name", CommonUtils.ConvertToString(UserDeviceInfoEntity.device_name)));
                                Parameters.Add(new NpgsqlParameter("@device_status", CommonUtils.ConvertToString(UserDeviceInfoEntity.device_status)));
                                Parameters.Add(new NpgsqlParameter("@os_version", CommonUtils.ConvertToString(UserDeviceInfoEntity.os_version)));
                                Parameters.Add(new NpgsqlParameter("@hw_id", CommonUtils.ConvertToString(UserDeviceInfoEntity.hw_id)));
                                Parameters.Add(new NpgsqlParameter("@user_token", CommonUtils.ConvertToString(UserDeviceInfoEntity.user_token)));
                                Parameters.Add(new NpgsqlParameter("@user_id", _uid));
                                Parameters.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                                Parameters.Add(new NpgsqlParameter("@isactive", true));
                                objDAL.ExecutParameterizedQry(SQLQry, Parameters);
                            }
                     }
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                    throw ex;
                }
            
        }

        public bool CheckEmailMobileAvailablity(UserEntity userEntity)
        {
            try
            {
                string SQLQry = @"select user_id  from user_master where  LOWER(email_id1)= LOWER(@email_id1) and mobileno=@mobileno and  (coalesce(user_master.deleted,false) = false )  ";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@email_id1", CommonUtils.ConvertToString(userEntity.email_id1)));
                Parameters.Add(new NpgsqlParameter("@mobileno", CommonUtils.ConvertToString(userEntity.mobileno)));


                objDAL.CreateConnection();
                dtData = objDAL.ExecutParameterizedQrywithDataTable(SQLQry, Parameters);
                objDAL.CloseConnection();

                if (dtData != null && dtData.Rows.Count > 0)
                {
                    objDAL.CloseConnection();
                    return true;
                }

                return false;
            }

            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
        }

        public bool UpdatePremiumData(UserEntity userEntity)
        {
            try
            {
                objDAL.CreateConnection();
                _user_id = CommonUtils.ConvertToInt(userEntity.user_id);

                strSqlQry = @"UPDATE user_master  SET  transaction_id=@transaction_id,premium=@premium,premium_validity=@premium_validity  WHERE (user_id = @user_id)";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@user_id", _user_id));
                Parameters.Add(new NpgsqlParameter("@transaction_id", CommonUtils.ConvertToString(userEntity.transaction_id)));
                Parameters.Add(new NpgsqlParameter("@premium", CommonUtils.ConvertToBoolean(userEntity.premium)));
                Parameters.Add(new NpgsqlParameter("@premium_validity", CommonUtils.ConvertToDateTime(userEntity.premium_validity)));

                blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                objDAL.CloseConnection();

                return blnResult;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

    }
}
