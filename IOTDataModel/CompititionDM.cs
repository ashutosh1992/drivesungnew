﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Npgsql;
using IOTBusinessEntities;
using IOTUtilities;
using HashAndSalt;

namespace IOTDataModel
{
    public class CompititionDM : IDisposable
    {
        private BaseDAL objDAL;
        private List<NpgsqlParameter> Parameters;
        private List<NpgsqlParameter> Parameters1;
        private List<NpgsqlParameter> Parameters2;
        private List<NpgsqlParameter> Parameters3;
        private String strSqlQry;
        private String strSqlQry1;
        private String strSqlQry2;
        private Boolean blnResult;
        private string objResponseEntity;
        private Int32 _prize_id1;
        private Int32 _prize_id2;
        private Int32 _prize_id3;
        private Int32 _cmpt_id;
        private object objcmpt_id;
        private object objoint;
        private DataTable dtcompitition;
        private DataTable dtcompitition1;
        private DataTable dtcompitition2;
        private DataSet dscompitition;
        //  private Int32 _device_no;
        // private String strMaxQry;

        public CompititionDM()
        {
            objDAL = new BaseDAL();

        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        string Url = "";
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing)
            {
                // Free any other managed objects here.
                objDAL.Dispose();
                Parameters = null;
                strSqlQry = null;
                strSqlQry1 = null;
                strSqlQry2 = null;
                Parameters1 = null;
                Parameters2 = null;
                Parameters3 = null;
                dtcompitition = null;
                dtcompitition1 = null;
                dtcompitition2 = null;
                dscompitition = null;

                //  strMaxQry = null;
            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~CompititionDM()
        {
            Dispose(false);
        }

        #endregion

        public DataTable CreateCompitition(CompitionEntity compititionEntity)
        {
            try
            {
                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                if (compititionEntity.s_date != null && compititionEntity.e_date != null)
                {
                    compititionEntity.start_date = CommonUtils.ConvertToDateTime(CommonUtils.GetstringFromatyyyy_MM_dd(compititionEntity.s_date));
                    compititionEntity.end_date = CommonUtils.ConvertToDateTime(CommonUtils.GetstringFromatyyyy_MM_dd(compititionEntity.e_date));
                }

                strSqlQry = @"INSERT INTO public.league_prize_master(prize_desc, created_by, created_at)
	                              VALUES (@prize_desc, @created_by, @created_at)RETURNING prize_id";

                Parameters1 = new List<NpgsqlParameter>();
                Parameters1.Add(new NpgsqlParameter("@prize_desc", CommonUtils.ConvertToString(compititionEntity.prize1_desc)));
                Parameters1.Add(new NpgsqlParameter("@created_by", CommonUtils.ConvertToInt(compititionEntity.created_by)));
                Parameters1.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));

                Parameters2 = new List<NpgsqlParameter>();
                Parameters2.Add(new NpgsqlParameter("@prize_desc", CommonUtils.ConvertToString(compititionEntity.prize2_desc)));
                Parameters2.Add(new NpgsqlParameter("@created_by", CommonUtils.ConvertToInt(compititionEntity.created_by)));
                Parameters2.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));

                Parameters3 = new List<NpgsqlParameter>();
                Parameters3.Add(new NpgsqlParameter("@prize_desc", CommonUtils.ConvertToString(compititionEntity.prize3_desc)));
                Parameters3.Add(new NpgsqlParameter("@created_by", CommonUtils.ConvertToInt(compititionEntity.created_by)));
                Parameters3.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));

                object objprize_id1 = objDAL.ExecutParameterizedQryAndGetID(strSqlQry, Parameters1);
                object objprize_id2 = objDAL.ExecutParameterizedQryAndGetID(strSqlQry, Parameters2);
                object objprize_id3 = objDAL.ExecutParameterizedQryAndGetID(strSqlQry, Parameters3);

                if (objprize_id1 != null && objprize_id2 != null && objprize_id3 != null)
                {
                    _prize_id1 = CommonUtils.ConvertToInt(objprize_id1.ToString());
                    _prize_id2 = CommonUtils.ConvertToInt(objprize_id2.ToString());
                    _prize_id3 = CommonUtils.ConvertToInt(objprize_id3.ToString());


                    strSqlQry = @"INSERT INTO public.league_cmpt_master(cmpt_name, cmpt_type, start_date, end_date, terms_conditions, min_km_travel, cmpt_for_driving, vehicle_manufacturer,ec_agefrom,ec_ageto,gender,authentication_required, prize_id1,prize_id2,prize_id3, car_type, cmpt_country, cmpt_uniquecode, created_at, created_by, isactive,total_km_travel,cmpt_code,isclose_cmpt )
	                                  VALUES (@cmpt_name, @cmpt_type, @start_date, @end_date, @terms_conditions, @min_km_travel, @cmpt_for_driving, @vehicle_manufacturer,@ec_agefrom,@ec_ageto,@gender,@authentication_required,@prize_id1,@prize_id2,@prize_id3, @car_type, @cmpt_country, @cmpt_uniquecode, @created_at, @created_by, @isactive,@total_km_travel,@cmpt_code,@isclose_cmpt)RETURNING  cmpt_id ";
                    Parameters = new List<NpgsqlParameter>();
                    Parameters.Add(new NpgsqlParameter("@prize_id1", _prize_id1));
                    Parameters.Add(new NpgsqlParameter("@prize_id2", _prize_id2));
                    Parameters.Add(new NpgsqlParameter("@prize_id3", _prize_id3));
                    Parameters.Add(new NpgsqlParameter("@cmpt_name", CommonUtils.ConvertToString(compititionEntity.cmpt_name)));
                    Parameters.Add(new NpgsqlParameter("@cmpt_type", CommonUtils.ConvertToString(compititionEntity.cmpt_type)));
                    Parameters.Add(new NpgsqlParameter("@start_date", CommonUtils.ConvertToDateTime(compititionEntity.start_date)));
                    Parameters.Add(new NpgsqlParameter("@end_date", CommonUtils.ConvertToDateTime(compititionEntity.end_date)));
                    Parameters.Add(new NpgsqlParameter("@terms_conditions", CommonUtils.ConvertToString(compititionEntity.terms_conditions)));
                    Parameters.Add(new NpgsqlParameter("@min_km_travel", CommonUtils.ConvertToInt(compititionEntity.min_km_travel)));
                    Parameters.Add(new NpgsqlParameter("@total_km_travel", CommonUtils.ConvertToInt(compititionEntity.total_km_travel)));
                    Parameters.Add(new NpgsqlParameter("@cmpt_for_driving", CommonUtils.ConvertToString(compititionEntity.cmpt_for_driving)));
                    Parameters.Add(new NpgsqlParameter("@vehicle_manufacturer", CommonUtils.ConvertToString(compititionEntity.vehicle_manufacturer)));
                    Parameters.Add(new NpgsqlParameter("@ec_agefrom", CommonUtils.ConvertToDecimal(compititionEntity.ec_agefrom)));
                    Parameters.Add(new NpgsqlParameter("@ec_ageto", CommonUtils.ConvertToDecimal(compititionEntity.ec_ageto)));
                    Parameters.Add(new NpgsqlParameter("@gender", CommonUtils.ConvertToString(compititionEntity.gender)));
                    Parameters.Add(new NpgsqlParameter("@authentication_required", CommonUtils.ConvertToBoolean(compititionEntity.authentication_required)));
                    Parameters.Add(new NpgsqlParameter("@car_type", CommonUtils.ConvertToString(compititionEntity.car_type)));
                    Parameters.Add(new NpgsqlParameter("@cmpt_country", CommonUtils.ConvertToString(compititionEntity.cmpt_country)));
                    Parameters.Add(new NpgsqlParameter("@cmpt_uniquecode", CommonUtils.ConvertToString(compititionEntity.cmpt_uniquecode)));
                    Parameters.Add(new NpgsqlParameter("@created_by", CommonUtils.ConvertToInt(compititionEntity.s_user_id)));
                    Parameters.Add(new NpgsqlParameter("@cmpt_code", CommonUtils.ConvertToString(compititionEntity.cmpt_code)));
                    Parameters.Add(new NpgsqlParameter("@isclose_cmpt", CommonUtils.ConvertToBoolean(compititionEntity.isclose_cmpt)));
                    Parameters.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                    Parameters.Add(new NpgsqlParameter("@isactive", true));

                    objcmpt_id = objDAL.ExecutParameterizedQryAndGetID(strSqlQry, Parameters);
                }

                if (objcmpt_id != null)
                {
                    if (compititionEntity.s_role_Id != 2)
                    {
                        _cmpt_id = CommonUtils.ConvertToInt(objcmpt_id.ToString());

                        strSqlQry = @"INSERT INTO public.league_cmpt_user_join_details(cmpt_id, user_id,  join_at) VALUES (@cmpt_id, @user_id, @join_at);";

                        Parameters = new List<NpgsqlParameter>();
                        Parameters.Add(new NpgsqlParameter("@cmpt_id", _cmpt_id));
                        Parameters.Add(new NpgsqlParameter("@user_id", CommonUtils.ConvertToInt(compititionEntity.s_user_id)));
                        if (CommonUtils.ConvertToDateTime(compititionEntity.start_date) == null)
                        {
                            Parameters.Add(new NpgsqlParameter("@join_at", DBNull.Value));
                        }
                        else
                        {
                            Parameters.Add(new NpgsqlParameter("@join_at", CommonUtils.ConvertToDateTime(compititionEntity.start_date)));
                        }
                        objDAL.CreateConnection();
                        objoint = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);
                    }
                }
                if (objcmpt_id != null)
                {
                    _cmpt_id = CommonUtils.ConvertToInt(objcmpt_id.ToString());

                    strSqlQry = @"SELECT league_cmpt_master.cmpt_id, league_cmpt_master.cmpt_name, league_cmpt_master.cmpt_profile_picture_path, league_cmpt_master.cmpt_type, 
                          to_char( league_cmpt_master.start_date, 'yyyy-MM-dd HH24:MI:SS') as start_date,  to_char(league_cmpt_master.end_date, 'yyyy-MM-dd HH24:MI:SS') as end_date, league_cmpt_master.terms_conditions, league_cmpt_master.min_km_travel, 
                         league_cmpt_master.cmpt_for_driving, league_cmpt_master.vehicle_manufacturer, league_cmpt_master.ec_agefrom, league_cmpt_master.ec_ageto, 
                         league_cmpt_master.gender, league_cmpt_master.authentication_required, league_cmpt_master.prize_id1, league_cmpt_master.prize_id2, 
                         league_cmpt_master.prize_id3, league_cmpt_master.car_type, league_cmpt_master.cmpt_country, league_cmpt_master.cmpt_uniquecode, 
                         to_char( league_cmpt_master.created_at, 'yyyy-MM-dd HH24:MI:SS') as created_at , league_cmpt_master.created_by, league_cmpt_master.isactive, league_cmpt_master.deleted, 
                         league_prize_master.prize_code, league_prize_master.prize_desc as prize_desc1 , league_prize_master_1.prize_code AS prize_code2, 
                         league_prize_master_1.prize_desc AS prize_desc2, league_prize_master_2.prize_code AS prize_code3, league_prize_master_2.prize_desc AS prize_desc3,league_cmpt_master.total_km_travel,league_cmpt_master.cmpt_code,coalesce(league_cmpt_master.isclose_cmpt,false) as isclose_cmpt,user_master.first_name, user_master.middle_name, user_master.last_name
                        FROM            league_cmpt_master LEFT OUTER JOIN
                         league_prize_master ON league_cmpt_master.prize_id1 = league_prize_master.prize_id LEFT OUTER JOIN
                         league_prize_master AS league_prize_master_1 ON league_cmpt_master.prize_id2 = league_prize_master_1.prize_id LEFT OUTER JOIN
                         league_prize_master AS league_prize_master_2 ON league_cmpt_master.prize_id3 = league_prize_master_2.prize_id  inner join user_master on user_master.user_id = league_cmpt_master.created_by   where  cmpt_id=" + _cmpt_id;

                    objDAL.CreateConnection();
                    dtcompitition = objDAL.FetchRecords(strSqlQry);

                }


                objDAL.CommitTransaction();
                objDAL.CloseConnection();

                return dtcompitition;

            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public DataSet viewcmpt(CompitionEntity compititionEntity)
        {
            try
            {
                string strCmptUrlPath = System.Configuration.ConfigurationManager.AppSettings["URL"] + "/compitition/";
                string strProfUrlPath = System.Configuration.ConfigurationManager.AppSettings["URL"] + "/userprofile/";

                strSqlQry1 = "SELECT league_cmpt_master.cmpt_id, league_cmpt_master.cmpt_name, '" + strCmptUrlPath + "'||  league_cmpt_master.cmpt_profile_picture_path AS cmpt_profile_picture_path, league_cmpt_master.cmpt_type, ";
                strSqlQry1 = strSqlQry1 + @"to_char( league_cmpt_master.start_date, 'yyyy-MM-dd HH24:MI:SS') as start_date,  to_char(league_cmpt_master.end_date, 'yyyy-MM-dd HH24:MI:SS') as end_date, league_cmpt_master.terms_conditions, league_cmpt_master.min_km_travel, 
                         league_cmpt_master.cmpt_for_driving, league_cmpt_master.vehicle_manufacturer, league_cmpt_master.ec_agefrom, league_cmpt_master.ec_ageto, 
                         league_cmpt_master.gender, league_cmpt_master.authentication_required, league_cmpt_master.prize_id1, league_cmpt_master.prize_id2, 
                         league_cmpt_master.prize_id3, league_cmpt_master.car_type, league_cmpt_master.cmpt_country, league_cmpt_master.cmpt_uniquecode, 
                         to_char( league_cmpt_master.created_at, 'yyyy-MM-dd HH24:MI:SS') as created_at , league_cmpt_master.created_by, league_cmpt_master.isactive, league_cmpt_master.deleted, 
                         league_prize_master.prize_code, league_prize_master.prize_desc as prize_desc1 , league_prize_master_1.prize_code AS prize_code2, 
                         league_prize_master_1.prize_desc AS prize_desc2, league_prize_master_2.prize_code AS prize_code3, league_prize_master_2.prize_desc AS prize_desc3,league_cmpt_master.total_km_travel,user_master.first_name, user_master.middle_name, user_master.last_name,'" + strProfUrlPath + "'|| user_master.profile_picture_uri AS profile_picture_uri , ";
                strSqlQry1 = strSqlQry1 + @" (SELECT  count(cmpt_id) FROM league_cmpt_user_join_details  where  league_cmpt_user_join_details.cmpt_id =league_cmpt_master.cmpt_id) AS no_users 
                              FROM   league_cmpt_master LEFT OUTER JOIN
                                 league_prize_master ON league_cmpt_master.prize_id1 = league_prize_master.prize_id LEFT OUTER JOIN
                                 league_prize_master AS league_prize_master_1 ON league_cmpt_master.prize_id2 = league_prize_master_1.prize_id LEFT OUTER JOIN
                                 league_prize_master AS league_prize_master_2 ON league_cmpt_master.prize_id3 = league_prize_master_2.prize_id  inner join user_master on user_master.user_id = league_cmpt_master.created_by inner join league_cmpt_user_org_mapping on league_cmpt_user_org_mapping.cmpt_id= league_cmpt_master.cmpt_id and league_cmpt_user_org_mapping.user_id=" + compititionEntity.s_user_id + "where league_cmpt_master.cmpt_id not in(SELECT cmpt_id FROM public.league_cmpt_user_join_details where user_id=" + compititionEntity.s_user_id + " )and league_cmpt_master.isactive=true and cmpt_type = 'private' order by cmpt_id desc";


                strSqlQry2 = "SELECT league_cmpt_master.cmpt_id, league_cmpt_master.cmpt_name, '" + strCmptUrlPath + "'||  league_cmpt_master.cmpt_profile_picture_path AS cmpt_profile_picture_path, league_cmpt_master.cmpt_type, ";
                strSqlQry2 = strSqlQry2 + @"to_char( league_cmpt_master.start_date, 'yyyy-MM-dd HH24:MI:SS') as start_date,  to_char(league_cmpt_master.end_date, 'yyyy-MM-dd HH24:MI:SS') as end_date, league_cmpt_master.terms_conditions, league_cmpt_master.min_km_travel, 
                         league_cmpt_master.cmpt_for_driving, league_cmpt_master.vehicle_manufacturer, league_cmpt_master.ec_agefrom, league_cmpt_master.ec_ageto, 
                         league_cmpt_master.gender, league_cmpt_master.authentication_required, league_cmpt_master.prize_id1, league_cmpt_master.prize_id2, 
                         league_cmpt_master.prize_id3, league_cmpt_master.car_type, league_cmpt_master.cmpt_country, league_cmpt_master.cmpt_uniquecode, 
                         to_char( league_cmpt_master.created_at, 'yyyy-MM-dd HH24:MI:SS') as created_at , league_cmpt_master.created_by, league_cmpt_master.isactive, league_cmpt_master.deleted, 
                         league_prize_master.prize_code, league_prize_master.prize_desc as prize_desc1 , league_prize_master_1.prize_code AS prize_code2, 
                         league_prize_master_1.prize_desc AS prize_desc2, league_prize_master_2.prize_code AS prize_code3, league_prize_master_2.prize_desc AS prize_desc3,league_cmpt_master.total_km_travel,league_cmpt_master.cmpt_code,coalesce(league_cmpt_master.isclose_cmpt,false) as isclose_cmpt,user_master.first_name, user_master.middle_name, user_master.last_name,'" + strProfUrlPath + "'|| user_master.profile_picture_uri AS profile_picture_uri , ";
                strSqlQry2 = strSqlQry2 + @" (SELECT  count(cmpt_id) FROM league_cmpt_user_join_details  where  league_cmpt_user_join_details.cmpt_id =league_cmpt_master.cmpt_id) AS no_users 
                              FROM   league_cmpt_master LEFT OUTER JOIN
                                 league_prize_master ON league_cmpt_master.prize_id1 = league_prize_master.prize_id LEFT OUTER JOIN
                                 league_prize_master AS league_prize_master_1 ON league_cmpt_master.prize_id2 = league_prize_master_1.prize_id LEFT OUTER JOIN
                                 league_prize_master AS league_prize_master_2 ON league_cmpt_master.prize_id3 = league_prize_master_2.prize_id  inner join user_master on user_master.user_id = league_cmpt_master.created_by  where  cmpt_id not in(SELECT cmpt_id
	                          FROM public.league_cmpt_user_join_details where user_id=" + compititionEntity.s_user_id + " )and league_cmpt_master.isactive=true and cmpt_type = 'public' order by cmpt_id desc";
          
                dscompitition = new DataSet();
                DataTable dtData;
             
                objDAL.CreateConnection();

                dtData = objDAL.FetchRecords(strSqlQry1.ToString());
                //dtData.Columns.Add("no_users");
                dscompitition.Tables.Add(dtData);
                dscompitition.Tables[dscompitition.Tables.Count - 1].TableName = "private";


                //foreach (DataRow Userrow in dscompitition.Tables["private"].Rows)
                //{
                //    if (Userrow["cmpt_profile_picture_path"] != null)
                //    {
                //        string strUrlPath = CommonUtils.getUrlPath(Userrow["cmpt_profile_picture_path"].ToString(), "compitition", out Url);
                //        Userrow["cmpt_profile_picture_path"] = Url;
                //    }
                //    if (Userrow["profile_picture_uri"] != null)
                //    {
                //        string strUrlPath = CommonUtils.getUrlPath(Userrow["profile_picture_uri"].ToString(), "userprofile", out Url);
                //        Userrow["profile_picture_uri"] = Url;
                //    }
                //    //cntcmpt = objDAL.FetchSingleRecord(@"SELECT  count( cmpt_id) FROM public.league_cmpt_user_join_details where cmpt_id=" + Userrow["cmpt_id"]);
                //    // Userrow["no_users"] = cntcmpt;
                //}

                dtData = objDAL.FetchRecords(strSqlQry2.ToString());
                dscompitition.Tables.Add(dtData);
                //     dtData.Columns.Add("no_users");
                dscompitition.Tables[dscompitition.Tables.Count - 1].TableName = "public";
                //foreach (DataRow Userrow in dscompitition.Tables["public"].Rows)
                //{
                //    if (Userrow["cmpt_profile_picture_path"] != null)
                //    {
                //        string strUrlPath = CommonUtils.getUrlPath(Userrow["cmpt_profile_picture_path"].ToString(), "compitition", out Url);
                //        Userrow["cmpt_profile_picture_path"] = Url;
                //    }
                //    if (Userrow["profile_picture_uri"] != null)
                //    {
                //        string strUrlPath = CommonUtils.getUrlPath(Userrow["profile_picture_uri"].ToString(), "userprofile", out Url);
                //        Userrow["profile_picture_uri"] = Url;
                //    }
                //    //cntcmpt = objDAL.FetchSingleRecord(@"SELECT  count( cmpt_id) FROM public.league_cmpt_user_join_details where cmpt_id=" + Userrow["cmpt_id"]);
                //    //Userrow["no_users"] = cntcmpt;
                //}

                objDAL.CloseConnection();

                return dscompitition;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
            finally
            {
                objResponseEntity = null;
            }
        }

        public DataSet competitionlist(CompitionEntity compititionEntity)
        {
            try
            {
                string strCmptUrlPath = System.Configuration.ConfigurationManager.AppSettings["URL"] + "/compitition/";
                string strProfUrlPath = System.Configuration.ConfigurationManager.AppSettings["URL"] + "/userprofile/";
                strSqlQry1 = "SELECT league_cmpt_master.cmpt_id, league_cmpt_master.cmpt_name, '" + strCmptUrlPath + "'||  league_cmpt_master.cmpt_profile_picture_path AS cmpt_profile_picture_path, league_cmpt_master.cmpt_type, ";
                strSqlQry1 = strSqlQry1 + @"to_char( league_cmpt_master.start_date, 'yyyy-MM-dd HH24:MI:SS') as start_date,  to_char(league_cmpt_master.end_date, 'yyyy-MM-dd HH24:MI:SS') as end_date, league_cmpt_master.terms_conditions, league_cmpt_master.min_km_travel, 
                         league_cmpt_master.cmpt_for_driving, league_cmpt_master.vehicle_manufacturer, league_cmpt_master.ec_agefrom, league_cmpt_master.ec_ageto, 
                         league_cmpt_master.gender, league_cmpt_master.authentication_required, league_cmpt_master.prize_id1, league_cmpt_master.prize_id2, 
                         league_cmpt_master.prize_id3, league_cmpt_master.car_type, league_cmpt_master.cmpt_country, league_cmpt_master.cmpt_uniquecode, 
                         to_char( league_cmpt_master.created_at, 'yyyy-MM-dd HH24:MI:SS') as created_at , league_cmpt_master.created_by, league_cmpt_master.isactive, league_cmpt_master.deleted, 
                         league_prize_master.prize_code, league_prize_master.prize_desc as prize_desc1 , league_prize_master_1.prize_code AS prize_code2, 
                         league_prize_master_1.prize_desc AS prize_desc2, league_prize_master_2.prize_code AS prize_code3, league_prize_master_2.prize_desc AS prize_desc3,league_cmpt_master.total_km_travel,user_master.first_name, user_master.middle_name, user_master.last_name,'" + strProfUrlPath + "'|| user_master.profile_picture_uri AS profile_picture_uri , ";
                strSqlQry1 = strSqlQry1 + @" (SELECT  count(cmpt_id) FROM league_cmpt_user_join_details  where  league_cmpt_user_join_details.cmpt_id =league_cmpt_master.cmpt_id) AS no_users 
                              FROM            league_cmpt_master LEFT OUTER JOIN
                         league_prize_master ON league_cmpt_master.prize_id1 = league_prize_master.prize_id LEFT OUTER JOIN
                         league_prize_master AS league_prize_master_1 ON league_cmpt_master.prize_id2 = league_prize_master_1.prize_id LEFT OUTER JOIN
                         league_prize_master AS league_prize_master_2 ON league_cmpt_master.prize_id3 = league_prize_master_2.prize_id  inner join user_master on user_master.user_id = league_cmpt_master.created_by  where league_cmpt_master.isactive= true order by cmpt_id desc ";



                dscompitition = new DataSet();
                DataTable dtData;
                string cntcmpt = "";

                objDAL.CreateConnection();

                dtData = objDAL.FetchRecords(strSqlQry1.ToString());
                //dtData.Columns.Add("no_users");
                dscompitition.Tables.Add(dtData);
                dscompitition.Tables[dscompitition.Tables.Count - 1].TableName = "all";


                //foreach (DataRow Userrow in dscompitition.Tables["all"].Rows)
                //{
                //    if (Userrow["cmpt_profile_picture_path"] != null)
                //    {
                //        string strUrlPath = CommonUtils.getUrlPath(Userrow["cmpt_profile_picture_path"].ToString(), "compitition", out Url);
                //        Userrow["cmpt_profile_picture_path"] = Url;
                //    }
                //    cntcmpt = objDAL.FetchSingleRecord(@"SELECT  count( cmpt_id) FROM public.league_cmpt_user_join_details where cmpt_id=" + Userrow["cmpt_id"]);
                //    Userrow["no_users"] = cntcmpt;
                //}


                objDAL.CloseConnection();

                return dscompitition;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
            finally
            {
                objResponseEntity = null;
            }
        }

        public DataSet GetMyCompition(CompitionEntity compititionEntity)
        {
            try
            {
                string strCmptUrlPath = System.Configuration.ConfigurationManager.AppSettings["URL"] + "/compitition/";
                strSqlQry1 = @"SELECT league_cmpt_master.cmpt_id, league_cmpt_master.cmpt_name,'" + strCmptUrlPath + "'||  league_cmpt_master.cmpt_profile_picture_path AS cmpt_profile_picture_path, league_cmpt_master.cmpt_type,";
                strSqlQry1 = strSqlQry1 + @" to_char( league_cmpt_master.start_date, 'yyyy-MM-dd HH24:MI:SS') as start_date,  to_char(league_cmpt_master.end_date, 'yyyy-MM-dd HH24:MI:SS') as end_date, league_cmpt_master.terms_conditions, league_cmpt_master.min_km_travel, 
                         league_cmpt_master.cmpt_for_driving, league_cmpt_master.vehicle_manufacturer, league_cmpt_master.ec_agefrom, league_cmpt_master.ec_ageto, 
                         league_cmpt_master.gender, league_cmpt_master.authentication_required, league_cmpt_master.prize_id1, league_cmpt_master.prize_id2, 
                         league_cmpt_master.prize_id3, league_cmpt_master.car_type, league_cmpt_master.cmpt_country, league_cmpt_master.cmpt_uniquecode, 
                         to_char( league_cmpt_master.created_at, 'yyyy-MM-dd HH24:MI:SS') as created_at, league_cmpt_master.created_by, league_cmpt_master.isactive, league_cmpt_master.deleted, 
                         league_prize_master.prize_code, league_prize_master.prize_desc as prize_desc1 , league_prize_master_1.prize_code AS prize_code2, 
                         league_prize_master_1.prize_desc AS prize_desc2, league_prize_master_2.prize_code AS prize_code3, league_prize_master_2.prize_desc AS prize_desc3 ,round( coalesce(league_cmpt_wise_rating_rank.final_score,0)) as score,league_cmpt_wise_rating_rank.rankno ,league_cmpt_master.total_km_travel,user_master.first_name, user_master.middle_name, user_master.last_name ,";
                strSqlQry1 = strSqlQry1 + @" (SELECT  count(cmpt_id) FROM league_cmpt_user_join_details  where  league_cmpt_user_join_details.cmpt_id =league_cmpt_master.cmpt_id) AS no_users 
                         FROM league_cmpt_master LEFT OUTER JOIN
                         league_prize_master ON league_cmpt_master.prize_id1 = league_prize_master.prize_id LEFT OUTER JOIN
                         league_prize_master AS league_prize_master_1 ON league_cmpt_master.prize_id2 = league_prize_master_1.prize_id LEFT OUTER JOIN
                         league_prize_master AS league_prize_master_2 ON league_cmpt_master.prize_id3 = league_prize_master_2.prize_id  LEFT OUTER JOIN league_cmpt_wise_rating_rank on league_cmpt_wise_rating_rank.cmpt_id =league_cmpt_master.cmpt_id  and league_cmpt_wise_rating_rank.user_id=" + compititionEntity.s_user_id + "  inner join user_master on user_master.user_id = league_cmpt_master.created_by  where league_cmpt_master.cmpt_id  in(SELECT cmpt_id FROM public.league_cmpt_user_join_details where user_id=" + compititionEntity.s_user_id + ") and  cmpt_type = 'private'";


                strSqlQry2 = @"SELECT league_cmpt_master.cmpt_id, league_cmpt_master.cmpt_name,'" + strCmptUrlPath + "'||  league_cmpt_master.cmpt_profile_picture_path AS cmpt_profile_picture_path, league_cmpt_master.cmpt_type,";
                strSqlQry2 = strSqlQry2 + @" to_char( league_cmpt_master.start_date, 'yyyy-MM-dd HH24:MI:SS') as start_date,  to_char(league_cmpt_master.end_date, 'yyyy-MM-dd HH24:MI:SS') as end_date, league_cmpt_master.terms_conditions, league_cmpt_master.min_km_travel, 
                         league_cmpt_master.cmpt_for_driving, league_cmpt_master.vehicle_manufacturer, league_cmpt_master.ec_agefrom, league_cmpt_master.ec_ageto, 
                         league_cmpt_master.gender, league_cmpt_master.authentication_required, league_cmpt_master.prize_id1, league_cmpt_master.prize_id2, 
                         league_cmpt_master.prize_id3, league_cmpt_master.car_type, league_cmpt_master.cmpt_country, league_cmpt_master.cmpt_uniquecode, 
                         to_char( league_cmpt_master.created_at, 'yyyy-MM-dd HH24:MI:SS') as created_at, league_cmpt_master.created_by, league_cmpt_master.isactive, league_cmpt_master.deleted, 
                         league_prize_master.prize_code, league_prize_master.prize_desc as prize_desc1 , league_prize_master_1.prize_code AS prize_code2, 
                         league_prize_master_1.prize_desc AS prize_desc2, league_prize_master_2.prize_code AS prize_code3, league_prize_master_2.prize_desc AS prize_desc3 ,round( coalesce(league_cmpt_wise_rating_rank.final_score,0)) as score,league_cmpt_wise_rating_rank.rankno ,league_cmpt_master.total_km_travel,user_master.first_name, user_master.middle_name, user_master.last_name ,";
                strSqlQry2 = strSqlQry2 + @" (SELECT  count(cmpt_id) FROM league_cmpt_user_join_details  where  league_cmpt_user_join_details.cmpt_id =league_cmpt_master.cmpt_id) AS no_users 
                         FROM league_cmpt_master LEFT OUTER JOIN
                         league_prize_master ON league_cmpt_master.prize_id1 = league_prize_master.prize_id LEFT OUTER JOIN
                         league_prize_master AS league_prize_master_1 ON league_cmpt_master.prize_id2 = league_prize_master_1.prize_id LEFT OUTER JOIN
                         league_prize_master AS league_prize_master_2 ON league_cmpt_master.prize_id3 = league_prize_master_2.prize_id  LEFT OUTER JOIN league_cmpt_wise_rating_rank on league_cmpt_wise_rating_rank.cmpt_id =league_cmpt_master.cmpt_id  and league_cmpt_wise_rating_rank.user_id=" + compititionEntity.s_user_id + "  inner join user_master on user_master.user_id = league_cmpt_master.created_by  where league_cmpt_master.cmpt_id  in(SELECT cmpt_id FROM public.league_cmpt_user_join_details where user_id=" + compititionEntity.s_user_id + ") and  cmpt_type = 'public'";


                dscompitition = new DataSet();
                DataTable dtData;
                //string cntcmpt = "";
                string join_date = " ";

                objDAL.CreateConnection();

                dtData = objDAL.FetchRecords(strSqlQry1.ToString());
                //dtData.Columns.Add("no_users");
                dtData.Columns.Add("joinning_date");
                dscompitition.Tables.Add(dtData);
                dscompitition.Tables[dscompitition.Tables.Count - 1].TableName = "private";
                foreach (DataRow Userrow in dscompitition.Tables["private"].Rows)
                {
                    //if (Userrow["cmpt_profile_picture_path"] != null)
                    //{
                    //    string strUrlPath = CommonUtils.getUrlPath(Userrow["cmpt_profile_picture_path"].ToString(), "compitition", out Url);
                    //    Userrow["cmpt_profile_picture_path"] = Url;
                    //}
                    //cntcmpt = objDAL.FetchSingleRecord(@"SELECT  count( cmpt_id) FROM public.league_cmpt_user_join_details where cmpt_id=" + Userrow["cmpt_id"].ToString());
                    join_date = objDAL.FetchSingleRecord(@"SELECT  to_char(join_at, 'yyyy-MM-dd HH24:MI:SS') as join_at   FROM public.league_cmpt_user_join_details where cmpt_id=" + Userrow["cmpt_id"].ToString());
                    //Userrow["no_users"] = cntcmpt;
                    Userrow["joinning_date"] = join_date;
                }

                dtData = objDAL.FetchRecords(strSqlQry2.ToString());
                //dtData.Columns.Add("no_users");
                dtData.Columns.Add("joinning_date");
                dscompitition.Tables.Add(dtData);
                dscompitition.Tables[dscompitition.Tables.Count - 1].TableName = "public";
                foreach (DataRow Userrow in dscompitition.Tables["public"].Rows)
                {
                    //if (Userrow["cmpt_profile_picture_path"] != null)
                    //{
                    //    string strUrlPath = CommonUtils.getUrlPath(Userrow["cmpt_profile_picture_path"].ToString(), "compitition", out Url);
                    //    Userrow["cmpt_profile_picture_path"] = Url;

                    //}
                    //cntcmpt = objDAL.FetchSingleRecord(@"SELECT  count( cmpt_id) FROM public.league_cmpt_user_join_details where cmpt_id=" + Userrow["cmpt_id"].ToString());
                    join_date = objDAL.FetchSingleRecord(@"SELECT  to_char(join_at, 'yyyy-MM-dd HH24:MI:SS') as join_at  FROM public.league_cmpt_user_join_details where cmpt_id=" + Userrow["cmpt_id"].ToString());
                    //Userrow["no_users"] = cntcmpt;
                    Userrow["joinning_date"] = join_date;
                }

                objDAL.CloseConnection();

                return dscompitition;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
            finally
            {
                objResponseEntity = null;
            }
        }

        public DataTable GetCompitionbyId(CompitionEntity compititionEntity)
        {
            try
            {
                strSqlQry = @"SELECT league_cmpt_master.cmpt_id, league_cmpt_master.cmpt_name, league_cmpt_master.cmpt_profile_picture_path, league_cmpt_master.cmpt_type, 
                          to_char( league_cmpt_master.start_date, 'yyyy-MM-dd HH24:MI:SS') as start_date , to_char( league_cmpt_master.end_date, 'yyyy-MM-dd HH24:MI:SS') as end_date, league_cmpt_master.terms_conditions, league_cmpt_master.min_km_travel, 
                         league_cmpt_master.cmpt_for_driving, league_cmpt_master.vehicle_manufacturer, league_cmpt_master.ec_agefrom, league_cmpt_master.ec_ageto, 
                         league_cmpt_master.gender, league_cmpt_master.authentication_required, league_cmpt_master.prize_id1, league_cmpt_master.prize_id2, 
                         league_cmpt_master.prize_id3, league_cmpt_master.car_type, league_cmpt_master.cmpt_country, league_cmpt_master.cmpt_uniquecode, 
                         to_char( league_cmpt_master.created_at, 'yyyy-MM-dd HH24:MI:SS') as created_at, league_cmpt_master.created_by, league_cmpt_master.isactive, league_cmpt_master.deleted, 
                         league_prize_master.prize_code, league_prize_master.prize_desc as prize_desc1 , league_prize_master_1.prize_code AS prize_code2, 
                         league_prize_master_1.prize_desc AS prize_desc2, league_prize_master_2.prize_code AS prize_code3, league_prize_master_2.prize_desc AS prize_desc3,league_cmpt_master.total_km_travel,league_cmpt_master.cmpt_code, coalesce(league_cmpt_master.isclose_cmpt,false) as isclose_cmpt
                         FROM    league_cmpt_master LEFT OUTER JOIN
                         league_prize_master ON league_cmpt_master.prize_id1 = league_prize_master.prize_id LEFT OUTER JOIN
                         league_prize_master AS league_prize_master_1 ON league_cmpt_master.prize_id2 = league_prize_master_1.prize_id LEFT OUTER JOIN
                         league_prize_master AS league_prize_master_2 ON league_cmpt_master.prize_id3 = league_prize_master_2.prize_id   where  cmpt_id=" + compititionEntity.cmpt_id;

                objDAL.CreateConnection();
                DataTable dtcompititionbyid = objDAL.FetchRecords(strSqlQry);

                string strUrlPath = CommonUtils.getUrlPath(dtcompititionbyid.Rows[0]["cmpt_profile_picture_path"].ToString(), "compitition", out Url);
                dtcompititionbyid.Rows[0]["cmpt_profile_picture_path"] = Url;
                objDAL.CloseConnection();

                return dtcompititionbyid;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
            finally
            {
                objResponseEntity = null;
            }
        }

        public bool JoinCmpt(CompitionEntity compititionEntity)
        {
            try
            {
                objDAL.CreateConnection();

                strSqlQry = @"INSERT INTO public.league_cmpt_user_join_details(cmpt_id, user_id,  join_at) VALUES (@cmpt_id, @user_id, @join_at);";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@cmpt_id", CommonUtils.ConvertToInt(compititionEntity.cmpt_id)));
                Parameters.Add(new NpgsqlParameter("@user_id", CommonUtils.ConvertToInt(compititionEntity.s_user_id)));
                if (CommonUtils.ConvertToDateTime(compititionEntity.jointimestam) == null)
                {
                    Parameters.Add(new NpgsqlParameter("@join_at", DBNull.Value));
                }
                else
                {
                    Parameters.Add(new NpgsqlParameter("@join_at", CommonUtils.ConvertToDateTime(compititionEntity.jointimestam)));
                }

                objDAL.ExecutParameterizedQry(strSqlQry, Parameters);


                objDAL.CloseConnection();

                return true;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool UpdateCompititionProfilePath(CompitionEntity compititionEntity)
        {
            try
            {
                objDAL.CreateConnection();
                _cmpt_id = CommonUtils.ConvertToInt(compititionEntity.cmpt_id);

                strSqlQry = @"UPDATE league_cmpt_master  SET  cmpt_profile_picture_path=@profile_picture_uri  WHERE (cmpt_id = @cmpt_id)";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@cmpt_id", _cmpt_id));
                Parameters.Add(new NpgsqlParameter("@profile_picture_uri", CommonUtils.ConvertToString(compititionEntity.cmpt_profile_picture_path)));

                blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                objDAL.CloseConnection();

                return blnResult;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public DataSet GetTop10Player(CompitionEntity compititionEntity)
        {
            try
            {
                strSqlQry1 = @"  Select  user_master.user_id, user_master.first_name ,user_master.middle_name,user_master.last_name,user_master.profile_picture_uri,user_master.user_name,user_master.mobileno,user_master.email_id1 as Email_id , league_cmpt_master.cmpt_id, league_cmpt_master.cmpt_name, league_cmpt_master.cmpt_profile_picture_path, league_cmpt_master.cmpt_type,  to_char( league_cmpt_master.start_date, 'yyyy-MM-dd HH24:MI:SS') as start_date,  to_char(league_cmpt_master.end_date, 'yyyy-MM-dd HH24:MI:SS') as end_date, to_char( league_cmpt_user_join_details.join_at, 'yyyy-MM-dd HH24:MI:SS') as joining_date ,round( league_cmpt_wise_rating_rank.distance_travel,1) as distance_travel, league_cmpt_wise_rating_rank.deviceno, round( coalesce(league_cmpt_wise_rating_rank.final_score,0))  as score,  coalesce(league_cmpt_wise_rating_rank.rankno,0) as rankno
                        FROM user_master inner join  league_cmpt_user_join_details on user_master.user_id  = league_cmpt_user_join_details.user_id 
                        left outer join  league_cmpt_wise_rating_rank on user_master.user_id  = league_cmpt_wise_rating_rank.user_id   
                        and league_cmpt_wise_rating_rank.cmpt_id=" + compititionEntity.cmpt_id + " left outer join  league_cmpt_master on league_cmpt_master.cmpt_id  = league_cmpt_user_join_details.cmpt_id where  league_cmpt_user_join_details.cmpt_id =" + compititionEntity.cmpt_id + "  and cmpt_type = 'private'  ORDER BY  league_cmpt_wise_rating_rank.rankno asc limit 10 ";

                strSqlQry2 = @" Select  user_master.user_id, user_master.first_name ,user_master.middle_name,user_master.last_name,user_master.profile_picture_uri,user_master.user_name,user_master.mobileno,user_master.email_id1 as Email_id , league_cmpt_master.cmpt_id, league_cmpt_master.cmpt_name, league_cmpt_master.cmpt_profile_picture_path, league_cmpt_master.cmpt_type,  to_char( league_cmpt_master.start_date, 'yyyy-MM-dd HH24:MI:SS') as start_date,  to_char(league_cmpt_master.end_date, 'yyyy-MM-dd HH24:MI:SS') as end_date, to_char( league_cmpt_user_join_details.join_at, 'yyyy-MM-dd HH24:MI:SS') as joining_date ,round( league_cmpt_wise_rating_rank.distance_travel,1) as distance_travel, league_cmpt_wise_rating_rank.deviceno, round( coalesce(league_cmpt_wise_rating_rank.final_score,0))  as score,  coalesce(league_cmpt_wise_rating_rank.rankno,0) as rankno
                        FROM user_master inner join  league_cmpt_user_join_details on user_master.user_id  = league_cmpt_user_join_details.user_id 
                        left outer join  league_cmpt_wise_rating_rank on user_master.user_id  = league_cmpt_wise_rating_rank.user_id   
                        and league_cmpt_wise_rating_rank.cmpt_id=" + compititionEntity.cmpt_id + " left outer join  league_cmpt_master on league_cmpt_master.cmpt_id  = league_cmpt_user_join_details.cmpt_id where  league_cmpt_user_join_details.cmpt_id =" + compititionEntity.cmpt_id + "  and cmpt_type = 'public'  ORDER BY  league_cmpt_wise_rating_rank.rankno asc limit 10";

                dscompitition = new DataSet();
                DataTable dtData;

                objDAL.CreateConnection();

                dtData = objDAL.FetchRecords(strSqlQry1.ToString());
                dscompitition.Tables.Add(dtData);
                dscompitition.Tables[dscompitition.Tables.Count - 1].TableName = "private";
                foreach (DataRow Userrow in dscompitition.Tables["private"].Rows)
                {
                    if (Userrow["cmpt_profile_picture_path"] != null)
                    {
                        string strUrlPath = CommonUtils.getUrlPath(Userrow["cmpt_profile_picture_path"].ToString(), "compitition", out Url);
                        Userrow["cmpt_profile_picture_path"] = Url;
                    }
                    if (Userrow["profile_picture_uri"] != null)
                    {
                        string strUrlPath = CommonUtils.getUrlPath(Userrow["profile_picture_uri"].ToString(), "userprofile", out Url);
                        Userrow["profile_picture_uri"] = Url;
                    }
                }

                dtData = objDAL.FetchRecords(strSqlQry2.ToString());
                dscompitition.Tables.Add(dtData);
                dscompitition.Tables[dscompitition.Tables.Count - 1].TableName = "public";
                foreach (DataRow Userrow in dscompitition.Tables["public"].Rows)
                {
                    if (Userrow["cmpt_profile_picture_path"] != null)
                    {
                        string strUrlPath = CommonUtils.getUrlPath(Userrow["cmpt_profile_picture_path"].ToString(), "compitition", out Url);
                        Userrow["cmpt_profile_picture_path"] = Url;
                    }
                    if (Userrow["profile_picture_uri"] != null)
                    {
                        string strUrlPath = CommonUtils.getUrlPath(Userrow["profile_picture_uri"].ToString(), "compitition", out Url);
                        Userrow["profile_picture_uri"] = Url;
                    }
                }

                objDAL.CloseConnection();

                return dscompitition;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
            finally
            {
                objResponseEntity = null;
            }
        }

        public DataTable UpdateCompitition(CompitionEntity compititionEntity)
        {
            try
            {
                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                if (compititionEntity.s_date != null && compititionEntity.e_date != null)
                {
                    compititionEntity.start_date = CommonUtils.ConvertToDateTime(CommonUtils.GetstringFromatyyyy_MM_dd(compititionEntity.s_date));
                    compititionEntity.end_date = CommonUtils.ConvertToDateTime(CommonUtils.GetstringFromatyyyy_MM_dd(compititionEntity.e_date));
                }

                strSqlQry = @"UPDATE league_prize_master SET  prize_desc=@prize_desc, updated_by=@updated_by, updated_at=@updated_at   WHERE  prize_id=@prize_id";

                Parameters1 = new List<NpgsqlParameter>();
                Parameters1.Add(new NpgsqlParameter("@prize_id", CommonUtils.ConvertToInt(compititionEntity.prize_id1)));
                Parameters1.Add(new NpgsqlParameter("@prize_desc", CommonUtils.ConvertToString(compititionEntity.prize1_desc)));
                Parameters1.Add(new NpgsqlParameter("@updated_by", CommonUtils.ConvertToInt(compititionEntity.s_user_id)));
                Parameters1.Add(new NpgsqlParameter("@updated_at", CommonUtils.ConvertToDateTime(DateTime.Now)));

                Parameters2 = new List<NpgsqlParameter>();
                Parameters2.Add(new NpgsqlParameter("@prize_id", CommonUtils.ConvertToInt(compititionEntity.prize_id2)));
                Parameters2.Add(new NpgsqlParameter("@prize_desc", CommonUtils.ConvertToString(compititionEntity.prize2_desc)));
                Parameters2.Add(new NpgsqlParameter("@updated_by", CommonUtils.ConvertToInt(compititionEntity.s_user_id)));
                Parameters2.Add(new NpgsqlParameter("@updated_at", CommonUtils.ConvertToDateTime(DateTime.Now)));

                Parameters3 = new List<NpgsqlParameter>();
                Parameters3.Add(new NpgsqlParameter("@prize_id", CommonUtils.ConvertToInt(compititionEntity.prize_id3)));
                Parameters3.Add(new NpgsqlParameter("@prize_desc", CommonUtils.ConvertToString(compititionEntity.prize3_desc)));
                Parameters3.Add(new NpgsqlParameter("@updated_by", CommonUtils.ConvertToInt(compititionEntity.s_user_id)));
                Parameters3.Add(new NpgsqlParameter("@updated_at", CommonUtils.ConvertToDateTime(DateTime.Now)));

                objDAL.ExecutParameterizedQry(strSqlQry, Parameters1);
                objDAL.ExecutParameterizedQry(strSqlQry, Parameters2);
                objDAL.ExecutParameterizedQry(strSqlQry, Parameters3);



                strSqlQry = @"UPDATE league_cmpt_master SET  cmpt_name=@cmpt_name, cmpt_type=@cmpt_type, start_date=@start_date, end_date=@end_date, terms_conditions=@terms_conditions, min_km_travel=@min_km_travel,total_km_travel=@total_km_travel, cmpt_for_driving=@cmpt_for_driving, vehicle_manufacturer=@vehicle_manufacturer, ec_agefrom=@ec_agefrom, ec_ageto=@ec_ageto, gender=@gender, authentication_required=@authentication_required, car_type=@car_type, cmpt_country=@cmpt_country, cmpt_uniquecode=@cmpt_uniquecode, isclose_cmpt=@isclose_cmpt,cmpt_code=@cmpt_code, updated_at=@upadted_at, updated_by=@upadted_by
	                                              WHERE cmpt_id =@cmpt_id ";
                Parameters = new List<NpgsqlParameter>();

                Parameters.Add(new NpgsqlParameter("@cmpt_id", CommonUtils.ConvertToInt(compititionEntity.cmpt_id)));
                Parameters.Add(new NpgsqlParameter("@cmpt_name", CommonUtils.ConvertToString(compititionEntity.cmpt_name)));
                Parameters.Add(new NpgsqlParameter("@cmpt_type", CommonUtils.ConvertToString(compititionEntity.cmpt_type)));
                Parameters.Add(new NpgsqlParameter("@start_date", CommonUtils.ConvertToDateTime(compititionEntity.start_date)));
                Parameters.Add(new NpgsqlParameter("@end_date", CommonUtils.ConvertToDateTime(compititionEntity.end_date)));
                Parameters.Add(new NpgsqlParameter("@terms_conditions", CommonUtils.ConvertToString(compititionEntity.terms_conditions)));
                Parameters.Add(new NpgsqlParameter("@min_km_travel", CommonUtils.ConvertToInt(compititionEntity.min_km_travel)));
                Parameters.Add(new NpgsqlParameter("@total_km_travel", CommonUtils.ConvertToInt(compititionEntity.total_km_travel)));
                Parameters.Add(new NpgsqlParameter("@cmpt_for_driving", CommonUtils.ConvertToString(compititionEntity.cmpt_for_driving)));
                Parameters.Add(new NpgsqlParameter("@vehicle_manufacturer", CommonUtils.ConvertToString(compititionEntity.vehicle_manufacturer)));
                Parameters.Add(new NpgsqlParameter("@ec_agefrom", CommonUtils.ConvertToDecimal(compititionEntity.ec_agefrom)));
                Parameters.Add(new NpgsqlParameter("@ec_ageto", CommonUtils.ConvertToDecimal(compititionEntity.ec_ageto)));
                Parameters.Add(new NpgsqlParameter("@gender", CommonUtils.ConvertToString(compititionEntity.gender)));
                Parameters.Add(new NpgsqlParameter("@authentication_required", CommonUtils.ConvertToBoolean(compititionEntity.authentication_required)));
                Parameters.Add(new NpgsqlParameter("@car_type", CommonUtils.ConvertToString(compititionEntity.car_type)));
                Parameters.Add(new NpgsqlParameter("@cmpt_country", CommonUtils.ConvertToString(compititionEntity.cmpt_country)));
                Parameters.Add(new NpgsqlParameter("@cmpt_uniquecode", CommonUtils.ConvertToString(compititionEntity.cmpt_uniquecode)));
                Parameters.Add(new NpgsqlParameter("@cmpt_code", CommonUtils.ConvertToString(compititionEntity.cmpt_code)));
                Parameters.Add(new NpgsqlParameter("@isclose_cmpt", CommonUtils.ConvertToBoolean(compititionEntity.isclose_cmpt)));
                Parameters.Add(new NpgsqlParameter("@upadted_by", CommonUtils.ConvertToInt(compititionEntity.s_user_id)));
                Parameters.Add(new NpgsqlParameter("@upadted_at", CommonUtils.ConvertToDateTime(DateTime.Now)));


                objDAL.ExecutParameterizedQry(strSqlQry, Parameters);




                _cmpt_id = CommonUtils.ConvertToInt(compititionEntity.cmpt_id);

                strSqlQry = @"SELECT league_cmpt_master.cmpt_id, league_cmpt_master.cmpt_name, league_cmpt_master.cmpt_profile_picture_path, league_cmpt_master.cmpt_type, 
                          to_char( league_cmpt_master.start_date, 'yyyy-MM-dd HH24:MI:SS') as start_date,  to_char(league_cmpt_master.end_date, 'yyyy-MM-dd HH24:MI:SS') as end_date, league_cmpt_master.terms_conditions, league_cmpt_master.min_km_travel, 
                         league_cmpt_master.cmpt_for_driving, league_cmpt_master.vehicle_manufacturer, league_cmpt_master.ec_agefrom, league_cmpt_master.ec_ageto, 
                         league_cmpt_master.gender, league_cmpt_master.authentication_required, league_cmpt_master.prize_id1, league_cmpt_master.prize_id2, 
                         league_cmpt_master.prize_id3, league_cmpt_master.car_type, league_cmpt_master.cmpt_country, league_cmpt_master.cmpt_uniquecode, 
                         league_cmpt_master.created_at, league_cmpt_master.created_by, league_cmpt_master.isactive, league_cmpt_master.deleted, 
                         league_prize_master.prize_code, league_prize_master.prize_desc as prize_desc1 , league_prize_master_1.prize_code AS prize_code2, 
                         league_prize_master_1.prize_desc AS prize_desc2, league_prize_master_2.prize_code AS prize_code3, league_prize_master_2.prize_desc AS prize_desc3,league_cmpt_master.total_km_travel
                        FROM            league_cmpt_master LEFT OUTER JOIN
                         league_prize_master ON league_cmpt_master.prize_id1 = league_prize_master.prize_id LEFT OUTER JOIN
                         league_prize_master AS league_prize_master_1 ON league_cmpt_master.prize_id2 = league_prize_master_1.prize_id LEFT OUTER JOIN
                         league_prize_master AS league_prize_master_2 ON league_cmpt_master.prize_id3 = league_prize_master_2.prize_id   where  cmpt_id=" + _cmpt_id;

                objDAL.CreateConnection();
                dtcompitition = objDAL.FetchRecords(strSqlQry);




                objDAL.CommitTransaction();
                objDAL.CloseConnection();

                return dtcompitition;

            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool CancelCompetition(CompitionEntity compititionEntity)
        {
            try
            {
                objDAL.CreateConnection();

                //strSqlQry = @"update trip_planning_master set deleted=true,isactive=false,trip_end_time=@trip_end_time where trip_id = (SELECT trip_id FROM trip_planning_master and user_id=@u_id  where isactive=true order  by trip_id  desc limit 1)  ";
                strSqlQry = @"UPDATE league_cmpt_master SET deleted=true,isactive=false, updated_at=@updated_at WHERE cmpt_id=@cmpt_id;";
                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@cmpt_id", CommonUtils.ConvertToInt(compititionEntity.cmpt_id)));
                // Parameters.Add(new NpgsqlParameter("@trip_end_time", CommonUtils.ConvertToDateTime(tripEntity.trip_end_time)));
                Parameters.Add(new NpgsqlParameter("@updated_at", CommonUtils.ConvertToDateTime(DateTime.Now)));

                blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);


                objDAL.CloseConnection();


                return blnResult;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool CheckJointAvailablity(CompitionEntity compititionEntity)
        {
            try
            {
                string SQLQry = @"SELECT cmpt_id FROM public.league_cmpt_user_join_details where user_id=@user_id and cmpt_id=@cmpt_id";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@user_id", CommonUtils.ConvertToInt(compititionEntity.s_user_id)));
                Parameters.Add(new NpgsqlParameter("@cmpt_id", CommonUtils.ConvertToInt(compititionEntity.cmpt_id)));


                objDAL.CreateConnection();
                DataTable dtData = objDAL.ExecutParameterizedQrywithDataTable(SQLQry, Parameters);
                objDAL.CloseConnection();

                if (dtData != null && dtData.Rows.Count > 0)
                {
                    objDAL.CloseConnection();
                    return true;
                }

                return false;
            }

            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
        }

        public DataTable Checkisclosed_cmptAvailablity(CompitionEntity compititionEntity)
        {
            try
            {
                string SQLQry = @"SELECT cmpt_code FROM league_cmpt_master where cmpt_id=@cmpt_id and isclose_cmpt=true";

                Parameters = new List<NpgsqlParameter>();
               Parameters.Add(new NpgsqlParameter("@cmpt_id", CommonUtils.ConvertToInt(compititionEntity.cmpt_id)));


                objDAL.CreateConnection();
                DataTable dtData = objDAL.ExecutParameterizedQrywithDataTable(SQLQry, Parameters);
                objDAL.CloseConnection();

                //if (dtData != null && dtData.Rows.Count > 0)
                //{
                //    objDAL.CloseConnection();
                //    return true;
                //}
                objDAL.CloseConnection();
                return dtData;
            }

            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
        }

        public bool Checkshare_cmptAvailablity(CompitionEntity compititionEntity)
        {
            try
            {
                string SQLQry = @"SELECT cmpt_id FROM league_cmpt_user_org_mapping where user_id=@user_id and cmpt_id=@cmpt_id";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@user_id", CommonUtils.ConvertToInt(compititionEntity.s_user_id)));
                Parameters.Add(new NpgsqlParameter("@cmpt_id", CommonUtils.ConvertToInt(compititionEntity.cmpt_id)));


                objDAL.CreateConnection();
                DataTable dtData = objDAL.ExecutParameterizedQrywithDataTable(SQLQry, Parameters);
                objDAL.CloseConnection();

                if (dtData != null && dtData.Rows.Count > 0)
                {
                    objDAL.CloseConnection();
                    return true;
                }

                return false;
            }

            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
        }

        public bool ShareCmpt(CompitionEntity compititionEntity)
        {
            try
            {
                objDAL.CreateConnection();

                strSqlQry = @"INSERT INTO league_cmpt_user_org_mapping(cmpt_id, user_id, mapping_at,mapping_by) VALUES (@cmpt_id, @user_id, @mapping_at,@mapping_by);";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@cmpt_id", CommonUtils.ConvertToInt(compititionEntity.cmpt_id)));
                Parameters.Add(new NpgsqlParameter("@user_id", CommonUtils.ConvertToInt(compititionEntity.userid)));
                Parameters.Add(new NpgsqlParameter("@mapping_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                Parameters.Add(new NpgsqlParameter("@mapping_by", CommonUtils.ConvertToInt(compititionEntity.s_user_id)));
               

                objDAL.ExecutParameterizedQry(strSqlQry, Parameters);


                objDAL.CloseConnection();

                return true;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }
    }
}
