﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Npgsql;
using IOTBusinessEntities;
using IOTUtilities;
using HashAndSalt;
using System.IO;
using System.Web;
using System.Net.Mail;
using System.Web.Http;

namespace IOTDataModel
{
    public class GeofenceDM : IDisposable
    {
        private BaseDAL objDAL;
        private List<NpgsqlParameter> Parameters;
        private String strSqlQry;
        private ResponseEntity objResponseEntity;
        private Int32 _gc_id;
        DataTable dtGeofence;

        public GeofenceDM()
        {
            objDAL = new BaseDAL();
            objResponseEntity = new ResponseEntity();
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing)
            {
                // Free any other managed objects here.
                objDAL.Dispose();
                objResponseEntity = null;
                Parameters = null;
                strSqlQry = null;
                dtGeofence = null;

            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~GeofenceDM()
        {
            Dispose(false);
        }

        #endregion

        public bool CreateGeofence(GeofenceEntity geofenceEntity)
        {
            try
            {
                objDAL.CreateConnection();

                strSqlQry = @"INSERT INTO geofencemaster(geofence_name, geofenec_desc, lat_lng, created_at,created_by, is_active)
                            VALUES (@geofence_name, @geofenec_desc, @lat_lng, @created_at,@created_by, @is_active);";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@geofence_name", CommonUtils.ConvertToString(geofenceEntity.geofence_name)));
                Parameters.Add(new NpgsqlParameter("@geofenec_desc", CommonUtils.ConvertToString(geofenceEntity.geofenec_desc)));
                Parameters.Add(new NpgsqlParameter("@lat_lng", CommonUtils.ConvertToString(geofenceEntity.lat_lng)));
                Parameters.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                Parameters.Add(new NpgsqlParameter("@created_by", CommonUtils.ConvertToInt(geofenceEntity.created_by)));
                Parameters.Add(new NpgsqlParameter("@is_active", true));

                objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                objDAL.CloseConnection();

                return true;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public DataTable GetGeofenceList(GeofenceEntity geofenceEntity)
        {
            try
            {
                strSqlQry = @"Select gc_id,	geofence_name,geofenec_desc,lat_lng,icon_file_name,to_char(created_at,'dd-mm-yyyy HH24:MI:SS') as created_at,created_by,to_char(updated_at,'dd-mm-yyyy HH24:MI:SS') as updated_at ,updated_by  from geofencemaster WHERE  (coalesce(deleted,false) = '0')";

                objDAL.CreateConnection();
                dtGeofence = objDAL.FetchRecords(strSqlQry);
                objDAL.CloseConnection();

                return dtGeofence;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public DataTable GetGeofenceById(GeofenceEntity geofenceEntity)
        {
            try
            {
                string strSqlQry = @"Select * from geofencemaster WHERE  gc_id=" + CommonUtils.ConvertToInt(geofenceEntity.gc_id);

                objDAL.CreateConnection();
                dtGeofence = objDAL.FetchRecords(strSqlQry);
                objDAL.CloseConnection();

                return dtGeofence;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool UpdateGeofence(GeofenceEntity geofenceEntity)
        {
            try
            {
                objDAL.CreateConnection();

                strSqlQry = @"UPDATE geofencemaster SET	geofence_name =@geofence_name,geofenec_desc=@geofenec_desc,lat_lng =@lat_lng,updated_at =@updated_at,updated_by =@updated_by ,is_active=@is_active WHERE gc_id = @gc_id;";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@gc_id", CommonUtils.ConvertToInt(geofenceEntity.gc_id)));
                Parameters.Add(new NpgsqlParameter("@geofence_name", CommonUtils.ConvertToString(geofenceEntity.geofence_name)));
                Parameters.Add(new NpgsqlParameter("@geofenec_desc", CommonUtils.ConvertToString(geofenceEntity.geofenec_desc)));
                Parameters.Add(new NpgsqlParameter("@lat_lng", CommonUtils.ConvertToString(geofenceEntity.lat_lng)));
                Parameters.Add(new NpgsqlParameter("@updated_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                Parameters.Add(new NpgsqlParameter("@updated_by", CommonUtils.ConvertToInt(geofenceEntity.updated_by)));
                Parameters.Add(new NpgsqlParameter("@is_active", geofenceEntity.isactive));

                objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                objDAL.CloseConnection();

                return true;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool DeleteGeofence(GeofenceEntity geofenceEntity)
        {
            try
            {
                objDAL.CreateConnection();

                strSqlQry = @"update geofencemaster set deleted=true,updated_at=@updated_at,updated_by=@updated_by where gc_id = @gc_id; ";
                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@gc_id", CommonUtils.ConvertToInt(geofenceEntity.gc_id)));
                Parameters.Add(new NpgsqlParameter("@updated_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                Parameters.Add(new NpgsqlParameter("@updated_by", CommonUtils.ConvertToInt(geofenceEntity.updated_by)));
                objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                objDAL.CloseConnection();

                return true;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool CheckGeofenceAvailability(GeofenceEntity geofenceEntity)
        {
            try
            {
                string SQLQry = @"select gc_id  from geofencemaster where geofence_name=@geofence_name and coalesce(deleted,false) = false";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@geofence_name", CommonUtils.ConvertToString(geofenceEntity.geofence_name)));

                objDAL.CreateConnection();
                dtGeofence = objDAL.ExecutParameterizedQrywithDataTable(SQLQry, Parameters);
                objDAL.CloseConnection();

                if (dtGeofence != null && dtGeofence.Rows.Count > 0)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool CheckCheckGeofenceAvailabilityforupdate(GeofenceEntity geofenceEntity)
        {
            try
            {
                string SQLQry = @"select gc_id  from geofencemaster where geofence_name=@geofence_name and coalesce(deleted,false) = false and  gc_id <> @gc_id ";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@geofence_name", CommonUtils.ConvertToString(geofenceEntity.geofence_name)));
                Parameters.Add(new NpgsqlParameter("@gc_id", CommonUtils.ConvertToInt(geofenceEntity.gc_id)));

                objDAL.CreateConnection();
                dtGeofence = objDAL.ExecutParameterizedQrywithDataTable(SQLQry, Parameters);
                objDAL.CloseConnection();

                if (dtGeofence != null && dtGeofence.Rows.Count > 0)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
        }


        public bool UpdateGeofenceUserMapping(GeofenceEntity geofenceEntity)
        {
            try
            {
                if (geofenceEntity != null && geofenceEntity.geofenceUserMappingEntities != null)
                {
                    objDAL.CreateConnection();

                    strSqlQry = @"update geofence_user_mapping  set is_active=false where user_id=@user_id;";
                    Parameters = new List<NpgsqlParameter>();
                    Parameters.Add(new NpgsqlParameter("@user_id", geofenceEntity.user_id));
                    objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                    foreach (GeofenceUserMappingEntity geofenceUserMappingEntities in geofenceEntity.geofenceUserMappingEntities)
                    {
                        strSqlQry = @"INSERT INTO geofence_user_mapping (gc_id, user_id,blnmaster, created_at, created_by,is_active) VALUES (@gc_id, @user_id,@blnmaster, @created_at, @created_by,@is_active )";
                        Parameters = new List<NpgsqlParameter>();
                        Parameters.Add(new NpgsqlParameter("@gc_id", CommonUtils.ConvertToInt(geofenceUserMappingEntities.gc_id)));
                        Parameters.Add(new NpgsqlParameter("@user_id", CommonUtils.ConvertToInt(geofenceUserMappingEntities.user_id)));
                        Parameters.Add(new NpgsqlParameter("@blnmaster", geofenceUserMappingEntities.blnmaster));
                        Parameters.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                        Parameters.Add(new NpgsqlParameter("@created_by", CommonUtils.ConvertToInt(geofenceEntity.created_by)));
                        Parameters.Add(new NpgsqlParameter("@is_active", true));
                        objDAL.ExecutParameterizedQry(strSqlQry, Parameters);
                    }

                    strSqlQry = null;

                    objDAL.CloseConnection();

                }
                return true;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
        }

        public DataTable GetAssignUnassignGeofenceList(FilterEntity filterEntity)
        {
            try
            {
                strSqlQry = @" SELECT gc_id,geofence_name,geofenec_desc,to_char(created_at,'dd-mm-yyyy HH24:MI:SS') as created_at,created_by, false as chkforAll, false as chkforMaster  FROM geofencemaster  where gc_id not in (select gc_id  from geofence_user_mapping where is_active=true and user_id=" + filterEntity.user_id + ") union  SELECT geofencemaster.gc_id,	geofencemaster.geofence_name, geofencemaster.geofenec_desc,	to_char(geofencemaster.created_at,'dd-mm-yyyy HH24:MI:SS') as created_at,	geofencemaster.created_by,true as chkforAll,geofence_user_mapping.blnmaster as chkforMaster FROM geofencemaster,	geofence_user_mapping WHERE	geofencemaster.gc_id = geofence_user_mapping.gc_id and geofence_user_mapping.is_active=true and geofence_user_mapping.user_id=" + filterEntity.user_id + "  order by chkforAll,gc_id desc ";
                
                objDAL.CreateConnection();
                DataTable dtGeofence = objDAL.FetchRecords(strSqlQry);
                objDAL.CloseConnection();

                return dtGeofence;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

    }
}
