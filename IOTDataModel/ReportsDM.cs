﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Npgsql;
using IOTBusinessEntities;
using IOTUtilities;
using HashAndSalt;
using System.Configuration;
using System.IO;

namespace IOTDataModel
{
    public class ReportsDM : IDisposable
    {

        private BaseDAL objDAL;
        private DataSet dsData;
        private StringBuilder strSqlQry1;

        public ReportsDM()
        {
            objDAL = new BaseDAL();
            dsData = new DataSet();
            strSqlQry1 = new StringBuilder();
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing)
            {
                // Free any other managed objects here.
                objDAL.Dispose();
                if (dsData != null) dsData.Dispose();
                strSqlQry1 = null;
            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~ReportsDM()
        {
            Dispose(false);
        }

        #endregion

        public DataSet GetDriverperformanceData(FilterEntity filterEntity)
        {

            try
            {
                //strSqlQry1.Append("SELECT * FROM dr_daily_parameter_wise_rating order by recorddate asc ");


                //---for overall_rating 
                string stroverall_rating = @"SELECT deviceno, round(coalesce(distance,0),1) as distance, round(coalesce(overspeedscore,0)) as overspeedscore ,round(coalesce(harsh_score,0)) as harsh_score ,  round(coalesce(age_score,0)) as age_score,round(coalesce(gender_score,0)) as gender_score ,
                                          round(coalesce(car_score,0)) as car_score ,round(coalesce(weather_score,0)) as weather_score  ,round(coalesce(calling_score,0)) as  calling_score ,  round(coalesce(rating,0)) as rating from dr_overall_rating where deviceno =" + filterEntity.DeviceNo.ToString() + ";";
                objDAL.CreateConnection();
                DataTable dtData = objDAL.FetchRecords(stroverall_rating);
                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "overall_rating";
                stroverall_rating = null;


                //---for daily_parameter_wise_rating 
                string strdaily_rating = @"select to_char (recorddate, 'dd-mm-yyyy') as recorddate,round(coalesce(final_score,0)) as final_score , round(coalesce(distance,0),1) as distance  from dr_daily_parameter_wise_rating where deviceno =" + filterEntity.DeviceNo.ToString() + " order by recorddate desc;";
                dtData = objDAL.FetchRecords(strdaily_rating);
                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "daily_rating";
                strdaily_rating = null;


                //---for overall_trip_rating 
                string stroverall_trip_rating = @"select dr_overall_trip_rating.trip_id,trip_planning_master.trip_name, round(coalesce(rating,0)) as rating , round(coalesce(distance,0),1) as distance ,trip_planning_master.m_distance from dr_overall_trip_rating inner join trip_planning_master on trip_planning_master.trip_id=dr_overall_trip_rating.trip_id  where deviceno =" + filterEntity.DeviceNo.ToString() + " order by trip_id desc limit 30;";
                dtData = objDAL.FetchRecords(stroverall_trip_rating);
                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "overall_trip_rating";
                stroverall_trip_rating = null;


                //DataTable dtData;

                ////open conn
                //objDAL.CreateConnection();

                ////---for history
                //dtData = objDAL.FetchRecords(strSqlQry1.ToString());
                ////dtData.DefaultView.Sort = "recorddate asc";
                //dtData = dtData.DefaultView.ToTable();

                //dsData.Tables.Add(dtData);
                //dsData.Tables[dsData.Tables.Count - 1].TableName = "All";


                ////close conn
                objDAL.CloseConnection();

                return dsData;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public DataSet GetAggregateData(FilterEntity filterEntity)
        {

            try
            {
                strSqlQry1.Append("SELECT * FROM rpt_daily_agg_parameter order by recorddatetime asc ");

                DataTable dtData;

                //open conn
                objDAL.CreateConnection();

                //---for history
                dtData = objDAL.FetchRecords(strSqlQry1.ToString());
                //dtData.DefaultView.Sort = "recorddate asc";
                dtData = dtData.DefaultView.ToTable();

                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "All";


                ////close conn
                objDAL.CloseConnection();

                return dsData;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public DataSet GetStoppage(FilterEntity filterEntity)
        {

            try
            {
                string fromdate = CommonUtils.GetstringFromatyyyy_MM_dd(filterEntity.sfDate);
                string todate = CommonUtils.GetstringFromatyyyy_MM_dd(filterEntity.stDate);

                string[] stoppageRange = new[] { "0-10", "11-20", "21-30", "31-40", "41-999" };
                int[] stoppagecnt = new int[stoppageRange.Length];

                strSqlQry1.Append("SELECT deviceno,recorddatetime as rd,to_char(recorddatetime, 'dd-mm') as recorddatetime ,to_char(recorddatetime, 'dd-mm-yyyy ') as gridrecorddate, latitude, longitude,to_char(startdatetime, 'dd-mm-yyyy HH24:MI:SS') as startdatetime, to_char(enddatetime, 'dd-mm-yyyy HH24:MI:SS') as enddatetime, sysdatetime, noofrecords,  round (stoppage) as  stoppage_minut FROM rpt_daily_stoppage where deviceno=" + filterEntity.DeviceNo + " and ( to_char (recorddatetime, 'yyyy-MM-dd')  between '" + fromdate + "' and '" + todate + "') and round (stoppage)> 0 order by  to_char(recorddatetime, 'yyyy-mm-dd') desc limit 30");

                DataTable dtData;

                //open conn
                objDAL.CreateConnection();

                //---for history
                dtData = objDAL.FetchRecords(strSqlQry1.ToString());

                ////close conn
                objDAL.CloseConnection();

                //dtData.DefaultView.Sort = "recorddate asc";
                dtData = dtData.DefaultView.ToTable();

                DataView view = new DataView(dtData);
                DataTable distinctValues = view.ToTable(true, "recorddatetime");

                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "griddata";



                #region create dtstoppage coloumn
                DataTable dtstoppage = new DataTable();
                dtstoppage.Columns.Add("recorddatetime");
                for (int i = 0; i <= stoppageRange.Length - 1; i++)
                {
                    dtstoppage.Columns.Add(stoppageRange[i].ToString(), typeof(int));
                    stoppagecnt[i] = 0;
                }
                #endregion

                foreach (DataRow datarow in distinctValues.Rows)
                {
                    String filter = "recorddatetime='" + datarow["recorddatetime"] + "'";
                    DataRow[] dr;
                    dr = dtData.Select(filter);

                    #region create dtstoppage coloumn
                    foreach (DataRow dtrow in dr)
                    {
                        for (int i = 0; i <= stoppageRange.Length - 1; i++)
                        {
                            string[] strval = stoppageRange[i].ToString().Split('-');
                            if ((CommonUtils.ConvertToInt(dtrow["stoppage_minut"]) >= CommonUtils.ConvertToInt(strval[0].ToString())) && (CommonUtils.ConvertToInt(dtrow["stoppage_minut"]) <= CommonUtils.ConvertToInt(strval[1].ToString())))
                            {
                                stoppagecnt[i] = stoppagecnt[i] + 1;
                                break;
                            }
                        }

                    }
                    #endregion

                    DataRow data = dtstoppage.NewRow();
                    data["recorddatetime"] = datarow["recorddatetime"];
                    for (int i = 0; i <= stoppageRange.Length - 1; i++)
                    {
                        data[stoppageRange[i].ToString()] = CommonUtils.ConvertToInt(stoppagecnt[i].ToString());
                    }
                    dtstoppage.Rows.Add(data);

                    for (int i = 0; i <= stoppageRange.Length - 1; i++)
                    {
                        stoppagecnt[i] = 0;
                    }

                }
                dsData.Tables.Add(dtstoppage);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "stoppagedata";



                return dsData;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }


        public DataTable Getdistancesrpt(FilterEntity filterEntity)
        {

            try
            {
                //ErrorLog.LogToTripQuery("pass by fileter from date=" + filterEntity.sfDate, "Getdistancesrpt");
                //ErrorLog.LogToTripQuery("pass by fileter ToDate =" + filterEntity.stDate, "Getdistancesrpt");

                string fromdate = CommonUtils.GetstringFromatyyyy_MM_dd(filterEntity.sfDate);
                string todate = CommonUtils.GetstringFromatyyyy_MM_dd(filterEntity.stDate);

                //ErrorLog.LogToTripQuery("after convert fileter from date=" + fromdate, "Getdistancesrpt");
                //ErrorLog.LogToTripQuery("after convert fileter ToDate =" + todate, "Getdistancesrpt");

                strSqlQry1.Append("SELECT deviceno,recorddate as rd,to_char(recorddate, 'dd-mm ') as recorddate ,to_char(recorddate, 'dd-mm-yyyy ') as gridrecorddate , round (distance,1) as distance , round (distance_d,1) as distance_d ,  round (distance_n,1) as distance_n FROM public.dr_daily_parameter_wise_rating where deviceno=" + filterEntity.DeviceNo + " and ( to_char (recorddate, 'yyyy-MM-dd')  between '" + fromdate + "' and '" + todate + "')order by rd desc limit 30");
                //strSqlQry1.Append("SELECT * FROM public.dr_daily_parameter_wise_rating where deviceno=" + filterEntity.DeviceNo + " and  to_char (recorddate, 'yyyy-MM-dd') ='"+fromdate+"'");

                //ErrorLog.LogToTripQuery("Query " + strSqlQry1.ToString(), "Getdistancesrpt");


                DataTable dtData;

                //open conn
                objDAL.CreateConnection();

                //---for history
                dtData = objDAL.FetchRecords(strSqlQry1.ToString());
                //dtData.DefaultView.Sort = "recorddate asc";
                //dtData = dtData.DefaultView.ToTable();

                //dsData.Tables.Add(dtData);
                //dsData.Tables[dsData.Tables.Count - 1].TableName = "WeeklyDistance";


                ////close conn
                objDAL.CloseConnection();

                return dtData;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }


        }

        public DataTable Getday_nightHrsrpt(FilterEntity filterEntity)
        {

            try
            {
                string fromdate = CommonUtils.GetstringFromatyyyy_MM_dd(filterEntity.sfDate);
                string todate = CommonUtils.GetstringFromatyyyy_MM_dd(filterEntity.stDate);

                strSqlQry1.Append("SELECT deviceno,recorddatetime as rd,to_char(recorddatetime, 'dd-mm ') as recorddatetime , to_char(recorddatetime, 'dd-mm-yyyy ') as gridrecorddate, drivinghrs,  daydrivinghrs, nightdrivinghrs FROM  rpt_daily_agg_parameter where deviceno=" + filterEntity.DeviceNo + " and ( to_char (recorddatetime, 'yyyy-MM-dd')  between '" + fromdate + "' and '" + todate + "')order by rd desc limit 30 ");
                //strSqlQry1.Append("SELECT * FROM public.dr_daily_parameter_wise_rating where deviceno=" + filterEntity.DeviceNo + " and  to_char (recorddate, 'yyyy-MM-dd') ='"+fromdate+"'");

                DataTable dtData;

                //open conn
                objDAL.CreateConnection();

                //---for history
                dtData = objDAL.FetchRecords(strSqlQry1.ToString());
                //dtData.DefaultView.Sort = "recorddate asc";
                //dtData = dtData.DefaultView.ToTable();

                //dsData.Tables.Add(dtData);
                //dsData.Tables[dsData.Tables.Count - 1].TableName = "WeeklyDistance";


                ////close conn
                objDAL.CloseConnection();

                return dtData;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }


        }

        public DataTable Gettempreturerpt(FilterEntity filterEntity)
        {

            try
            {
                string fromdate = CommonUtils.GetstringFromatyyyy_MM_dd(filterEntity.sfDate);
                string todate = CommonUtils.GetstringFromatyyyy_MM_dd(filterEntity.stDate);

                strSqlQry1.Append("SELECT deviceno,recorddatetime as rd,to_char(recorddatetime, 'dd-mm') as recorddatetime, to_char(recorddatetime, 'dd-mm-yyyy ') as gridrecorddate, round ( avgcoolanttemperature) as avgcoolanttemperature, round ( avgcoolanttemperature_day) as avgcoolanttemperature_day,  round (avgcoolanttemperature_night) as avgcoolanttemperature_night FROM public.rpt_daily_agg_parameter where deviceno=" + filterEntity.DeviceNo + " and ( to_char (recorddatetime, 'yyyy-MM-dd')  between '" + fromdate + "' and '" + todate + "')order by rd desc limit 30 ");
                //strSqlQry1.Append("SELECT * FROM public.dr_daily_parameter_wise_rating where deviceno=" + filterEntity.DeviceNo + " and  to_char (recorddate, 'yyyy-MM-dd') ='"+fromdate+"'");

                DataTable dtData;

                //open conn
                objDAL.CreateConnection();

                //---for history
                dtData = objDAL.FetchRecords(strSqlQry1.ToString());
                //dtData.DefaultView.Sort = "recorddate asc";
                //dtData = dtData.DefaultView.ToTable();

                //dsData.Tables.Add(dtData);
                //dsData.Tables[dsData.Tables.Count - 1].TableName = "WeeklyDistance";


                ////close conn
                objDAL.CloseConnection();

                return dtData;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }


        }

        public DataSet Getspeedrpt(FilterEntity filterEntity)
        {

            try
            {
                string fromdate = CommonUtils.GetstringFromatyyyy_MM_dd(filterEntity.sfDate);
                string todate = CommonUtils.GetstringFromatyyyy_MM_dd(filterEntity.stDate);

                strSqlQry1.Append("SELECT deviceno,recorddatetime as rd,to_char(recorddatetime, 'dd-mm ') as recorddatetime,to_char(recorddatetime, 'dd-mm-yyyy ') as gridrecorddate,round(avgspeed) as avgspeed,round(avgspeed_day) as avgspeed_day, round(avgspeed_night) as avgspeed_night, round(maxspeed) as maxspeed, maxspeed_day, maxspeed_night FROM public.rpt_daily_agg_parameter where deviceno=" + filterEntity.DeviceNo + " and ( to_char (recorddatetime, 'yyyy-MM-dd')  between '" + fromdate + "' and '" + todate + "') order by rd desc limit 30 ");

                DataTable dtData;
                objDAL.CreateConnection();

                dtData = objDAL.FetchRecords(strSqlQry1.ToString());
                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "aggregate";
                strSqlQry1 = null;
                dtData.Dispose();
                ////
                string[] speedRange = new[] { "0-10", "11-20", "21-30", "31-40", "41-50", "51-60", "61-70", "71-80", "81-90", "91-100", "101-120", "121-140", "141-500" };
                string[] speedcolor = new[] { "#f4a641", "#f4b841", "#f4c741", "#f4dc41", "#e5f441", "#c4f441", "#acf441", "#76f441", "#41f44f", "#f4b241", "#f4b841", "#f4b841", "#f4a641" };
                int[] speedCnt = new int[speedRange.Length];

                strSqlQry1 = new StringBuilder();

                strSqlQry1.Append("SELECT deviceno, to_char(recorddatetime, 'dd-mm-yyyy') as recorddatetime , round(iot_hub.speed) as speed  from iot_hub ");
                strSqlQry1.Append(" where DeviceNo =" + filterEntity.DeviceNo.ToString() + " ");

                if (filterEntity.sfDate != null && filterEntity.stDate != null)
                {
                    strSqlQry1.Append(" and (recorddatetime>='" + fromdate + "' and recorddatetime<='" + todate + "')");
                }
                strSqlQry1.Append(" order by recorddatetime asc");

                strSqlQry1.Append(" limit 60000");

                dtData = objDAL.FetchRecords(strSqlQry1.ToString());
                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "speedrecord";


                dtData = dtData.DefaultView.ToTable();
                DataView view = new DataView(dtData);
                DataTable distinctValues = view.ToTable(true, "recorddatetime");

                #region create dtspeed coloumn
                DataTable dtspeed = new DataTable();
                dtspeed.Columns.Add("recorddatetime");
                for (int i = 0; i <= speedRange.Length - 1; i++)
                {
                    dtspeed.Columns.Add(speedRange[i].ToString(), typeof(int));
                    speedCnt[i] = 0;
                }
                #endregion

                foreach (DataRow datarow in distinctValues.Rows)
                {
                    String filter = "recorddatetime='" + datarow["recorddatetime"] + "'";
                    DataRow[] dr;
                    dr = dtData.Select(filter);
                    int intRcount = dr.Length;
                    DataRow data = dtspeed.NewRow();
                    for (int i = 0; i <= speedRange.Length - 1; i++)
                    {
                        string[] strval = speedRange[i].ToString().Split('-');

                        DataRow[] drCount = dtData.Select("recorddatetime='" + datarow["recorddatetime"] + "' and speed>=" + CommonUtils.ConvertToInt(strval[0].ToString()) + " and speed<=" + CommonUtils.ConvertToInt(strval[1].ToString()));
                        data["recorddatetime"] = datarow["recorddatetime"];
                        decimal speedPercentage = (100 * drCount.Length) / intRcount;
                        data[speedRange[i].ToString()] = speedPercentage;
                    }
                    dtspeed.Rows.Add(data);
                }
                dsData.Tables.Add(dtspeed);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "speeddata";

                objDAL.CloseConnection();
                dtData = null;
                distinctValues = null;
                dtspeed = null;

                return dsData;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }


        }

        public DataTable Gettriplistrpt(FilterEntity filterEntity)
        {


            try
            {
                objDAL.CreateConnection();
                string Url = "";

                strSqlQry1.Append(@"SELECT trip_planning_master.trip_id, trip_name,category, trip_from, trip_to,to_char(trip_start_time, 'dd-mm-yyyy HH24:MI:SS') as  trip_start_time, to_char( trip_end_time, 'dd-mm-yyyy HH24:MI:SS') as trip_end_time , round(coalesce(dr_overall_trip_rating.rating, 0)) as rating ,round(coalesce(dr_overall_trip_rating.distance, 0),1) as distance ,round(coalesce(dr_overall_trip_rating.distance_d,0)) as distance_d, round(coalesce(dr_overall_trip_rating.distance_n,0)) as distance_n,user_master.vehicle_no,user_master.deviceno,trip_planning_master.m_distance ,uploadfile FROM trip_planning_master left outer join  dr_overall_trip_rating on trip_planning_master.trip_id=dr_overall_trip_rating.trip_id inner join user_master on trip_planning_master.user_id=user_master.user_id ");
                if (filterEntity.s_role_Id == 1)
                {
                    strSqlQry1.Append("  where trip_planning_master.user_id=" + filterEntity.s_user_id + " order by trip_planning_master.trip_start_time desc limit 200");
                }
                else if (filterEntity.s_role_Id == 2)
                {
                    //for admin
                    strSqlQry1.Append(" where user_master.deviceno in (SELECT deviceno FROM user_master where o_id in (SELECT o_id FROM user_master where user_id =" + filterEntity.s_user_id + ")) order by trip_planning_master.trip_start_time desc limit 200");
                }
                //else
                //{
                //    //for sys admin
                //    strSqlQry1.Append(" where user_master.isactive=True and iot_live.deviceno in(SELECT deviceno FROM user_master where isactive=True) order by iot_live.recorddatetime desc");
                //}

                DataTable dtTrips = objDAL.FetchRecords(strSqlQry1.ToString());
                foreach (DataRow Triprow in dtTrips.Rows)
                {
                    string pathname = Triprow["uploadfile"].ToString();
                    if (pathname != "")
                    {
                        string strUrlPath = CommonUtils.getUrlPath(Triprow["uploadfile"].ToString(), "tripvideodata", out Url);
                        Triprow["uploadfile"] = Url;
                    }
                    else
                    {
                        Triprow["uploadfile"] = "";

                    }

                }

                objDAL.CloseConnection();

                return dtTrips;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public DataTable GetHB_HArpt(FilterEntity filterEntity)
        {

            try
            {
                string fromdate = CommonUtils.GetstringFromatyyyy_MM_dd(filterEntity.sfDate);
                string todate = CommonUtils.GetstringFromatyyyy_MM_dd(filterEntity.stDate);

                strSqlQry1.Append("SELECT deviceno,recorddatetime as rd,to_char(recorddatetime, 'dd-mm') as recorddatetime,to_char(recorddatetime, 'dd-mm-yyyy ') as gridrecorddate, hardacceleration, hardacceleration_day, hardacceleration_night, hardbraking, hardbraking_day, hardbraking_night, cornerpacket, cornerpacket_day, cornerpacket_night, overspeedstarted,overspeedstarted_day,overspeedstarted_night,mcall,mcall_day,mcall_night FROM rpt_daily_agg_parameter where deviceno=" + filterEntity.DeviceNo + " and ( to_char (recorddatetime, 'yyyy-MM-dd')  between '" + fromdate + "' and '" + todate + "')order by rd desc limit 30 ");
                //strSqlQry1.Append("SELECT * FROM public.dr_daily_parameter_wise_rating where deviceno=" + filterEntity.DeviceNo + " and  to_char (recorddate, 'yyyy-MM-dd') ='"+fromdate+"'");

                DataTable dtData;

                //open conn
                objDAL.CreateConnection();

                //---for history
                dtData = objDAL.FetchRecords(strSqlQry1.ToString());
                //dtData.DefaultView.Sort = "recorddate asc";
                //dtData = dtData.DefaultView.ToTable();

                //dsData.Tables.Add(dtData);
                //dsData.Tables[dsData.Tables.Count - 1].TableName = "WeeklyDistance";


                ////close conn
                objDAL.CloseConnection();

                return dtData;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }


        }

        public DataTable GetHA_timerpt(FilterEntity filterEntity)
        {

            try
            {
                string fromdate = CommonUtils.GetstringFromatyyyy_MM_dd(filterEntity.sfDate);
                string todate = CommonUtils.GetstringFromatyyyy_MM_dd(filterEntity.stDate);

                strSqlQry1.Append("SELECT  deviceno,recorddatetime as rd,to_char(recorddatetime, 'dd-mm-yyyy HH24:MI:SS') as recorddatetime,coalesce(hardacceleration,0) as hardacceleration ,coalesce(hardbraking,0) as hardbraking,coalesce(cornerpacket,0) as cornerpacket,coalesce(mcall,0) as mcall,coalesce(overspeedstarted,0) as overspeedstarted  FROM iot_hub  where deviceno=" + filterEntity.DeviceNo + " and ( to_char (recorddatetime, 'yyyy-MM-dd')  between '" + fromdate + "' and '" + todate + "') order by rd desc");
                //strSqlQry1.Append("SELECT * FROM public.dr_daily_parameter_wise_rating where deviceno=" + filterEntity.DeviceNo + " and  to_char (recorddate, 'yyyy-MM-dd') ='"+fromdate+"'");

                DataTable dtData;

                //open conn
                objDAL.CreateConnection();

                //---for history
                dtData = objDAL.FetchRecords(strSqlQry1.ToString());
                //dtData.DefaultView.Sort = "recorddate asc";
                //dtData = dtData.DefaultView.ToTable();

                //dsData.Tables.Add(dtData);
                //dsData.Tables[dsData.Tables.Count - 1].TableName = "WeeklyDistance";


                ////close conn
                objDAL.CloseConnection();

                return dtData;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }


        }


        //






    }
}
