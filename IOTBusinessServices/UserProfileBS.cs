﻿using System;
//using System.Collections.Generic;
//using System.Linq;
using System.Net;
using System.Data;
using IOTDataModel;
using IOTBusinessEntities;
using System.Threading.Tasks;


namespace IOTBusinessServices
{
    public class UserProfileBS : IDisposable
    {
        private UserProfileDM objUserProfileDM;

        public UserProfileBS()
        {
            objUserProfileDM = new UserProfileDM();
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                if (objUserProfileDM != null) { objUserProfileDM.Dispose(); }
            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~UserProfileBS()
        {
            Dispose(false);
        }

        #endregion


        //public Task<bool> CreateUserProfile(UserEntity userEntity)
        //{
        //    return Task.FromResult(objUserProfileDM.CreateUserProfile(userEntity));
        //}

        public Task<Int32> CreateUserProfile(UserEntity userEntity)
        {
            return Task.FromResult(objUserProfileDM.CreateUserProfile(userEntity));
        }



        public Task<DataTable> GetUserList(FilterEntity filterEntity)
        {
            using (DataTable dtUser = objUserProfileDM.GetUserList(filterEntity))
            {
                return Task.FromResult(dtUser);
            }
        }

        public Task<DataTable> GetUserById(FilterEntity filterEntity)
        {
            using (DataTable dtUser = objUserProfileDM.GetUserById(filterEntity))
            {
                return Task.FromResult(dtUser);
            }
        }

        public Task<DataTable> UpdateUserProfile(UserEntity userEntity)
        {
           // return Task.FromResult(objUserProfileDM.UpdateUserProfile(userEntity));
            using (DataTable dtUser = objUserProfileDM.UpdateUserProfile(userEntity))
            {
                return Task.FromResult(dtUser);
            }
        }

        public Task<bool> UpdateUserProfilePath(UserEntity userEntity)
        {
            return Task.FromResult(objUserProfileDM.UpdateUserProfilePath(userEntity));
        }

        public Task<bool> UpdatePremiumData(UserEntity userEntity)
        {
            return Task.FromResult(objUserProfileDM.UpdatePremiumData(userEntity));
        }
            
        
        public Task<bool> DeleteUserProfile(UserEntity userEntity)
        {
            return Task.FromResult(objUserProfileDM.DeleteUserProfile(userEntity));
        }

        public Task<DataTable> GetRoleList(FilterEntity filterEntity)
        {
            using (DataTable dtRole = objUserProfileDM.GetRoleList(filterEntity))
            {
                return Task.FromResult(dtRole);
            }
        }

        public Task<bool> ChangeUserPwd(UserEntity userEntity)
        {
            return Task.FromResult(objUserProfileDM.ChangeUserPwd(userEntity));
        }

        public bool CheckUserAvailability(UserEntity userEntity)
        {
            return objUserProfileDM.CheckUserAvailability(userEntity);
        }
        public bool CheckEmailAvailability(UserEntity userEntity)
        {
            return objUserProfileDM.CheckEmailAvailability(userEntity);
        }
        public bool CheckMobileAvailability(UserEntity userEntity)
        {
            return objUserProfileDM.CheckMobileAvailability(userEntity);
        }
        public bool CheckMobileAvailabilityforupdate(UserEntity userEntity)
        {
            return objUserProfileDM.CheckMobileAvailabilityforupdate(userEntity);
        }

        public Task<bool> UserlougOutLog(UserEntity userEntity)
        {
            return Task.FromResult(objUserProfileDM.UserlougOutLog(userEntity));
        }

        public Task<DataSet> GetOverallRating(UserEntity userEntity)
        {
            return Task.FromResult(objUserProfileDM.GetOverallRating(userEntity));
        }
        public Task<bool> ForgotPassword_email(UserEntity userEntity)
        {
            return Task.FromResult(objUserProfileDM.ForgotPassword_email(userEntity));
        }
        public UserEntity CheckSocialUserAvailability(UserEntity userEntity)
        {
            return objUserProfileDM.CheckSocialUserAvailability(userEntity);
        }

        public bool CheckEmailMobileAvailablity(UserEntity userEntity)
        {
            return objUserProfileDM.CheckEmailMobileAvailablity(userEntity);
        }
        
    }
}