﻿using System;
using System.Net;
using System.Data;
using IOTDataModel;
using IOTBusinessEntities;
using System.Threading.Tasks;

namespace IOTBusinessServices
{
    public class GeofenceBS : IDisposable
    {
        private GeofenceDM objGeofenceDM;

        public GeofenceBS()
        {
            objGeofenceDM = new GeofenceDM();
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                if (objGeofenceDM != null) { objGeofenceDM.Dispose(); }
            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~GeofenceBS()
        {
            Dispose(false);
        }

        #endregion

        public Task<bool> CreateGeofence(GeofenceEntity geofenceEntity)
        {
            return Task.FromResult(objGeofenceDM.CreateGeofence(geofenceEntity));
        }

        public Task<DataTable> GetGeofenceList(GeofenceEntity geofenceEntity)
        {
            using (DataTable dtGeofence = objGeofenceDM.GetGeofenceList(geofenceEntity))
            {
                return Task.FromResult(dtGeofence);
            }
        }

        public Task<DataTable> GetGeofenceById(GeofenceEntity geofenceEntity)
        {
            using (DataTable dtGeofence = objGeofenceDM.GetGeofenceById(geofenceEntity))
            {
                return Task.FromResult(dtGeofence);
            }
        }

        public Task<bool> UpdateGeofence(GeofenceEntity geofenceEntity)
        {
            return Task.FromResult(objGeofenceDM.UpdateGeofence(geofenceEntity));
        }

        public Task<bool> DeleteGeofence(GeofenceEntity geofenceEntity)
        {
            return Task.FromResult(objGeofenceDM.DeleteGeofence(geofenceEntity));
        }

        public bool CheckGeofenceAvailability(GeofenceEntity geofenceEntity)
        {
            return objGeofenceDM.CheckGeofenceAvailability(geofenceEntity);
        }

        public bool CheckCheckGeofenceAvailabilityforupdate(GeofenceEntity geofenceEntity)
        {
            return objGeofenceDM.CheckCheckGeofenceAvailabilityforupdate(geofenceEntity);
        }

        public Task<bool> UpdateGeofenceUserMapping(GeofenceEntity geofenceEntity)
        {
            return Task.FromResult(objGeofenceDM.UpdateGeofenceUserMapping(geofenceEntity));
        }


        public Task<DataTable> GetAssignUnassignGeofenceList(FilterEntity filterEntity)
        {
            using (DataTable dtGeofence = objGeofenceDM.GetAssignUnassignGeofenceList(filterEntity))
            {
                return Task.FromResult(dtGeofence);
            }
        }
    }
}
