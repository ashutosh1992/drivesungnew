﻿using IOTBusinessEntities;
using IOTDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOTBusinessServices
{
  public  class MobileErrorLogBS: IDisposable
    {
        private MobileErrorLogDM objMobileErrorLogDM;
       
        public MobileErrorLogBS()
        {
            objMobileErrorLogDM = new  MobileErrorLogDM();
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                if (objMobileErrorLogDM != null) { objMobileErrorLogDM.Dispose(); }
            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~MobileErrorLogBS()
        {
            Dispose(false);
        }

        #endregion

        public Task<bool> SaveMob_ErrorLog(MobileErrorLogEntity mobileErrorLogEntity)
        {
            return Task.FromResult(objMobileErrorLogDM.SaveMob_ErrorLog(mobileErrorLogEntity));
        }
    
    }
}
