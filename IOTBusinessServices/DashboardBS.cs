﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using IOTBusinessEntities;
using IOTDataModel;

namespace IOTBusinessServices
{
    public class DashboardBS : IDisposable
    {
        private DashboardDM objDashboardDM;

        public DashboardBS()
        {
            objDashboardDM = new DashboardDM();
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                if (objDashboardDM != null) { objDashboardDM.Dispose(); }
            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~DashboardBS()
        {
            Dispose(false);
        }

        #endregion


        public Task<DataSet> GetVehicleList(FilterEntity filterEntity)
        {
            using (DataSet dsData = objDashboardDM.GetVehicleList(filterEntity))
            {
                return Task.FromResult(dsData);
            }
        }

        public Task<DataSet> GetDashBoardList(FilterEntity filterEntity)
        {
            using (DataSet dsData = objDashboardDM.GetDashBoardList(filterEntity))
            {
                return Task.FromResult(dsData);
            }
        }

        public Task<DataSet> GetLiveTrackingData(FilterEntity filterEntity)
        {
            using (DataSet dsData = objDashboardDM.GetLiveTrackingData(filterEntity))
            {
                return Task.FromResult(dsData);
            }

        }


        public Task<DataSet> GetLiveOBDData(FilterEntity filterEntity)
        {
            using (DataSet dsData = objDashboardDM.GetLiveOBDData(filterEntity))
            {
                return Task.FromResult(dsData);
            }

        }

        public Task<DataTable> GetLiveData(FilterEntity filterEntity)
        {
            using (DataTable dtData = objDashboardDM.GetLiveData(filterEntity))
            {
                return Task.FromResult(dtData);
            }

        }


        public Task<DataSet> GetTripPlayData(FilterEntity filterEntity)
        {
            using (DataSet dsData = objDashboardDM.GetTripPlayData(filterEntity))
            {
                return Task.FromResult(dsData);
            }

        }


        public Task<DataSet> GetGraphData(FilterEntity filterEntity)
        {
            using (DataSet dsData = objDashboardDM.GetGraphData(filterEntity))
            {
                return Task.FromResult(dsData);
            }
        }

        public Task<DataSet> GetMapAllVehiclesData(FilterEntity filterEntity)
        {
            using (DataSet dsData = objDashboardDM.GetMapAllVehiclesData(filterEntity))
            {
                return Task.FromResult(dsData);
            }
        }

        public Task<DataSet> GetBlotterViewData(FilterEntity filterEntity)
        {
            using (DataSet dsData = objDashboardDM.GetBlotterViewData(filterEntity))
            {
                return Task.FromResult(dsData);
            }
        }

        public Task<DataSet> GetSubHeaderCount(FilterEntity filterEntity)
        {
            using (DataSet dsData = objDashboardDM.GetSubHeaderCount(filterEntity))
            {
                return Task.FromResult(dsData);
            }
        }


        //public Task<DataSet> GetBlotterViewAdminfilterData(FilterEntity filterEntity)
        //{
        //    using (DataSet dsData = objDashboardDM.GetBlotterViewAdminfilterData(filterEntity))
        //    {
        //        return Task.FromResult(dsData);
        //    }

        //}

    }
}
