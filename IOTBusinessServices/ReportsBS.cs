﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using IOTBusinessEntities;
using IOTDataModel;

namespace IOTBusinessServices
{
    public class ReportsBS : IDisposable
    {
        private ReportsDM objReportsDM;

        public ReportsBS()
        {
            objReportsDM = new ReportsDM();
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                if (objReportsDM != null) { objReportsDM.Dispose(); }
            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~ReportsBS()
        {
            Dispose(false);
        }

        #endregion

        public Task<DataSet> GetDriverperformanceData(FilterEntity filterEntity)
        {
            using (DataSet dsData = objReportsDM.GetDriverperformanceData(filterEntity))
            {
                return Task.FromResult(dsData);
            }
        }

        public Task<DataSet> GetAggregateData(FilterEntity filterEntity)
        {
            using (DataSet dsData = objReportsDM.GetAggregateData(filterEntity))
            {
                return Task.FromResult(dsData);
            }
        }

        public Task<DataSet> GetStoppage(FilterEntity filterEntity)
        {
            using (DataSet dsData = objReportsDM.GetStoppage(filterEntity))
            {
                return Task.FromResult(dsData);
            }
        }

        public Task<DataTable> Getdistancesrpt(FilterEntity filterEntity)
        {
            using (DataTable dtData = objReportsDM.Getdistancesrpt(filterEntity))
            {
                return Task.FromResult(dtData);
            }
        }

        public Task<DataTable> Gettempreturerpt(FilterEntity filterEntity)
        {
            using (DataTable dtData = objReportsDM.Gettempreturerpt(filterEntity))
            {
                return Task.FromResult(dtData);
            }
        }

        public Task<DataSet> Getspeedrpt(FilterEntity filterEntity)
         {
             using (DataSet dtData = objReportsDM.Getspeedrpt(filterEntity))
               {
                    return Task.FromResult(dtData);
               }
         }

        public Task<DataTable> Gettriplistrpt(FilterEntity filterEntity)
         {
             using (DataTable dtData = objReportsDM.Gettriplistrpt(filterEntity))
               {
                    return Task.FromResult(dtData);
               }
         }
        public Task<DataTable> GetHB_HArpt(FilterEntity filterEntity)
         {
             using (DataTable dtData = objReportsDM.GetHB_HArpt(filterEntity))
               {
                    return Task.FromResult(dtData);
               }
         }
        public Task<DataTable> GetHA_timerpt(FilterEntity filterEntity)
         {
             using (DataTable dtData = objReportsDM.GetHA_timerpt(filterEntity))
               {
                    return Task.FromResult(dtData);
               }
         }

        public Task<DataTable> Getday_nightHrsrpt(FilterEntity filterEntity)
         {
             using (DataTable dtData = objReportsDM.Getday_nightHrsrpt(filterEntity))
               {
                    return Task.FromResult(dtData);
               }
         }
        
        
        

        
        
        


        
    }
}
