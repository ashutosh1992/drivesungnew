﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOTBusinessEntities;
using IOTUtilities;
using System.Data;

namespace IOTBusinessServices
{
    public class ValidateEntity
    {
        public static string ValidateUserEntity(UserEntity userEntity,
            bool user_id = false,
            bool role_Id = false,
            bool user_name = false,
            bool first_name = false,
            bool middle_name = false,
            bool last_name = false,
            bool date_of_birth = false,
            bool gender = false,
            bool address1 = false,
            bool address2 = false,
            bool address3 = false,
            bool mobileno = false,
            bool Checkemailavailablity = false,
            bool email_id1=false,
            bool email_id2 = false,
            bool profile_picture_uri = false,
            bool deviceno = false,
            bool vehicle_no = false,
            bool license_no = false,
            bool license_expiry_date = false,
            bool registration_no = false,
            bool vehicle_manufacturer = false,
            bool vehicle_color = false,
            bool vehicle_type = false,
            bool user_login_type = false,
            bool insurance_mapping_key = false,
            bool user_pwd = false,
            bool o_id = false,
            bool transaction_id =false,
            bool premium =false,
            bool premium_validity=false,
            bool s_user_id = false,
            bool s_role_Id = false,
            bool CheckUserAvailability = false,
            bool CheckForgot_Password = false,
            bool Checkmobilenoavailablityforupdate=false,
            bool CheckEmailMobileAvailablity=false 
            //bool CheckMobile_No=false

            )
        {

            string strResult = "";

            // check  userEntity
            if (userEntity == null)
            {
                //return "empty entity not allowed.";
                return MessageCode.ErrorMsgInJson(MsgCode.userEntity);
            }

            // check  user_id
            if (user_id)
            {
                if (userEntity.user_id == 0)
                {
                    //  return "user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.user_id);
                }

            }

            // check  user_name
            if (user_name)
            {
                if (userEntity.user_name == null || userEntity.user_name == "")
                {
                    // return "user_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.user_name);
                }
                if (CheckUserAvailability)
                {
                    using (UserProfileBS objUserProfileBS = new UserProfileBS())
                    {
                        if (objUserProfileBS.CheckUserAvailability(userEntity))
                        {
                            // return "username already available.";
                            return MessageCode.ErrorMsgInJson(MsgCode.CheckUserAvailability);
                        }
                    }
                }

            }
            if (CheckForgot_Password)
                {
                    if ((userEntity.user_name == null || userEntity.user_name == "") && (userEntity.mobileno == null) && (userEntity.email_id1 ==null))
                        {
                            // return "user_name required.";
                            return MessageCode.ErrorMsgInJson(MsgCode.atlest_one);
                        }
                    else
                    {
                        if (userEntity.user_name != null && userEntity.user_name != "")
                        {
                            using (UserProfileBS objUserProfileBS = new UserProfileBS())
                            {
                                if (!objUserProfileBS.CheckUserAvailability(userEntity))
                                {
                                    // return "username already available.";
                                    return MessageCode.ErrorMsgInJson(MsgCode.CheckUserAvailability);
                                }
                                else
                                {
                                    return "Email";
                                }
                            }

                        }
                        if (userEntity.email_id1 != null)
                        {
                             using (UserProfileBS objUserProfileBS = new UserProfileBS())
                                {
                                    if (!objUserProfileBS.CheckEmailAvailability(userEntity))
                                    {
                                        // return "username already available.";
                                        return MessageCode.ErrorMsgInJson(MsgCode.CheckEmailMatch);
                                    }
                                    else
                                    {
                                        return "Email";
                                    }
                                }
                         }
                        if (userEntity.mobileno != null)
                        {
                            using (UserProfileBS objUserProfileBS = new UserProfileBS())
                            {
                                if (!objUserProfileBS.CheckMobileAvailability(userEntity))
                                {
                                    // return "username already available.";
                                    return MessageCode.ErrorMsgInJson(MsgCode.CheckMobileMatch);
                                }
                                else
                                {
                                    return "Mobile";
                                }
                            }
                        }
                       
                       
                    }

              }
          

              // check  role_Id
            if (role_Id)
            {
                if (userEntity.role_Id == null || userEntity.role_Id == 0)
                {
                    // return "role_Id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.role_Id);
                }
            }


            // check  first_name
            if (first_name)
            {
                if (userEntity.first_name == null || userEntity.first_name == "")
                {
                    //return "first_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.first_name);
                }
            }

            // check  middle_name
            if (middle_name)
            {
                if (userEntity.middle_name == null || userEntity.middle_name == "")
                {
                    //return "middle_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.middle_name);
                }
            }

            // check  last_name
            if (last_name)
            {
                if (userEntity.last_name == null || userEntity.last_name == "")
                {
                    //return "last_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.last_name);
                }
            }

            // check  date_of_birth
            if (date_of_birth)
            {
                if (userEntity.date_of_birth == null)
                {
                    // return "date_of_birth required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.date_of_birth);
                }
            }

            // check  gender
            if (gender)
            {
                if (userEntity.gender == null)
                {
                    // return "gender required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.gender);
                }
            }



            // check  address1
            if (address1)
            {
                if (userEntity.address1 == null)
                {
                    //  return "address1 required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.address1);
                }
            }

            // check  middle_name
            if (address2)
            {
                if (userEntity.address2 == null)
                {
                    // return "address2 required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.address2);
                }
            }

            // check  address3
            if (address3)
            {
                if (userEntity.address3 == null)
                {
                    //   return "address3 required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.address3);
                }
            }

            // check  mobileno
            if (mobileno)
            {
                //if (userEntity.social_id == null || userEntity.social_id == "")
                //{
                     if (userEntity.mobileno == null || userEntity.mobileno == "")
                        {
                            // return "mobileno1 required.";
                            return MessageCode.ErrorMsgInJson(MsgCode.mobileno);
                        }
                        //else 
                        //{
                        //    using (UserProfileBS objUserProfileBS = new UserProfileBS())
                        //    {
                        //        if (objUserProfileBS.CheckMobileAvailability(userEntity))
                        //        {
                        //            // return "username already available.";
                        //            return MessageCode.ErrorMsgInJson(MsgCode.CheckMobileAvailability);
                        //        }
                     
                        //    }
                        //}
                //}
            }
            //check availablity for mobile 
            //if (Checkmobilenoavailablityforupdate)
            //{
            //    //if (userEntity.social_id == null || userEntity.social_id == "")
            //    //{


            //        if (userEntity.mobileno == null || userEntity.mobileno=="")
            //        {
            //            // return "mobileno1 required.";
            //            return MessageCode.ErrorMsgInJson(MsgCode.mobileno);
            //        }
            //        else
            //        {
            //            using (UserProfileBS objUserProfileBS = new UserProfileBS())
            //            {
            //                if (objUserProfileBS.CheckMobileAvailabilityforupdate(userEntity))
            //                {
            //                    // return "username already available.";
            //                    return MessageCode.ErrorMsgInJson(MsgCode.CheckMobileAvailability);
            //                }

            //            }
            //        }
            //    //}
            //}

            // check  email_id1
            //if (Checkemailavailablity)
            //{
            //    if (userEntity.email_id1 != null && userEntity.email_id1 !="")
            //    {
              
            //        using (UserProfileBS objUserProfileBS = new UserProfileBS())
            //        {
            //            if (objUserProfileBS.CheckEmailAvailability(userEntity))
            //            {
            //                // return "username already available.";
            //                return MessageCode.ErrorMsgInJson(MsgCode.CheckEmailAvailability);
            //            }
                       
            //        }
            //    }
            //    else
            //    {
            //        return MessageCode.ErrorMsgInJson(MsgCode.email_id1);
            //    }
            //}

            // check  email_id1
            if (email_id1)
            {
                if (userEntity.email_id1 == null || userEntity.email_id1 == "")
                {
                    return MessageCode.ErrorMsgInJson(MsgCode.email_id1);
                }

            }

            // check  email_id2
            if (email_id2)
            {
                if (userEntity.email_id2 == null)
                {
                    // return "email_id2 required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.email_id2);
                }
            }

            // check  profile_picture_uri
            if (profile_picture_uri)
            {
                if (userEntity.profile_picture_uri == null)
                {
                    // return "profile_picture_uri required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.profile_picture_uri);
                }
            }

            // check  deviceno
            if (deviceno)
            {
                if (userEntity.deviceno == null)
                {
                    // return "profile_picture_uri required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.deviceno);
                }
            }

            // check  vehicle_no
            if (vehicle_no)
            {
                if (userEntity.vehicle_no == null)
                {
                    // return "profile_picture_uri required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.vehicle_no);
                }
            }

            // check  license_no
            if (license_no)
            {
                if (userEntity.license_no == null)
                {
                    //  return "license_no required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.license_no);
                }
            }

            // check  license_expiry_date
            if (license_expiry_date)
            {
                if (userEntity.license_expiry_date == null)
                {
                    // return "license_expiry_date required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.license_expiry_date);
                }
            }

            // check registration_no
            if (registration_no)
            {
                if (userEntity.registration_no == null)
                {
                    //  return "license_no required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.registration_no);
                }
            }

            // check vehicle_manufacturer
            if (vehicle_manufacturer)
            {
                if (userEntity.vehicle_manufacturer == null)
                {
                    //  return "license_no required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.vehicle_manufacturer);
                }
            }

            // check vehicle_color
            if (vehicle_color)
            {
                if (userEntity.vehicle_color == null)
                {
                    //  return "license_no required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.vehicle_color);
                }
            }


            // check vehicle_type
            if (vehicle_type)
            {
                if (userEntity.vehicle_type == null)
                {
                    //  return "license_no required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.vehicle_type);
                }
            }

            // check user_login_type
            if (user_login_type)
            {
                if (userEntity.user_login_type == null)
                {
                    //  return "license_no required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.user_login_type);
                }
            }

            // check insurance_mapping_key
            if (insurance_mapping_key)
            {
                if (userEntity.insurance_mapping_key == null)
                {
                    //  return "license_no required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.insurance_mapping_key);
                }
            }

            // check  user_pwd
            if (user_pwd)
            {
               if (userEntity.social_id == null || userEntity.social_id == "")
                {
                    if (userEntity.user_pwd == null || userEntity.user_pwd =="" )
                    {
                        // return "user_pwd required.";
                        return MessageCode.ErrorMsgInJson(MsgCode.user_pwd);
                    }
                }
            }

            // check  o_id
            if (o_id)
            {
                if (userEntity.o_id == null || userEntity.o_id == 0)
                {
                    // return "o_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.o_id);
                }
            }
            //check transaction_id
            if (transaction_id)
            {
                if (userEntity.transaction_id == null || userEntity.transaction_id == "")
                {
                    // return "o_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.transaction_id);
                }
            }

            //check premium
            if (premium)
            {
                if (userEntity.premium == null )
                {
                    // return "o_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.premium);
                }
            }

           

            //check premium
            if (premium_validity)
            {
                if (userEntity.premium_validity == null)
                {
                    // return "o_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.premium_validity);
                }
            }

            // check  s_user_id
            if (s_user_id)
            {
                if (userEntity.s_user_id == null || userEntity.s_user_id == 0)
                {
                    // return "s_user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_user_id);
                }
            }

            // check  s_user_role
            if (s_role_Id)
            {
                if (userEntity.s_role_Id == null || userEntity.s_role_Id == 0)
                {
                    //return "s_role_Id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_role_Id);
                }
            }
            if (CheckEmailMobileAvailablity)
            {
                if ((userEntity.email_id1 != null || userEntity.email_id1 != "") && (userEntity.mobileno != null || userEntity.mobileno != ""))
                {
                    using (UserProfileBS objUserProfileBS = new UserProfileBS())
                    {
                        if (objUserProfileBS.CheckEmailMobileAvailablity(userEntity))
                        {
                            // return "username already available.";
                            return MessageCode.ErrorMsgInJson(MsgCode.CheckEmailMobileAvailablity);
                        }
                    }

                }
            }
            return strResult;
        }


        public static string ValidateOrganization(OrganizationEntity organizationEntity,
        bool o_id = false,
        bool o_code = false,
        bool o_name = false,
        bool o_type = false,
        bool address1 = false,
        bool address2 = false,
        bool address3 = false,
        bool pincode_no = false,
        bool email_id1 = false,
        bool email_id2 = false,
        bool contact_no1 = false,
        bool contact_no2 = false,
        bool s_user_id = false,
        bool s_role_Id = false,
        bool checkOrgContactEntities = false,
        bool checkOrgAvailability = false
        )
        {
            string strResult = "";

            // check  organizationEntity
            if (organizationEntity == null)
            {
                //return "empty organization entity not allowed.";
                return MessageCode.ErrorMsgInJson(MsgCode.orgEntityEmpt);
            }

            // check  o_id
            if (o_id)
            {
                if (organizationEntity.o_id == 0)
                {
                    // return "o_id required";
                    return MessageCode.ErrorMsgInJson(MsgCode.o_id);

                }
            }

            // check  o_code
            if (o_code)
            {
                if (organizationEntity.o_code == null || organizationEntity.o_code == "")
                {
                    //return "o_code required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.o_code);
                }
                if (checkOrgAvailability)
                {
                    //using (UserProfileBS objUserProfileBS = new UserProfileBS())
                    //{
                    //    if (objUserProfileBS.CheckUserAvailability(userEntity))
                    //    {
                    //        return "o_code already available.";
                    //        //return ""; //setting for load test
                    //    }
                    //}
                }

            }


            // check  o_name
            if (o_name)
            {
                if (organizationEntity.o_name == null || organizationEntity.o_name == "")
                {
                    //return "o_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.o_name);
                }
            }

            // check  o_type
            if (o_type)
            {
                if (organizationEntity.o_type == null || organizationEntity.o_type == "")
                {
                    //return "o_type required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.o_type);
                }
            }

            // check  address1
            if (address1)
            {
                if (organizationEntity.address1 == null || organizationEntity.address1 == "")
                {
                    //   return "address1 required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.address1);
                }
            }

            // check  address2
            if (address2)
            {
                if (organizationEntity.address2 == null || organizationEntity.address2 == "")
                {
                    // return "address2 required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.address2);
                }
            }

            // check  address3
            if (address3)
            {
                if (organizationEntity.address3 == null || organizationEntity.address3 == "")
                {
                    //   return "address3 required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.address3);
                }
            }

            // check  pincode_no
            if (pincode_no)
            {
                if (organizationEntity.pincode_no == null || organizationEntity.pincode_no == "")
                {
                    //return "pincode_no required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.pincode_no);
                }
            }

            // check  email_id1
            if (email_id1)
            {
                if (organizationEntity.email_id1 == null || organizationEntity.email_id1 == "")
                {
                    // return "email_id1 required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.email_id1);
                }
            }

            // check  o_name
            if (email_id2)
            {
                if (organizationEntity.email_id2 == null || organizationEntity.email_id2 == "")
                {
                    // return "email_id2 required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.email_id2);
                }
            }

            // check  contact_no1
            if (contact_no1)
            {
                if (organizationEntity.contact_no1 == null || organizationEntity.contact_no1 == "")
                {
                    //   return "contact_no1 required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.contact_no1);
                }
            }

            // check  contact_no2
            if (contact_no2)
            {
                if (organizationEntity.contact_no2 == null || organizationEntity.contact_no2 == "")
                {
                    // return "contact_no2 required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.contact_no2);
                }
            }




            // check  s_user_id
            if (s_user_id)
            {
                if (organizationEntity.s_user_id == null || organizationEntity.s_user_id == 0)
                {
                    // return "s_user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_user_id);
                }
            }

            // check  s_user_role
            if (s_role_Id)
            {
                if (organizationEntity.s_role_Id == null || organizationEntity.s_role_Id == 0)
                {
                    //  return "s_role_Id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_role_Id);
                }
            }
            return strResult;
        }

        public static string ValidateConfigSetting(ConfigSettingEntity configSettingEntity,
             bool o_id = false,
             bool s_user_id = false,
             bool s_role_Id = false,
             bool checkConfigSettingAvailability = false
        )
        {
            string strResult = "";

            // check  organizationEntity
            if (configSettingEntity == null)
            {
                //return "empty organization entity not allowed.";
                return MessageCode.ErrorMsgInJson(MsgCode.cs_configSettingEntityEmpt);
            }

            // check  o_id
            if (o_id)
            {
                if (configSettingEntity.o_id == 0)
                {
                    // return "o_id required";
                    return MessageCode.ErrorMsgInJson(MsgCode.cs_o_id);

                }
            }

            //check org id avaliable
            if (checkConfigSettingAvailability)
            {
                using (ConfigSettingBS objConfigSettingBS = new ConfigSettingBS())
                {
                    if (objConfigSettingBS.CheckConfigSettingAvailability(configSettingEntity))
                    {
                        // return "O_id already available.";
                        return MessageCode.ErrorMsgInJson(MsgCode.checkConfigSettingAvailability);
                    }
                }
            }
            // check  s_user_id
            if (s_user_id)
            {
                if (configSettingEntity.s_user_id == null || configSettingEntity.s_user_id == 0)
                {
                    // return "s_user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_user_id);
                }
            }

            // check  s_user_role
            if (s_role_Id)
            {
                if (configSettingEntity.s_role_Id == null || configSettingEntity.s_role_Id == 0)
                {
                    //  return "s_role_Id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_role_Id);
                }
            }

            return strResult;
        }

        public static string ValidateTrip(TripEntity tripEntity,
                bool CheckTripStart = false,
                bool CheckTripEnd = false,
                bool trip_id = false,
                bool trip_name = false,
                bool user_id = false,
                bool trip_from = false,
                bool trip_to = false,
                bool trip_start_time = false,
                bool trip_end_time = false,
                bool created_at = false,
                bool updated_at = false,
                bool s_user_id = false,
                bool s_role_Id = false
         )
        {
            string strResult = "";

            // check  tripEntity
            if (tripEntity == null)
            {
                //return "empty trip entity not allowed.";
                return MessageCode.ErrorMsgInJson(MsgCode.trip_entity);
            }

            if (CheckTripStart)
            {
                using (TripBS objTripBS = new TripBS())
                {
                    if (objTripBS.CheckTripStart(tripEntity))
                    {
                        // return "username already available.";
                        return MessageCode.ErrorMsgInJson(MsgCode.CheckTripStart);
                    }
                }
            }

            if (CheckTripEnd)
            {
                using (TripBS objTripBS = new TripBS())
                {
                    if (objTripBS.CheckTripEnd(tripEntity))
                    {
                        // return "username already available.";
                        return MessageCode.ErrorMsgInJson(MsgCode.CheckTripEnd);
                    }
                }
            }

            // check  trip_id
            if (trip_id)
            {
                if (tripEntity.trip_id == 0)
                {
                    // return "trip_id required";
                    return MessageCode.ErrorMsgInJson(MsgCode.trip_id);

                }
            }

            //check trip_name avaliable
            if (trip_name)
            {

                if (tripEntity.trip_name == null || tripEntity.trip_name == "")
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.trip_name);
                }

            }
            // check  user_id
            if (user_id)
            {
                if (tripEntity.user_id == null || tripEntity.user_id == 0)
                {
                    // return "user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.user_id);
                }
            }

            // check  trip_start_time
            if (trip_start_time)
            {
                if (tripEntity.trip_start_time == null)
                {
                    // return "trip_start_time required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.trip_start_time);
                }
            }

            if (trip_end_time)
            {
                if (tripEntity.trip_end_time == null)
                {
                    // return "trip_end_time required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.trip_end_time);
                }
            }

            if (trip_from)
            {
                if (tripEntity.trip_from == null || tripEntity.trip_from == "")
                {
                    //  return "s_role_Id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.trip_from);
                }
            }

            if (trip_to)
            {
                if (tripEntity.trip_to == null || tripEntity.trip_to == "")
                {
                    //  return "s_role_Id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.trip_to);
                }
            }

            if (s_user_id)
            {
                if (tripEntity.s_user_id == null || tripEntity.s_user_id == 0)
                {
                    // return "s_user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_user_id);
                }
            }

            // check  s_user_role
            if (s_role_Id)
            {
                if (tripEntity.s_role_Id == null || tripEntity.s_role_Id == 0)
                {
                    //  return "s_role_Id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_role_Id);
                }
            }


            return strResult;
        }


        public static string ValidateDevice(DeviceEntity deviceEntity,
                bool deviceno = false,
                bool vendor_name = false,
                bool framework = false,
                bool versionno = false,
                bool sim_no = false,
                bool imei_no = false,
                bool sim_installation = false,
                bool sim_activation_date = false,
                bool sim_expiry_date = false,
                bool sms_active = false,
                bool device_live_date = false,
                bool created_at = false,
                bool updated_at = false,
                bool deleted = false,
                bool isactive = false,
                bool s_user_id = false,
                bool s_role_Id = false
           )
        {
            string strResult = "";

            // check  tripEntity
            if (deviceEntity == null)
            {
                //return "empty trip entity not allowed.";
                return MessageCode.ErrorMsgInJson(MsgCode.orgEntityEmpt);
            }
            if (deviceno)
            {

                if (deviceEntity.deviceno == 0)
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.deviceno);
                }
            }
            if (vendor_name)
            {
                if (deviceEntity.vendor_name == null || deviceEntity.vendor_name == "")
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.vendor_name);

                }
            }
            if (framework)
            {
                if (deviceEntity.framework == null || deviceEntity.framework == "")
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.framework);
                }
            }
            if (versionno)
            {
                if (deviceEntity.versionno == null || deviceEntity.versionno == "")
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.versionno);
                }
            }
            if (sim_no)
            {
                if (deviceEntity.sim_no == null || deviceEntity.sim_no == "")
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.sim_no);
                }
            }
            if (imei_no)
            {
                if (deviceEntity.imei_no == null || deviceEntity.imei_no == "")
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.imei_no);
                }
            }
            if (sim_installation)
            {
                if (deviceEntity.sim_installation == null)
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.sim_installation);
                }
            }
            if (sim_activation_date)
            {
                if (deviceEntity.sim_activation_date == null)
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.sim_activation_date);
                }
            }
            if (sim_expiry_date)
            {
                if (deviceEntity.sim_expiry_date == null)
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.sim_expiry_date);
                }
            }
            if (sim_expiry_date)
            {
                if (deviceEntity.sim_expiry_date == null)
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.sim_expiry_date);
                }
            }
            if (sim_expiry_date)
            {
                if (deviceEntity.sim_expiry_date == null)
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.sim_expiry_date);
                }
            }
            if (sms_active)
            {
                if (deviceEntity.sms_active == null)
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.sms_active);
                }
            }
            if (device_live_date)
            {
                if (deviceEntity.device_live_date == null)
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.device_live_date);
                }
            }
            if (created_at)
            {
                if (deviceEntity.created_at == null)
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.created_at);
                }
            }
            if (updated_at)
            {
                if (deviceEntity.updated_at == null)
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.updated_at);
                }
            }
            if (deleted)
            {
                if (deviceEntity.deleted == null)
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.sms_active);
                }
            }
            //   if (isactive)
            //{
            //     if (deviceEntity.isactive == null )
            //    {
            //        //return "trip_name required.";
            //        return MessageCode.ErrorMsgInJson(MsgCode.isactive);
            //    }
            // }
            if (s_user_id)
            {
                if (deviceEntity.s_user_id == null || deviceEntity.s_user_id == 0)
                {
                    // return "s_user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_user_id);
                }
            }

            // check  s_user_role
            if (s_role_Id)
            {
                if (deviceEntity.s_role_Id == null || deviceEntity.s_role_Id == 0)
                {
                    //  return "s_role_Id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_role_Id);
                }
            }

            return strResult;



        }

        public static string ValidationCompitition(CompitionEntity compititionEntity,

             bool cmpt_id = false,
             bool cmpt_name = false,
             bool cmpt_profile_picture_path = false,
             bool cmpt_type = false,
             bool start_date = false,
             bool end_date = false,
             bool terms_conditions = false,
             bool min_km_travel = false,
             bool cmpt_for_driving = false,
             bool vehicle_manufacturer = false,
             bool ec_agefrom = false,
             bool ec_ageto = false,
             bool gender = false,
             bool authentication_required = false,
             bool car_type = false,
             bool cmpt_country = false,
             bool cmpt_uniquecode = false,
             bool prize1_desc = false,
             bool prize2_desc = false,
             bool prize3_desc = false,
             bool userid = false,
             bool jointimestam = false,
             bool s_user_id = false,
             bool s_role_Id = false,
             bool deviceno = false,
             bool CheckJointAvailablity=false,
            bool Checkshare_cmptAvailablity=false,
            bool Checkisclose_cmptAvailablity=false 
           )
        {


            string strResult = "";

            // check  tripEntity
            if (compititionEntity == null)
            {
                //return "empty trip entity not allowed.";
                return MessageCode.ErrorMsgInJson(MsgCode.orgEntityEmpt);
            }
            if (cmpt_id)
            {

                if (compititionEntity.cmpt_id == 0)
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.cmpt_id);
                }
            }
            if (cmpt_name)
            {
                if (compititionEntity.cmpt_name == null || compititionEntity.cmpt_name == "")
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.cmpt_name);

                }
            }
            if (cmpt_profile_picture_path)
            {
                if (compititionEntity.cmpt_profile_picture_path == null || compititionEntity.cmpt_profile_picture_path == "")
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.cmpt_profile_picture_path);

                }
            }
            if (cmpt_type)
            {
                if (compititionEntity.cmpt_type == null || compititionEntity.cmpt_type == "")
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.cmpt_type);

                }
            }
            if (start_date)
            {
                if (compititionEntity.start_date == null)
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.start_date);

                }
            }
            if (end_date)
            {
                if (compititionEntity.end_date == null)
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.end_date);

                }
            }
            if (terms_conditions)
            {
                if (compititionEntity.terms_conditions == null || compititionEntity.terms_conditions == "")
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.terms_conditions);

                }
            }
            if (min_km_travel)
            {
                if (compititionEntity.min_km_travel == null || compititionEntity.min_km_travel == 0)
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.min_km_travel);

                }
            }
            if (cmpt_for_driving)
            {
                if (compititionEntity.cmpt_for_driving == null || compititionEntity.cmpt_for_driving == "")
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.cmpt_for_driving);

                }
            }
            if (vehicle_manufacturer)
            {
                if (compititionEntity.vehicle_manufacturer == null || compititionEntity.vehicle_manufacturer == "")
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.vehicle_manufacturer);

                }
            }
            if (ec_agefrom)
            {
                if (compititionEntity.ec_agefrom == null || compititionEntity.ec_agefrom == 0)
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.ec_agefrom);

                }
            }
            if (ec_ageto)
            {
                if (compititionEntity.ec_ageto == null || compititionEntity.ec_ageto == 0)
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.ec_ageto);

                }
            }
            if (gender)
            {
                if (compititionEntity.gender == null || compititionEntity.gender == "")
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.gender);

                }
            }
            if (authentication_required)
            {
                if (compititionEntity.authentication_required == null)
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.authentication_required);

                }
            }
            if (car_type)
            {
                if (compititionEntity.car_type == null || compititionEntity.car_type == "")
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.car_type);

                }
            }
            if (cmpt_country)
            {
                if (compititionEntity.cmpt_country == null || compititionEntity.cmpt_country == "")
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.cmpt_country);

                }
            }
            if (cmpt_uniquecode)
            {
                if (compititionEntity.cmpt_uniquecode == null || compititionEntity.cmpt_uniquecode == "")
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.cmpt_uniquecode);

                }
            }
            if (prize1_desc)
            {
                if (compititionEntity.prize1_desc == null || compititionEntity.prize1_desc == "")
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.prize1_desc);

                }
            }
            if (prize2_desc)
            {
                if (compititionEntity.prize2_desc == null || compititionEntity.prize2_desc == "")
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.prize1_desc);

                }
            }
            if (prize3_desc)
            {
                if (compititionEntity.prize3_desc == null || compititionEntity.prize3_desc == "")
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.prize3_desc);

                }
            }
            if (s_user_id)
            {
                if (compititionEntity.s_user_id == null || compititionEntity.s_user_id == 0)
                {
                    // return "s_user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_user_id);
                }
            }

            // check  s_user_role
            if (s_role_Id)
            {
                if (compititionEntity.s_role_Id == null || compititionEntity.s_role_Id == 0)
                {
                    //  return "s_role_Id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_role_Id);
                }
            }
            if (userid)
            {
                if (compititionEntity.userid == null || compititionEntity.userid == 0)
                {
                    // return "user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.userid);
                }

            }
            if (jointimestam)
            {
                if (compititionEntity.jointimestam == null || compititionEntity.jointimestam.ToString().Contains("0001") == true)
                {
                    // return "user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.jointimestam);
                }
            }
            if (deviceno)
            {

                if (compititionEntity.deviceno == 0)
                {
                    //return "trip_name required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.deviceno);
                }
            }
            if (CheckJointAvailablity)
            {

                using (CompititionBS objCompititionBS = new CompititionBS())
                {
                    if (objCompititionBS.CheckJointAvailablity(compititionEntity))
                    {
                        // return "username already available.";
                        return MessageCode.ErrorMsgInJson(MsgCode.CheckJointAvailablity);
                    }
                }


            }

            if (Checkshare_cmptAvailablity)
            {

                using (CompititionBS objCompititionBS = new CompititionBS())
                {
                    if (objCompititionBS.Checkshare_cmptAvailablity(compititionEntity))
                    {
                        // return "username already available.";
                        return MessageCode.ErrorMsgInJson(MsgCode.CheckJointAvailablity);
                    }
                }


            }

            if (Checkisclose_cmptAvailablity)
            {
                using (CompititionBS objCompititionBS = new CompititionBS())
                {
                    DataTable isclosed_cmptdata = objCompititionBS.Checkisclosed_cmptAvailablity(compititionEntity);
                    if (isclosed_cmptdata != null && isclosed_cmptdata.Rows.Count > 0)
                    {
                        //if (Convert.ToBoolean( isclosed_cmptdata.Rows[0]["isclose_cmpt"]))
                        //{
                            string promocode = isclosed_cmptdata.Rows[0]["cmpt_code"].ToString() ;
                            if (promocode != compititionEntity.cmpt_code)
                            {
                                return MessageCode.ErrorMsgInJson(MsgCode.CheckclosecmptAvailablity);
                            }
                        //}
                        
                    }
                }


            }
            return strResult;



        }


        public static string ValidateNotiSetting(NotificationSetting notiSettingEntity,bool s_user_id = false)
        {
            string strResult = "";

            // check  organizationEntity
            if (notiSettingEntity == null)
            {
                //return "empty organization entity not allowed.";
                return MessageCode.ErrorMsgInJson(MsgCode.notiSettingEntityEmpt);
            }

            if (s_user_id)
            {
                if (notiSettingEntity.s_user_id == null || notiSettingEntity.s_user_id == 0)
                {
                    // return "s_user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_user_id);
                }
            }

            return strResult;
        }


        public static string ValidatePolicyClaim(PolicyClaimEntity policyClaimEntity,
             
             bool claim_id = false,
             bool user_id = false,
             bool policy_no = false,
             bool period_from = false,
             bool period_to = false,
             bool date_accident_or_loss = false,
             bool name_of_driver = false,
             bool driver_birth_date = false,
             bool driving_licence_no = false,
             bool accident_or_loss_details = false,
             bool status = false,
             bool remark = false,
             bool s_user_id = false,
             bool s_role_Id = false
         
            )
        {
            string strResult = "";

            // check  organizationEntity
            if (policyClaimEntity == null)
            {
                //return "empty organization entity not allowed.";
                return MessageCode.ErrorMsgInJson(MsgCode.policyClaimEntity);
            }

            if (claim_id)
            {
                if (policyClaimEntity.claim_id == null || policyClaimEntity.claim_id == 0)
                {
                    // return "s_user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.claim_id);
                }
            }
            if (user_id)
            {
                if (policyClaimEntity.user_id == null || policyClaimEntity.user_id == 0)
                {
                    // return "s_user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.user_id);
                }
            }
            if (policy_no)
            {
                if (policyClaimEntity.policy_no == null)
                {
                    // return "s_user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.policy_no);
                }
            }
            if (period_from)
            {
                if (policyClaimEntity.period_from == null )
                {
                    // return "s_user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.period_from);
                }
            }
            if (period_to)
            {
                if (policyClaimEntity.period_to == null )
                {
                    // return "s_user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.period_to);
                }
            }
            if (date_accident_or_loss)
            {
                if (policyClaimEntity.date_accident_or_loss == null )
                {
                    // return "s_user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.date_accident_or_loss);
                }
            }
            if (name_of_driver)
            {
                if (policyClaimEntity.name_of_driver == null || policyClaimEntity.name_of_driver == "")
                {
                    // return "s_user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.name_of_driver);
                }
            }
            if (driver_birth_date)
            {
                if (policyClaimEntity.driver_birth_date == null)
                {
                    // return "s_user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.driver_birth_date);
                }
            }
            if (driving_licence_no)
            {
                if (policyClaimEntity.driving_licence_no == null || policyClaimEntity.driving_licence_no == "")
                {
                    // return "s_user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.driving_licence_no);
                }
            }
            if (accident_or_loss_details)
            {
                if (policyClaimEntity.accident_or_loss_details == null || policyClaimEntity.accident_or_loss_details == "")
                {
                    // return "s_user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.accident_or_loss_details);
                }
            }
            if (status)
            {
                if (policyClaimEntity.status == null || policyClaimEntity.status == "")
                {
                    // return "s_user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.status);
                }
            }
            if (remark)
            {
                if (policyClaimEntity.remark == null || policyClaimEntity.remark == "")
                {
                    // return "s_user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.remark);
                }
            }
            if (s_user_id)
            {
                if (policyClaimEntity.s_user_id == null || policyClaimEntity.s_user_id == 0)
                {
                    // return "s_user_id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_user_id);
                }
            }

            // check  s_user_role
            if (s_role_Id)
            {
                if (policyClaimEntity.s_role_Id == null || policyClaimEntity.s_role_Id == 0)
                {
                    //  return "s_role_Id required.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_role_Id);
                }
            }
          
            
            return strResult;
        }
          

    }


}
