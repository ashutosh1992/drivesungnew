﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOTBusinessEntities;
using IOTUtilities;

namespace IOTBusinessServices
{
   public class ValidateNotificationEntity
    {
       public static string validateNotificationEntity(NotificationEntity notificationEntity,
           
                bool s_user_id = false,
                bool s_role_Id = false

            )
       {

           string strResult = "";

           // check  user_id
           if (notificationEntity == null)
           {
               // return "empty filter entity not allowed.";
               return MessageCode.ErrorMsgInJson(MsgCode.filterEntity);


           }

       

           //bool s_user_id = false,
           if (s_user_id)
           {
               if (notificationEntity.s_user_id == null || notificationEntity.s_user_id == 0)
               {
                   //return "s_user_id required in filter entity.";
                   return MessageCode.ErrorMsgInJson(MsgCode.s_user_id);
               }
           }
           //bool s_role_Id = false
           if (s_role_Id)
           {
               if (notificationEntity.s_role_Id == null || notificationEntity.s_role_Id == 0)
               {
                   //  return "s_role_Id required in filter entity.";
                   return MessageCode.ErrorMsgInJson(MsgCode.s_role_Id);
               }
           }

           return strResult;
       }

    }
}
