﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using IOTBusinessEntities;
using IOTDataModel;
using IOTUtilities;
using System.Threading.Tasks;

namespace IOTBusinessServices
{

    public class UserAuthenticationBS : IDisposable
    {
        private UserAuthenticationDM objUserAuthenticationDM;
        private bool _blnsessionmanage;
        private Sessions _session;
        public UserAuthenticationBS(bool sessionmanage = true)
        {
            objUserAuthenticationDM = new UserAuthenticationDM(sessionmanage);

            _blnsessionmanage = sessionmanage;
            if (_blnsessionmanage)
            {
                _session = new Sessions();
            }
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                if (objUserAuthenticationDM != null) { objUserAuthenticationDM.Dispose(); }
                _session = null;

            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~UserAuthenticationBS()
        {
            Dispose(false);
        }

        #endregion

        //public ResponseEntity UserLogin(string UserName)
        //{
        //    ResponseEntity objResponseEntity = new ResponseEntity();
        //    BaseDAL objDAL = new BaseDAL();
        //    try
        //    {


        //        objDAL.CreateConnection();
        //        string SQLQry = @"select * from tbluser where username='" + UserName + "' limit 1";
        //        DataTable dtData = objDAL.FetchRecords(SQLQry);
        //        objDAL.CloseConnection();

        //        if (dtData != null && dtData.Rows.Count > 0)
        //        {
        //            DataRow row = dtData.Rows[0];
        //            sessionEntity.UserId = CommonUtils.ConvertToInt(row["id"]);
        //            sessionEntity.UserName = CommonUtils.ConvertToString(row["username"]);
        //            sessionEntity.UserPIN = CommonUtils.ConvertToString(row["userpwd"]);
        //            sessionEntity.UserType = CommonUtils.ConvertToString(row["role_id"]); ;
        //            row = null;

        //            if (IsSessionManage)
        //            {
        //                Sessions objSession = new Sessions();
        //                objSession.SetSessionEntity(sessionEntity);
        //                objSession = null;
        //            }

        //            objResponseEntity.IsComplete = true;
        //            objResponseEntity.Description = "success";
        //            objResponseEntity.data = sessionEntity;
        //        }
        //        else
        //        {
        //            objResponseEntity.IsComplete = false;
        //            objResponseEntity.Description = "invalid login details";
        //        }

        //        return objResponseEntity;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        objResponseEntity = null;
        //        objDAL = null;
        //    }

        //}

        public Task<ResponseEntity> UserLogin(string user_name, string user_pwd, UserEntity userEntity = null)
        {
          //  return objUserAuthenticationDM.UserLogin(UserName, pwd);
            return Task.FromResult(objUserAuthenticationDM.UserLogin(user_name, user_pwd,userEntity));

        }

        public Task<bool> CheckUserAvailability(string user_name)
        {
            //  return objUserAuthenticationDM.UserLogin(UserName, pwd);
            return Task.FromResult(objUserAuthenticationDM.CheckUserAvailability(user_name));

        }

        public string CheckPageSecurity(string PageName, Int32 userId, Int32 role_Id, string action)
        {
            return objUserAuthenticationDM.CheckPageSecurity(PageName, userId, role_Id, action);
        }

        public string CheckSessionExpired()
        {
            string resultMsg = "";
            if (_blnsessionmanage)
            {
                resultMsg = _session.CheckSessionExpired();
            }
            return resultMsg;
        }



    }
}