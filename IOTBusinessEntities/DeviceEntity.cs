﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOTBusinessEntities
{
   public class DeviceEntity
    {
       public Int32 deviceno { get; set; }
       public string vendor_name { get; set; }
       public string framework { get; set; }
       public string versionno { get; set; }
       public string sim_no { get; set; }
       public string imei_no { get; set; }
       public DateTime? sim_installation { get; set; }
       public DateTime? sim_activation_date { get; set; }
       public DateTime? sim_expiry_date { get; set; }
       public bool sms_active { get; set; }
       public DateTime? device_live_date { get; set; }
       public DateTime? created_at { get; set; }
       public DateTime? updated_at { get; set; }
       public bool deleted { get; set; }
       public bool isactive { get; set; }
       public Int32 s_user_id { get; set; }
       public Int32 s_role_Id { get; set; }

     

    }
}
