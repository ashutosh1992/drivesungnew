﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IOTBusinessEntities
{
    public class UserEntity
    {
        //user_master
        public Int32 user_id { get; set; }
        public Int32 role_Id { get; set; }
        public string user_name { get; set; }
        public string user_role { get; set; }
        public string first_name { get; set; }
        public string middle_name { get; set; }
        public string last_name { get; set; }
        public DateTime? date_of_birth { get; set; }
        public string gender { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }
        public string mobileno { get; set; }
        public string email_id1 { get; set; }
        public string email_id2 { get; set; }
        public string profile_picture_uri { get; set; }
        public Int64 deviceno { get; set; }
        public string vehicle_no { get; set; }
        public string license_no { get; set; }
        public DateTime? license_expiry_date { get; set; }
        public string registration_no { get; set; }
        public string vehicle_manufacturer { get; set; }
        public string vehicle_color { get; set; }
        public string vehicle_type { get; set; }
        public string user_login_type { get; set; }
        public string insurance_mapping_key { get; set; }
        public Int32 o_id { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? update_at { get; set; }
        public bool deleted { get; set; }
        public bool isactive { get; set; }

        //user_pwd
        public string user_pwd { get; set; }

        public Int32 s_user_id { get; set; }
        public Int32 s_role_Id { get; set; }
        public List<UserDeviceInfoEntity> userDeviceInfoEntities { get; set; }
        public object upload_image { get; set; }
        public DateTime devicedate { get; set; }
        public string social_id { get; set; }
        //public string social_type { get; set; }
        public string country { get; set; }
        public string state { get; set; }
        public string pincode { get; set; }
        public string region { get; set; }
        public string city { get; set; }
        public string vehiclengine_type { get; set; }
        public Int32 vin_no {get; set;}
        public string insurance_provider { get; set; }
        public string insurer_emailid { get; set; }
        public string engine_capacity { get; set; }
        public string insurance_comp_contact { get; set; }
        public string sos_contact { get; set; }
        public string family_contact { get; set; }
        public string transaction_id { get; set; }
        public bool premium { get; set; }
        public DateTime premium_validity { get; set; }
         



    }
    public class setting
    {
        public string mUrl { get; set; }
        public string dUrl { get; set; }
        public string HA { get; set; }
        public string HB { get; set; }
        public string HC { get; set; }
        public string OverSpeed { get; set; }
        public string spike { get; set; }
        public string HCspeed { get; set; }
        

    }
   


}