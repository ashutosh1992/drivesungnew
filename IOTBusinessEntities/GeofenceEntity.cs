﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOTBusinessEntities
{
    public class GeofenceEntity
    {
        public Int32 gc_id { get; set; }
        public string geofence_name { get; set; }
        public string geofenec_desc { get; set; }
        public string lat_lng { get; set; }
        public string icon_file_name { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public bool deleted { get; set; }
        public bool isactive { get; set; }
        public Int32 created_by { get; set; }
        public Int32 updated_by { get; set; }
        public Int32 s_user_id { get; set; }
        public Int32 s_role_Id { get; set; }
        public Int32 user_id { get; set; }
        public List<GeofenceUserMappingEntity> geofenceUserMappingEntities { get; set; }
    }

    public class GeofenceUserMappingEntity
    {
        public Int32 gc_id { get; set; }
        public Int32 user_id { get; set; }
        public Boolean blnmaster { get; set; }

    }



}
