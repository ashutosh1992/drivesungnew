﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOTBusinessEntities
{
   public class CompitionEntity
    {

        public Int32 cmpt_id { get; set; }
        public string cmpt_name { get; set; }
        public string cmpt_profile_picture_path { get; set; }
        public string cmpt_type { get; set; }
        public DateTime? start_date { get; set; }
        public DateTime? end_date { get; set; }
        public string terms_conditions { get; set; }
        public Int32 min_km_travel { get; set; }
        public Int32 total_km_travel { get; set; }
        public string cmpt_for_driving { get; set; }
        public string vehicle_manufacturer { get; set; }
        public Int32 ec_agefrom { get; set; }
        public Int32 ec_ageto { get; set; }
        public string gender { get; set; }
        public bool authentication_required { get; set; }
        public Int32 prize_id1 { get; set; }
        public Int32 prize_id2 { get; set; }
        public Int32 prize_id3 { get; set; }
        public string car_type { get; set; }
        public string cmpt_country { get; set; }
        public string cmpt_uniquecode { get; set; }
        public DateTime created_at { get; set; }
        public Int32 created_by { get; set; }
        public bool isactive { get; set; }
        public bool deleted { get; set; }
       
        //add extra entity for discription
        public string prize1_desc { get; set; }
        public string prize2_desc { get; set; }
        public string prize3_desc { get; set; }
        public Int32 userid { get; set; }
        public DateTime jointimestam { get; set; }
        public Int32 s_user_id { get; set; }
        public Int32 s_role_Id { get; set; }
        public Int32 deviceno { get; set; }
        public Int32 score { get; set; }
        public Int32 rankno { get; set; }
        public string first_name { get; set; }
        public string middle_name { get; set; }
        public string last_name { get; set; }
        public DateTime? joining_date { get; set; }
        public Int32 distance_travel { get; set; }
        public string profile_picture_uri { get; set; }
        public string mobileno { get; set; }
        public string Email_id { get; set; }
        public string Emailbody { get; set; }
        public string subject { get; set; }
        public string s_date { get; set; }
        public string e_date { get; set; }
        public bool isclose_cmpt { get; set; }
        public string cmpt_code { get; set; }

       

       

        
    }

   public class PrizeEntity
   {
       public Int32 prize_id { get; set; }
       public string prize_code { get; set; }
       public string prize_desc { get; set; }
       public string prize_type { get; set; }
       public string allocation { get; set; }
       public string amt { get; set; }
       public string qty { get; set; }
       public string created_by { get; set; }
       public DateTime created_at { get; set; }

   }


}
