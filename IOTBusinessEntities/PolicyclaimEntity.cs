﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOTBusinessEntities
{
   public class PolicyClaimEntity
    {
       public int claim_id { get; set; }
       public int user_id { get; set; }
       public string policy_no { get; set; }
       public DateTime? period_from { get; set; }
       public string spf { get; set; }
       public DateTime? period_to { get; set; }
       public string spt { get; set; }
       public DateTime? date_accident_or_loss { get; set; }
       public string sdaccount { get; set; }
       public string name_of_driver { get; set; }
       public DateTime? driver_birth_date { get; set; }
       public string dbd { get; set; }
       public string driving_licence_no { get; set; }
       public string accident_or_loss_details { get; set; }
       public string address { get; set; }
       public string status { get; set; }
       public string remark { get; set; }
       public string location { get; set; }
       public string note { get; set; }
       public string Email_id { get; set; }
       public DateTime? created_at { get; set; }
       public DateTime? updated_at { get; set; }
       public int s_user_id { get; set; }
       public int s_role_Id { get; set; }
       public List<policy_claim_attach_docEntity> PolicyDocEntities { get; set; }

   }

   public class policy_claim_attach_docEntity
   {
       public int claim_id { get; set; }
       public string doc_name { get; set; }
       public string doc_file_url { get; set; }
       public string doc_file_path { get; set; }
       public string doc_file_save { get; set; }
       public string  status { get; set; }
       public string  remark { get; set; }
       public DateTime? created_at { get; set; }
       public DateTime? updated_at { get; set; }
   }

  
}
