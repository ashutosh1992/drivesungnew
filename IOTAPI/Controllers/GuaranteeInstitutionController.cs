﻿using PFTrustAPI.Controllers.CommonUtility;
using PFTrustAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PFTrustAPI.Controllers
{
    [Authorize]
    public class GuaranteeInstitutionController : ApiController
    {
        UserAuthentication auth = new UserAuthentication();
        BaseDAL objDAL;

        [HttpPost]
        public IHttpActionResult GetGuaranteeInstitution(FilterEntity filterEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                objDAL = new BaseDAL();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int64 UserId = sessionEntity.UserId;

                objDAL.CreateConnection();

                string strQuery = "Select * from PFT_tblGuaranteeInstitutionMaster where IsDelete = 0 AND GuaranteeInstitutionCode like '%" + filterEntity.Param1 + "%'";
                DataTable dtGi = objDAL.FetchRecords(strQuery);

                objDAL.CloseConnection();

                return Ok(dtGi);
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "GuaranteeInstitutionController");
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult SaveGuaranteeInstitution(GuaranteeInstituionEntity objGiEntity)
        {
            Sessions session = new Sessions();
            SessionEntity sessionEntity = session.GetSessionEntity();
            Int64 _userId = sessionEntity.UserId;
            BaseDAL objDAL = new BaseDAL();
            try
            {
                UserAuthentication auth = new UserAuthentication();
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                ResponseEntity responseEntity = new ResponseEntity();
                responseEntity.IsComplete = false;

                string _actionState = "";
                Int64 guarantorID = 0;

                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                //clsRembDocumentSettingBO objclsRembDocumentSettingBO = new clsRembDocumentSettingBO();
                SqlParameter sqlParameter;

                if (objGiEntity.actionEntity != null)
                    _actionState = objGiEntity.actionEntity.ActionState;

                if (_actionState.Equals("New"))
                {
                    bool IsExist = Validate(objGiEntity.GuaranteeInstitutionCode);
                    if (IsExist)
                    {
                        responseEntity.IsComplete = false;
                        responseEntity.Description = "Guarantor Code Already Exists.";
                        return Ok(responseEntity);
                    }
                }
                else if (_actionState.Equals("Edit"))
                {
                    guarantorID = objGiEntity.actionEntity.RecordId;
                }

                // Transition Is Started....
                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                sqlParameter = new SqlParameter("@GuaranteeInstitutionID", SqlDbType.Int, 9);
                sqlParameter.Value = guarantorID;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@GuaranteeInstitutionCode", SqlDbType.NVarChar, 20);
                sqlParameter.Value = objGiEntity.GuaranteeInstitutionCode;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@InstitutionName", SqlDbType.NVarChar, 250);
                sqlParameter.Value = objGiEntity.InstitutionName;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Address1", SqlDbType.NVarChar, 250);
                sqlParameter.Value = objGiEntity.Address1;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Address2", SqlDbType.NVarChar, 250);
                sqlParameter.Value = objGiEntity.Address2;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Address3", SqlDbType.NVarChar, 250);
                sqlParameter.Value = objGiEntity.Address3;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@PinCode", SqlDbType.NVarChar, 6);
                sqlParameter.Value = objGiEntity.PinCode;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@City", SqlDbType.NVarChar, 150);
                sqlParameter.Value = objGiEntity.City;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@State", SqlDbType.NVarChar, 150);
                sqlParameter.Value = objGiEntity.State;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@ContactName", SqlDbType.NVarChar, 255);
                sqlParameter.Value = objGiEntity.ContactName;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Mobile", SqlDbType.NVarChar, 10);
                sqlParameter.Value = objGiEntity.Mobile;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Telephone", SqlDbType.NVarChar, 20);
                sqlParameter.Value = objGiEntity.Telephone;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Email", SqlDbType.NVarChar, 70);
                sqlParameter.Value = objGiEntity.Email;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Designation", SqlDbType.NVarChar, 70);
                sqlParameter.Value = objGiEntity.Designation;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@IsDelete", SqlDbType.Bit, 1);
                sqlParameter.Value = 0;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@ICreater", SqlDbType.Int, 9);
                sqlParameter.Value = _userId;
                sqlParameters.Add(sqlParameter);

                if (_actionState.Equals("New"))
                {
                    sqlParameter = new SqlParameter("@edit", SqlDbType.Bit, 1);
                    sqlParameter.Value = 1;
                    sqlParameters.Add(sqlParameter);
                }
                else if (_actionState.Equals("Edit"))
                {
                    sqlParameter = new SqlParameter("@edit", SqlDbType.Bit, 1);
                    sqlParameter.Value = 0;
                    sqlParameters.Add(sqlParameter);
                }

                objDAL.ExecuteSP("spPFT_tblGuaranteeInstitutionMasterAddUpdate", ref sqlParameters);

                objDAL.CommitTransaction();
                objDAL.CloseConnection();

                responseEntity.IsComplete = true;
                responseEntity.Description = "Issuer Saved Successfully.";

                return Ok(responseEntity);
            }
            
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "GuaranteeInstitutionController");
                return Ok();
            }
        }

        public bool Validate(string GuaranteeInstitutionCode)
        {
            BaseDAL objDAL = new BaseDAL();
            bool IsExist = true;

            try
            {
                string strQuery = "Select COUNT(GuaranteeInstitutionCode) As CNT From PFT_tblGuaranteeInstitutionMaster Where GuaranteeInstitutionCode = '" + GuaranteeInstitutionCode + "' AND IsDelete = 0";
                objDAL.CreateConnection();

                DataTable dtData = objDAL.FetchRecords(strQuery);

                if (dtData != null && dtData.Rows.Count > 0)
                {
                    int Records = CommonUtils.ConvertToInt(dtData.Rows[0]["CNT"]);

                    if (Records > 0)
                    {
                        IsExist = true;
                    }
                    else
                    {
                        IsExist = false;
                    }
                }
                return IsExist;
            }
            catch (Exception x)
            {
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(x, "IssuerController");
                return false;
            }
        }

        [HttpPost]
        public IHttpActionResult GetGuaranteeInstitutionById(ActionEntity objActionEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                GuaranteeInstituionEntity queryEntity = new GuaranteeInstituionEntity();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL = new BaseDAL();
                objDAL.CreateConnection();

                Int32 QueryRequestId = 0;

                if (objActionEntity.RecordId != 0)
                {
                    QueryRequestId = objActionEntity.RecordId;
                }

                if (objActionEntity.ActionState == "Edit")
                {
                    queryEntity = ShowRecord(QueryRequestId);
                }

                else if (objActionEntity.ActionState == "View")
                {
                    queryEntity = ShowRecord(QueryRequestId);
                }

                return Ok(queryEntity);
            }
            catch (Exception ex)
            {
                WebUtils.ErrorHandler.HandleException(ex, "GuaranteeInstitutionController");
                return BadRequest(ex.Message);
            }
        }

        private GuaranteeInstituionEntity ShowRecord(Int32 GuaranteeInstitutionID)
        {
            GuaranteeInstituionEntity obj = new GuaranteeInstituionEntity();
            string SQLQuery = "select * from PFT_tblGuaranteeInstitutionMaster where GuaranteeInstitutionID=" + GuaranteeInstitutionID;
            DataTable dtData = objDAL.FetchRecords(SQLQuery);
            if (dtData != null && dtData.Rows.Count > 0)
            {
                obj.GuaranteeInstitutionID = CommonUtils.ConvertToInt64(dtData.Rows[0]["GuaranteeInstitutionID"]);
                obj.GuaranteeInstitutionCode = CommonUtils.ConvertToString(dtData.Rows[0]["GuaranteeInstitutionCode"]);
                obj.InstitutionName = CommonUtils.ConvertToString(dtData.Rows[0]["InstitutionName"]);
                obj.Address1 = CommonUtils.ConvertToString(dtData.Rows[0]["Address1"]);
                obj.Address2 = CommonUtils.ConvertToString(dtData.Rows[0]["Address2"]);
                obj.Address3 = CommonUtils.ConvertToString(dtData.Rows[0]["Address3"]);
                obj.PinCode = CommonUtils.ConvertToString(dtData.Rows[0]["PinCode"]);
                obj.City = CommonUtils.ConvertToString(dtData.Rows[0]["City"]);
                obj.State = CommonUtils.ConvertToString(dtData.Rows[0]["State"]);
                obj.ContactName = CommonUtils.ConvertToString(dtData.Rows[0]["ContactName"]);
                obj.Mobile = CommonUtils.ConvertToString(dtData.Rows[0]["Mobile"]);
                obj.Telephone = CommonUtils.ConvertToString(dtData.Rows[0]["Telephone"]);
                obj.Email = CommonUtils.ConvertToString(dtData.Rows[0]["Email"]);
                obj.Designation = CommonUtils.ConvertToString(dtData.Rows[0]["Designation"]);
                obj.IsDelete = CommonUtils.ConvertToBoolean(dtData.Rows[0]["IsDelete"]);
            }

            return obj;
        }

        [HttpPost]
        public IHttpActionResult DeleteGuaranteeInstitution(ActionEntity objActionEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                GuaranteeInstituionEntity queryEntity = new GuaranteeInstituionEntity();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL = new BaseDAL();
                objDAL.CreateConnection();

                Int32 RequestId = 0;

                if (objActionEntity.RecordId != 0)
                {
                    RequestId = objActionEntity.RecordId;
                }

                if (objActionEntity.ActionState == "Delete")
                {
                    List<SqlParameter> Parameters = new List<SqlParameter>();
                    SqlParameter parameter = new SqlParameter();

                    Parameters.Add(new SqlParameter("@GuaranteeInstitutionID", RequestId));

                    objDAL.ExecuteSP("spPFT_tblGuaranteeInstitutionMasterDelete", ref Parameters);
                }

                return Ok(queryEntity);
            }
            catch (Exception x)
            {
                WebUtils.ErrorHandler.HandleException(x, "GuaranteeInstitutionController");
                return BadRequest(x.Message);
            }
        }

        #region DropDown Method

        [HttpPost]
        public IHttpActionResult GetGuaranteeInstitutionDropDown(FilterEntity filterEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                objDAL = new BaseDAL();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL.CreateConnection();

                string strQuery = "Select GuaranteeInstitutionID,GuaranteeInstitutionCode from PFT_tblGuaranteeInstitutionMaster where IsDelete = 0";
                DataTable dtGi = objDAL.FetchRecords(strQuery);

                objDAL.CloseConnection();

                return Ok(dtGi);
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "GuaranteeInstitutionController");
                return BadRequest(ex.Message);
            }
        }

        #endregion
    }
}
