﻿using PFTrustAPI.Controllers.CommonUtility;
using PFTrustAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using PFTrustAPI.Models;
using PFTrustAPI.Models.SellerMaster;

namespace PFTrustAPI.Controllers
{
    [Authorize]
    public class SellerController : ApiController
    {
        UserAuthentication auth = new UserAuthentication();
        BaseDAL objDAL;

        [HttpPost]
        public IHttpActionResult GetSellerMasterList(FilterEntity filterEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/SellerMaster/sellermasterlist.html");

                objDAL = new BaseDAL();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int64 UserId = sessionEntity.UserId;

                objDAL.CreateConnection();

                string strQuery = "Select * from PFT_tblwSellerMaster where IsDelete = 0";
                DataTable dtGi = objDAL.FetchRecords(strQuery);

                objDAL.CloseConnection();

                return Ok(dtGi);
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "sellerMasterCtrl");
                return BadRequest(ex.Message);
            }
        }


        [HttpPost]
        public IHttpActionResult SaveSellerMaster(SellerMasterEntity objGiEntity)
        {
            Sessions session = new Sessions();
            SessionEntity sessionEntity = session.GetSessionEntity();
            Int64 _userId = sessionEntity.UserId;
            BaseDAL objDAL = new BaseDAL();
            try
            {
                UserAuthentication auth = new UserAuthentication();
                auth.CheckPageSecurity("~/SellerMaster/sellermasterlist.html");

                ResponseEntity responseEntity = new ResponseEntity();
                responseEntity.IsComplete = false;

                string _actionState = "";
                Int64 sellerID = 0;

                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                //clsRembDocumentSettingBO objclsRembDocumentSettingBO = new clsRembDocumentSettingBO();
                SqlParameter sqlParameter;

                if (objGiEntity.actionEntity != null)
                    _actionState = objGiEntity.actionEntity.ActionState;

                if (_actionState.Equals("New"))
                {
                    bool IsExist = Validate(objGiEntity.SellerCode);
                    if (IsExist)
                    {
                        responseEntity.IsComplete = false;
                        responseEntity.Description = "Seller Code Already Exists.";
                        return Ok(responseEntity);
                    }
                }
                else if (_actionState.Equals("Edit"))
                {
                    sellerID = objGiEntity.actionEntity.RecordId;
                }

                // Transition Is Started....
                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                sqlParameter = new SqlParameter("@SellerID", SqlDbType.Int, 9);
                sqlParameter.Value = sellerID;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@SellerCode", SqlDbType.NVarChar, 20);
                sqlParameter.Value = objGiEntity.SellerCode;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@SellerName", SqlDbType.NVarChar, 250);
                sqlParameter.Value = objGiEntity.SellerName;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Address1", SqlDbType.NVarChar, 250);
                sqlParameter.Value = objGiEntity.Address1;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Address2", SqlDbType.NVarChar, 250);
                sqlParameter.Value = objGiEntity.Address2;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Address3", SqlDbType.NVarChar, 250);
                sqlParameter.Value = objGiEntity.Address3;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@PinCode", SqlDbType.NVarChar, 6);
                sqlParameter.Value = objGiEntity.PinCode;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@City", SqlDbType.NVarChar, 150);
                sqlParameter.Value = objGiEntity.City;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@State", SqlDbType.NVarChar, 150);
                sqlParameter.Value = objGiEntity.State;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@ContactName", SqlDbType.NVarChar, 255);
                sqlParameter.Value = objGiEntity.ContactName;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Mobile", SqlDbType.NVarChar, 10);
                sqlParameter.Value = objGiEntity.Mobile;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Telephone", SqlDbType.NVarChar, 20);
                sqlParameter.Value = objGiEntity.Telephone;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Email", SqlDbType.NVarChar, 70);
                sqlParameter.Value = objGiEntity.Email;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Designation", SqlDbType.NVarChar, 70);
                sqlParameter.Value = objGiEntity.Designation;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@IsDelete", SqlDbType.Bit, 1);
                sqlParameter.Value = 0;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@ICreater", SqlDbType.Int, 9);
                sqlParameter.Value = _userId;
                sqlParameters.Add(sqlParameter);

                if (_actionState.Equals("New"))
                {
                    sqlParameter = new SqlParameter("@edit", SqlDbType.Bit, 1);
                    sqlParameter.Value = 1;
                    sqlParameters.Add(sqlParameter);
                }
                else if (_actionState.Equals("Edit"))
                {
                    sqlParameter = new SqlParameter("@edit", SqlDbType.Bit, 1);
                    sqlParameter.Value = 0;
                    sqlParameters.Add(sqlParameter);
                }

                objDAL.ExecuteSP("spPFT_tblwSellerMasterAddUpdate", ref sqlParameters);

                objDAL.CommitTransaction();
                objDAL.CloseConnection();

                responseEntity.IsComplete = true;
                responseEntity.Description = "Seller Saved Successfully.";

                return Ok(responseEntity);
            }

            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "SellerMasterController");
                return Ok();
            }
        }

        public bool Validate(string SellerCode)
        {
            BaseDAL objDAL = new BaseDAL();
            bool IsExist = true;

            try
            {
                string strQuery = "Select COUNT(SellerCode) As CNT From PFT_tblwSellerMaster Where SellerCode = '" + SellerCode + "' AND IsDelete = 0";
                objDAL.CreateConnection();

                DataTable dtData = objDAL.FetchRecords(strQuery);

                if (dtData != null && dtData.Rows.Count > 0)
                {
                    int Records = CommonUtils.ConvertToInt(dtData.Rows[0]["CNT"]);

                    if (Records > 0)
                    {
                        IsExist = true;
                    }
                    else
                    {
                        IsExist = false;
                    }
                }
                return IsExist;
            }
            catch (Exception x)
            {
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(x, "SellerMasterController");
                return false;
            }
        }

        [HttpPost]
        public IHttpActionResult GetSellerMasterById(ActionEntity objActionEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/SellerMaster/sellermasterlist.html");

                SellerMasterEntity queryEntity = new SellerMasterEntity();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL = new BaseDAL();
                objDAL.CreateConnection();

                Int32 QueryRequestId = 0;

                if (objActionEntity.RecordId != 0)
                {
                    QueryRequestId = objActionEntity.RecordId;
                }

                if (objActionEntity.ActionState == "Edit")
                {
                    queryEntity = ShowRecord(QueryRequestId);
                }

                else if (objActionEntity.ActionState == "View")
                {
                    queryEntity = ShowRecord(QueryRequestId);
                }

                return Ok(queryEntity);
            }
            catch (Exception ex)
            {
                WebUtils.ErrorHandler.HandleException(ex, "SellerMasterController");
                return BadRequest(ex.Message);
            }
        }

        private SellerMasterEntity ShowRecord(Int32 SellerID)
        {
            SellerMasterEntity obj = new SellerMasterEntity();
            string SQLQuery = "select * from PFT_tblwSellerMaster where SellerID=" + SellerID;
            DataTable dtData = objDAL.FetchRecords(SQLQuery);
            if (dtData != null && dtData.Rows.Count > 0)
            {
                obj.SellerID = CommonUtils.ConvertToInt64(dtData.Rows[0]["SellerID"]);
                obj.SellerCode = CommonUtils.ConvertToString(dtData.Rows[0]["SellerCode"]);
                obj.SellerName = CommonUtils.ConvertToString(dtData.Rows[0]["SellerName"]);
                obj.Address1 = CommonUtils.ConvertToString(dtData.Rows[0]["Address1"]);
                obj.Address2 = CommonUtils.ConvertToString(dtData.Rows[0]["Address2"]);
                obj.Address3 = CommonUtils.ConvertToString(dtData.Rows[0]["Address3"]);
                obj.PinCode = CommonUtils.ConvertToString(dtData.Rows[0]["PinCode"]);
                obj.City = CommonUtils.ConvertToString(dtData.Rows[0]["City"]);
                obj.State = CommonUtils.ConvertToString(dtData.Rows[0]["State"]);
                obj.ContactName = CommonUtils.ConvertToString(dtData.Rows[0]["ContactName"]);
                obj.Mobile = CommonUtils.ConvertToString(dtData.Rows[0]["Mobile"]);
                obj.Telephone = CommonUtils.ConvertToString(dtData.Rows[0]["Telephone"]);
                obj.Email = CommonUtils.ConvertToString(dtData.Rows[0]["Email"]);
                obj.Designation = CommonUtils.ConvertToString(dtData.Rows[0]["Designation"]);
                obj.IsDelete = CommonUtils.ConvertToBoolean(dtData.Rows[0]["IsDelete"]);
            }

            return obj;
        }

        [HttpPost]
        public IHttpActionResult DeleteSellerMaster(ActionEntity objActionEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/SellerMaster/sellermasterlist.html");

                SellerMasterEntity queryEntity = new SellerMasterEntity();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL = new BaseDAL();
                objDAL.CreateConnection();

                Int32 RequestId = 0;

                if (objActionEntity.RecordId != 0)
                {
                    RequestId = objActionEntity.RecordId;
                }

                if (objActionEntity.ActionState == "Delete")
                {
                    List<SqlParameter> Parameters = new List<SqlParameter>();
                    SqlParameter parameter = new SqlParameter();

                    Parameters.Add(new SqlParameter("@SellerID", RequestId));

                    objDAL.ExecuteSP("spPFT_tblwSellerMasterDelete", ref Parameters);
                }

                return Ok(queryEntity);
            }
            catch (Exception x)
            {
                WebUtils.ErrorHandler.HandleException(x, "SellerMasterController");
                return BadRequest(x.Message);
            }
        }

        #region DropDown Method

        [HttpPost]
        public IHttpActionResult GetSellerMasterDropDown(FilterEntity filterEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/SellerMaster/sellermasterlist.html");

                objDAL = new BaseDAL();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL.CreateConnection();

                string strQuery = "Select SellerID,SellerCode from PFT_tblwSellerMaster where IsDelete = 0";
                DataTable dtGi = objDAL.FetchRecords(strQuery);

                objDAL.CloseConnection();

                return Ok(dtGi);
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "SellerMasterController");
                return BadRequest(ex.Message);
            }
        }

        #endregion

    }
}
