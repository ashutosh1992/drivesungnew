﻿using Newtonsoft.Json;
using PFTrustAPI.Controllers.CommonUtility;
using PFTrustAPI.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;


namespace PFTrustAPI.Controllers
{
    [Authorize]
    public class RegistrarController : ApiController
    {
        UserAuthentication auth = new UserAuthentication();
        BaseDAL objDAL;

        [HttpPost]
        public IHttpActionResult GetRegistrar(FilterEntity filterEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                objDAL = new BaseDAL();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL.CreateConnection();

                string strQuery = "Select * from PFT_tblRegistrarMaster where IsDelete = 0";
                DataTable dtGi = objDAL.FetchRecords(strQuery);

                objDAL.CloseConnection();

                return Ok(dtGi);
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "RegistrarController");
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult SaveRegistrar(RegistrarEntity objRegistrar)
        {
            Sessions session = new Sessions();
            SessionEntity sessionEntity = session.GetSessionEntity();
            Int64 _userId = sessionEntity.UserId;
            BaseDAL objDAL = new BaseDAL();
            try
            {
                UserAuthentication auth = new UserAuthentication();
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                ResponseEntity responseEntity = new ResponseEntity();
                responseEntity.IsComplete = false;

                string _actionState = "";
                Int64 registrarID = 0;

                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                //clsRembDocumentSettingBO objclsRembDocumentSettingBO = new clsRembDocumentSettingBO();
                SqlParameter sqlParameter;

                if (objRegistrar.actionEntity != null)
                    _actionState = objRegistrar.actionEntity.ActionState;

                if (_actionState.Equals("New"))
                {
                    bool IsExist = Validate(objRegistrar.RegistrarCode);
                    if (IsExist)
                    {
                        responseEntity.IsComplete = false;
                        responseEntity.Description = "Registrar Code Already Exists.";
                        return Ok(responseEntity);
                    }
                }
                else if (_actionState.Equals("Edit"))
                {
                    registrarID = objRegistrar.actionEntity.RecordId;
                }

                // Transition Is Started....
                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                sqlParameter = new SqlParameter("@RegistrarID", SqlDbType.Int, 9);
                sqlParameter.Value = registrarID;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@RegistrarCode", SqlDbType.NVarChar, 20);
                sqlParameter.Value = objRegistrar.RegistrarCode;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@RegistrarName", SqlDbType.NVarChar, 250);
                sqlParameter.Value = objRegistrar.RegistrarName;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Address1", SqlDbType.NVarChar, 250);
                sqlParameter.Value = objRegistrar.Address1;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Address2", SqlDbType.NVarChar, 250);
                sqlParameter.Value = objRegistrar.Address2;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Address3", SqlDbType.NVarChar, 250);
                sqlParameter.Value = objRegistrar.Address3;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@PinCode", SqlDbType.NVarChar, 6);
                sqlParameter.Value = objRegistrar.PinCode;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@City", SqlDbType.NVarChar, 150);
                sqlParameter.Value = objRegistrar.City;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@State", SqlDbType.NVarChar, 150);
                sqlParameter.Value = objRegistrar.State;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@ContactName", SqlDbType.NVarChar, 255);
                sqlParameter.Value = objRegistrar.ContactName;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Mobile", SqlDbType.NVarChar, 10);
                sqlParameter.Value = objRegistrar.Mobile;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Telephone", SqlDbType.NVarChar, 20);
                sqlParameter.Value = objRegistrar.Telephone;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Email", SqlDbType.NVarChar, 70);
                sqlParameter.Value = objRegistrar.Email;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Designation", SqlDbType.NVarChar, 70);
                sqlParameter.Value = objRegistrar.Designation;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@IsDelete", SqlDbType.Bit, 1);
                sqlParameter.Value = 0;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@ICreater", SqlDbType.Int, 9);
                sqlParameter.Value = _userId;
                sqlParameters.Add(sqlParameter);

                if (_actionState.Equals("New"))
                {
                    sqlParameter = new SqlParameter("@edit", SqlDbType.Bit, 1);
                    sqlParameter.Value = 1;
                    sqlParameters.Add(sqlParameter);
                }
                else if (_actionState.Equals("Edit"))
                {
                    sqlParameter = new SqlParameter("@edit", SqlDbType.Bit, 1);
                    sqlParameter.Value = 0;
                    sqlParameters.Add(sqlParameter);
                }

                objDAL.ExecuteSP("spPFT_tblRegistrarMasterAddUpdate", ref sqlParameters);

                objDAL.CommitTransaction();
                objDAL.CloseConnection();

                responseEntity.IsComplete = true;
                responseEntity.Description = "Registrar Saved Successfully.";

                return Ok(responseEntity);
            }

            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "RegistrarController");
                return Ok();
            }
        }

        [HttpPost]
        public IHttpActionResult GetRegistrarById(ActionEntity objActionEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                RegistrarEntity registrarEntity = new RegistrarEntity();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL = new BaseDAL();
                objDAL.CreateConnection();

                Int32 RequestId = 0;

                if (objActionEntity.RecordId != 0)
                {
                    RequestId = objActionEntity.RecordId;
                }

                if (objActionEntity.ActionState == "Edit")
                {
                    registrarEntity = ShowRecord(RequestId);
                }

                else if (objActionEntity.ActionState == "View")
                {
                    registrarEntity = ShowRecord(RequestId);
                }

                return Ok(registrarEntity);
            }
            catch (Exception ex)
            {
                WebUtils.ErrorHandler.HandleException(ex, "RegistrarController");
                return BadRequest(ex.Message);
            }
        }

        private RegistrarEntity ShowRecord(Int32 RegistrarID)
        {
            RegistrarEntity obj = new RegistrarEntity();
            string SQLQuery = "select * from PFT_tblRegistrarMaster where RegistrarID=" + RegistrarID;
            DataTable dtData = objDAL.FetchRecords(SQLQuery);
            if (dtData != null && dtData.Rows.Count > 0)
            {
                obj.RegistrarID = CommonUtils.ConvertToDecimal(dtData.Rows[0]["RegistrarID"]);
                obj.RegistrarCode = CommonUtils.ConvertToString(dtData.Rows[0]["RegistrarCode"]);
                obj.RegistrarName = CommonUtils.ConvertToString(dtData.Rows[0]["RegistrarName"]);
                obj.Address1 = CommonUtils.ConvertToString(dtData.Rows[0]["Address1"]);
                obj.Address2 = CommonUtils.ConvertToString(dtData.Rows[0]["Address2"]);
                obj.Address3 = CommonUtils.ConvertToString(dtData.Rows[0]["Address3"]);
                obj.PinCode = CommonUtils.ConvertToString(dtData.Rows[0]["PinCode"]);
                obj.City = CommonUtils.ConvertToString(dtData.Rows[0]["City"]);
                obj.State = CommonUtils.ConvertToString(dtData.Rows[0]["State"]);
                obj.ContactName = CommonUtils.ConvertToString(dtData.Rows[0]["ContactName"]);
                obj.Mobile = CommonUtils.ConvertToString(dtData.Rows[0]["Mobile"]);
                obj.Telephone = CommonUtils.ConvertToString(dtData.Rows[0]["Telephone"]);
                obj.Email = CommonUtils.ConvertToString(dtData.Rows[0]["Email"]);
                obj.Designation = CommonUtils.ConvertToString(dtData.Rows[0]["Designation"]);
                obj.IsDelete = CommonUtils.ConvertToBoolean(dtData.Rows[0]["IsDelete"]);
            }

            return obj;
        }

        [HttpPost]
        public IHttpActionResult DeleteRegistrar(ActionEntity objActionEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                RegistrarEntity registrarEntity = new RegistrarEntity();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL = new BaseDAL();
                objDAL.CreateConnection();

                Int32 RequestId = 0;

                if (objActionEntity.RecordId != 0)
                {
                    RequestId = objActionEntity.RecordId;
                }

                if (objActionEntity.ActionState == "Delete")
                {
                    List<SqlParameter> Parameters = new List<SqlParameter>();
                    SqlParameter parameter = new SqlParameter();

                    Parameters.Add(new SqlParameter("@RegistrarID", RequestId));

                    objDAL.ExecuteSP("spPFT_tblRegistrarMasterDelete", ref Parameters);
                }

                return Ok(registrarEntity);
            }
            catch (Exception x)
            {
                WebUtils.ErrorHandler.HandleException(x, "RegistrarController");
                return BadRequest(x.Message);
            }
        }

        public bool Validate(string RegistrarCode)
        {
            BaseDAL objDAL = new BaseDAL();
            bool IsExist = true;

            try
            {
                string strQuery = "Select COUNT(RegistrarCode) As CNT From PFT_tblRegistrarMaster Where RegistrarCode = '" + RegistrarCode + "' AND IsDelete = 0";
                objDAL.CreateConnection();

                DataTable dtData = objDAL.FetchRecords(strQuery);

                if (dtData != null && dtData.Rows.Count > 0)
                {
                    int Records = CommonUtils.ConvertToInt(dtData.Rows[0]["CNT"]);

                    if (Records > 0)
                    {
                        IsExist = true;
                    }
                    else
                    {
                        IsExist = false;
                    }
                }
                return IsExist;
            }
            catch (Exception x)
            {
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(x, "RegistrarController");
                return false;
            }
        }

        #region DropDown Method

        [HttpPost]
        public IHttpActionResult GetRegistrarDropDown(FilterEntity filterEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                objDAL = new BaseDAL();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL.CreateConnection();

                string strQuery = "Select RegistrarID,RegistrarName from PFT_tblRegistrarMaster where IsDelete = 0";
                DataTable dtGi = objDAL.FetchRecords(strQuery);

                objDAL.CloseConnection();

                return Ok(dtGi);
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "RegistrarController");
                return BadRequest(ex.Message);
            }
        }

        #endregion
    }
}
