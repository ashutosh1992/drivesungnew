﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using IOTBusinessEntities;
using IOTBusinessServices;
using IOTUtilities;
using IOTWebAPI.Filters;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Text;
using System.IO;

//suppress the warning of xml documentation 
#pragma warning disable 1591
namespace IOTWebAPI.Controllers
{
    //[Authorize]
    [CustomExceptionFilter]
    [DeflateCompression]
    public class CommonController : ApiController
    {
        [HttpPost]
        public async Task<IHttpActionResult> GetUserList(FilterEntity filterEntity)
        {
            // Validatde entity
            string strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //Result
            using (CommonBS objCommonBS = new CommonBS())
            {
                DataTable dtUser = await objCommonBS.GetUserList(filterEntity);
                return Ok(CustomeResponse.getOkResponse(dtUser));
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> SendPushNotification(NotificationEntity notificationEntity)
        {
            // Validatde entity
          try
            {
                string deviceId;
                string strResult = ValidateNotificationEntity.validateNotificationEntity(notificationEntity, s_user_id: true, s_role_Id: true);
                if (strResult != "")
                {
                    return BadRequest(strResult);
                }

                using (CommonBS objCommonBS = new CommonBS())
                {
                    DataTable dtUser = await objCommonBS.GetUserToken(notificationEntity);
                 if(  dtUser.Rows.Count > 0)
                 {
                    
                    deviceId =CommonUtils.ConvertToString( dtUser.Rows[0]["user_token"]);
                 }
                    else
                  {
                     deviceId = " ";
                     strResult = "Token is not Available";
                     return BadRequest(strResult);
                  }
                }
                string serverKey = System.Configuration.ConfigurationManager.AppSettings["serverKey"];

                string applicationID = System.Configuration.ConfigurationManager.AppSettings["applicationID"];

                string senderId = System.Configuration.ConfigurationManager.AppSettings["senderId"];

              

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var data = new
                {
                    to = deviceId,
                    notification = new
                    {
                        body = notificationEntity.body,
                        title = notificationEntity.title,
                        sound = "Enabled"

                    }
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                //  tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                tRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                                Console.WriteLine(str);
                            }
                        }
                    }
                }
                return Ok(CustomeResponse.getMsgResponse("Success"));
            }
            catch (Exception ex)
            {
                string strResult = ex.Message;
                return BadRequest(strResult);
                //string str = ex.Message;
                //return Ok(CustomeResponse.getMsgResponse("Success"));
            }
        }


        #region ------------Engine capacity -----------

        [HttpPost]
        public async Task<IHttpActionResult> GetEngineCapacityList(Engine_CapacityEntity enginecapacityEntity)
        {
            // Validatde entity
            string strResult = ValidateEngineCapacityEntity.ValidatenginecapacityEntity(enginecapacityEntity, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //Result
            using (CommonBS objCommonBS = new CommonBS())
            {
                DataTable dtUser = await objCommonBS.GetEngineCapacityList();
                return Ok(CustomeResponse.getOkResponse(dtUser));
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> CreateEngineCapacity(Engine_CapacityEntity enginecapacityEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEngineCapacityEntity.ValidatenginecapacityEntity(enginecapacityEntity,eng_capacity_range:true, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (CommonBS objCommonBS = new CommonBS())
            {
                if (await objCommonBS.CreateEngineCapacity(enginecapacityEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }
            }

        }

        [HttpPost]
        public async Task<IHttpActionResult> GetEngineCapacityById(Engine_CapacityEntity enginecapacityEntity)
        {
            // Validatde entity
            string strResult = ValidateEngineCapacityEntity.ValidatenginecapacityEntity(enginecapacityEntity,eng_capacity_id:true, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //Result
            using (CommonBS objCommonBS = new CommonBS())
            {
                DataTable dtUser = await objCommonBS.GetEngineCapacityById(enginecapacityEntity);
                return Ok(CustomeResponse.getOkResponse(dtUser));
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> UpdateEngineCapacity(Engine_CapacityEntity enginecapacityEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEngineCapacityEntity.ValidatenginecapacityEntity(enginecapacityEntity, eng_capacity_id:true, eng_capacity_range: true, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (CommonBS objCommonBS = new CommonBS())
            {
                if (await objCommonBS.UpdateEngineCapacity(enginecapacityEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }
            }

        }
        

        #endregion

        #region ------------Engine type -----------

        [HttpPost]
        public async Task<IHttpActionResult> GetEngineTypeList(Engine_TypeEntity enginetypeEntity)
        {
            // Validatde entity
            string strResult = ValidateEngineTypeEntity.ValidatenginetypeEntity(enginetypeEntity, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //Result
            using (CommonBS objCommonBS = new CommonBS())
            {
                DataTable dtUser = await objCommonBS.GetEngineTypeList();
                return Ok(CustomeResponse.getOkResponse(dtUser));
            }
        }


        [HttpPost]
        public async Task<IHttpActionResult> CreateEngineType(Engine_TypeEntity enginetypeEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEngineTypeEntity.ValidatenginetypeEntity(enginetypeEntity, eng_type: true, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (CommonBS objCommonBS = new CommonBS())
            {
                if (await objCommonBS.CreateEngineType(enginetypeEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }
            }

        }

        [HttpPost]
        public async Task<IHttpActionResult> GetEngineTypeById(Engine_TypeEntity enginetypeEntity)
        {
            string strResult;
            strResult = ValidateEngineTypeEntity.ValidatenginetypeEntity(enginetypeEntity, eng_type_id:true, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //Result
            using (CommonBS objCommonBS = new CommonBS())
            {
                DataTable dtUser = await objCommonBS.GetEngineTypeById(enginetypeEntity);
                return Ok(CustomeResponse.getOkResponse(dtUser));
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> UpdateEngineType(Engine_TypeEntity enginetypeEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEngineTypeEntity.ValidatenginetypeEntity(enginetypeEntity, eng_type: true, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (CommonBS objCommonBS = new CommonBS())
            {
                if (await objCommonBS.UpdateEngineType(enginetypeEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }
            }

        }

        #endregion

        #region ------------Vehicle  type -----------

        [HttpPost]
        public async Task<IHttpActionResult> GetVehicleTypeList(Engine_CapacityEntity enginecapacityEntity)
        {
            // Validatde entity
            string strResult = ValidateEngineCapacityEntity.ValidatenginecapacityEntity(enginecapacityEntity, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //Result
            using (CommonBS objCommonBS = new CommonBS())
            {
                DataTable dtUser = await objCommonBS.GetVehicleTypeList();
                return Ok(CustomeResponse.getOkResponse(dtUser));
            }
        }


        [HttpPost]
        public async Task<IHttpActionResult> CreateVehicleType(Vehicle_TypeEntity vehicletypeEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateVehicleTypeEntity.ValidatvehicletypeEntity(vehicletypeEntity, vehicle_type: true, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (CommonBS objCommonBS = new CommonBS())
            {
                if (await objCommonBS.CreateVehicleType(vehicletypeEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }
            }

        }

        [HttpPost]
        public async Task<IHttpActionResult> GetVehicleTypeById(Vehicle_TypeEntity vehicletypeEntity)
        {
            string strResult;
            strResult = ValidateVehicleTypeEntity.ValidatvehicletypeEntity(vehicletypeEntity, vehicle_type_id:true, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //Result
            using (CommonBS objCommonBS = new CommonBS())
            {
                DataTable dtUser = await objCommonBS.GetVehicleTypeById(vehicletypeEntity);
                return Ok(CustomeResponse.getOkResponse(dtUser));
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> UpdateVehicleType(Vehicle_TypeEntity vehicletypeEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateVehicleTypeEntity.ValidatvehicletypeEntity(vehicletypeEntity, vehicle_type: true, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (CommonBS objCommonBS = new CommonBS())
            {
                if (await objCommonBS.UpdateVehicleType(vehicletypeEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }
            }

        }

        #endregion


        #region ------------Country Wise Road Assistance  -----------

        [HttpPost]
        public async Task<IHttpActionResult> GetRoadSideAssistanceByCountry(Roadside_AssistanceEntity roadsideassistanceEntity)
        {
            // Validatde entity
            string strResult = ValidateRoadsideAssistanceEntity.ValidatroadsideassistanceEntity(roadsideassistanceEntity, country_name:true, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //Result
            using (CommonBS objCommonBS = new CommonBS())
            {
                DataTable dtUser = await objCommonBS.GetRoadSideAssistanceByCountry(roadsideassistanceEntity);
                return Ok(CustomeResponse.getOkResponse(dtUser));
            }
        }
        #endregion







    }
}