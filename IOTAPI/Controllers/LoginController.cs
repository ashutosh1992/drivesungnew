﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using IOTBusinessEntities;
using IOTBusinessServices;
using IOTWebAPI.Filters;
using System.Threading.Tasks;
using IOTUtilities;
//suppress the warning of xml documentation 
#pragma warning disable 1591

namespace IOTWebAPI.Controllers
{
    [Authorize]
    [CustomExceptionFilter]
    [DeflateCompression]
    public class LoginController : ApiController
    {
        /// <summary>  
        /// authenticate user.
        /// </summary>  
        /// <param name="userEntity">Parameters list for Auth = user_name (*),user_pwd(*)(</param>  
        /// <returns>response = 200-Ok-{"isComplete": true,"description": "success","data": {"user_id": 2,"user_name": "user_name11111", "role_Id": 1, "user_role": "cluser", "userPIN": null }}
        /// ///400- bad request-{ "message": "bad req msg"}
        /// ///500 -internal server error-{ "message": "server erroe msg"}
        /// </returns>  
        [HttpPost]
        public async Task<IHttpActionResult> Auth(UserEntity userEntity)
        {
            string strResult = ValidateEntity.ValidateUserEntity(userEntity, user_name: true,user_pwd:true );
            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (UserAuthenticationBS objUserAuthenticationBS = new UserAuthenticationBS())
            {
                ResponseEntity objResponseEntity = new ResponseEntity();
                objResponseEntity = await objUserAuthenticationBS.UserLogin(userEntity.user_name,userEntity.user_pwd,userEntity);
                return Ok(objResponseEntity);
            }
        }

    }
}
