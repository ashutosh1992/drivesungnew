﻿using IOTBusinessEntities;
using IOTBusinessServices;
using IOTUtilities;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace IOTWebAPI.Controllers
{
    public class PolicyClaimController : ApiController
    {
        [HttpPost]
        public async Task<IHttpActionResult> CreatePolicyClaim()
        {

            var obj = HttpContext.Current.Request.Form["file"];


            if (HttpContext.Current.Request.Form["data"] == null || HttpContext.Current.Request.Form["data"] == "")
                return BadRequest("invalid data entity");

            string policyClaimData = HttpContext.Current.Request.Form["data"];
            PolicyClaimEntity policyClaimEntity = JsonConvert.DeserializeObject<PolicyClaimEntity>(policyClaimData);

            // Validatde entity
            string strResult;
            string Url = " ";
            strResult = ValidateEntity.ValidatePolicyClaim(policyClaimEntity, s_role_Id: true, s_user_id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }
            using (PolicyClaimBS objPolicyClaimBS = new PolicyClaimBS())
            {
                int claim_id = await objPolicyClaimBS.createPolicyClaim(policyClaimEntity);
                if (claim_id > 0)
                {
                    int count = HttpContext.Current.Request.Files.Count;
                    if (count > 0)
                    {
                        CommonUtils.CheckexistPath(CommonUtils.ConvertToString(claim_id), "PolicyClaim");
                        for (int i = 0; i < count; i++)
                        {
                            var file = HttpContext.Current.Request.Files[i];
                            if (file != null && file.ContentLength > 0)
                            {

                                string profile_picture_uri = "";
                                string strPostedPath = CommonUtils.PolicyClaimPath(file.FileName, CommonUtils.ConvertToString(claim_id), CommonUtils.ConvertToString(claim_id) + "-" + CommonUtils.ConvertToString(i), "PolicyClaim", out profile_picture_uri);
                                file.SaveAs(strPostedPath);
                                //policyClaimEntity.PolicyDocEntities[i].doc_file_path = profile_picture_uri;
                                //await objPolicyClaimBS.UpdatePolicyProfilePath(claim_id, profile_picture_uri, policyClaimEntity.PolicyDocEntities[i].doc_name);
                                string strUrlPath = CommonUtils.getUrlDocPath(strPostedPath, "PolicyClaim", CommonUtils.ConvertToString(claim_id), out Url);
                                string path = Url;

                            }

                        }

                    }

                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }

        }

        [HttpPost]
        public async Task<IHttpActionResult> getPolicyClaimList(PolicyClaimEntity policyClaimEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidatePolicyClaim(policyClaimEntity, s_role_Id: true, s_user_id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (PolicyClaimBS objPolicyClaimBS = new PolicyClaimBS())
            {
                List<PolicyClaimEntity> dtPolicyClaim = await objPolicyClaimBS.GetPolicyClaimList(policyClaimEntity);
                return Ok(CustomeResponse.getOkResponse(dtPolicyClaim));
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> getPolicyClaimById(PolicyClaimEntity policyClaimEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidatePolicyClaim(policyClaimEntity, s_role_Id: true, s_user_id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (PolicyClaimBS objPolicyClaimBS = new PolicyClaimBS())
            {
                List<PolicyClaimEntity> dtPolicyClaim = await objPolicyClaimBS.GetPolicyClaimById(policyClaimEntity);
                return Ok(CustomeResponse.getOkResponse(dtPolicyClaim));
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> UpdatePolicyClaim()
        {

            if (HttpContext.Current.Request.Form["data"] == null || HttpContext.Current.Request.Form["data"] == "")
                return BadRequest("invalid data entity");

            string policyClaimData = HttpContext.Current.Request.Form["data"];
            PolicyClaimEntity policyClaimEntity = JsonConvert.DeserializeObject<PolicyClaimEntity>(policyClaimData);

            // Validatde entity
            string strResult;
            string Url = " ";
            strResult = ValidateEntity.ValidatePolicyClaim(policyClaimEntity, claim_id: true, s_role_Id: true, s_user_id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }
            using (PolicyClaimBS objPolicyClaimBS = new PolicyClaimBS())
            {

                if (await objPolicyClaimBS.UpdatePolicyClaim(policyClaimEntity))
                {
                    int count = HttpContext.Current.Request.Files.Count;
                    if (count > 0)
                    {
                        CommonUtils.CheckexistPath(CommonUtils.ConvertToString(policyClaimEntity.claim_id), "PolicyClaim");
                        for (int i = 0; i < count; i++)
                        {
                            var file = HttpContext.Current.Request.Files[i];
                            if (file != null && file.ContentLength > 0)
                            {

                                string profile_picture_uri = "";
                                int no = policyClaimEntity.PolicyDocEntities.Count;
                                int total = no - (count - i);
                                string strPostedPath = CommonUtils.PolicyClaimPath(file.FileName, CommonUtils.ConvertToString(policyClaimEntity.claim_id), CommonUtils.ConvertToString(policyClaimEntity.claim_id) + "-" + CommonUtils.ConvertToString(i), "PolicyClaim", out profile_picture_uri);
                                file.SaveAs(strPostedPath);
                                //await objPolicyClaimBS.UpdatePolicyProfilePath(policyClaimEntity.claim_id, profile_picture_uri, policyClaimEntity.PolicyDocEntities[total].doc_name);
                                string strUrlPath = CommonUtils.getUrlDocPath(strPostedPath, "PolicyClaim", CommonUtils.ConvertToString(policyClaimEntity.claim_id), out Url);
                                string path = Url;
                            }
                        }

                    }

                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }

        }

        [HttpPost]
        public async Task<IHttpActionResult> DeletePolicyClaim(PolicyClaimEntity policyClaimEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidatePolicyClaim(policyClaimEntity, claim_id: true, s_role_Id: true, s_user_id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (PolicyClaimBS objPolicyClaimBS = new PolicyClaimBS())
            {
                if (await objPolicyClaimBS.DeletePolicyClaim(policyClaimEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }

        }


        [HttpPost]
        public async Task<IHttpActionResult> AddPolicyClaimRemark(PolicyClaimEntity policyClaimEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidatePolicyClaim(policyClaimEntity, claim_id: true, remark: true, status: true, s_role_Id: true, s_user_id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (PolicyClaimBS objPolicyClaimBS = new PolicyClaimBS())
            {
                if (await objPolicyClaimBS.AddPolicyClaimRemark(policyClaimEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());

                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }
        }

        #region ----Send Insurance to company-----

        [HttpPost]
        public async Task<IHttpActionResult> SendClaimInsurance()
        {

            var obj = HttpContext.Current.Request.Form["file"];


            if (HttpContext.Current.Request.Form["data"] == null || HttpContext.Current.Request.Form["data"] == "")
                return BadRequest("invalid data entity");

            string policyClaimData = HttpContext.Current.Request.Form["data"];
            PolicyClaimEntity policyClaimEntity = JsonConvert.DeserializeObject<PolicyClaimEntity>(policyClaimData);

            // Validatde entity
            string strResult;
            string Url = " ";
            MailMessage msg = new MailMessage();
            AlternateView AV = AlternateView.CreateAlternateViewFromString("", null, MediaTypeNames.Text.Html);
            strResult = ValidateEntity.ValidatePolicyClaim(policyClaimEntity, s_role_Id: true, s_user_id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            int count = HttpContext.Current.Request.Files.Count;
            if (count > 0)
            {
                CommonUtils.CheckexistPath(CommonUtils.ConvertToString(policyClaimEntity.policy_no), "PolicyClaim");
                for (int i = 0; i < count; i++)
                {
                    var file = HttpContext.Current.Request.Files[i];
                    if (file != null && file.ContentLength > 0)
                    {

                        string profile_picture_uri = "";
                        string strPostedPath = CommonUtils.PolicyClaimPath(file.FileName, CommonUtils.ConvertToString(policyClaimEntity.policy_no), CommonUtils.ConvertToString(policyClaimEntity.policy_no) + "-" + CommonUtils.ConvertToString(i), "PolicyClaim", out profile_picture_uri);
                        file.SaveAs(strPostedPath);
                        //policyClaimEntity.PolicyDocEntities[i].doc_file_path = profile_picture_uri;
                        //await objPolicyClaimBS.UpdatePolicyProfilePath(claim_id, profile_picture_uri, policyClaimEntity.PolicyDocEntities[i].doc_name);
                        string strUrlPath = CommonUtils.getUrlDocPath(file.FileName, "PolicyClaim", CommonUtils.ConvertToString(policyClaimEntity.policy_no), out Url);
                        string path = Url;

                        MemoryStream files = new MemoryStream(PDFGenerate("Insurance Document", strPostedPath).ToArray());

                        files.Seek(0, SeekOrigin.Begin);
                        Attachment data = new Attachment(files, "Claim Insurance Document.pdf", "application/pdf");
                        ContentDisposition disposition = data.ContentDisposition;
                        disposition.CreationDate = System.DateTime.Now;
                        disposition.ModificationDate = System.DateTime.Now;
                        disposition.DispositionType = DispositionTypeNames.Attachment;
                        msg.Attachments.Add(data);//Attach the file  
                    }

                }


                string mailfrom = System.Configuration.ConfigurationManager.AppSettings["Mailfrom"];
                msg.From = new MailAddress(mailfrom, mailfrom);
                msg.To.Add(policyClaimEntity.Email_id);
                msg.Subject = "Insurance Mail";
                msg.AlternateViews.Add(AV);
                msg.IsBodyHtml = true;
                msg.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                msg.Priority = MailPriority.High;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = System.Configuration.ConfigurationManager.AppSettings["Hostname"];
                //smtp.EnableSsl = true;
                smtp.Port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Portno"]);
                smtp.UseDefaultCredentials = false;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["smtpusername"], System.Configuration.ConfigurationManager.AppSettings["smtppassword"]);
                try
                {
                    smtp.Send(msg);


                }
                catch
                {

                }

            }


            return Ok(CustomeResponse.getOkResponse());


        }

        private MemoryStream PDFGenerate(string message, string ImagePath)
        {

            MemoryStream output = new MemoryStream();

            Document pdfDoc = new Document(PageSize.A4, 25, 10, 25, 10);
            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, output);

            pdfDoc.Open();
            Paragraph Text = new Paragraph(message);
            pdfDoc.Add(Text);

            byte[] file;
            file = System.IO.File.ReadAllBytes(ImagePath);

            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(file);
            jpg.ScaleToFit(550F, 200F);
            pdfDoc.Add(jpg);

            pdfWriter.CloseStream = false;
            pdfDoc.Close();
            output.Position = 0;

            return output;
        }

       #endregion






    }
}



