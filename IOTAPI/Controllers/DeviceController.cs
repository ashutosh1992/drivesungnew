﻿using IOTBusinessEntities;
using IOTBusinessServices;
using IOTUtilities;
using IOTWebAPI.Filters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace IOTWebAPI.Controllers
{
    [Authorize]
    [CustomExceptionFilter]
    [DeflateCompression]
    public class DeviceController : ApiController
    {
        [HttpPost]
        public async Task<IHttpActionResult> CreateDevice(DeviceEntity deviceEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidateDevice(deviceEntity,deviceno:true,vendor_name:true,framework:true,versionno:true,sim_no:true,imei_no:true,sim_installation:true,sim_activation_date:true,sim_expiry_date:true,sms_active:true,device_live_date:true,s_user_id:true,s_role_Id:true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (DeviceBS objDeviceBS = new DeviceBS())
            {
                if (await objDeviceBS.CreateDevice(deviceEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }

        }

        [HttpPost]
        public async Task<IHttpActionResult> GetDeviceList(DeviceEntity deviceEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidateDevice(deviceEntity, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (DeviceBS objDeviceBS = new DeviceBS())
            {
               DataTable dtDevice =await objDeviceBS.GetDeviceList();
               return Ok(CustomeResponse.getOkResponse(dtDevice));

            }
         }

        [HttpPost]
        public async Task<IHttpActionResult> GetDeviceById(DeviceEntity deviceEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidateDevice(deviceEntity, deviceno:true, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (DeviceBS objDeviceBS = new DeviceBS())
            {
                DataTable dtDevice = await objDeviceBS.GetDeviceById(deviceEntity);
                return Ok(CustomeResponse.getOkResponse(dtDevice));

            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> UpdateDevice(DeviceEntity deviceEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidateDevice(deviceEntity, deviceno: true, vendor_name: true, framework: true, versionno: true, sim_no: true, imei_no: true, sim_installation: true, sim_activation_date: true, sim_expiry_date: true, sms_active: true, device_live_date: true, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (DeviceBS objDeviceBS = new DeviceBS())
            {
                if (await objDeviceBS.Updatedevice(deviceEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }

        }

        [HttpPost]
        public async Task<IHttpActionResult> DeleteDevice(DeviceEntity deviceEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidateDevice(deviceEntity, deviceno:true, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }


            using (DeviceBS objDeviceBS = new DeviceBS())
            {
                if (await objDeviceBS.DeleteDevice(deviceEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }

        }

        [HttpPost]
        public IHttpActionResult CheckDeviceAvailability(DeviceEntity deviceEntity)
        {
            // Validatde entity
            string strResult = ValidateEntity.ValidateDevice(deviceEntity, deviceno: true);
            if (strResult != "")
            {
                return BadRequest(strResult);

            }

            //Result
            using (DeviceBS objDeviceBS = new DeviceBS())
            {
                if (objDeviceBS.CheckDeviceAvailability(deviceEntity))
                {
                    return Ok(CustomeResponse.getOkResponse("false"));
                }
                else
                {
                    return Ok(CustomeResponse.getOkResponse("true"));
                }

            }

        }

        

     }

  }



