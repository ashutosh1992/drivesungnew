﻿using IOTBusinessEntities;
using IOTBusinessServices;
using IOTUtilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace IOTWebAPI.Controllers
{
    public class MobileErrorController : ApiController
    {

        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ErrorReport()
        {
            if (HttpContext.Current.Request.Form["data"] == null || HttpContext.Current.Request.Form["data"] == "")
                return BadRequest("invalid data entity");
            //UserEntity userEntitytemp;
            string Tripdata = HttpContext.Current.Request.Form["data"];
            MobileErrorLogEntity mobilerrorLogEntity = JsonConvert.DeserializeObject<MobileErrorLogEntity>(Tripdata);
            string strResult;
            string Url = " ";
            //strResult = ValidateEntity.ValidationCompitition(compititionEntity, cmpt_name: true, cmpt_type: true, start_date: true, end_date: true, terms_conditions: true, min_km_travel: true, s_user_id: true, s_role_Id: true);

            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            using (MobileErrorLogBS objMobileErrorLogBS = new MobileErrorLogBS())
            {

                int count = HttpContext.Current.Request.Files.Count;

                if (count > 0)
                {
                    CommonUtils.CheckexistPath(CommonUtils.ConvertToString(mobilerrorLogEntity.user_id), "M_ErrorReport");
                    string filename="";
                     string strPostedPath="";
                    for (int i = 0; i < count; i++)
                    {
                        var file = HttpContext.Current.Request.Files[i];
                        if (file != null && file.ContentLength > 0)
                        {
                            string profile_picture_uri = "";
                            strPostedPath = CommonUtils.SyncLogPath(file.FileName, CommonUtils.ConvertToString(mobilerrorLogEntity.user_id), "M_ErrorReport", out profile_picture_uri);
                            file.SaveAs(strPostedPath);
                            if (filename=="")
                            {
                               filename = file.FileName;
                            }
                            else
                            {
                                 filename =filename +  "," + file.FileName;
                            }
                             
                           
                        }
                    }
                    mobilerrorLogEntity.error_file = filename;
                    if (await objMobileErrorLogBS.SaveMob_ErrorLog(mobilerrorLogEntity))
                    {
                        string strUrlPath = CommonUtils.getUrlDocPath(strPostedPath, "M_ErrorReport", CommonUtils.ConvertToString(mobilerrorLogEntity.user_id), out Url);
                        mobilerrorLogEntity.error_file = strUrlPath;
                    }
                    // return Ok(CustomeResponse.getMsgResponse("Success"));
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }

        }
    }
}
