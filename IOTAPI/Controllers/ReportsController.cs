﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Configuration;
using System.Diagnostics;
using IOTBusinessEntities;
using IOTBusinessServices;
using IOTWebAPI.Filters;
using IOTUtilities;
using System.Threading.Tasks;

//suppress the warning of xml documentation 
#pragma warning disable 1591

namespace IOTWebAPI.Controllers
{

    [Authorize]
    [CustomExceptionFilter]
    [DeflateCompression]
    public class ReportsController : ApiController
    {
          

        [HttpPost]
        public async Task<IHttpActionResult> GetAggregateData(FilterEntity filterEntity)
        {
            string strResult;
            strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, o_id: true, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);

            }

            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(filterEntity.s_user_id), CommonUtils.ConvertToInt(filterEntity.s_role_Id), page_NM: "dashboard");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            using (ReportsBS objReportsBS = new ReportsBS())
            {
                DataSet dsData = await objReportsBS.GetAggregateData(filterEntity);
                return Ok(CustomeResponse.getOkResponse(dsData));
                //return Ok(dsData);
            }
        }

         [HttpPost]
        public async Task<IHttpActionResult> GetStoppage(FilterEntity filterEntity)
        {
            string strResult;
            strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);

            }

            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(filterEntity.s_user_id), CommonUtils.ConvertToInt(filterEntity.s_role_Id), page_NM: "dashboard");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            using (ReportsBS objReportsBS = new ReportsBS())
            {
                DataSet dsData = await objReportsBS.GetStoppage(filterEntity);
                return Ok(CustomeResponse.getOkResponse(dsData));
                //return Ok(dsData);
            }
        }

         [HttpPost]
         public async Task<IHttpActionResult> GetDistancesRpt(FilterEntity filterEntity)
         {
             string strResult;
          
             strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, DeviceNo:true,FromDate:true, ToDate:true, s_user_id: true, s_role_Id: true);
             if (strResult != "")
             {
                 return BadRequest(strResult);

             }

             //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(filterEntity.s_user_id), CommonUtils.ConvertToInt(filterEntity.s_role_Id), page_NM: "dashboard");
             //if (strResult != "")
             //{
             //    return BadRequest(strResult);
             //}

             using (ReportsBS objReportsBS = new ReportsBS())
             {
                 DataTable dtData = await objReportsBS.Getdistancesrpt(filterEntity);
                 return Ok(CustomeResponse.getOkResponse(dtData));
                 //return Ok(dsData);
             }
         }

         [HttpPost]
         public async Task<IHttpActionResult> GetTempretureRpt(FilterEntity filterEntity)
         {
             string strResult;
             strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, DeviceNo:true,FromDate:true, ToDate:true, s_user_id: true, s_role_Id: true);
             if (strResult != "")
             {
                 return BadRequest(strResult);

             }

             //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(filterEntity.s_user_id), CommonUtils.ConvertToInt(filterEntity.s_role_Id), page_NM: "dashboard");
             //if (strResult != "")
             //{
             //    return BadRequest(strResult);
             //}

             using (ReportsBS objReportsBS = new ReportsBS())
             {
                 DataTable dtData = await objReportsBS.Gettempreturerpt(filterEntity);
                 return Ok(CustomeResponse.getOkResponse(dtData));
                 //return Ok(dsData);
             }
         }

         [HttpPost]
         public async Task<IHttpActionResult> GetSpeedRpt(FilterEntity filterEntity)
         {
             string strResult;
             strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, DeviceNo: true, FromDate: true, ToDate: true, s_user_id: true, s_role_Id: true);
             if (strResult != "")
             {
                 return BadRequest(strResult);

             }

             //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(filterEntity.s_user_id), CommonUtils.ConvertToInt(filterEntity.s_role_Id), page_NM: "dashboard");
             //if (strResult != "")
             //{
             //    return BadRequest(strResult);
             //}

             using (ReportsBS objReportsBS = new ReportsBS())
             {
                 DataSet dsdata = await objReportsBS.Getspeedrpt(filterEntity);
                 return Ok(CustomeResponse.getOkResponse(dsdata));
                 //return Ok(dsData);
             }
         }

        [HttpPost]
         public async Task<IHttpActionResult> GetlisttripsRpt(FilterEntity filterEntity)
         {
             string strResult;
             strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, s_user_id: true, s_role_Id: true);
             if (strResult != "")
             {
                 return BadRequest(strResult);

             }

             //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(filterEntity.s_user_id), CommonUtils.ConvertToInt(filterEntity.s_role_Id), page_NM: "dashboard");
             //if (strResult != "")
             //{
             //    return BadRequest(strResult);
             //}

             using (ReportsBS objReportsBS = new ReportsBS())
             {
                 DataTable dtData = await objReportsBS.Gettriplistrpt(filterEntity);
                 return Ok(CustomeResponse.getOkResponse(dtData));
                 //return Ok(dsData);
             }
         }

         [HttpPost]
        public async Task<IHttpActionResult> GetHB_HARpt(FilterEntity filterEntity)
         {
             string strResult;
             strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, DeviceNo: true, FromDate: true, ToDate: true, s_user_id: true, s_role_Id: true);
             if (strResult != "")
             {
                 return BadRequest(strResult);

             }

             //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(filterEntity.s_user_id), CommonUtils.ConvertToInt(filterEntity.s_role_Id), page_NM: "dashboard");
             //if (strResult != "")
             //{
             //    return BadRequest(strResult);
             //}

             using (ReportsBS objReportsBS = new ReportsBS())
             {
                 DataTable dtData = await objReportsBS.GetHB_HArpt(filterEntity);
                 return Ok(CustomeResponse.getOkResponse(dtData));
                 //return Ok(dsData);
             }
         }

         [HttpPost]
         public async Task<IHttpActionResult> GetHA_timeRpt(FilterEntity filterEntity)
         {
             string strResult;
             strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, DeviceNo: true, FromDate: true, ToDate: true, s_user_id: true, s_role_Id: true);
             if (strResult != "")
             {
                 return BadRequest(strResult);

             }

             //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(filterEntity.s_user_id), CommonUtils.ConvertToInt(filterEntity.s_role_Id), page_NM: "dashboard");
             //if (strResult != "")
             //{
             //    return BadRequest(strResult);
             //}

             using (ReportsBS objReportsBS = new ReportsBS())
             {
                 DataTable dtData = await objReportsBS.GetHA_timerpt(filterEntity);
                 return Ok(CustomeResponse.getOkResponse(dtData));
                 //return Ok(dsData);
             }
         }

        [HttpPost]
         public async Task<IHttpActionResult> GetDay_NightHrsRpt(FilterEntity filterEntity)
         {
             string strResult;
             strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, DeviceNo: true, FromDate: true, ToDate: true, s_user_id: true, s_role_Id: true);
             if (strResult != "")
             {
                 return BadRequest(strResult);

             }

             //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(filterEntity.s_user_id), CommonUtils.ConvertToInt(filterEntity.s_role_Id), page_NM: "dashboard");
             //if (strResult != "")
             //{
             //    return BadRequest(strResult);
             //}

             using (ReportsBS objReportsBS = new ReportsBS())
             {
                 DataTable dtData = await objReportsBS.Getday_nightHrsrpt(filterEntity);
                 return Ok(CustomeResponse.getOkResponse(dtData));
                 //return Ok(dsData);
             }
         }

        [HttpPost]
        public async Task<IHttpActionResult> GetDriverreportData(FilterEntity filterEntity)
        {
            string strResult;
            strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, DeviceNo: true,  s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);

            }

            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(filterEntity.s_user_id), CommonUtils.ConvertToInt(filterEntity.s_role_Id), page_NM: "dashboard");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            using (ReportsBS objReportsBS = new ReportsBS())
            {
                DataSet dsData = await objReportsBS.GetDriverperformanceData(filterEntity);
                return Ok(CustomeResponse.getOkResponse(dsData));
                //return Ok(dsData);
            }
        }

        //[HttpPost]
        //public async Task<IHttpActionResult> GetDriverreportData(FilterEntity filterEntity)
        //{
        //    string strResult;
        //    strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, DeviceNo: true, FromDate: true, ToDate: true, s_user_id: true, s_role_Id: true);
        //    if (strResult != "")
        //    {
        //        return BadRequest(strResult);

        //    }

        //    //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(filterEntity.s_user_id), CommonUtils.ConvertToInt(filterEntity.s_role_Id), page_NM: "dashboard");
        //    //if (strResult != "")
        //    //{
        //    //    return BadRequest(strResult);
        //    //}

        //    using (ReportsBS objReportsBS = new ReportsBS())
        //    {
        //        DataSet dsData = await objReportsBS.GetDriverperformanceData(filterEntity);
        //        return Ok(CustomeResponse.getOkResponse(dsData));
        //        //return Ok(dsData);
        //    }
        //}


     
    }
}
