﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using IOTBusinessEntities;
using IOTBusinessServices;
using IOTUtilities;
using IOTWebAPI.Filters;
using System.Threading.Tasks;

using System.Web;
using System.Web.Http.Cors;
using Newtonsoft.Json;

//suppress the warning of xml documentation 

#pragma warning disable 1591
namespace IOTWebAPI.Controllers
{

    [Authorize]
    [CustomExceptionFilter]
    [DeflateCompression]
    public class UserProfileController : ApiController
    {
        
        /// <summary>  
        /// check user availability.
        /// </summary>  
        /// <param name="userEntity">Parameters list for CheckUserAvailability = user_name (*)(</param>  
        /// <returns>response = 200-Ok-{isComplete: true,description: success,data: true/false}
        /// ///400- bad request-{ "message": "bad req msg"}
        /// ///500 -internal server error-{ "message": "server erroe msg"}
        /// </returns>  
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult CheckUserAvailability(UserEntity userEntity)
        {
            // Validatde entity
            string strResult = ValidateEntity.ValidateUserEntity(userEntity, user_name: true);
            if (strResult != "")
            {
                return BadRequest(strResult);

            }

            //Result
            using (UserProfileBS objUserProfileBS = new UserProfileBS())
            {
                if (objUserProfileBS.CheckUserAvailability(userEntity))
                {
                    return Ok(CustomeResponse.getOkResponse("false"));
                }
                else
                {
                    return Ok(CustomeResponse.getOkResponse("true"));
                }

            }

        }


        /// <summary>  
        /// create user profile.
        /// </summary>  
        /// <param name="userEntity">Parameters list for CreateUserProfile = role_id(*), user_name (*),user_pwd (*), first_name (*), middle_name, last_name (*), date_of_birth (*), gender (*), license_no (*), license_expiry_date (*), address1 (*),address2,address3, mobileno1 (*),mobileno2, email_id1 (*),email_id2, profile_picture_uri,s_user_id, s_role_Id</param>  
        /// <returns>response = 200-Ok-{isComplete: true,description: success,data: null}
        /// ///400- bad request-{ "message": "bad req msg"}
        /// ///500 -internal server error-{ "message": "server erroe msg"}
        /// </returns>  
        //[HttpPost]
        //[AllowAnonymous]
        //public async Task<IHttpActionResult> CreateUserProfile(UserEntity userEntity)
        //{
        //    // Validatde entity
        //    string strResult;
        //    strResult = ValidateEntity.ValidateUserEntity(userEntity, CheckUserAvailability: true, role_Id: true, user_name: true, user_pwd: true, first_name: true, last_name: true, date_of_birth: true, gender: true, mobileno: true, vehicle_color: true, email_id1: true);

        //    if (strResult != "")
        //    {
        //        return BadRequest(strResult);
        //    }

        //    //// Check Permission and session
        //    //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(userEntity.s_user_id), CommonUtils.ConvertToInt(userEntity.s_role_Id), page_NM: "userprofile", _action: "C");
        //    //if (strResult != "")
        //    //{
        //    //    return BadRequest(strResult);
        //    //}

        //    //Result
        //    using (UserProfileBS objUserProfileBS = new UserProfileBS())
        //    {
        //        if (await objUserProfileBS.CreateUserProfile(userEntity))
        //        {
        //            return Ok(CustomeResponse.getOkResponse());
        //        }
        //        else
        //        {
        //            strResult = MessageCode.MsgInJson(MsgCode.bad_request);
        //            return BadRequest(strResult);
        //        }

        //    }

        //}


        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> CreateUserProfile() 

        {
            var file1 = HttpContext.Current.Request.Files.Count;
            string strResult = "";
            string Url = "";
            if (HttpContext.Current.Request.Form["data"] == null || HttpContext.Current.Request.Form["data"] == "")
                return BadRequest("invalid data entity");
            UserEntity userEntitytemp;
            string Userdata = HttpContext.Current.Request.Form["data"];
            UserEntity userEntity = JsonConvert.DeserializeObject<UserEntity>(Userdata);
            if (userEntity != null)
            {
                if ((userEntity.user_login_type != "regi"))
                {
                    using (UserProfileBS objUserProfileBS = new UserProfileBS())
                    {
                        userEntitytemp = objUserProfileBS.CheckSocialUserAvailability(userEntity);
                    }
                    if (userEntitytemp != null)
                    {

                        //string strUrlPath = CommonUtils.getUrlPath(userEntitytemp.profile_picture_uri, "userprofile", out Url);
                        //userEntitytemp.profile_picture_uri = Url;
                        return Ok(CustomeResponse.getOkResponse(userEntitytemp));
                    }

                }
            }

            // Validatde entity

            strResult = ValidateEntity.ValidateUserEntity(userEntity, CheckUserAvailability: true, email_id1: true, mobileno: true , CheckEmailMobileAvailablity:true, role_Id: true, user_name: true, user_pwd: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(userEntity.s_user_id), CommonUtils.ConvertToInt(userEntity.s_role_Id), page_NM: "userprofile", _action: "C");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            //Result
            using (UserProfileBS objUserProfileBS = new UserProfileBS())
            {
                Int32 _uid = await objUserProfileBS.CreateUserProfile(userEntity);
                if (_uid > 0)
                {
                    userEntity.user_id = _uid;

                    var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : null;
                   
                    //if ((userEntity.social_id == null || userEntity.social_id == ""))
                    //{
                        if (file != null && file.ContentLength > 0)
                        {

                            string profile_picture_uri = "";
                            string strPostedPath = CommonUtils.getFilePath(file.FileName, "user_" + CommonUtils.ConvertToString(_uid), "userprofile", out profile_picture_uri);
                            file.SaveAs(strPostedPath);
                            string strUrlPath = CommonUtils.getUrlPath(profile_picture_uri, "userprofile", out Url);
                            userEntity.profile_picture_uri = profile_picture_uri;
                            await objUserProfileBS.UpdateUserProfilePath(userEntity);
                            userEntity.profile_picture_uri = Url;

                        }
                        else
                        {
                            userEntity.profile_picture_uri = string.Empty;
                        }
                    //}
                    //else
                    //{
                    //    await objUserProfileBS.UpdateUserProfilePath(userEntity);

                    //}

                    userEntity.user_id = _uid;
                    userEntity.deviceno = _uid;
                    return Ok(CustomeResponse.getOkResponse(userEntity));
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }

        }

        /// <summary>  
        /// get user list.
        /// </summary>  
        /// <param name="filterEntity">Parameters list for GetUserList: s_user_id, s_user_role </param>  
        /// <returns>response = 200-Ok-{"isComplete": true,  "description": "success",  "data": [ {},{}]
        /// ///400- bad request-{ "message": "bad req msg"}
        /// /// 500 -internal server error-{ "message": "server erroe msg"}
        /// </returns>  
        [HttpPost]
        public async Task<IHttpActionResult> GetUserList(FilterEntity filterEntity)
        {
            // Validatde entity
            string strResult;
            string Url = "";
            strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(filterEntity.s_user_id), CommonUtils.ConvertToInt(filterEntity.s_role_Id), page_NM: "userprofile", _action: "V");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            //Result
            using (UserProfileBS objUserProfileBS = new UserProfileBS())
            {
                DataTable dtUser = await objUserProfileBS.GetUserList(filterEntity);
                foreach (DataRow Userrow in dtUser.Rows)
                {
                    if (Userrow["profile_picture_uri"] != null)
                    {
                        string strUrlPath = CommonUtils.getUrlPath(Userrow["profile_picture_uri"].ToString(), "userprofile", out Url);
                        Userrow["profile_picture_uri"] = Url;
                    }
                }
                return Ok(CustomeResponse.getOkResponse(dtUser));

            }

        }


        /// <summary>  
        /// get user by id.
        /// </summary>  
        /// <param name="filterEntity">Parameters list for GetUserById :user_id, s_user_id, s_user_role </param>  
        /// <returns>response =200-Ok-{"isComplete": true,  "description": "success",  "data": [{}]</returns> 
        /// ///400- bad request-{ "message": "bad req msg"}
        /// ///500 -internal server error-{ "message": "server erroe msg"}
        /// </returns>  
        [HttpPost]
        public async Task<IHttpActionResult> GetUserById(FilterEntity filterEntity)
        {
            // Validatde entity
            string strResult;
            string Url = " ";
            strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, user_id: true, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(filterEntity.s_user_id), CommonUtils.ConvertToInt(filterEntity.s_role_Id), page_NM: "userprofile", _action: "V");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            //Result
            using (UserProfileBS objUserProfileBS = new UserProfileBS())
            {
                DataTable dtUser = await objUserProfileBS.GetUserById(filterEntity);
                string image_file = Convert.ToString(dtUser.Rows[0]["profile_picture_uri"]);
                string strUrlPath = CommonUtils.getUrlPath(image_file, "userprofile", out Url);
                dtUser.Rows[0]["profile_picture_uri"] = Url;
                return Ok(CustomeResponse.getOkResponse(dtUser));

            }
        }


        /// <summary>  
        /// update user profile.
        /// </summary>  
        /// <param name="userEntity">Parameters list for UpdateUserProfile =  first_name (*), middle_name, last_name (*), date_of_birth (*), gender (*), license_no (*), license_expiry_date (*), address1 (*),address2,address3, mobileno1 (*),mobileno2, email_id1 (*),email_id2, profile_picture_uri,s_user_id(*), s_role_Id(*)</param>  
        /// <returns>response = 200-Ok-{isComplete: true,description: success,data: null}
        /// ///400- bad request-{ "message": "bad req msg"}
        /// ///500 -internal server error-{ "message": "server erroe msg"}
        /// </returns>  
        [HttpPost]
        public async Task<IHttpActionResult> UpdateUserProfile()
        {

            if (HttpContext.Current.Request.Form["data"] == null || HttpContext.Current.Request.Form["data"] == "")
                return BadRequest("invalid data entity");

            string Userdata = HttpContext.Current.Request.Form["data"];
            UserEntity userEntity = JsonConvert.DeserializeObject<UserEntity>(Userdata);

            // Validatde entity
            string strResult;
            string Url = "";
            strResult = ValidateEntity.ValidateUserEntity(userEntity, first_name: true, last_name: true, gender: true, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(userEntity.s_user_id), CommonUtils.ConvertToInt(userEntity.s_role_Id), page_NM: "userprofile", _action: "U");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            //Result
            using (UserProfileBS objUserProfileBS = new UserProfileBS())
            {
                DataTable dtuser = await objUserProfileBS.UpdateUserProfile(userEntity);

                if (dtuser.Rows.Count > 0)
                {
                    Int32 user_id = Convert.ToInt32(dtuser.Rows[0]["user_id"]);
                    if (user_id > 0)
                    {
                        userEntity.user_id = user_id;
                        var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : null;

                        if (file != null && file.ContentLength > 0)
                        {
                            string profile_picture_uri = "";
                            string strPostedPath = CommonUtils.getFilePath(file.FileName, "user_" + CommonUtils.ConvertToString(user_id), "userprofile", out profile_picture_uri);
                            file.SaveAs(strPostedPath);
                            userEntity.profile_picture_uri = profile_picture_uri;
                            await objUserProfileBS.UpdateUserProfilePath(userEntity);
                            string strUrlPath = CommonUtils.getUrlPath(profile_picture_uri, "userprofile", out Url);
                            dtuser.Rows[0]["profile_picture_uri"] = Url;
                        }
                        else
                        {
                            string image_file = Convert.ToString(dtuser.Rows[0]["profile_picture_uri"]);
                            string strUrlPath = CommonUtils.getUrlPath(image_file, "userprofile", out Url);
                            dtuser.Rows[0]["profile_picture_uri"] = Url;
                            userEntity.profile_picture_uri = string.Empty;
                        }

                        userEntity.user_id = user_id;
                        return Ok(CustomeResponse.getOkResponse(dtuser));
                    }
                    else
                    {
                        strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                        return BadRequest(strResult);
                    }

                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }

        }


        /// <summary>  
        /// delete user profile.
        /// </summary>  
        /// <param name="userEntity">Parameters list for DeleteUserProfile =  user_id (*),s_user_id(*), s_role_Id(*)</param>  
        /// <returns>response = 200-Ok-{isComplete: true,description: success,data: null}
        /// ///400- bad request-{ "message": "bad req msg"}
        /// ///500 -internal server error-{ "message": "server erroe msg"}
        /// </returns>  
        [HttpPost]
        public async Task<IHttpActionResult> DeleteUserProfile(UserEntity userEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidateUserEntity(userEntity, user_id: true, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(userEntity.s_user_id), CommonUtils.ConvertToInt(userEntity.s_role_Id), page_NM: "userprofile", _action: "D");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            //Result
            using (UserProfileBS objUserProfileBS = new UserProfileBS())
            {
                if (await objUserProfileBS.DeleteUserProfile(userEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }

        }

        /// <summary>  
        /// get role list.
        /// </summary>  
        /// <param name="filterEntity">Parameters list for GetUserList: s_user_id, s_user_role </param>  
        /// <returns>response = 200-Ok-{"isComplete": true,  "description": "success",  "data": [ {},{}]
        /// ///400- bad request-{ "message": "bad req msg"}
        /// ///500 -internal server error-{ "message": "server erroe msg"}
        /// </returns>  
        [HttpPost]
        public async Task<IHttpActionResult> GetRoleList(FilterEntity filterEntity)
        {
            // Validatde entity
            string strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(filterEntity.s_user_id), CommonUtils.ConvertToInt(filterEntity.s_role_Id), chkSession: true);
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}


            using (UserProfileBS objUserProfileBS = new UserProfileBS())
            {
                DataTable dtRole = await objUserProfileBS.GetRoleList(filterEntity);
                return Ok(CustomeResponse.getOkResponse(dtRole));

            }
        }

        /// <summary>  
        /// change user password
        /// </summary>  
        /// <param name="userEntity">Parameters list for ChangeUserPwd :user_id,user_pwd,s_user_id, s_user_role </param>  
        /// <returns>response = 200-Ok-{isComplete: true,description: success,data: null}
        /// ///400- bad request-{ "message": "bad req msg"}
        /// ///500 -internal server error-{ "message": "server erroe msg"}
        /// </returns>  
        [HttpPost]
        public async Task<IHttpActionResult> ChangeUserPwd(UserEntity userEntity)
        {
            string strResult = ValidateEntity.ValidateUserEntity(userEntity, user_id: true, user_pwd: true, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(userEntity.s_user_id), CommonUtils.ConvertToInt(userEntity.s_role_Id), chkSession: true);
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}


            using (UserProfileBS objUserProfileBS = new UserProfileBS())
            {
                if (await objUserProfileBS.ChangeUserPwd(userEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {

                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }
        }


        public async Task<IHttpActionResult> UserLogOut(UserEntity userEntity)
        {
            string strResult = ValidateEntity.ValidateUserEntity(userEntity, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }
            using (UserProfileBS objUserProfileBS = new UserProfileBS())
            {
                if (await objUserProfileBS.UserlougOutLog(userEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> GetOverallRating(UserEntity userEntity)
        {
            // Validatde entity
            string strResult = ValidateEntity.ValidateUserEntity(userEntity, user_id: true, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(filterEntity.s_user_id), CommonUtils.ConvertToInt(filterEntity.s_role_Id), chkSession: true);
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}


            using (UserProfileBS objUserProfileBS = new UserProfileBS())
            {
                DataSet dtRating = await objUserProfileBS.GetOverallRating(userEntity);
                return Ok(CustomeResponse.getOkResponse(dtRating));

            }
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ForgotPassword(UserEntity userEntity)
        {
            string strResult = ValidateEntity.ValidateUserEntity(userEntity, CheckForgot_Password: true);
            if (strResult != "" && strResult != "Email" && strResult != "Mobile")
            {
                return BadRequest(strResult);
            }

            if (strResult == "Email")
            {
                using (UserProfileBS objUserProfileBS = new UserProfileBS())
                {
                    ResponseEntity objResponseEntity = new ResponseEntity();
                    bool result = await objUserProfileBS.ForgotPassword_email(userEntity);
                    if (result == true)
                    {
                        return Ok(CustomeResponse.getMsgResponse("Email Send Successfully"));

                    }
                    else
                    {
                        return Ok(CustomeResponse.getMsgResponse("Email Send fail"));
                        //return BadRequest("Email Send fail");
                    }

                }

            }
            else
            {
                //using (UserProfileBS objUserProfileBS = new UserProfileBS())
                //{
                //    ResponseEntity objResponseEntity = new ResponseEntity();
                //    await objUserProfileBS.ForgotPassword(userEntity);
                //    return Ok();
                //}
                return Ok();
            }


        }

        
        [HttpPost]
       public async Task<IHttpActionResult> UpdatePremiumData(UserEntity userEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidateUserEntity(userEntity, premium:true,  s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (UserProfileBS objUserProfileBS = new UserProfileBS())
            {
                if (await objUserProfileBS.UpdatePremiumData(userEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }
            }

        }


        //[HttpPost]
        //public async Task<IHttpActionResult> GetOverallData(UserEntity userEntity)
        //{
        //    // Validatde entity
        //    string strResult = ValidateEntity.ValidateUserEntity(userEntity, user_id: true, s_user_id: true, s_role_Id: true);

        //    if (strResult != "")
        //    {
        //        return BadRequest(strResult);
        //    }

        //    //// Check Permission and session
        //    //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(filterEntity.s_user_id), CommonUtils.ConvertToInt(filterEntity.s_role_Id), chkSession: true);
        //    //if (strResult != "")
        //    //{
        //    //    return BadRequest(strResult);
        //    //}


        //    using (UserProfileBS objUserProfileBS = new UserProfileBS())
        //    {
        //        //DataTable dtRating = await objUserProfileBS.GetOverallRating(userEntity);

        //        DataTable dtcount = new DataTable();
        //        dtcount.Clear();
        //        dtcount.Columns.Add("hb");
        //        dtcount.Columns.Add("ha");
        //        dtcount.Columns.Add("over_speed");
        //        dtcount.Columns.Add("calls_taken");
        //        dtcount.Columns.Add("calls_taken");
        //        DataRow dr = dtcount.NewRow();
        //        dr["hb"] = 10;
        //        dr["ha"] = 20;
        //        dr["cor"] = 30;
        //        dr["overspeed"] = 40;
        //        //dr["hb"] = 10;



        //        return Ok(CustomeResponse.getOkResponse(dtcount));

        //    }
        //}

    }
}
