﻿using PFTrustAPI.Controllers.CommonUtility;
using PFTrustAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PFTrustAPI.Controllers
{
    [Authorize]
    public class PurchaseController : ApiController
    {
        UserAuthentication auth = new UserAuthentication();
        BaseDAL objDAL;

        #region Purchase Master List

        [HttpPost]
        public IHttpActionResult GetPurchaseList(FilterEntity filterEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                objDAL = new BaseDAL();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL.CreateConnection();

                string strQuery = @"Select VoucherID,VoucherNo,EntryDate,SellerName,BrokerName,PartyDocNo,SellerAccountNo,DealDate,FaceValue,TotalPrice,TotalPurchaseValue
                                    FROM PFT_tblwPurchaseTransaction PT JOIN PFT_tblwSellerMaster SM ON PT.SellerID = SM.SellerID
                                    JOIN PFT_tblBrokerMaster BM ON PT.BrokerID = BM.BrokerID Where PT.IsDelete = 0";

                DataTable dtGi = objDAL.FetchRecords(strQuery);

                objDAL.CloseConnection();

                return Ok(dtGi);
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "PurchaseController");
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult GetPurchaseById(ActionEntity objActionEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                PurchaseEntity queryEntity = new PurchaseEntity();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL = new BaseDAL();
                objDAL.CreateConnection();

                Int32 QueryRequestId = 0;

                if (objActionEntity.RecordId != 0)
                {
                    QueryRequestId = objActionEntity.RecordId;
                }

                if (objActionEntity.ActionState == "Edit")
                {
                    queryEntity = ShowRecord(QueryRequestId);
                }

                else if (objActionEntity.ActionState == "View")
                {
                    queryEntity = ShowRecord(QueryRequestId);
                }

                return Ok(queryEntity);
            }
            catch (Exception ex)
            {
                WebUtils.ErrorHandler.HandleException(ex, "PurchaseController");
                return BadRequest(ex.Message);
            }
        }

        private PurchaseEntity ShowRecord(Int64 VoucherID)
        {
            PurchaseEntity obj = new PurchaseEntity();
            string SQLQuery = "select * from PFT_tblwPurchaseTransaction where VoucherID=" + VoucherID;
            DataTable dtData = objDAL.FetchRecords(SQLQuery);
            if (dtData != null && dtData.Rows.Count > 0)
            {
                obj.VoucherID = CommonUtils.ConvertToInt64(dtData.Rows[0]["VoucherID"]);
                obj.VoucherNo = CommonUtils.ConvertToString(dtData.Rows[0]["VoucherNo"]);
                obj.EntryDate = CommonUtils.ConvertToDateTime(dtData.Rows[0]["EntryDate"]);
                obj.SellerID = CommonUtils.ConvertToInt64(dtData.Rows[0]["SellerID"]);
                obj.BrokerID = CommonUtils.ConvertToInt64(dtData.Rows[0]["BrokerID"]);
                obj.PartyDocNo = CommonUtils.ConvertToString(dtData.Rows[0]["PartyDocNo"]);
                obj.PartyDocDate = CommonUtils.ConvertToDateTime(dtData.Rows[0]["PartyDocDate"]);
                obj.SellerAccountNo = CommonUtils.ConvertToString(dtData.Rows[0]["SellerAccountNo"]);
                obj.DealDate = CommonUtils.ConvertToDateTime(dtData.Rows[0]["DealDate"]);
                obj.DealDate = CommonUtils.ConvertToDateTime(dtData.Rows[0]["SettlementDate"]);
                obj.DealDate = CommonUtils.ConvertToDateTime(dtData.Rows[0]["DeliveryDate"]);
                obj.ModeOfDelivery = CommonUtils.ConvertToString(dtData.Rows[0]["ModeOfDelivery"]);
                obj.PurchaseMethod = CommonUtils.ConvertToString(dtData.Rows[0]["PurchaseMethod"]);
                obj.SecurityID = CommonUtils.ConvertToInt64(dtData.Rows[0]["SecurityID"]);
                obj.NoOfBonds = CommonUtils.ConvertToInt64(dtData.Rows[0]["NoOfBonds"]);
                obj.FaceValue = CommonUtils.ConvertToDecimal(dtData.Rows[0]["FaceValue"]);
                obj.TotalFaceValue = CommonUtils.ConvertToDecimal(dtData.Rows[0]["TotalFaceValue"]);
                obj.EffectiveFaceValuePer = CommonUtils.ConvertToDecimal(dtData.Rows[0]["EffectiveFaceValuePer"]);
                obj.EffectiveFaceValueAmt = CommonUtils.ConvertToDecimal(dtData.Rows[0]["EffectiveFaceValueAmt"]);
                obj.RatePerUnit = CommonUtils.ConvertToDecimal(dtData.Rows[0]["RatePerUnit"]);
                obj.Interest = CommonUtils.ConvertToString(dtData.Rows[0]["Interest"]);
                obj.InterestAmount = CommonUtils.ConvertToDecimal(dtData.Rows[0]["InterestAmount"]);
                obj.TotalPrice = CommonUtils.ConvertToDecimal(dtData.Rows[0]["TotalPrice"]);
                obj.SecurityPremium = CommonUtils.ConvertToDecimal(dtData.Rows[0]["SecurityPremium"]);
                obj.SecurityDiscount = CommonUtils.ConvertToDecimal(dtData.Rows[0]["SecurityDiscount"]);
                obj.SecurityBrokerageAmount = CommonUtils.ConvertToDecimal(dtData.Rows[0]["SecurityBrokerageAmount"]);
                obj.SecurityBrokerageRefund = CommonUtils.ConvertToDecimal(dtData.Rows[0]["SecurityBrokerageRefund"]);
                obj.STT = CommonUtils.ConvertToDecimal(dtData.Rows[0]["STT"]);
                obj.TotalPurchaseValue = CommonUtils.ConvertToDecimal(dtData.Rows[0]["TotalPurchaseValue"]);
                obj.YTM = CommonUtils.ConvertToDecimal(dtData.Rows[0]["YTM"]);
                obj.YieldPutCallOpt = CommonUtils.ConvertToString(dtData.Rows[0]["YieldPutCallOpt"]);
                obj.CleanPrice = CommonUtils.ConvertToDecimal(dtData.Rows[0]["CleanPrice"]);
                obj.DirtyPrice = CommonUtils.ConvertToDecimal(dtData.Rows[0]["DirtyPrice"]);
                obj.InvestmentAccount = CommonUtils.ConvertToDecimal(dtData.Rows[0]["InvestmentAccount"]);
                obj.PremiumAccount = CommonUtils.ConvertToDecimal(dtData.Rows[0]["PremiumAccount"]);
                obj.Brokerage = CommonUtils.ConvertToDecimal(dtData.Rows[0]["Brokerage"]);
                obj.InterestNotDue = CommonUtils.ConvertToDecimal(dtData.Rows[0]["InterestNotDue"]);
                obj.Discount = CommonUtils.ConvertToDecimal(dtData.Rows[0]["Discount"]);
                obj.BrokerageRefund = CommonUtils.ConvertToDecimal(dtData.Rows[0]["BrokerageRefund"]);
                obj.Party = CommonUtils.ConvertToDecimal(dtData.Rows[0]["Party"]);
                obj.IsActive = CommonUtils.ConvertToBoolean(dtData.Rows[0]["IsActive"]);
                obj.ICreater = CommonUtils.ConvertToInt64(dtData.Rows[0]["ICreater"]);
                obj.IsDelete = CommonUtils.ConvertToBoolean(dtData.Rows[0]["IsDelete"]);
                obj.IDate = CommonUtils.ConvertToDateTime(dtData.Rows[0]["IDate"]);
            }

            return obj;
        }

        #endregion

        #region Save Purchase Transaction

        [HttpPost]
        public IHttpActionResult SavePurchaseTransaction(PurchaseEntity objPurchaseEntity)
        {
            Sessions session = new Sessions();
            SessionEntity sessionEntity = session.GetSessionEntity();
            Int64 _userId = sessionEntity.UserId;
            BaseDAL objDAL = new BaseDAL();
            try
            {
                UserAuthentication auth = new UserAuthentication();
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                ResponseEntity responseEntity = new ResponseEntity();
                responseEntity.IsComplete = false;

                string _actionState = "";
                Int64 VoucherID = 0;

                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                //clsRembDocumentSettingBO objclsRembDocumentSettingBO = new clsRembDocumentSettingBO();
                SqlParameter sqlParameter;

                if (objPurchaseEntity.actionEntity != null)
                    _actionState = objPurchaseEntity.actionEntity.ActionState;

                if (_actionState.Equals("New"))
                {
                    bool IsExist = Validate(objPurchaseEntity.VoucherNo);
                    if (IsExist)
                    {
                        responseEntity.IsComplete = false;
                        responseEntity.Description = "Voucher Number Already Exists.";
                        return Ok(responseEntity);
                    }
                }
                else if (_actionState.Equals("Edit"))
                {
                    VoucherID = objPurchaseEntity.actionEntity.RecordId;
                }

                // Transition Is Started....
                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                sqlParameter = new SqlParameter("@VoucherID", SqlDbType.Int, 9);
                sqlParameter.Value = VoucherID;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@VoucherNo", SqlDbType.NVarChar, 30);
                sqlParameter.Value = objPurchaseEntity.VoucherNo;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@EntryDate", SqlDbType.DateTime);
                sqlParameter.Value = objPurchaseEntity.EntryDate;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@SellerID", SqlDbType.Int, 9);
                sqlParameter.Value = objPurchaseEntity.SellerID;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@BrokerID", SqlDbType.Int, 9);
                sqlParameter.Value = objPurchaseEntity.BrokerID;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@PartyDocNo", SqlDbType.NVarChar, 30);
                sqlParameter.Value = objPurchaseEntity.PartyDocNo;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@PartyDocDate", SqlDbType.Date);
                sqlParameter.Value = objPurchaseEntity.PartyDocDate;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@SellerAccountNo", SqlDbType.NVarChar, 30);
                sqlParameter.Value = objPurchaseEntity.SellerAccountNo;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@DealDate", SqlDbType.DateTime);
                sqlParameter.Value = objPurchaseEntity.DealDate;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@SettlementDate", SqlDbType.DateTime);
                sqlParameter.Value = objPurchaseEntity.SettlementDate;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@DeliveryDate", SqlDbType.DateTime);
                sqlParameter.Value = objPurchaseEntity.DeliveryDate;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@ModeOfDelivery", SqlDbType.NVarChar, 30);
                sqlParameter.Value = objPurchaseEntity.ModeOfDelivery;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@PurchaseMethod", SqlDbType.NVarChar, 30);
                sqlParameter.Value = objPurchaseEntity.PurchaseMethod;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@SecurityID", SqlDbType.Int, 9);
                sqlParameter.Value = objPurchaseEntity.SecurityID;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@NoOfBonds", SqlDbType.Int, 9);
                sqlParameter.Value = objPurchaseEntity.NoOfBonds;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@FaceValue", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.FaceValue;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@TotalFaceValue", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.TotalFaceValue;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@EffectiveFaceValuePer", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.EffectiveFaceValuePer;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@EffectiveFaceValueAmt", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.EffectiveFaceValueAmt;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@RatePerUnit", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.RatePerUnit;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Interest", SqlDbType.NVarChar, 20);
                sqlParameter.Value = objPurchaseEntity.Interest;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@InterestAmount", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.InterestAmount;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@TotalPrice", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.TotalPrice;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@SecurityPremium", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.SecurityPremium;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@SecurityDiscount", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.SecurityDiscount;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@SecurityBrokerageAmount", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.SecurityBrokerageAmount;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@SecurityBrokerageRefund", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.SecurityBrokerageRefund;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@STT", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.STT;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@TotalPurchaseValue", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.TotalPurchaseValue;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@YTM", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.YTM;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@YieldPutCallOpt", SqlDbType.NVarChar, 50);
                sqlParameter.Value = objPurchaseEntity.YieldPutCallOpt;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@CleanPrice", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.CleanPrice;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@DirtyPrice", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.DirtyPrice;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@InvestmentAccount", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.InvestmentAccount;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@PremiumAccount", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.PremiumAccount;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Brokerage", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.Brokerage;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@InterestNotDue", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.InterestNotDue;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Discount", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.Discount;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@BrokerageRefund", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.BrokerageRefund;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Party", SqlDbType.Decimal, 18);
                sqlParameter.Value = objPurchaseEntity.Party;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@ICreater", SqlDbType.Int, 9);
                sqlParameter.Value = _userId;
                sqlParameters.Add(sqlParameter);

                if (_actionState.Equals("New"))
                {
                    sqlParameter = new SqlParameter("@edit", SqlDbType.Bit, 1);
                    sqlParameter.Value = 1;
                    sqlParameters.Add(sqlParameter);
                }
                else if (_actionState.Equals("Edit"))
                {
                    sqlParameter = new SqlParameter("@edit", SqlDbType.Bit, 1);
                    sqlParameter.Value = 0;
                    sqlParameters.Add(sqlParameter);
                }

                objDAL.ExecuteSP("spPFT_tblwPurchaseTransactionAddUpdate", ref sqlParameters);

                objDAL.CommitTransaction();
                objDAL.CloseConnection();

                responseEntity.IsComplete = true;
                responseEntity.Description = "Purchase Saved Successfully.";

                return Ok(responseEntity);
            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "PurchaseController");
                return Ok();
                //return BadRequest(ex.Message);
            }
            finally
            {
                //objDAL.CloseConnection();
            }
        }

        public bool Validate(string VoucherNo)
        {
            BaseDAL objDAL = new BaseDAL();
            bool IsExist = true;

            try
            {
                string strQuery = "Select COUNT(VoucherNo) As CNT From PFT_tblwPurchaseTransaction Where VoucherNo = '" + VoucherNo + "' AND IsDelete = 0";
                objDAL.CreateConnection();

                DataTable dtData = objDAL.FetchRecords(strQuery);

                if (dtData != null && dtData.Rows.Count > 0)
                {
                    int Records = CommonUtils.ConvertToInt(dtData.Rows[0]["CNT"]);

                    if (Records > 0)
                    {
                        IsExist = true;
                    }
                    else
                    {
                        IsExist = false;
                    }
                }
                return IsExist;
            }
            catch (Exception x)
            {
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(x, "PurchaseController");
                return false;
            }
        }

        #endregion

        #region Delete Purchase

        [HttpPost]
        public IHttpActionResult DeletePurchase(ActionEntity objActionEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                PurchaseEntity purchaseEntity = new PurchaseEntity();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int64 UserId = sessionEntity.UserId;

                objDAL = new BaseDAL();
                objDAL.CreateConnection();

                Int32 RequestId = 0;

                if (objActionEntity.RecordId != 0)
                {
                    RequestId = objActionEntity.RecordId;
                }

                if (objActionEntity.ActionState == "Delete")
                {
                    List<SqlParameter> Parameters = new List<SqlParameter>();
                    SqlParameter parameter = new SqlParameter();
                    Parameters.Add(new SqlParameter("@VoucherID", RequestId));
                    Parameters.Add(new SqlParameter("@ICreater", UserId));

                    objDAL.ExecuteSP("spPFT_tblwPurchaseTransactionDelete", ref Parameters);
                }

                return Ok(purchaseEntity);
            }
            catch (Exception x)
            {
                WebUtils.ErrorHandler.HandleException(x, "PurchaseController");
                return BadRequest(x.Message);
            }
        }

        #endregion

        #region Dropdowns Needed on Purchase Form

        [HttpPost]
        public IHttpActionResult GetPurchaseFormDropDown(FilterEntity filterEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                objDAL = new BaseDAL();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL.CreateConnection();

                string strQuery = @"Select SellerID,SellerCode from PFT_tblwSellerMaster where IsDelete = 0;
                                    Select BrokerID,BrokerCode from PFT_tblBrokerMaster where IsDelete = 0;
                                    Select SecurityId,SecurityCode from PFT_tblwSecurityMaster where IsDelete = 0";
                DataSet dt = objDAL.FetchDataSetRecords(strQuery);

                objDAL.CloseConnection();

                return Ok(dt);
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "PurchaseController");
                return BadRequest(ex.Message);
            }
        }

        #endregion
    }
}
