﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using IOTBusinessEntities;
using IOTBusinessServices;
using IOTUtilities;
using IOTWebAPI.Filters;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
//suppress the warning of xml documentation 
#pragma warning disable 1591
namespace IOTWebAPI.Controllers
{

    [Authorize]
    [CustomExceptionFilter]
    [DeflateCompression]
     public class OrganizationController : ApiController
    {

        /// <summary>  
        /// check Organization availability.
        /// </summary>  
        /// <param name="organizationEntity">Parameters list for CheckOrganizationAvailability = o_code (*)(</param>  
        /// <returns>response = 200-Ok-{isComplete: true,description: success,data: true/false}
        /// ///400- bad request-{ "message": "bad req msg"}
        /// ///500 -internal server error-{ "message": "server erroe msg"}
        /// </returns>  
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult CheckOrganizationAvailability(OrganizationEntity organizationEntity)
        {
            // Validatde entity
            string strResult = ValidateEntity.ValidateOrganization(organizationEntity, o_code: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //Result
            using (OrganizationBS objOrganizationBS = new OrganizationBS())
            {
                if (objOrganizationBS.CheckOrganizatioAvailability(organizationEntity))
                {
                    return Ok(CustomeResponse.getOkResponse("false"));
                }
                else
                {
                    return Ok(CustomeResponse.getOkResponse("true"));
                }

            }

        }

        /// <summary>  
        /// create organization.
        /// </summary> 
        /// <param name="organizationEntity">Parameters list for CreateOrganization = o_code(*), o_name (*),o_type (*), address1 (*),address2,address3, email_id1(*),email_id2, s_role_Id (*), s_user_id (*),address2,address3, mobileno1 (*),mobileno2, email_id1 (*),email_id2, contact_no1,contact_no2,s_user_id, s_role_Id</param>  
        /// <returns>response = 200-Ok-{isComplete: true,description: success,data: null}
        /// ///400- bad request-{ "message": "bad req msg"}
        /// ///500 -internal server error-{ "message": "server erroe msg"}
        /// </returns>  
        [HttpPost]
        public async Task<IHttpActionResult> CreateOrganization()
        {
            // Validatde entity
            string strResult;
            string Url = " ";

            if (HttpContext.Current.Request.Form["data"] == null || HttpContext.Current.Request.Form["data"] == "")
                return BadRequest("invalid data entity");

            string organizationData = HttpContext.Current.Request.Form["data"];
            OrganizationEntity organizationEntity = JsonConvert.DeserializeObject<OrganizationEntity>(organizationData);

            strResult = ValidateEntity.ValidateOrganization(organizationEntity, checkOrgAvailability: true,o_code:true,o_name:true,s_role_Id:true,s_user_id:true );

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(userEntity.s_user_id), CommonUtils.ConvertToInt(userEntity.s_role_Id), page_NM: "userprofile", _action: "C");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            //Result
            using (OrganizationBS objOrganizationBS = new OrganizationBS())
            {
               int org_id= await objOrganizationBS.CreateOrganization(organizationEntity);
               if (org_id >0)
                {

                     int count = HttpContext.Current.Request.Files.Count;
                     if (count > 0)
                     {
                         CommonUtils.CheckexistPath(CommonUtils.ConvertToString(org_id), "organization");
                         for (int i = 0; i < count; i++)
                         {
                             var file = HttpContext.Current.Request.Files[i];
                             if (file != null && file.ContentLength > 0)
                             {

                                 string profile_picture_uri = "";
                                 string strPostedPath = CommonUtils.PolicyClaimPath(file.FileName, CommonUtils.ConvertToString(org_id), CommonUtils.ConvertToString(org_id) + "-" + CommonUtils.ConvertToString(i), "organization", out profile_picture_uri);
                                 file.SaveAs(strPostedPath);
                                 //policyClaimEntity.PolicyDocEntities[i].doc_file_path = profile_picture_uri;
                                 //await objPolicyClaimBS.UpdatePolicyProfilePath(claim_id, profile_picture_uri, policyClaimEntity.PolicyDocEntities[i].doc_name);
                                 string strUrlPath = CommonUtils.getUrlDocPath(strPostedPath, "organization", CommonUtils.ConvertToString(org_id), out Url);
                                 string path = Url;

                             }

                         }
                     }
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }

        }

        /// <summary>  
        /// get organization list
        /// </summary>  
        /// <param name="filterEntity">Parameters list for GetOrganizationList: s_user_id, s_user_role </param>  
        /// <returns>response = 200-Ok-{"isComplete": true,  "description": "success",  "data": [ {},{}]
        /// ///400- bad request-{ "message": "bad req msg"}
        /// /// 500 -internal server error-{ "message": "server erroe msg"}
        /// </returns>  
        [HttpPost]
        public async Task<IHttpActionResult> GetOrganizationList(FilterEntity filterEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(filterEntity.s_user_id), CommonUtils.ConvertToInt(filterEntity.s_role_Id), page_NM: "userprofile", _action: "V");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            //Result
            using (OrganizationBS objOrganizationBS = new OrganizationBS())
            {
                List<OrganizationEntity> organizationEntities  = await objOrganizationBS.GetOrganizationList (filterEntity);
                return Ok(CustomeResponse.getOkResponse(organizationEntities));

            }

        }


        /// <summary>  
        /// get organization byId.
        /// </summary>  
        /// <param name="filterEntity">Parameters list for GetOrganizationById :o_id, s_user_id, s_user_role </param>  
        /// <returns>response =200-Ok-{"isComplete": true,  "description": "success",  "data": [{}]</returns> 
        /// ///400- bad request-{ "message": "bad req msg"}
        /// ///500 -internal server error-{ "message": "server erroe msg"}
        /// </returns>  
        [HttpPost]
        public async Task<IHttpActionResult> GetOrganizationById(FilterEntity filterEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, o_id:true, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(filterEntity.s_user_id), CommonUtils.ConvertToInt(filterEntity.s_role_Id), page_NM: "userprofile", _action: "V");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            //Result
           using (OrganizationBS objOrganizationBS = new OrganizationBS())
            {
                OrganizationEntity organizationEntity = await objOrganizationBS.GetOrganizationById(filterEntity);
                return Ok(CustomeResponse.getOkResponse(organizationEntity));

            }
        }


        /// <summary>  
        /// update organization.
        /// </summary>  
        /// <param name="userEntity">Parameters list for UpdateOrganization = o_id(*), o_code(*), o_name (*),o_type (*), address1 (*),address2,address3, email_id1(*),email_id2, s_role_Id (*), s_user_id (*),address2,address3, mobileno1 (*),mobileno2, email_id1 (*),email_id2, contact_no1,contact_no2,s_user_id, s_role_Id</param>  
        /// <returns>response = 200-Ok-{isComplete: true,description: success,data: null}
        /// ///400- bad request-{ "message": "bad req msg"}
        /// ///500 -internal server error-{ "message": "server erroe msg"}
        /// </returns>  
        [HttpPost]
        public async Task<IHttpActionResult> UpdateOrganization()
        {
            string strResult;
            string Url = " ";

            if (HttpContext.Current.Request.Form["data"] == null || HttpContext.Current.Request.Form["data"] == "")
                return BadRequest("invalid data entity");

            string organizationData = HttpContext.Current.Request.Form["data"];
            OrganizationEntity organizationEntity = JsonConvert.DeserializeObject<OrganizationEntity>(organizationData);

            strResult = ValidateEntity.ValidateOrganization(organizationEntity, checkOrgAvailability: true, o_id:true, o_code: true, o_name: true, s_role_Id: true, s_user_id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(userEntity.s_user_id), CommonUtils.ConvertToInt(userEntity.s_role_Id), page_NM: "userprofile", _action: "U");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            //Result
            using (OrganizationBS objOrganizationBS = new OrganizationBS())
            {
                if (await objOrganizationBS.UpdateOrganization(organizationEntity))
                {
                    
                        int count = HttpContext.Current.Request.Files.Count;
                        if (count > 0)
                        {
                            CommonUtils.CheckexistPath(CommonUtils.ConvertToString(organizationEntity.o_id), "organization");
                            for (int i = 0; i < count; i++)
                            {
                                var file = HttpContext.Current.Request.Files[i];
                                if (file != null && file.ContentLength > 0)
                                {

                                    string profile_picture_uri = "";
                                    string strPostedPath = CommonUtils.PolicyClaimPath(file.FileName, CommonUtils.ConvertToString(organizationEntity.o_id), CommonUtils.ConvertToString(organizationEntity.o_id) + "-" + CommonUtils.ConvertToString(i), "organization", out profile_picture_uri);
                                    file.SaveAs(strPostedPath);
                                    //policyClaimEntity.PolicyDocEntities[i].doc_file_path = profile_picture_uri;
                                    //await objPolicyClaimBS.UpdatePolicyProfilePath(claim_id, profile_picture_uri, policyClaimEntity.PolicyDocEntities[i].doc_name);
                                    string strUrlPath = CommonUtils.getUrlDocPath(strPostedPath, "organization", CommonUtils.ConvertToString(organizationEntity.o_id), out Url);
                                    string path = Url;

                                }

                            }
                       
                       }
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }

        }

        /// <summary>  
        /// delete organization.
        /// </summary>  
        /// <param name="userEntity">Parameters list for DeleteOrganization =  o_id (*),s_user_id(*), s_role_Id(*)</param>  
        /// <returns>response = 200-Ok-{isComplete: true,description: success,data: null}
        /// ///400- bad request-{ "message": "bad req msg"}
        /// ///500 -internal server error-{ "message": "server erroe msg"}
        /// </returns>  
        [HttpPost]
        public async Task<IHttpActionResult> DeleteOrganization(OrganizationEntity organizationEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidateOrganization(organizationEntity, o_id: true, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(userEntity.s_user_id), CommonUtils.ConvertToInt(userEntity.s_role_Id), page_NM: "userprofile", _action: "D");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            //Result
            using (OrganizationBS objOrganizationBS = new OrganizationBS())
            {
                if (await objOrganizationBS.DeleteOrganization(organizationEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }

        }

       
    }
}
