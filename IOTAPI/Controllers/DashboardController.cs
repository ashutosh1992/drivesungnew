﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Configuration;
using System.Diagnostics;
using IOTBusinessEntities;
using IOTBusinessServices;
using IOTWebAPI.Filters;
using IOTUtilities;
using System.Threading.Tasks;

//suppress the warning of xml documentation 
#pragma warning disable 1591

namespace IOTWebAPI.Controllers
{

    [Authorize]
    [CustomExceptionFilter]
    [DeflateCompression]
    public class DashboardController : ApiController
    {


        [HttpPost]
        public async Task<IHttpActionResult> GetVehicleList(FilterEntity filterEntity)
        {
            string strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (DashboardBS objDashboardBS = new DashboardBS())
            {
                DataSet dsData = await objDashboardBS.GetVehicleList(filterEntity);
                return Ok(dsData);

            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> GetDashBoardList(FilterEntity filterEntity)
        {
            string strResult;
            strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);

            }

            using (DashboardBS objDashboardBS = new DashboardBS())
            {
                DataSet dsData = await objDashboardBS.GetDashBoardList(filterEntity);
                return Ok(CustomeResponse.getOkResponse(dsData));
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> GetLiveTrackingData(FilterEntity filterEntity)
        {
            string strResult;
            strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, DeviceNo: true, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);

            }
            using (DashboardBS objDashboardBS = new DashboardBS())
            {
                DataSet dsData = await objDashboardBS.GetLiveTrackingData(filterEntity);
                return Ok(CustomeResponse.getOkResponse(dsData));

            }
        }


        [HttpPost]
        public async Task<IHttpActionResult> GetLiveOBDData(FilterEntity filterEntity)
        {
            string strResult;
            strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, DeviceNo: true, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);

            }
            using (DashboardBS objDashboardBS = new DashboardBS())
            {
                DataSet dsData = await objDashboardBS.GetLiveOBDData(filterEntity);
                return Ok(CustomeResponse.getOkResponse(dsData));

            }
        }




        [HttpPost]
        public async Task<IHttpActionResult> GetTripPlayData(FilterEntity filterEntity)
        {
            string strResult;
            strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, DeviceNo: true, FromDate: true, ToDate: true, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);

            }
            using (DashboardBS objDashboardBS = new DashboardBS())
            {
                DataSet dsData = await objDashboardBS.GetTripPlayData(filterEntity);
                return Ok(CustomeResponse.getOkResponse(dsData));

            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> GetGraphData(FilterEntity filterEntity)
        {
            string strResult;
            strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, DeviceNo: true, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);

            }
            using (DashboardBS objDashboardBS = new DashboardBS())
            {
                DataSet dsData = await objDashboardBS.GetGraphData(filterEntity);
                return Ok(CustomeResponse.getOkResponse(dsData));

            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> GetMapAllVehiclesData(FilterEntity filterEntity)
        {
            string strResult;
            strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (DashboardBS objDashboardBS = new DashboardBS())
            {
                DataSet dsData = await objDashboardBS.GetMapAllVehiclesData(filterEntity);
                return Ok(CustomeResponse.getOkResponse(dsData));

            }
        }


        [HttpPost]
        public async Task<IHttpActionResult> GetBlotterViewData(FilterEntity filterEntity)
        {
            string strResult;
            strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);

            }

            if (filterEntity.s_role_Id == 1)
            {
                strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, DeviceNo: true, s_user_id: true, s_role_Id: true);
                if (strResult != "")
                {
                    return BadRequest(strResult);

                }
            }



            using (DashboardBS objDashboardBS = new DashboardBS())
            {
                DataSet dsData = await objDashboardBS.GetBlotterViewData(filterEntity);
                return Ok(CustomeResponse.getOkResponse(dsData));

            }
        }


        [HttpPost]
        public async Task<IHttpActionResult> GetSubHeaderCount(FilterEntity filterEntity)
        {
            string strResult;
            strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);

            }

            //if (filterEntity.s_role_Id == 1)
            //{
            //    strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, DeviceNo: true, s_user_id: true, s_role_Id: true);
            //    if (strResult != "")
            //    {
            //        return BadRequest(strResult);

            //    }
            //}



            using (DashboardBS objDashboardBS = new DashboardBS())
            {
                DataSet dsData = await objDashboardBS.GetSubHeaderCount(filterEntity);
                return Ok(CustomeResponse.getOkResponse(dsData));

            }
        }





        //[HttpPost]
        //public async Task<IHttpActionResult> GetBlotterViewAdminfilterData(FilterEntity filterEntity)
        //{
        //    string strResult;
        //    strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, s_user_id: true, s_role_Id: true);
        //    if (strResult != "")
        //    {
        //        return BadRequest(strResult);

        //    }
        //    using (DashboardBS objDashboardBS = new DashboardBS())
        //    {
        //        DataSet dsData = await objDashboardBS.GetBlotterViewAdminfilterData(filterEntity);
        //        return Ok(CustomeResponse.getOkResponse(dsData));

        //    }
        //}

        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> GetLiveData(FilterEntity filterEntity)
        {
            string strResult;
            strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, DeviceNo: true);
            if (strResult != "")
            {
                return BadRequest(strResult);

            }
            using (DashboardBS objDashboardBS = new DashboardBS())
            {
                DataTable dsData = await objDashboardBS.GetLiveData(filterEntity);
                return Ok(CustomeResponse.getOkResponse(dsData));

            }
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> SyncLog()
    {
           if (HttpContext.Current.Request.Form["data"] == null || HttpContext.Current.Request.Form["data"] == "")
            return BadRequest("invalid data entity");
            UserEntity userEntitytemp;
            string Userdata = HttpContext.Current.Request.Form["data"];
            UserEntity userEntity = JsonConvert.DeserializeObject<UserEntity>(Userdata);
            if (userEntity != null)
            {
                int count = HttpContext.Current.Request.Files.Count;

                if (count > 0)
                {
                    CommonUtils.CheckexistPath(CommonUtils.ConvertToString(userEntity.s_user_id), "synclog");

                    for (int i = 0; i < count; i++)
                    {
                        var file = HttpContext.Current.Request.Files[i];
                        if (file != null && file.ContentLength > 0)
                        {

                            string profile_picture_uri = "";
                            string strPostedPath = CommonUtils.SyncLogPath(file.FileName, CommonUtils.ConvertToString(userEntity.s_user_id), "synclog", out profile_picture_uri);
                            file.SaveAs(strPostedPath);


                        }

                     }
                   
                }
                return Ok(CustomeResponse.getMsgResponse("Success"));
            }
            else
                return Ok(CustomeResponse.getMsgResponse("error"));
                 
         }


    }
}
