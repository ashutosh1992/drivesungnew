﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using IOTBusinessEntities;
using IOTBusinessServices;
using IOTUtilities;
using IOTWebAPI.Filters;
using System.Threading.Tasks;

//suppress the warning of xml documentation 
#pragma warning disable 1591
namespace IOTWebAPI.Controllers
{
    [Authorize]
    [CustomExceptionFilter]
    [DeflateCompression]
    public class GeofenceController : ApiController
    {
        [HttpPost]
        public async Task<IHttpActionResult> CreateGeofence(GeofenceEntity geofenceEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateGeofence.ValidateGeofencefunc(geofenceEntity, geofence_name: true, CheckGeofenceAvailability: true, lat_lng: true, created_by: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (GeofenceBS objGeofenceBS = new GeofenceBS())
            {
                if (await objGeofenceBS.CreateGeofence(geofenceEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }
            }

        }

        [HttpPost]
        public async Task<IHttpActionResult> GetGeofenceList(GeofenceEntity geofenceEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateGeofence.ValidateGeofencefunc(geofenceEntity, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //Result
            using (GeofenceBS objGeofenceBS = new GeofenceBS())
            {
                DataTable dtGeofence = await objGeofenceBS.GetGeofenceList(geofenceEntity);
                return Ok(CustomeResponse.getOkResponse(dtGeofence));
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> GetGeofenceById(GeofenceEntity geofenceEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateGeofence.ValidateGeofencefunc(geofenceEntity, gc_id: true, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //Result
            using (GeofenceBS objGeofenceBS = new GeofenceBS())
            {
                DataTable dtGeofence = await objGeofenceBS.GetGeofenceById(geofenceEntity);
                return Ok(CustomeResponse.getOkResponse(dtGeofence));
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> UpdateGeofence(GeofenceEntity geofenceEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateGeofence.ValidateGeofencefunc(geofenceEntity, gc_id: true, geofence_name: true, CheckGeofenceAvailabilityforupdate: true, lat_lng: true, updated_by: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (GeofenceBS objGeofenceBS = new GeofenceBS())
            {
                if (await objGeofenceBS.UpdateGeofence(geofenceEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }
            }

        }

        [HttpPost]
        public async Task<IHttpActionResult> DeleteGeofence(GeofenceEntity geofenceEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateGeofence.ValidateGeofencefunc(geofenceEntity, gc_id: true, updated_by: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (GeofenceBS objGeofenceBS = new GeofenceBS())
            {
                if (await objGeofenceBS.DeleteGeofence(geofenceEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }
            }

        }

        [HttpPost]
        public async Task<IHttpActionResult> UpdateGeofenceUserMapping(GeofenceEntity geofenceEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateGeofence.ValidateGeofencefunc(geofenceEntity, user_id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (GeofenceBS objGeofenceBS = new GeofenceBS())
            {
                if (await objGeofenceBS.UpdateGeofenceUserMapping(geofenceEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }
            }

        }

        [HttpPost]
        public async Task<IHttpActionResult> GetAssignUnassignGeofenceList(FilterEntity filterEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, user_id: true, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //Result
            using (GeofenceBS objGeofenceBS = new GeofenceBS())
            {
                DataTable dtGeofence = await objGeofenceBS.GetAssignUnassignGeofenceList(filterEntity);
                return Ok(CustomeResponse.getOkResponse(dtGeofence));
            }
        }

    }
}