﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PFTrustAPI.Models
{
    public class DocumentNoDetailsEntity
    {
        public ActionEntity actionEntity { get; set; }
        public Int64 DocumentNoDetailsID { get; set; }
        public Int64 DocumentNoID { get; set; }
        public Int64 DocumentID { get; set; }
        public Int64? StartNumber { get; set; }
        public Int64? EndNumber { get; set; }
        public Int64? CurrentSequence { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public Int64 ICreater { get; set; }
        public DateTime IDate { get; set; }
    }
}