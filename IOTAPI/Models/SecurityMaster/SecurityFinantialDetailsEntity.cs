﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PFTrustAPI.Models.SecurityMaster
{
    public class SecurityFinantialDetailsEntity
    {
        public int SecurityFinantialDetailsId { get; set; }
        public int SecurityId { get; set; }
        public DateTime? secfdFromDate { get; set; }
        public DateTime? secfdToDate { get; set; }
        public decimal secfdRateOfInterest { get; set; }
    }
}