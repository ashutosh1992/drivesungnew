﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PFTrustAPI.Models.SecurityMaster
{
    public class SecurityGuaranteeDetailsEntity
    {
        public int SecurityGuaranteeDetailsId { get; set; }
        public int SecurityId { get; set; }
        public int guaranteeInstitutionID { get; set; }
        public string guaranteeInstitutionCode { get; set; }
        public string secGuaranteeInstitutionName { get; set; }
        public decimal secInstitutionGuarantee { get; set; }
    }
}