﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PFTrustAPI.Models.SecurityMaster
{
    public class SecurityIssueDetailsEntity
    {
        public int SecurityIssueDetailsId { get; set; }
        public int SecurityId { get; set; }
        public DateTime? secIdIssueDate { get; set; }
        public decimal secIdPercentage { get; set; }
        public decimal secIdValue { get; set; }
        public decimal secIdPremium { get; set; }
        public decimal secIdDiscount { get; set; }
        public decimal secIdNetCashFlow { get; set; }
    }
}