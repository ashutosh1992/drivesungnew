﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PFTrustAPI.Models
{
    public class PurchaseEntity
    {
        public ActionEntity actionEntity { get; set; }
        public Int64 VoucherID { get; set; }
        public string VoucherNo { get; set; }
        public DateTime? EntryDate { get; set; }
        public Int64 SellerID { get; set; }
        public Int64 BrokerID { get; set; }
        public string PartyDocNo { get; set; }
        public DateTime? PartyDocDate { get; set; }
        public string SellerAccountNo { get; set; }
        public DateTime? DealDate { get; set; }
        public DateTime? SettlementDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public string ModeOfDelivery { get; set; }
        public string PurchaseMethod { get; set; }
        public Int64 SecurityID { get; set; }
        public Int64 NoOfBonds { get; set; }
        public decimal FaceValue { get; set; }
        public decimal TotalFaceValue { get; set; }
        public decimal EffectiveFaceValuePer { get; set; }
        public decimal EffectiveFaceValueAmt { get; set; }
        public decimal RatePerUnit { get; set; }
        public string Interest { get; set; }
        public decimal InterestAmount { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal SecurityPremium { get; set; }
        public decimal SecurityDiscount { get; set; }
        public decimal SecurityBrokerageAmount { get; set; }
        public decimal SecurityBrokerageRefund { get; set; }
        public decimal STT { get; set; }
        public decimal TotalPurchaseValue { get; set; }
        public decimal YTM { get; set; }
        public string YieldPutCallOpt { get; set; }
        public decimal CleanPrice { get; set; }
        public decimal DirtyPrice { get; set; }
        public decimal InvestmentAccount { get; set; }
        public decimal PremiumAccount { get; set; }
        public decimal Brokerage { get; set; }
        public decimal InterestNotDue { get; set; }
        public decimal Discount { get; set; }
        public decimal BrokerageRefund { get; set; }
        public decimal Party { get; set; }
        public bool IsActive { get; set; }
        public Int64 ICreater { get; set; }
        public bool IsDelete { get; set; }
        public DateTime? IDate { get; set; }
    }
}