﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOTUtilities;
using IOTDataModel;
using System.Data;
using Npgsql;

using System.Configuration;
using System.IO;
using System.Net;

namespace IOTTripWiseDataProcessEngine
{
    public class DailyDataProcess : IDisposable
    {
        //   private DashboardDM objDashboardDM;
        private StringBuilder strSqlQry;
        private BaseDAL objDAL;
        // private AggregationParameterEntity aggregationParameterEntity;
        private DataTable dtData;
        private DataSet dsData;
        private List<NpgsqlParameter> Parameters;
        private DateTime _Afromdate, _Atodate, _fromdate, _todate, _privousfromdate, _Acmptfromdate, _Acmpttodate;
        private DataSet dsDailyAggParameters, dsAllDateAggParameters;
        private DataTable dtDeviceDetails;
        //private Int64 deviceno;

        private string dayfromhours;
        private string daytohours;
        private string nightfromhours;
        private string nighttohours;
        private string nightfromhours2;
        private string nighttohours2;
        private DateTime _dayFromDate;
        private DateTime _dayToDate;
        private DateTime _nightFromDate;
        private DateTime _nightToDate;
        private DateTime _nightFromDate2;
        private DateTime _nightToDate2;

        public DailyDataProcess()
        {
            objDAL = new BaseDAL();
            // aggregationParameterEntity = new AggregationParameterEntity();
            // objDashboardDM = new DashboardDM();
            //strSqlQry = new StringBuilder();
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                if (objDAL != null) { objDAL.Dispose(); }
                // aggregationParameterEntity = null;
                if (dtData != null) { dtData.Dispose(); }
                strSqlQry = null;
                if (dsDailyAggParameters != null) { dsDailyAggParameters.Dispose(); }
                if (dsAllDateAggParameters != null) { dsAllDateAggParameters.Dispose(); }
                if (dtDeviceDetails != null) { dtDeviceDetails.Dispose(); }
            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~DailyDataProcess()
        {
            Dispose(false);
        }

        #endregion

        public bool TripDataProcessEngine()
        {
            try
            {
                objDAL.CreateConnection();
                //objDAL.BeginTransaction();
                string strSqlQry = "";
                Int64 trip_id;
                Int64 deviceno;
                strSqlQry = @"select user_master.deviceno , * from trip_planning_master inner join user_master on user_master.user_id=trip_planning_master.user_id where (coalesce(tripprocess,false) = false) and trip_end_time is not null order by trip_end_time asc";
                DataTable dtTrips = objDAL.FetchRecords(strSqlQry);
                foreach (DataRow Triprow in dtTrips.Rows)
                {



                    string fromdate, todate;
                    fromdate = string.Format("{0:yyyy-MM-dd HH:mm:ss}", Triprow["trip_start_time"]).ToString().Replace(".", ":");
                    todate = string.Format("{0:yyyy-MM-dd HH:mm:ss}", Triprow["trip_end_time"]).ToString().Replace(".", ":");
                    trip_id = CommonUtils.ConvertToInt64(Triprow["trip_id"].ToString());
                    deviceno = CommonUtils.ConvertToInt64(Triprow["deviceno"].ToString());
                    decimal m_distance = CommonUtils.ConvertToDecimal(Triprow["m_distance"].ToString());

                    //set fromdate todate
                    _Afromdate = Convert.ToDateTime(fromdate);
                    _Atodate = Convert.ToDateTime(todate);

                    int synclogdata = SyncTripLogData(trip_id, m_distance, deviceno, fromdate, todate);

                    getDayNightDefination(1);

                    DeleteAlreadyProcessedData(trip_id);

                    TripReportDataProcess(deviceno, trip_id);

                    DailyParameterWiseRatingprocess(deviceno, trip_id, 1);

                    #region update trip process flag
                    strSqlQry = (@"update trip_planning_master set tripprocess=true,synclogdata=" + synclogdata + " where trip_id=" + trip_id);
                    objDAL.ExecuteSQL(strSqlQry);
                    strSqlQry = null;
                    #endregion

                }
                //objDAL.CommitTransaction();
                objDAL.CloseConnection();

                return true;

            }
            catch (Exception ex)
            {
                //  objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                throw ex;
            }


        }

        public bool TripReportDataProcess(Int64 deviceno, Int64 trip_id)
        {

            double daysdiff = CommonUtils.DateDiff(_Afromdate, _Atodate);

            #region ------daywise process loop-----------

            for (int i = 0; i < daysdiff; i++)
            {
                string strfromdate = string.Format("{0:yyyy-MM-dd}", _Afromdate.AddDays(i)).ToString().Replace(".", ":");
                string strtodate = string.Format("{0:yyyy-MM-dd}", _Afromdate.AddDays(i)).ToString().Replace(".", ":");
                string filterRD = string.Format("{0:dd-MM-yyyy}", _Afromdate.AddDays(i)).ToString().Replace(".", ":");
                strfromdate = strfromdate + " 00:00:00";
                strtodate = strtodate + " 23:59:59";

                _fromdate = Convert.ToDateTime(strfromdate);
                _todate = Convert.ToDateTime(strtodate);

                if (_Afromdate > _fromdate) _fromdate = _Afromdate;
                if (_Atodate < _todate) _todate = _Atodate;

                ConsoleLog("processing for day " + strfromdate);

                ConsoleLog("getAllDateAggregationParameters");
                dsAllDateAggParameters = GetAllDateAggregationParameters(deviceno, _fromdate, _todate);

                ConsoleLog("getDailyAggregationParameters");
                dsDailyAggParameters = GetDailyAggregationParameters(deviceno, _fromdate, _todate);

                ConsoleLog("getDeviceDetails");
                dtDeviceDetails = GetDeviceDetails(trip_id);

                foreach (DataRow datarow in dtDeviceDetails.Rows)
                {
                    string recorddatetime;
                    decimal hardacceleration = 0, hardbraking = 0, cornerpacket = 0, overspeedstarted = 0, mcall = 0, weather = 0, avgspeed = 0, maxspeed = 0,
                        distance = 0, drivinghrs = 0, drivinghrsideal = 0, daydrivinghrs = 0, daydrivinghrsideal = 0, nightdrivinghrs = 0,
                        nightdrivinghrsideal = 0, ignitiononday = 0, ignitionondayideal = 0,
                        ignitiononnight = 0, ignitiononnightideal = 0, avgrpm = 0, maxrpm = 0, minrpm = 0, avgcoolanttemperature = 0, maxcoolanttemperature = 0,
                        mincoolanttemperature = 0, avgcalculatedengineload = 0, maxcalculatedengineload = 0, mincalculatedengineload = 0;

                    decimal hardacceleration_day = 0, hardbraking_day = 0, cornerpacket_day = 0, overspeedstarted_day = 0, mcall_day = 0, weather_day = 0, avgspeed_day = 0, maxspeed_day = 0,
                       distance_day = 0, avgrpm_day = 0, maxrpm_day = 0, minrpm_day = 0, avgcoolanttemperature_day = 0, maxcoolanttemperature_day = 0,
                       mincoolanttemperature_day = 0, avgcalculatedengineload_day = 0, maxcalculatedengineload_day = 0, mincalculatedengineload_day = 0;

                    decimal hardacceleration_night = 0, hardbraking_night = 0, cornerpacket_night = 0, overspeedstarted_night = 0, mcall_night = 0, weather_night = 0, avgspeed_night = 0, maxspeed_night = 0,
                    distance_night = 0, avgrpm_night = 0, maxrpm_night = 0, minrpm_night = 0, avgcoolanttemperature_night = 0, maxcoolanttemperature_night = 0,
                    mincoolanttemperature_night = 0, avgcalculatedengineload_night = 0, maxcalculatedengineload_night = 0, mincalculatedengineload_night = 0;

                    deviceno = CommonUtils.ConvertToInt64(datarow["deviceno"]);
                    recorddatetime = strfromdate;

                    ConsoleLog("processing device no= " + deviceno);

                    String filter = "deviceno=" + deviceno + " and rd='" + filterRD + "'";
                    DataRow[] dr;

                    #region get parameters value from dsAllDateAggParameters

                    #region hardbraking
                    dr = dsAllDateAggParameters.Tables["hardbraking"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["hardbraking"] != null)
                        {
                            hardbraking = CommonUtils.ConvertToDecimal(dtrow["hardbraking"]);
                        }
                    }

                    dr = dsAllDateAggParameters.Tables["hardbrakingDay"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["hardbraking"] != null)
                        {
                            hardbraking_day = CommonUtils.ConvertToDecimal(dtrow["hardbraking"]);
                        }
                    }

                    dr = dsAllDateAggParameters.Tables["hardbrakingNight"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["hardbraking"] != null)
                        {
                            hardbraking_night = CommonUtils.ConvertToDecimal(dtrow["hardbraking"]);
                        }
                    }
                    #endregion

                    #region hardacceleration
                    dr = dsAllDateAggParameters.Tables["hardacceleration"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["hardacceleration"] != null)
                        {
                            hardacceleration = CommonUtils.ConvertToDecimal(dtrow["hardacceleration"]);
                        }
                    }

                    dr = dsAllDateAggParameters.Tables["hardaccelerationDay"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["hardacceleration"] != null)
                        {
                            hardacceleration_day = CommonUtils.ConvertToDecimal(dtrow["hardacceleration"]);
                        }
                    }

                    dr = dsAllDateAggParameters.Tables["hardaccelerationNight"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["hardacceleration"] != null)
                        {
                            hardacceleration_night = CommonUtils.ConvertToDecimal(dtrow["hardacceleration"]);
                        }
                    }
                    #endregion

                    #region cornerpacket
                    dr = dsAllDateAggParameters.Tables["cornerpacket"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["cornerpacket"] != null)
                        {
                            cornerpacket = CommonUtils.ConvertToDecimal(dtrow["cornerpacket"]);
                        }
                    }

                    dr = dsAllDateAggParameters.Tables["cornerpacketDay"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["cornerpacket"] != null)
                        {
                            cornerpacket_day = CommonUtils.ConvertToDecimal(dtrow["cornerpacket"]);
                        }
                    }

                    dr = dsAllDateAggParameters.Tables["cornerpacketNight"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["cornerpacket"] != null)
                        {
                            cornerpacket_night = CommonUtils.ConvertToDecimal(dtrow["cornerpacket"]);
                        }
                    }
                    #endregion

                    #region overspeedstarted
                    dr = dsAllDateAggParameters.Tables["overspeedstarted"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["overspeedstarted"] != null)
                        {
                            overspeedstarted = CommonUtils.ConvertToDecimal(dtrow["overspeedstarted"]);
                        }
                    }

                    dr = dsAllDateAggParameters.Tables["overspeedstartedDay"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["overspeedstarted"] != null)
                        {
                            overspeedstarted_day = CommonUtils.ConvertToDecimal(dtrow["overspeedstarted"]);
                        }
                    }

                    dr = dsAllDateAggParameters.Tables["overspeedstartedNight"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["overspeedstarted"] != null)
                        {
                            overspeedstarted_night = CommonUtils.ConvertToDecimal(dtrow["overspeedstarted"]);
                        }
                    }
                    #endregion

                    #region mcall
                    dr = dsAllDateAggParameters.Tables["mcall"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["mcall"] != null)
                        {
                            mcall = CommonUtils.ConvertToDecimal(dtrow["mcall"]);
                        }
                    }

                    dr = dsAllDateAggParameters.Tables["mcallDay"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["mcall"] != null)
                        {
                            mcall_day = CommonUtils.ConvertToDecimal(dtrow["mcall"]);
                        }
                    }

                    dr = dsAllDateAggParameters.Tables["mcallNight"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["mcall"] != null)
                        {
                            mcall_night = CommonUtils.ConvertToDecimal(dtrow["mcall"]);
                        }
                    }
                    #endregion

                    #region weather
                    dr = dsAllDateAggParameters.Tables["weather"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["weather"] != null)
                        {
                            weather = CommonUtils.ConvertToDecimal(dtrow["weather"]);
                        }
                    }

                    dr = dsAllDateAggParameters.Tables["weatherDay"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["weather"] != null)
                        {
                            weather_day = CommonUtils.ConvertToDecimal(dtrow["weather"]);
                        }
                    }

                    dr = dsAllDateAggParameters.Tables["weatherNight"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["weather"] != null)
                        {
                            weather_night = CommonUtils.ConvertToDecimal(dtrow["weather"]);
                        }
                    }
                    #endregion


                    #region speeddistance
                    dr = dsAllDateAggParameters.Tables["speeddistance"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["avgspeed"] != null)
                        {
                            avgspeed = CommonUtils.ConvertToDecimal(dtrow["avgspeed"]);
                        }
                        if (dtrow["maxspeed"] != null)
                        {
                            maxspeed = CommonUtils.ConvertToDecimal(dtrow["maxspeed"]);
                        }
                        if (dtrow["distance"] != null)
                        {
                            distance = CommonUtils.ConvertToDecimal(dtrow["distance"]);
                        }

                    }

                    dr = dsAllDateAggParameters.Tables["speeddistanceDay"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["avgspeed"] != null)
                        {
                            avgspeed_day = CommonUtils.ConvertToDecimal(dtrow["avgspeed"]);
                        }
                        if (dtrow["maxspeed"] != null)
                        {
                            maxspeed_day = CommonUtils.ConvertToDecimal(dtrow["maxspeed"]);
                        }
                        if (dtrow["distance"] != null)
                        {
                            distance_day = CommonUtils.ConvertToDecimal(dtrow["distance"]);
                        }

                    }


                    dr = dsAllDateAggParameters.Tables["speeddistanceNight"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["avgspeed"] != null)
                        {
                            avgspeed_night = CommonUtils.ConvertToDecimal(dtrow["avgspeed"]);
                        }
                        if (dtrow["maxspeed"] != null)
                        {
                            maxspeed_night = CommonUtils.ConvertToDecimal(dtrow["maxspeed"]);
                        }
                        if (dtrow["distance"] != null)
                        {
                            distance_night = CommonUtils.ConvertToDecimal(dtrow["distance"]);
                        }
                    }

                    #endregion

                    #endregion

                    #region get parameters value from iot_obd_hub_Para

                    #region Accross day night
                    dr = dsAllDateAggParameters.Tables["iot_obd_hub_Para"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["avgrpm"] != null)
                        {
                            avgrpm = CommonUtils.ConvertToDecimal(dtrow["avgrpm"]);
                        }

                        if (dtrow["maxrpm"] != null)
                        {
                            maxrpm = CommonUtils.ConvertToDecimal(dtrow["maxrpm"]);
                        }

                        if (dtrow["minrpm"] != null)
                        {
                            minrpm = CommonUtils.ConvertToDecimal(dtrow["minrpm"]);
                        }

                        if (dtrow["avgcoolanttemperature"] != null)
                        {
                            avgcoolanttemperature = CommonUtils.ConvertToDecimal(dtrow["avgcoolanttemperature"]);
                        }

                        if (dtrow["maxcoolanttemperature"] != null)
                        {
                            maxcoolanttemperature = CommonUtils.ConvertToDecimal(dtrow["maxcoolanttemperature"]);
                        }

                        if (dtrow["mincoolanttemperature"] != null)
                        {
                            mincoolanttemperature = CommonUtils.ConvertToDecimal(dtrow["mincoolanttemperature"]);
                        }

                        if (dtrow["avgcalculatedengineload"] != null)
                        {
                            avgcalculatedengineload = CommonUtils.ConvertToDecimal(dtrow["avgcalculatedengineload"]);
                        }

                        if (dtrow["maxcalculatedengineload"] != null)
                        {
                            maxcalculatedengineload = CommonUtils.ConvertToDecimal(dtrow["maxcalculatedengineload"]);
                        }

                        if (dtrow["mincalculatedengineload"] != null)
                        {
                            mincalculatedengineload = CommonUtils.ConvertToDecimal(dtrow["mincalculatedengineload"]);
                        }
                    }
                    #endregion

                    #region Day
                    dr = dsAllDateAggParameters.Tables["iot_obd_hub_ParaDay"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["avgrpm"] != null)
                        {
                            avgrpm_day = CommonUtils.ConvertToDecimal(dtrow["avgrpm"]);
                        }

                        if (dtrow["maxrpm"] != null)
                        {
                            maxrpm_day = CommonUtils.ConvertToDecimal(dtrow["maxrpm"]);
                        }

                        if (dtrow["minrpm"] != null)
                        {
                            minrpm_day = CommonUtils.ConvertToDecimal(dtrow["minrpm"]);
                        }

                        if (dtrow["avgcoolanttemperature"] != null)
                        {
                            avgcoolanttemperature_day = CommonUtils.ConvertToDecimal(dtrow["avgcoolanttemperature"]);
                        }

                        if (dtrow["maxcoolanttemperature"] != null)
                        {
                            maxcoolanttemperature_day = CommonUtils.ConvertToDecimal(dtrow["maxcoolanttemperature"]);
                        }

                        if (dtrow["mincoolanttemperature"] != null)
                        {
                            mincoolanttemperature_day = CommonUtils.ConvertToDecimal(dtrow["mincoolanttemperature"]);
                        }

                        if (dtrow["avgcalculatedengineload"] != null)
                        {
                            avgcalculatedengineload_day = CommonUtils.ConvertToDecimal(dtrow["avgcalculatedengineload"]);
                        }

                        if (dtrow["maxcalculatedengineload"] != null)
                        {
                            maxcalculatedengineload_day = CommonUtils.ConvertToDecimal(dtrow["maxcalculatedengineload"]);
                        }

                        if (dtrow["mincalculatedengineload"] != null)
                        {
                            mincalculatedengineload_day = CommonUtils.ConvertToDecimal(dtrow["mincalculatedengineload"]);
                        }
                    }
                    #endregion

                    #region Night
                    dr = dsAllDateAggParameters.Tables["iot_obd_hub_ParaNight"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["avgrpm"] != null)
                        {
                            avgrpm_night = CommonUtils.ConvertToDecimal(dtrow["avgrpm"]);
                        }

                        if (dtrow["maxrpm"] != null)
                        {
                            maxrpm_night = CommonUtils.ConvertToDecimal(dtrow["maxrpm"]);
                        }

                        if (dtrow["minrpm"] != null)
                        {
                            minrpm_night = CommonUtils.ConvertToDecimal(dtrow["minrpm"]);
                        }

                        if (dtrow["avgcoolanttemperature"] != null)
                        {
                            avgcoolanttemperature_night = CommonUtils.ConvertToDecimal(dtrow["avgcoolanttemperature"]);
                        }

                        if (dtrow["maxcoolanttemperature"] != null)
                        {
                            maxcoolanttemperature_night = CommonUtils.ConvertToDecimal(dtrow["maxcoolanttemperature"]);
                        }

                        if (dtrow["mincoolanttemperature"] != null)
                        {
                            mincoolanttemperature_night = CommonUtils.ConvertToDecimal(dtrow["mincoolanttemperature"]);
                        }

                        if (dtrow["avgcalculatedengineload"] != null)
                        {
                            avgcalculatedengineload_night = CommonUtils.ConvertToDecimal(dtrow["avgcalculatedengineload"]);
                        }

                        if (dtrow["maxcalculatedengineload"] != null)
                        {
                            maxcalculatedengineload_night = CommonUtils.ConvertToDecimal(dtrow["maxcalculatedengineload"]);
                        }

                        if (dtrow["mincalculatedengineload"] != null)
                        {
                            mincalculatedengineload_night = CommonUtils.ConvertToDecimal(dtrow["mincalculatedengineload"]);
                        }
                    }
                    #endregion

                    #endregion

                    #region get parameters value from dsDailyAggParameters

                    filter = "deviceno=" + deviceno;
                    dr = dsDailyAggParameters.Tables["ignitiononday"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["ignitiononday"] != null)
                        {
                            ignitiononday = CommonUtils.ConvertToDecimal(dtrow["ignitiononday"]);
                        }
                    }

                    dr = dsDailyAggParameters.Tables["ignitionondayideal"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["ignitionondayideal"] != null)
                        {
                            ignitionondayideal = CommonUtils.ConvertToDecimal(dtrow["ignitionondayideal"]);
                        }
                    }

                    dr = dsDailyAggParameters.Tables["ignitiononnight"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["ignitiononnight"] != null)
                        {
                            ignitiononnight = CommonUtils.ConvertToDecimal(dtrow["ignitiononnight"]);
                        }
                    }

                    dr = dsDailyAggParameters.Tables["ignitiononnightideal"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["ignitiononnightideal"] != null)
                        {
                            ignitiononnight = CommonUtils.ConvertToDecimal(dtrow["ignitiononnightideal"]);
                        }
                    }

                    #endregion

                    drivinghrs = ignitiononday + ignitiononnight;
                    drivinghrsideal = ignitionondayideal + ignitiononnightideal;

                    daydrivinghrs = ignitiononday;
                    daydrivinghrsideal = ignitionondayideal;

                    nightdrivinghrs = ignitiononnight;
                    nightdrivinghrsideal = ignitiononnightideal;

                    #region Insert into rpt_daily_agg_parameter
                    if (distance > 0)
                    {
                        strSqlQry = new StringBuilder();
                        strSqlQry.Append(@"INSERT INTO rpt_daily_trip_agg_parameter(deviceno,trip_id, recorddatetime, hardacceleration, hardacceleration_day, hardacceleration_night, hardbraking, hardbraking_day, hardbraking_night,cornerpacket,cornerpacket_day,cornerpacket_night,overspeedstarted,overspeedstarted_day,overspeedstarted_night,mcall,mcall_day,mcall_night, avgspeed, avgspeed_day, avgspeed_night, maxspeed, maxspeed_day, maxspeed_night,  distance, distance_day, distance_night, drivinghrs, drivinghrsideal, daydrivinghrs, daydrivinghrsideal, nightdrivinghrs, nightdrivinghrsideal, avgrpm, avgrpm_day, avgrpm_night, maxrpm, maxrpm_day, maxrpm_night, minrpm, minrpm_day, minrpm_night, avgcoolanttemperature, avgcoolanttemperature_day, avgcoolanttemperature_night, maxcoolanttemperature, maxcoolanttemperature_day, maxcoolanttemperature_night, mincoolanttemperature, mincoolanttemperature_day, mincoolanttemperature_night, avgcalculatedengineload, avgcalculatedengineload_day, avgcalculatedengineload_night, maxcalculatedengineload, maxcalculatedengineload_day, maxcalculatedengineload_night, mincalculatedengineload, mincalculatedengineload_day, mincalculatedengineload_night, sysdatetime,weather,weather_day,weather_night) VALUES (
                    @deviceno,@trip_id, @recorddatetime, @hardacceleration, @hardacceleration_day, @hardacceleration_night, @hardbraking, @hardbraking_day, @hardbraking_night,@cornerpacket,@cornerpacket_day,@cornerpacket_night,@overspeedstarted,@overspeedstarted_day,@overspeedstarted_night,@mcall,@mcall_day,@mcall_night, @avgspeed, @avgspeed_day, @avgspeed_night, @maxspeed, @maxspeed_day, @maxspeed_night,  @distance, @distance_day, @distance_night, @drivinghrs, @drivinghrsideal, @daydrivinghrs, @daydrivinghrsideal, @nightdrivinghrs, @nightdrivinghrsideal, @avgrpm, @avgrpm_day, @avgrpm_night, @maxrpm, @maxrpm_day, @maxrpm_night, @minrpm, @minrpm_day, @minrpm_night, @avgcoolanttemperature, @avgcoolanttemperature_day, @avgcoolanttemperature_night, @maxcoolanttemperature, @maxcoolanttemperature_day, @maxcoolanttemperature_night, @mincoolanttemperature, @mincoolanttemperature_day, @mincoolanttemperature_night, @avgcalculatedengineload, @avgcalculatedengineload_day, @avgcalculatedengineload_night, @maxcalculatedengineload, @maxcalculatedengineload_day, @maxcalculatedengineload_night, @mincalculatedengineload, @mincalculatedengineload_day, @mincalculatedengineload_night, @sysdatetime,@weather,@weather_day,@weather_night);");

                        Parameters = new List<NpgsqlParameter>();
                        //Parameters.Add(new NpgsqlParameter("@user_id", _user_id));
                        Parameters.Add(new NpgsqlParameter("@deviceno", deviceno));
                        Parameters.Add(new NpgsqlParameter("@trip_id", trip_id));

                        Parameters.Add(new NpgsqlParameter("@recorddatetime", CommonUtils.ConvertToDateTime(recorddatetime)));

                        Parameters.Add(new NpgsqlParameter("@hardacceleration", hardacceleration));
                        Parameters.Add(new NpgsqlParameter("@hardacceleration_day", hardacceleration_day));
                        Parameters.Add(new NpgsqlParameter("@hardacceleration_night", hardacceleration_night));

                        Parameters.Add(new NpgsqlParameter("@hardbraking", hardbraking));
                        Parameters.Add(new NpgsqlParameter("@hardbraking_day", hardbraking_day));
                        Parameters.Add(new NpgsqlParameter("@hardbraking_night", hardbraking_night));

                        Parameters.Add(new NpgsqlParameter("@cornerpacket", cornerpacket));
                        Parameters.Add(new NpgsqlParameter("@cornerpacket_day", cornerpacket_day));
                        Parameters.Add(new NpgsqlParameter("@cornerpacket_night", cornerpacket_night));

                        Parameters.Add(new NpgsqlParameter("@overspeedstarted", overspeedstarted));
                        Parameters.Add(new NpgsqlParameter("@overspeedstarted_day", overspeedstarted_day));
                        Parameters.Add(new NpgsqlParameter("@overspeedstarted_night", overspeedstarted_night));

                        Parameters.Add(new NpgsqlParameter("@mcall", mcall));
                        Parameters.Add(new NpgsqlParameter("@mcall_day", mcall_day));
                        Parameters.Add(new NpgsqlParameter("@mcall_night", mcall_night));

                        Parameters.Add(new NpgsqlParameter("@avgspeed", avgspeed));
                        Parameters.Add(new NpgsqlParameter("@avgspeed_day", avgspeed_day));
                        Parameters.Add(new NpgsqlParameter("@avgspeed_night", avgspeed_night));

                        Parameters.Add(new NpgsqlParameter("@maxspeed", maxspeed));
                        Parameters.Add(new NpgsqlParameter("@maxspeed_day", maxspeed_day));
                        Parameters.Add(new NpgsqlParameter("@maxspeed_night", maxspeed_night));

                        Parameters.Add(new NpgsqlParameter("@distance", distance));
                        Parameters.Add(new NpgsqlParameter("@distance_day", distance_day));
                        Parameters.Add(new NpgsqlParameter("@distance_night", distance_night));

                        Parameters.Add(new NpgsqlParameter("@drivinghrs", drivinghrs));
                        Parameters.Add(new NpgsqlParameter("@drivinghrsideal", drivinghrsideal));

                        Parameters.Add(new NpgsqlParameter("@daydrivinghrs", daydrivinghrs));
                        Parameters.Add(new NpgsqlParameter("@daydrivinghrsideal", daydrivinghrsideal));

                        Parameters.Add(new NpgsqlParameter("@nightdrivinghrs", nightdrivinghrs));
                        Parameters.Add(new NpgsqlParameter("@nightdrivinghrsideal", nightdrivinghrsideal));

                        Parameters.Add(new NpgsqlParameter("@avgrpm", avgrpm));
                        Parameters.Add(new NpgsqlParameter("@avgrpm_day", avgrpm_day));
                        Parameters.Add(new NpgsqlParameter("@avgrpm_night", avgrpm_night));

                        Parameters.Add(new NpgsqlParameter("@maxrpm", maxrpm));
                        Parameters.Add(new NpgsqlParameter("@maxrpm_day", maxrpm_day));
                        Parameters.Add(new NpgsqlParameter("@maxrpm_night", maxrpm_night));

                        Parameters.Add(new NpgsqlParameter("@minrpm", minrpm));
                        Parameters.Add(new NpgsqlParameter("@minrpm_day", minrpm_day));
                        Parameters.Add(new NpgsqlParameter("@minrpm_night", minrpm_night));

                        Parameters.Add(new NpgsqlParameter("@avgcoolanttemperature", avgcoolanttemperature));
                        Parameters.Add(new NpgsqlParameter("@avgcoolanttemperature_day", avgcoolanttemperature_day));
                        Parameters.Add(new NpgsqlParameter("@avgcoolanttemperature_night", avgcoolanttemperature_night));

                        Parameters.Add(new NpgsqlParameter("@maxcoolanttemperature", maxcoolanttemperature));
                        Parameters.Add(new NpgsqlParameter("@maxcoolanttemperature_day", maxcoolanttemperature_day));
                        Parameters.Add(new NpgsqlParameter("@maxcoolanttemperature_night", maxcoolanttemperature_night));

                        Parameters.Add(new NpgsqlParameter("@mincoolanttemperature", mincoolanttemperature));
                        Parameters.Add(new NpgsqlParameter("@mincoolanttemperature_day", mincoolanttemperature_day));
                        Parameters.Add(new NpgsqlParameter("@mincoolanttemperature_night", mincoolanttemperature_night));

                        Parameters.Add(new NpgsqlParameter("@avgcalculatedengineload", avgcalculatedengineload));
                        Parameters.Add(new NpgsqlParameter("@avgcalculatedengineload_day", avgcalculatedengineload_day));
                        Parameters.Add(new NpgsqlParameter("@avgcalculatedengineload_night", avgcalculatedengineload_night));

                        Parameters.Add(new NpgsqlParameter("@maxcalculatedengineload", maxcalculatedengineload));
                        Parameters.Add(new NpgsqlParameter("@maxcalculatedengineload_day", maxcalculatedengineload_day));
                        Parameters.Add(new NpgsqlParameter("@maxcalculatedengineload_night", maxcalculatedengineload_night));

                        Parameters.Add(new NpgsqlParameter("@mincalculatedengineload", mincalculatedengineload));
                        Parameters.Add(new NpgsqlParameter("@mincalculatedengineload_day", mincalculatedengineload_day));
                        Parameters.Add(new NpgsqlParameter("@mincalculatedengineload_night", mincalculatedengineload_night));

                        Parameters.Add(new NpgsqlParameter("@sysdatetime", CommonUtils.ConvertToDateTime(DateTime.Now)));

                        Parameters.Add(new NpgsqlParameter("@weather", weather));
                        Parameters.Add(new NpgsqlParameter("@weather_day", weather_day));
                        Parameters.Add(new NpgsqlParameter("@weather_night", weather_night));

                        objDAL.ExecutParameterizedQry(strSqlQry.ToString(), Parameters);
                        strSqlQry = null;
                    }
                    #endregion

                    //stoppage processing
                    ConsoleLog("update stoppage data");
                    #region Get Stoppage details and insert into rpt_daily_stoppage
                    filter = "deviceno=" + deviceno + " and rd='" + filterRD + "'";

                    DateTime? minrecorddatetime = null, maxrecorddatetime = null;
                    string latitude = "", longitude = "";
                    Int64 noofrecords = 0;
                    decimal stoppage = 0;

                    dr = dsAllDateAggParameters.Tables["stoppage"].Select(filter);
                    foreach (DataRow dtrow in dr)
                    {
                        if (dtrow["latitude"] != null)
                        {
                            latitude = CommonUtils.ConvertToString(dtrow["latitude"]);
                        }
                        if (dtrow["longitude"] != null)
                        {
                            longitude = CommonUtils.ConvertToString(dtrow["longitude"]);
                        }
                        if (dtrow["minrecorddatetime"] != null)
                        {
                            minrecorddatetime = CommonUtils.ConvertToDateTime(dtrow["minrecorddatetime"]);
                        }
                        if (dtrow["maxrecorddatetime"] != null)
                        {
                            maxrecorddatetime = CommonUtils.ConvertToDateTime(dtrow["maxrecorddatetime"]);
                        }
                        if (dtrow["diffinsec"] != null)
                        {
                            stoppage = CommonUtils.ConvertToDecimal(dtrow["diffinsec"]);
                            stoppage = stoppage / 60;
                        }
                        if (dtrow["rcnt"] != null)
                        {
                            noofrecords = CommonUtils.ConvertToInt64(dtrow["rcnt"]);
                        }

                        strSqlQry = new StringBuilder();
                        strSqlQry.Append(@"INSERT INTO rpt_daily_trip_stoppage(deviceno,trip_id, recorddatetime, latitude, longitude, startdatetime, enddatetime, stoppage, sysdatetime, noofrecords) VALUES (
                        @deviceno,@trip_id, @recorddatetime, @latitude, @longitude, @startdatetime, @enddatetime, @stoppage, @sysdatetime, @noofrecords);");

                        Parameters = new List<NpgsqlParameter>();
                        //Parameters.Add(new NpgsqlParameter("@user_id", _user_id));
                        Parameters.Add(new NpgsqlParameter("@deviceno", deviceno));
                        Parameters.Add(new NpgsqlParameter("@trip_id", trip_id));
                        Parameters.Add(new NpgsqlParameter("@recorddatetime", CommonUtils.ConvertToDateTime(recorddatetime)));
                        Parameters.Add(new NpgsqlParameter("@latitude", latitude));
                        Parameters.Add(new NpgsqlParameter("@longitude", longitude));
                        Parameters.Add(new NpgsqlParameter("@startdatetime", CommonUtils.ConvertToDateTime(minrecorddatetime)));
                        Parameters.Add(new NpgsqlParameter("@enddatetime", CommonUtils.ConvertToDateTime(maxrecorddatetime)));
                        Parameters.Add(new NpgsqlParameter("@stoppage", stoppage));
                        Parameters.Add(new NpgsqlParameter("@sysdatetime", CommonUtils.ConvertToDateTime(DateTime.Now)));
                        Parameters.Add(new NpgsqlParameter("@noofrecords", noofrecords));

                        objDAL.ExecutParameterizedQry(strSqlQry.ToString(), Parameters);
                        strSqlQry = null;
                    }
                    #endregion
                }

            }
            #endregion

            return true;

        }

        public DataRow getsingleRow(DataTable dt, string strfilter)
        {
            DataRow[] dr = dt.Select(strfilter);
            return dr[0];
        }


        public DataSet GetAllDateAggregationParameters(Int64 deviceno, DateTime? FromDate, DateTime? ToDate)
        {
            //for get day night range according to trip start and end date
            string strfromdateday, strtodateday, strfromdatenight, strtodatenight, strfromdatenight2, strtodatenight2, strfromdate, strtodate;
            GetDayNightDefinationRange(FromDate, ToDate, out strfromdate, out strtodate, out strfromdateday, out strtodateday, out strfromdatenight, out strtodatenight,
              out strfromdatenight2, out strtodatenight2);

            //Accross Day Night 
            dsData = new DataSet();
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as hardbraking
            FROM  iot_hub where deviceno=" + deviceno + " and coalesce(hardbraking,0)=1 and  (recorddatetime between '" + strfromdate + "' and '"
            + strtodate + "')  group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "hardbraking";

            //day

            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as hardbraking
            FROM  iot_hub where  deviceno=" + deviceno + " and coalesce(hardbraking,0)=1 and  (recorddatetime between '" + strfromdateday + "' and '"
            + strtodateday + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "hardbrakingDay";

            //night

            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as hardbraking
            FROM  iot_hub where  deviceno=" + deviceno + " and  coalesce(hardbraking,0)=1 and  ((recorddatetime between '" + strfromdatenight + "' and '"
            + strtodatenight + "') or (recorddatetime between '" + strfromdatenight2 + "' and '"
            + strtodatenight2 + "')) group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "hardbrakingNight";

            //--------------------------------------------------------

            //Accross Day Night 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as hardacceleration
            FROM  iot_hub where  deviceno=" + deviceno + " and  coalesce(hardacceleration,0)=1 and  (recorddatetime between '" + strfromdate + "' and '"
            + strtodate + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "hardacceleration";

            //Day 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as hardacceleration
            FROM  iot_hub where  deviceno=" + deviceno + " and  coalesce(hardacceleration,0)=1 and  (recorddatetime between '" + strfromdateday + "' and '"
            + strtodateday + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "hardaccelerationDay";


            //Night 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as hardacceleration
            FROM  iot_hub where  deviceno=" + deviceno + " and  coalesce(hardacceleration,0)=1 and  ((recorddatetime between '" + strfromdatenight + "' and '"
            + strtodatenight + "') or (recorddatetime between '" + strfromdatenight2 + "' and '"
            + strtodatenight2 + "')) group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "hardaccelerationNight";

            //--------------------------------------------------------


            //Accross Day Night 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as cornerpacket
            FROM  iot_hub where  deviceno=" + deviceno + " and  coalesce(cornerpacket,0)=1 and  (recorddatetime between '" + strfromdate + "' and '"
            + strtodate + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "cornerpacket";

            //Day 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as cornerpacket
            FROM  iot_hub where  deviceno=" + deviceno + " and  coalesce(cornerpacket,0)=1 and  (recorddatetime between '" + strfromdateday + "' and '"
            + strtodateday + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "cornerpacketDay";


            //Night 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as cornerpacket
            FROM  iot_hub where  deviceno=" + deviceno + " and  coalesce(cornerpacket,0)=1 and  ((recorddatetime between '" + strfromdatenight + "' and '"
            + strtodatenight + "') or (recorddatetime between '" + strfromdatenight2 + "' and '"
            + strtodatenight2 + "')) group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "cornerpacketNight";

            //--------------------------------------------------------

            //Accross Day Night 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as overspeedstarted
            FROM  iot_hub where coalesce(overspeedstarted,0)=1 and  (recorddatetime between '" + strfromdate + "' and '"
            + strtodate + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "overspeedstarted";

            //Day 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as overspeedstarted
            FROM  iot_hub where coalesce(overspeedstarted,0)=1 and  (recorddatetime between '" + strfromdateday + "' and '"
            + strtodateday + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "overspeedstartedDay";


            //Night 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as overspeedstarted
            FROM  iot_hub where coalesce(overspeedstarted,0)=1 and  ((recorddatetime between '" + strfromdatenight + "' and '"
            + strtodatenight + "') or (recorddatetime between '" + strfromdatenight2 + "' and '"
            + strtodatenight2 + "')) group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "overspeedstartedNight";

            //--------------------------------------------------------

            //Accross Day Night 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as mcall
            FROM  iot_hub where coalesce(mcall,0)=1 and  (recorddatetime between '" + strfromdate + "' and '"
            + strtodate + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "mcall";

            //Day 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as mcall
            FROM  iot_hub where coalesce(mcall,0)=1 and  (recorddatetime between '" + strfromdateday + "' and '"
            + strtodateday + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "mcallDay";


            //Night 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as mcall
            FROM  iot_hub where coalesce(mcall,0)=1 and  ((recorddatetime between '" + strfromdatenight + "' and '"
            + strtodatenight + "') or (recorddatetime between '" + strfromdatenight2 + "' and '"
            + strtodatenight2 + "')) group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "mcallNight";

            //--------------------------------------------------------

            //Accross Day Night 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as weather
            FROM  iot_hub where coalesce(weather,0)=1 and  (recorddatetime between '" + strfromdate + "' and '"
            + strtodate + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "weather";

            //Day 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as weather
            FROM  iot_hub where coalesce(weather,0)=1 and  (recorddatetime between '" + strfromdateday + "' and '"
            + strtodateday + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "weatherDay";


            //Night 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as weather
            FROM  iot_hub where coalesce(weather,0)=1 and  ((recorddatetime between '" + strfromdatenight + "' and '"
            + strtodatenight + "') or (recorddatetime between '" + strfromdatenight2 + "' and '"
            + strtodatenight2 + "')) group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "weatherNight";

            //--------------------------------------------------------

            //Accross Day Night 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno, to_char (recorddatetime, 'DD-MM-YYYY') as rd,avg(speed) as avgspeed,max(speed) maxspeed,sum(distance)/1000 as distance
            FROM  iot_hub where   deviceno=" + deviceno + " and  speed >0 and  (recorddatetime between '" + strfromdate + "' and '"
            + strtodate + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "speeddistance";

            //Day
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno, to_char (recorddatetime, 'DD-MM-YYYY') as rd,avg(speed) as avgspeed,max(speed) maxspeed,sum(distance)/1000 as distance
            FROM  iot_hub where  deviceno=" + deviceno + " and  speed >0 and  (recorddatetime between '" + strfromdateday + "' and '"
            + strtodateday + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "speeddistanceDay";

            //Night
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno, to_char (recorddatetime, 'DD-MM-YYYY') as rd,avg(speed) as avgspeed,max(speed) maxspeed,sum(distance)/1000 as distance
            FROM  iot_hub where  deviceno=" + deviceno + " and  speed >0 and  ((recorddatetime between '" + strfromdatenight + "' and '"
            + strtodatenight + "') or (recorddatetime between '" + strfromdatenight2 + "' and '"
            + strtodatenight2 + "')) group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "speeddistanceNight";


            //--------------------------------------------------------

            //Accross Day Night 
            strSqlQry = new StringBuilder();
            strSqlQry.Append("SELECT  deviceno, to_char (recorddatetime, 'DD-MM-YYYY') as rd,avg(CAST(rpm AS decimal)) as avgrpm,max(rpm) as maxrpm,min(CAST(rpm AS decimal)) as minrpm ,");
            strSqlQry.Append(" avg(CAST(coolanttemperature AS decimal)) as avgcoolanttemperature,max(CAST(coolanttemperature AS decimal)) as maxcoolanttemperature,min(CAST(coolanttemperature AS decimal)) as mincoolanttemperature ,");
            strSqlQry.Append(" avg(CAST(calculatedengineload AS decimal)) as avgcalculatedengineload,max(CAST(calculatedengineload AS decimal)) as maxcalculatedengineload,min(CAST(calculatedengineload AS decimal)) as mincalculatedengineload ");
            strSqlQry.Append(" FROM  iot_obd_hub where  deviceno=" + deviceno + " and  CAST(speedcan AS decimal) >0 and  (recorddatetime between '" + strfromdate + "' and '");
            strSqlQry.Append(strtodate + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "iot_obd_hub_Para";

            //Day
            strSqlQry = new StringBuilder();
            strSqlQry.Append("SELECT  deviceno, to_char (recorddatetime, 'DD-MM-YYYY') as rd,avg(CAST(rpm AS decimal)) as avgrpm,max(rpm) as maxrpm,min(CAST(rpm AS decimal)) as minrpm ,");
            strSqlQry.Append(" avg(CAST(coolanttemperature AS decimal)) as avgcoolanttemperature,max(CAST(coolanttemperature AS decimal)) as maxcoolanttemperature,min(CAST(coolanttemperature AS decimal)) as mincoolanttemperature ,");
            strSqlQry.Append(" avg(CAST(calculatedengineload AS decimal)) as avgcalculatedengineload,max(CAST(calculatedengineload AS decimal)) as maxcalculatedengineload,min(CAST(calculatedengineload AS decimal)) as mincalculatedengineload ");
            strSqlQry.Append(" FROM  iot_obd_hub where  deviceno=" + deviceno + " and  CAST(speedcan AS decimal) >0 and  (recorddatetime between '" + strfromdateday + "' and '");
            strSqlQry.Append(strtodateday + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "iot_obd_hub_ParaDay";


            //Night 
            strSqlQry = new StringBuilder();
            strSqlQry.Append("SELECT  deviceno, to_char (recorddatetime, 'DD-MM-YYYY') as rd,avg(CAST(rpm AS decimal)) as avgrpm,max(rpm) as maxrpm,min(CAST(rpm AS decimal)) as minrpm ,");
            strSqlQry.Append(" avg(CAST(coolanttemperature AS decimal)) as avgcoolanttemperature,max(CAST(coolanttemperature AS decimal)) as maxcoolanttemperature,min(CAST(coolanttemperature AS decimal)) as mincoolanttemperature ,");
            strSqlQry.Append(" avg(CAST(calculatedengineload AS decimal)) as avgcalculatedengineload,max(CAST(calculatedengineload AS decimal)) as maxcalculatedengineload,min(CAST(calculatedengineload AS decimal)) as mincalculatedengineload ");
            strSqlQry.Append(" FROM  iot_obd_hub where  deviceno=" + deviceno + " and CAST(speedcan AS decimal)  >0 and  ((recorddatetime between '" + strfromdatenight + "' and '" + strtodatenight + "') or (recorddatetime between '" + strfromdatenight2 + "' and '" + strtodatenight2 + "'))");
            strSqlQry.Append(" group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "iot_obd_hub_ParaNight";



            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno, count(*) as rcnt ,to_char (recorddatetime, 'DD-MM-YYYY') as rd, latitude, longitude,min(recorddatetime) as minrecorddatetime, max(recorddatetime) as maxrecorddatetime,
                        EXTRACT(EPOCH FROM (max(recorddatetime) - min(recorddatetime))) as diffinsec 
                         FROM iot_hub  where  deviceno=" + deviceno + " and  speed=0 and (recorddatetime between '" + strfromdate + "' and '"
            + strtodate + "') group by deviceno,latitude,longitude,to_char (recorddatetime, 'DD-MM-YYYY') having EXTRACT(EPOCH FROM (max(recorddatetime) - min(recorddatetime))) >0");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "stoppage";

            return dsData;
        }

        public DataSet GetDailyAggregationParameters(Int64 deviceno, DateTime FromDate, DateTime ToDate)
        {
            //for get day night range according to trip start and end date
            string strfromdateday, strtodateday, strfromdatenight, strtodatenight, strfromdatenight2, strtodatenight2, strfromdate, strtodate;
            GetDayNightDefinationRange(FromDate, ToDate, out strfromdate, out strtodate, out strfromdateday, out strtodateday, out strfromdatenight, out strtodatenight,
               out strfromdatenight2, out strtodatenight2);

            dsData = new DataSet();

            //for day 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,count(*) ignitiononday  FROM iot_hub  where  deviceno=" + deviceno + " and ignitionstatus=1  and (recorddatetime between '" + strfromdateday + "' and '"
            + strtodateday + "') group by deviceno");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "ignitiononday";

            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,count(*) ignitionondayideal  FROM iot_hub  where  deviceno=" + deviceno + " and ignitionstatus=1 and speed=0 and (recorddatetime between '" + strfromdateday + "' and '"
            + strtodateday + "') group by deviceno");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "ignitionondayideal";



            //for night
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,count(*) ignitiononnight  FROM iot_hub  where  deviceno=" + deviceno + " and ignitionstatus=1 and ((recorddatetime between '" + strfromdatenight + "' and '"
            + strtodatenight + "') or (recorddatetime between '" + strfromdatenight2 + "' and '"
            + strtodatenight2 + "')) group by deviceno");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "ignitiononnight";

            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,count(*) ignitiononnightideal  FROM iot_hub  where  deviceno=" + deviceno + " and ignitionstatus=1 and speed=0 and ((recorddatetime between '" + strfromdatenight + "' and '"
            + strtodatenight + "') or (recorddatetime between '" + strfromdatenight2 + "' and '"
            + strtodatenight2 + "')) group by deviceno");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "ignitiononnightideal";


            return dsData;

        }

        public DataTable GetDeviceDetails(Int64 trip_id)
        {
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@" SELECT	deviceno,user_id, user_name,first_name,middle_name,last_name,date_of_birth,date_part('year', age(CURRENT_TIMESTAMP,date_of_birth ))  as age,gender,address1,mobileno,email_id1,email_id2,profile_picture_uri,
	                            vehicle_no,	license_no,	license_expiry_date,registration_no,vehicle_manufacturer,vehicle_color,
	                            vehicle_type FROM  user_master where user_id in (select user_id from trip_planning_master where trip_id=" + trip_id + ")");

            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            return dtData;
        }

        public DataTable GetTripOverallScore(Int64 trip_id)
        {
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@" select deviceno,coalesce(sum(distance),0) as distance,coalesce(avg(final_score),0) as final_score from dr_daily_trip_parameter_wise_rating where trip_id=" + trip_id + " group by deviceno");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            return dtData;
        }

        public bool DeleteAlreadyProcessedData(Int64 trip_id)
        {
            try
            {
                ConsoleLog("deleting rpt_daily_trip_agg_parameter data ......");
                strSqlQry = new StringBuilder();
                strSqlQry.Append(@"DELETE FROM rpt_daily_trip_agg_parameter where trip_id=" + trip_id);
                objDAL.ExecuteSQL(strSqlQry.ToString());
                strSqlQry = null;

                ConsoleLog("deleting rpt_daily_trip_stoppage data.....");
                strSqlQry = new StringBuilder();
                strSqlQry.Append(@"DELETE FROM rpt_daily_trip_stoppage where trip_id=" + trip_id);
                objDAL.ExecuteSQL(strSqlQry.ToString());
                strSqlQry = null;


                ConsoleLog("deleting dr_daily_trip_parameter_wise_rating data.....");
                strSqlQry = new StringBuilder();
                strSqlQry.Append(@"DELETE FROM dr_daily_trip_parameter_wise_rating where trip_id=" + trip_id);
                objDAL.ExecuteSQL(strSqlQry.ToString());
                strSqlQry = null;

                ConsoleLog("deleting dr_overall_trip_rating data.....");
                strSqlQry = new StringBuilder();
                strSqlQry.Append(@"DELETE FROM dr_overall_trip_rating where trip_id=" + trip_id);
                objDAL.ExecuteSQL(strSqlQry.ToString());
                strSqlQry = null;

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void getDayNightDefination(int org_id)
        {
            try
            {
                strSqlQry = new StringBuilder();
                strSqlQry.Append(@"select o_id, outof_rating, dayfromhours, daytohours, nightfromhours, nighttohours,nightfromhours2, nighttohours2 from org_setting where o_id=" + org_id);
                DataTable dtorg_setting = objDAL.FetchRecords(strSqlQry.ToString());
                strSqlQry = null;

                if (dtorg_setting.Rows.Count > 0)
                {
                    dayfromhours = CommonUtils.ConvertToString(dtorg_setting.Rows[0]["dayfromhours"]);
                    daytohours = CommonUtils.ConvertToString(dtorg_setting.Rows[0]["daytohours"]);
                    nightfromhours = CommonUtils.ConvertToString(dtorg_setting.Rows[0]["nightfromhours"]);
                    nighttohours = CommonUtils.ConvertToString(dtorg_setting.Rows[0]["nighttohours"]);
                    nightfromhours2 = CommonUtils.ConvertToString(dtorg_setting.Rows[0]["nightfromhours2"]);
                    nighttohours2 = CommonUtils.ConvertToString(dtorg_setting.Rows[0]["nighttohours2"]);

                }
                dtorg_setting.Dispose();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GetDayNightDefinationRange(DateTime? FromDate, DateTime? ToDate, out string strfromdate, out string strtodate,
         out string strfromdateday, out string strtodateday, out string strfromdatenight, out string strtodatenight, out string strfromdatenight2, out string strtodatenight2)
        {
            DateTime _dayFromDate;
            DateTime _dayToDate;
            DateTime _nightFromDate;
            DateTime _nightToDate;
            DateTime _nightFromDate2;
            DateTime _nightToDate2;

            strfromdate = string.Format("{0:yyyy-MM-dd}", FromDate).ToString().Replace(".", ":");
            strtodate = string.Format("{0:yyyy-MM-dd}", ToDate).ToString().Replace(".", ":");

            strfromdateday = strfromdate;
            strtodateday = strtodate;
            strfromdatenight = strfromdate;
            strtodatenight = strtodate;
            strfromdatenight2 = strfromdate;
            strtodatenight2 = strtodate;

            strfromdate = string.Format("{0:yyyy-MM-dd HH:mm:ss}", FromDate).ToString().Replace(".", ":");
            strtodate = string.Format("{0:yyyy-MM-dd HH:mm:ss}", ToDate).ToString().Replace(".", ":");

            strfromdateday = strfromdateday + " " + dayfromhours;
            strtodateday = strtodateday + " " + daytohours;
            _dayFromDate = Convert.ToDateTime(strfromdateday);
            _dayToDate = Convert.ToDateTime(strtodateday);

            strfromdatenight = strfromdatenight + " " + nightfromhours;
            strtodatenight = strtodatenight + " " + nighttohours;
            _nightFromDate = Convert.ToDateTime(strfromdatenight);
            _nightToDate = Convert.ToDateTime(strtodatenight);

            strfromdatenight2 = strfromdatenight2 + " " + nightfromhours2;
            strtodatenight2 = strtodatenight2 + " " + nighttohours2;
            _nightFromDate2 = Convert.ToDateTime(strfromdatenight2);
            _nightToDate2 = Convert.ToDateTime(strtodatenight2);

            if ((_nightFromDate <= FromDate && FromDate <= _nightToDate) || (_nightFromDate <= ToDate && ToDate <= _nightToDate))
            {
                if (_nightFromDate < FromDate)
                {
                    _nightFromDate = Convert.ToDateTime(FromDate);
                }

                if (_nightToDate > ToDate)
                {
                    _nightToDate = Convert.ToDateTime(ToDate);
                }

            }
            else
            {
                _nightFromDate = Convert.ToDateTime("2100-12-31 00:00:00");
                _nightToDate = Convert.ToDateTime("2100-12-31 00:00:00");
            }

            if ((_dayFromDate <= FromDate && FromDate <= _dayToDate) || (_dayFromDate <= ToDate && ToDate <= _dayToDate))
            {
                if (_dayFromDate < FromDate)
                {
                    _dayFromDate = Convert.ToDateTime(FromDate);
                }

                if (_dayToDate > ToDate)
                {
                    _dayToDate = Convert.ToDateTime(ToDate);
                }
            }
            else
            {
                if (!(FromDate <= _dayFromDate && ToDate >= _dayToDate))
                {
                    _dayFromDate = Convert.ToDateTime("2100-12-31 00:00:00");
                    _dayToDate = Convert.ToDateTime("2100-12-31 00:00:00");
                }
            }

            if ((_nightFromDate2 <= FromDate && FromDate <= _nightToDate2) || (_nightFromDate2 <= ToDate && ToDate <= _nightToDate2))
            {
                if (_nightFromDate2 < FromDate)
                {
                    _nightFromDate2 = Convert.ToDateTime(FromDate);
                }

                if (_nightToDate2 > ToDate)
                {
                    _nightToDate2 = Convert.ToDateTime(ToDate);
                }

            }
            else
            {
                _nightFromDate2 = Convert.ToDateTime("2100-12-31 00:00:00");
                _nightToDate2 = Convert.ToDateTime("2100-12-31 00:00:00");
            }



            strfromdatenight = string.Format("{0:yyyy-MM-dd HH:mm:ss}", _nightFromDate).ToString().Replace(".", ":");
            strtodatenight = string.Format("{0:yyyy-MM-dd HH:mm:ss}", _nightToDate).ToString().Replace(".", ":");

            strfromdateday = string.Format("{0:yyyy-MM-dd HH:mm:ss}", _dayFromDate).ToString().Replace(".", ":");
            strtodateday = string.Format("{0:yyyy-MM-dd HH:mm:ss}", _dayToDate).ToString().Replace(".", ":");

            strfromdatenight2 = string.Format("{0:yyyy-MM-dd HH:mm:ss}", _nightFromDate2).ToString().Replace(".", ":");
            strtodatenight2 = string.Format("{0:yyyy-MM-dd HH:mm:ss}", _nightToDate2).ToString().Replace(".", ":");


        }


        public bool DailyParameterWiseRatingprocess(Int64 deviceno, Int64 trip_id, int org_id)
        {
            #region localvariable


            Int64 o_id;
            decimal maxspeed = 0, minspeed = 0, overspeedlow_d_wt = 0,
                                 overspeed10_d_wt = 0, overspeed20_d_wt = 0, overspeed30_d_wt = 0, overspeedlow_n_wt = 0, overspeed10_n_wt = 0, overspeed20_n_wt = 0, overspeed30_n_wt = 0,
                                   age_criteria1_wt = 0, age_criteria2_wt = 0, age_criteria3_wt = 0, age_criteria4_wt = 0, gender_male_wt = 0,
                                 gender_female_wt = 0, gender_other_wt = 0, car_white_wt = 0, car_lightother_wt = 0, car_metallic_light_wt = 0, car_red_wt = 0,
                                 car_metallic_dark_wt = 0, car_darkother_wt = 0, car_black_wt = 0;

            string overspeedlow = "", overspeed10 = "", overspeed20 = "", overspeed30 = "", age_criteria1 = "", age_criteria2 = "", age_criteria3 = "", age_criteria4 = "";

            double overspeed_percentile = 0, harsh_ha_d_percentile = 0, harsh_hb_d_percentile = 0, harsh_cornering_d_percentile = 0,
                                 harsh_ha_n_percentile = 0, harsh_hb_n_percentile = 0, harsh_cornering_n_percentile = 0,
                                  overspeed_overall_wt = 0, harsh_overall_wt = 0, age_overall_wt = 0, gender_overall_wt = 0, car_overall_wt = 0, overspeed_per_def = 0, ha_d_per_def = 0, hb_d_per_def = 0,
                                 hc_d_per_def = 0, ha_n_per_def = 0, hb_n_per_def = 0, hc_n_per_def = 0,
                                 daily_overspeed_percentile = 0, daily_ha_d_percentile = 0, daily_hb_d_percentile = 0, daily_hc_d_percentile = 0, daily_ha_n_percentile = 0, daily_hb_n_percentile = 0, daily_hc_n_percentile = 0,
                                 call_d_percentile = 0, call_n_percentile = 0, call_d_per_def = 0, call_n_per_def = 0, call_wt = 0, weather_d_percentile = 0, weather_n_percentile = 0, weather_d_per_def = 0, weather_n_per_def = 0, weather_wt = 0,
                                 daily_call_d_percentile = 0, daily_call_n_percentile = 0, daily_weather_d_percentile = 0, daily_weather_n_percentile = 0;

            List<string> InsertQuereies = new List<string>();
            DataTable dtPercentile;

            #endregion

            try
            {
                //speed
                //Get parameter list applicable for organization
                #region Get dtParDetails
                strSqlQry = new StringBuilder();
                strSqlQry.Append(@"select o_id, maxspeed, minspeed, overspeedlow, overspeed10, overspeed20, overspeed30, overspeedlow_d_wt,
                                overspeed10_d_wt, overspeed20_d_wt, overspeed30_d_wt, overspeedlow_n_wt, overspeed10_n_wt, overspeed20_n_wt, overspeed30_n_wt,
                                overspeed_percentile, overspeed_overall_wt, harsh_ha_d_percentile, harsh_hb_d_percentile, harsh_cornering_d_percentile,
                                harsh_ha_n_percentile, harsh_hb_n_percentile, harsh_cornering_n_percentile, harsh_overall_wt, age_criteria1, age_criteria2,
                                age_criteria3, age_criteria4, age_criteria1_wt, age_criteria2_wt, age_criteria3_wt, age_criteria4_wt, age_overall_wt, gender_male_wt, 
                                gender_female_wt, gender_other_wt, gender_overall_wt, car_white_wt, car_lightother_wt, car_metallic_light_wt, car_red_wt, 
                                car_metallic_dark_wt, car_darkother_wt, car_black_wt, car_overall_wt ,overspeed_per_def,ha_d_per_def,hb_d_per_def,hc_d_per_def,
                                ha_n_per_def,hb_n_per_def,hc_n_per_def,call_d_percentile, call_n_percentile, call_d_per_def, call_n_per_def, call_wt,weather_d_percentile, 
                                weather_n_percentile, weather_d_per_def, weather_n_per_def, weather_wt from dr_parameter_config_setting WHERE o_id = " + org_id);

                DataTable dtParDetails = objDAL.FetchRecords(strSqlQry.ToString());
                #endregion


                double daysdiff = CommonUtils.DateDiff(_Afromdate, _Atodate);

                for (int i = 0; i < daysdiff; i++)
                {
                    InsertQuereies = new List<string>();

                    #region assign localvariable from dtParDetails
                    foreach (DataRow datarow in dtParDetails.Rows)
                    {
                        o_id = CommonUtils.ConvertToInt(datarow["o_id"]);
                        maxspeed = CommonUtils.ConvertToDecimal(datarow["maxspeed"]);
                        minspeed = CommonUtils.ConvertToDecimal(datarow["minspeed"]);

                        overspeedlow = CommonUtils.ConvertToString(datarow["overspeedlow"]);
                        overspeed10 = CommonUtils.ConvertToString(datarow["overspeed10"]);
                        overspeed20 = CommonUtils.ConvertToString(datarow["overspeed20"]);
                        overspeed30 = CommonUtils.ConvertToString(datarow["overspeed30"]);

                        overspeedlow_d_wt = CommonUtils.ConvertToDecimal(datarow["overspeedlow_d_wt"]);
                        overspeed10_d_wt = CommonUtils.ConvertToDecimal(datarow["overspeed10_d_wt"]);
                        overspeed20_d_wt = CommonUtils.ConvertToDecimal(datarow["overspeed20_d_wt"]);
                        overspeed30_d_wt = CommonUtils.ConvertToDecimal(datarow["overspeed30_d_wt"]);
                        overspeedlow_n_wt = CommonUtils.ConvertToDecimal(datarow["overspeedlow_n_wt"]);
                        overspeed10_n_wt = CommonUtils.ConvertToDecimal(datarow["overspeed10_n_wt"]);
                        overspeed20_n_wt = CommonUtils.ConvertToDecimal(datarow["overspeed20_n_wt"]);
                        overspeed30_n_wt = CommonUtils.ConvertToDecimal(datarow["overspeed30_n_wt"]);
                        overspeed_percentile = CommonUtils.ConvertToDouble(datarow["overspeed_percentile"]);
                        overspeed_overall_wt = CommonUtils.ConvertToDouble(datarow["overspeed_overall_wt"]);
                        harsh_ha_d_percentile = CommonUtils.ConvertToDouble(datarow["harsh_ha_d_percentile"]);
                        harsh_hb_d_percentile = CommonUtils.ConvertToDouble(datarow["harsh_hb_d_percentile"]);
                        harsh_cornering_d_percentile = CommonUtils.ConvertToDouble(datarow["harsh_cornering_d_percentile"]);

                        harsh_ha_n_percentile = CommonUtils.ConvertToDouble(datarow["harsh_ha_n_percentile"]);
                        harsh_hb_n_percentile = CommonUtils.ConvertToDouble(datarow["harsh_hb_n_percentile"]);
                        harsh_cornering_n_percentile = CommonUtils.ConvertToDouble(datarow["harsh_cornering_n_percentile"]);
                        harsh_overall_wt = CommonUtils.ConvertToDouble(datarow["harsh_overall_wt"]);

                        age_criteria1 = CommonUtils.ConvertToString(datarow["age_criteria1"]);
                        age_criteria2 = CommonUtils.ConvertToString(datarow["age_criteria2"]);
                        age_criteria3 = CommonUtils.ConvertToString(datarow["age_criteria3"]);
                        age_criteria4 = CommonUtils.ConvertToString(datarow["age_criteria4"]);

                        age_criteria1_wt = CommonUtils.ConvertToDecimal(datarow["age_criteria1_wt"]);
                        age_criteria2_wt = CommonUtils.ConvertToDecimal(datarow["age_criteria2_wt"]);
                        age_criteria3_wt = CommonUtils.ConvertToDecimal(datarow["age_criteria3_wt"]);
                        age_criteria4_wt = CommonUtils.ConvertToDecimal(datarow["age_criteria4_wt"]);
                        age_overall_wt = CommonUtils.ConvertToDouble(datarow["age_overall_wt"]);
                        gender_male_wt = CommonUtils.ConvertToDecimal(datarow["gender_male_wt"]);

                        gender_female_wt = CommonUtils.ConvertToDecimal(datarow["gender_female_wt"]);
                        gender_other_wt = CommonUtils.ConvertToDecimal(datarow["gender_other_wt"]);
                        gender_overall_wt = CommonUtils.ConvertToDouble(datarow["gender_overall_wt"]);
                        car_white_wt = CommonUtils.ConvertToDecimal(datarow["car_white_wt"]);
                        car_lightother_wt = CommonUtils.ConvertToDecimal(datarow["car_lightother_wt"]);
                        car_metallic_light_wt = CommonUtils.ConvertToDecimal(datarow["car_metallic_light_wt"]);
                        car_red_wt = CommonUtils.ConvertToDecimal(datarow["car_red_wt"]);

                        car_metallic_dark_wt = CommonUtils.ConvertToDecimal(datarow["car_metallic_dark_wt"]);
                        car_darkother_wt = CommonUtils.ConvertToDecimal(datarow["car_darkother_wt"]);
                        car_black_wt = CommonUtils.ConvertToDecimal(datarow["car_black_wt"]);
                        car_overall_wt = CommonUtils.ConvertToDouble(datarow["car_overall_wt"]);

                        overspeed_per_def = CommonUtils.ConvertToDouble(datarow["overspeed_per_def"]);
                        ha_d_per_def = CommonUtils.ConvertToDouble(datarow["ha_d_per_def"]);
                        hb_d_per_def = CommonUtils.ConvertToDouble(datarow["hb_d_per_def"]);
                        hc_d_per_def = CommonUtils.ConvertToDouble(datarow["hc_d_per_def"]);
                        ha_n_per_def = CommonUtils.ConvertToDouble(datarow["ha_n_per_def"]);
                        hb_n_per_def = CommonUtils.ConvertToDouble(datarow["hb_n_per_def"]);
                        hc_n_per_def = CommonUtils.ConvertToDouble(datarow["hc_n_per_def"]);

                        call_d_percentile = CommonUtils.ConvertToDouble(datarow["call_d_percentile"]);
                        call_n_percentile = CommonUtils.ConvertToDouble(datarow["call_n_percentile"]);
                        call_d_per_def = CommonUtils.ConvertToDouble(datarow["call_d_per_def"]);
                        call_n_per_def = CommonUtils.ConvertToDouble(datarow["call_n_per_def"]);
                        call_wt = CommonUtils.ConvertToDouble(datarow["call_wt"]);

                        weather_d_percentile = CommonUtils.ConvertToDouble(datarow["weather_d_percentile"]);
                        weather_n_percentile = CommonUtils.ConvertToDouble(datarow["weather_n_percentile"]);
                        weather_d_per_def = CommonUtils.ConvertToDouble(datarow["weather_d_per_def"]);
                        weather_n_per_def = CommonUtils.ConvertToDouble(datarow["weather_n_per_def"]);
                        weather_wt = CommonUtils.ConvertToDouble(datarow["weather_wt"]);
                    }
                    #endregion

                    #region set date according to day night


                    //for get day night range according to trip start and end date
                    string strfromdate = CommonUtils.GetDateFromatyyyy_MM_dd(_Afromdate.AddDays(i));
                    string strtodate = CommonUtils.GetDateFromatyyyy_MM_dd(_Afromdate.AddDays(i));
                    string filterRD = CommonUtils.GetDateFromatdd_MM_yyyy(_Afromdate.AddDays(i));
                    string strprivousdate = CommonUtils.GetDateFromatyyyy_MM_dd(_Afromdate.AddDays(i - 1));
                    string filterprivousdate = CommonUtils.GetDateFromatdd_MM_yyyy(_Afromdate.AddDays(i - 1));

                    strfromdate = strfromdate + " 00:00:00";
                    strtodate = strtodate + " 23:59:59";

                    _fromdate = Convert.ToDateTime(strfromdate);
                    _todate = Convert.ToDateTime(strtodate);
                    _privousfromdate = Convert.ToDateTime(strprivousdate);

                    if (_Afromdate > _fromdate) _fromdate = _Afromdate;
                    if (_Atodate < _todate) _todate = _Atodate;

                    string strfromdateday, strtodateday, strfromdatenight, strtodatenight, strfromdatenight2, strtodatenight2;
                    GetDayNightDefinationRange(_fromdate, _todate, out strfromdate, out strtodate, out strfromdateday, out strtodateday, out strfromdatenight, out strtodatenight,
                      out strfromdatenight2, out strtodatenight2);

                    DateTime _dayFromDate;
                    DateTime _dayToDate;
                    DateTime _nightFromDate;
                    DateTime _nightToDate;
                    DateTime _nightFromDate2;
                    DateTime _nightToDate2;

                    _dayFromDate = Convert.ToDateTime(strfromdateday);
                    _dayToDate = Convert.ToDateTime(strtodateday);

                    _nightFromDate = Convert.ToDateTime(strfromdatenight);
                    _nightToDate = Convert.ToDateTime(strtodatenight);

                    _nightFromDate2 = Convert.ToDateTime(strfromdatenight2);
                    _nightToDate2 = Convert.ToDateTime(strtodatenight2);


                    #endregion

                    ConsoleLog("DailyParameterWiseRatingprocess for day " + strfromdate);

                    #region Get GetDeviceDetails
                    dtDeviceDetails = GetDeviceDetails(trip_id);
                    #endregion

                    if (dtDeviceDetails.Rows.Count == 0) goto nextday;

                    ConsoleLog("GetAllDevicesSpeedCriteriaData");

                    DataSet dsAllSpeedCriteriasData = GetAllDevicesSpeedCriteriaData(deviceno, _fromdate, _todate, _dayFromDate, _dayToDate, _nightFromDate, _nightToDate, _nightFromDate2, _nightToDate2,
                       overspeedlow, overspeed10, overspeed20, overspeed30);

                    ConsoleLog("GetAllDevicesHAHBCorneringData");

                    DataSet dsAllDevicesHAHBCorneringData = GetAllDevicesHAHBCorneringData(deviceno, _fromdate, _todate, _dayFromDate, _dayToDate, _nightFromDate, _nightToDate, _nightFromDate2, _nightToDate2);

                    DataSet dsAllDevicesDistanceData = GetAllDevicesDistanceData(deviceno, _fromdate, _todate, _dayFromDate, _dayToDate, _nightFromDate, _nightToDate, _nightFromDate2, _nightToDate2);

                    DataTable dtdaily_percentile = objDAL.FetchRecords(@"SELECT o_id,recorddate,overspeed_percentile, ha_d_percentile,hb_d_percentile,hc_d_percentile,ha_n_percentile,hb_n_percentile,hc_n_percentile,call_d_percentile,call_n_percentile,weather_d_percentile,weather_n_percentile FROM dr_daily_percentile where o_id=" + org_id + "  and  recorddate<'" + strfromdate + "' order by recorddate  desc limit 1;");

                    foreach (DataRow datadaily_percentile in dtdaily_percentile.Rows)
                    {
                        daily_overspeed_percentile = CommonUtils.ConvertToDouble(datadaily_percentile["overspeed_percentile"]);
                        daily_ha_d_percentile = CommonUtils.ConvertToDouble(datadaily_percentile["ha_d_percentile"]);
                        daily_hb_d_percentile = CommonUtils.ConvertToDouble(datadaily_percentile["hb_d_percentile"]);
                        daily_hc_d_percentile = CommonUtils.ConvertToDouble(datadaily_percentile["hc_d_percentile"]);
                        daily_ha_n_percentile = CommonUtils.ConvertToDouble(datadaily_percentile["ha_n_percentile"]);
                        daily_hb_n_percentile = CommonUtils.ConvertToDouble(datadaily_percentile["hb_n_percentile"]);
                        daily_hc_n_percentile = CommonUtils.ConvertToDouble(datadaily_percentile["hc_n_percentile"]);

                        daily_call_d_percentile = CommonUtils.ConvertToDouble(datadaily_percentile["call_d_percentile"]);
                        daily_call_n_percentile = CommonUtils.ConvertToDouble(datadaily_percentile["call_n_percentile"]);
                        daily_weather_d_percentile = CommonUtils.ConvertToDouble(datadaily_percentile["weather_d_percentile"]);
                        daily_weather_n_percentile = CommonUtils.ConvertToDouble(datadaily_percentile["weather_n_percentile"]); ;
                    }

                    #region  wt and score calculation othe than percentile setting

                    foreach (DataRow dataDeviceRow in dtDeviceDetails.Rows)
                    {
                        deviceno = CommonUtils.ConvertToInt64(dataDeviceRow["deviceno"]);

                        ConsoleLog("device no= " + deviceno);


                        string vehicle_color = "";
                        Int32 _age = 0;
                        string gender = "";

                        if (dataDeviceRow["vehicle_color"] != null || dataDeviceRow["vehicle_color"].ToString() == "")
                        {
                            vehicle_color = dataDeviceRow["vehicle_color"].ToString();
                        }

                        if (dataDeviceRow["age"] != null || dataDeviceRow["age"].ToString() == "")
                        {
                            _age = CommonUtils.ConvertToInt(dataDeviceRow["age"].ToString());
                        }

                        if (dataDeviceRow["gender"] != null || dataDeviceRow["gender"].ToString() == "")
                        {
                            gender = dataDeviceRow["gender"].ToString();
                        }

                        String filter = "deviceno=" + deviceno + " and rd='" + filterRD + "'";


                        ConsoleLog("calculate Speed  cateria val and wt avg");
                        #region  calculate Speed  cateria val and wt avg
                        decimal overspeed_wt_avg = 0, overspeedlow_d_val = 0, overspeedlow_n_val = 0, overspeed10_d_val = 0,
                      overspeed10_n_val = 0, overspeed20_d_val = 0, overspeed20_n_val = 0, overspeed30_d_val = 0, overspeed30_n_val = 0;

                        if (overspeedlow != "")
                        {
                            overspeedlow_d_val = GetParameterCateriaVal(filter, "overspeedlow_day", "CriteriaVal", dsAllSpeedCriteriasData);
                            overspeedlow_n_val = GetParameterCateriaVal(filter, "overspeedlow_night", "CriteriaVal", dsAllSpeedCriteriasData);

                        }

                        if (overspeed10 != "")
                        {
                            overspeed10_d_val = GetParameterCateriaVal(filter, "overspeed10_day", "CriteriaVal", dsAllSpeedCriteriasData);
                            overspeed10_n_val = GetParameterCateriaVal(filter, "overspeed10_night", "CriteriaVal", dsAllSpeedCriteriasData);

                        }

                        if (overspeed20 != "")
                        {
                            overspeed20_d_val = GetParameterCateriaVal(filter, "overspeed20_day", "CriteriaVal", dsAllSpeedCriteriasData);
                            overspeed20_n_val = GetParameterCateriaVal(filter, "overspeed20_night", "CriteriaVal", dsAllSpeedCriteriasData);

                        }

                        if (overspeed30 != "")
                        {
                            overspeed30_d_val = GetParameterCateriaVal(filter, "overspeed30_day", "CriteriaVal", dsAllSpeedCriteriasData);
                            overspeed30_n_val = GetParameterCateriaVal(filter, "overspeed30_night", "CriteriaVal", dsAllSpeedCriteriasData);

                        }

                        //overspeed_wt_avg = ((overspeedlow_d_val * overspeedlow_d_wt) + (overspeedlow_n_val * overspeedlow_n_wt) +
                        //    (overspeed10_d_val * overspeed10_d_wt) + (overspeed10_n_val * overspeed10_n_wt) +
                        //    (overspeed20_d_val * overspeed20_d_wt) + (overspeed20_n_val * overspeed20_n_wt) +
                        //    (overspeed30_d_val * overspeed30_d_wt) + (overspeed30_n_val * overspeed30_n_wt))
                        //    / (overspeedlow_d_wt + overspeedlow_n_wt + overspeed10_d_wt + overspeed10_n_wt
                        //    + overspeed20_d_wt + overspeed20_n_wt + overspeed30_d_wt + overspeed30_n_wt);
                        overspeed_wt_avg = ((overspeed10_d_val * overspeed10_d_wt) + (overspeed10_n_val * overspeed10_n_wt) +
                          (overspeed20_d_val * overspeed20_d_wt) + (overspeed20_n_val * overspeed20_n_wt) +
                          (overspeed30_d_val * overspeed30_d_wt) + (overspeed30_n_val * overspeed30_n_wt))
                          / (overspeed10_d_wt + overspeed10_n_wt + overspeed20_d_wt + overspeed20_n_wt + overspeed30_d_wt + overspeed30_n_wt);
                        #endregion

                        ConsoleLog("get ha/hb/cornering  criteria val");
                        #region  get ha/hb/cornering  criteria val
                        decimal harsh_ha_d_val, harsh_hb_d_val, harsh_cornering_d_val, harsh_ha_n_val, harsh_hb_n_val, harsh_cornering_n_val, harsh_score_val = 0,
                            call_d_val = 0, call_n_val = 0, calling_score_val = 0, weather_d_val = 0, weather_n_val = 0, weather_score_val = 0;

                        harsh_hb_d_val = GetParameterCateriaVal(filter, "hardbrakingDay", "hardbraking", dsAllDevicesHAHBCorneringData);
                        harsh_hb_n_val = GetParameterCateriaVal(filter, "hardbrakingNight", "hardbraking", dsAllDevicesHAHBCorneringData);
                        harsh_ha_d_val = GetParameterCateriaVal(filter, "hardaccelerationDay", "hardacceleration", dsAllDevicesHAHBCorneringData);
                        harsh_ha_n_val = GetParameterCateriaVal(filter, "hardaccelerationNight", "hardacceleration", dsAllDevicesHAHBCorneringData);
                        harsh_cornering_d_val = GetParameterCateriaVal(filter, "cornerpacketDay", "cornerpacket", dsAllDevicesHAHBCorneringData);
                        harsh_cornering_n_val = GetParameterCateriaVal(filter, "cornerpacketNight", "cornerpacket", dsAllDevicesHAHBCorneringData);

                        call_d_val = GetParameterCateriaVal(filter, "callDay", "mcall", dsAllDevicesHAHBCorneringData);
                        call_n_val = GetParameterCateriaVal(filter, "callNight", "mcall", dsAllDevicesHAHBCorneringData);
                        weather_d_val = GetParameterCateriaVal(filter, "weatherDay", "weather", dsAllDevicesHAHBCorneringData);
                        weather_n_val = GetParameterCateriaVal(filter, "weatherNight", "weather", dsAllDevicesHAHBCorneringData);

                        #endregion

                        ConsoleLog("calculate age_score");
                        #region  calculate age_score

                        decimal age_score = 0;
                        bool age_criteria1_val = false, age_criteria2_val = false, age_criteria3_val = false, age_criteria4_val = false;
                        if (age_criteria1 != "")
                        {
                            age_score = GetAgeScore(age_criteria1, age_criteria1_wt, _age, out age_criteria1_val);
                        }
                        if (!age_criteria1_val && age_criteria2 != "")
                        {
                            age_score = GetAgeScore(age_criteria2, age_criteria2_wt, _age, out age_criteria2_val);
                        }
                        if (!age_criteria1_val && !age_criteria2_val && age_criteria3 != "")
                        {
                            age_score = GetAgeScore(age_criteria3, age_criteria3_wt, _age, out age_criteria3_val);
                        }

                        if (!age_criteria1_val && !age_criteria2_val && !age_criteria3_val && age_criteria4 != "")
                        {
                            age_score = GetAgeScore(age_criteria4, age_criteria4_wt, _age, out age_criteria4_val);
                        }

                        //temp if age not avaliable
                        if (age_score == 0)
                        {
                            _age = 25;
                            age_score = GetAgeScore(age_criteria1, age_criteria1_wt, _age, out age_criteria1_val);
                        }
                        #endregion

                        ConsoleLog("calculate Gender_score");
                        #region  calculate Gender_score
                        decimal gender_score = 0;
                        bool gender_male_val = false, gender_female_val = false, gender_other_val = false;

                        if (gender.ToLower() == "male")
                        {
                            gender_score = 100 * gender_male_wt;
                            gender_male_val = true;
                        }
                        else if (gender.ToLower() == "female")
                        {
                            gender_score = 100 * gender_female_wt;
                            gender_female_val = true;
                        }
                        else if (gender.ToLower() == "other")
                        {
                            gender_score = 100 * gender_other_wt;
                            gender_other_val = true;
                        }
                        //tem if gender not avaliable
                        if (gender_score == 0)
                        {
                            gender_score = 100 * gender_male_wt;
                            gender_male_val = true;
                        }
                        #endregion

                        ConsoleLog("calculate car_color_score");
                        #region  calculate car_color_score

                        decimal car_color_score = 0;
                        bool car_white_val = false, car_lightother_val = false, car_metallic_light_val = false,
                            car_red_val = false, car_metallic_dark_val = false, car_darkother_val = false, car_black_val = false;

                        if (vehicle_color.ToLower() == "red")
                        {
                            car_color_score = 100 * car_red_wt;
                            car_red_val = true;
                        }
                        else if (vehicle_color.ToLower() == "black")
                        {
                            car_color_score = 100 * car_black_wt;
                            car_black_val = true;
                        }
                        else if (vehicle_color.ToLower() == "white")
                        {
                            car_color_score = 100 * car_white_wt;
                            car_white_val = true;
                        }
                        else if (vehicle_color.ToLower() == "metallicdark")
                        {
                            car_color_score = 100 * car_metallic_dark_wt;
                            car_metallic_dark_val = true;
                        }
                        else if (vehicle_color.ToLower() == "metalliclight")
                        {
                            car_color_score = 100 * car_metallic_light_wt;
                            car_metallic_light_val = true;
                        }
                        else if (vehicle_color.ToLower() == "otherdark")
                        {
                            car_color_score = 100 * car_darkother_wt;
                            car_darkother_val = true;
                        }
                        else// for dark light
                        {
                            car_color_score = 100 * car_lightother_wt;
                            car_lightother_val = true;
                        }

                        #endregion

                        ConsoleLog("get distance val");
                        #region  get distance val
                        decimal distance = 0, distance_d_val = 0, distance_n_val = 0;


                        distance_d_val = GetParameterCateriaVal(filter, "distanceDay", "distance", dsAllDevicesDistanceData);
                        distance_n_val = GetParameterCateriaVal(filter, "distanceNight", "distance", dsAllDevicesDistanceData);
                        distance = distance_d_val + distance_n_val;
                        #endregion



                        #region prepare Insert query and add in to InsertQuereies list
                        if (distance > 0)
                        {
                            strSqlQry = new StringBuilder();
                            strSqlQry.Append(@"INSERT INTO dr_daily_trip_parameter_wise_rating(
	                         deviceno,trip_id, recorddate,distance,distance_d,distance_n, overspeedlow_d, overspeed10_d, overspeed20_d, overspeed30_d, overspeedlow_n, overspeed10_n, overspeed20_n, 
                             overspeed30_n, overspeedwt_avg, overspeedscore, harsh_ha_d, harsh_hb_d, harsh_cornering_d, harsh_ha_n, harsh_hb_n, harsh_cornering_n, harsh_score, age_criteria1, age_criteria2, age_criteria3, age_criteria4, age_score, gender_male, gender_female, gender_other, gender_score, car_white, car_lightother, car_metallic_light, car_red, car_metallic_dark, car_darkother, car_black, car_score,call_d,call_n,calling_score,weather_d,weather_n,weather_score)
	                         VALUES (" + deviceno + "," + trip_id + ",'" + CommonUtils.GetDateFromatyyyy_MM_dd(_fromdate) + "'," + distance + "," + distance_d_val + "," + distance_n_val
                                      + "," + overspeedlow_d_val + "," + overspeed10_d_val + "," + overspeed20_d_val
                                      + "," + overspeed30_d_val + "," + overspeedlow_n_val + "," + overspeed10_n_val + "," + overspeed20_n_val + "," + overspeed30_n_val + "," + overspeed_wt_avg + ",null," + harsh_ha_d_val + "," + harsh_hb_d_val + ","
                                      + harsh_cornering_d_val + "," + harsh_ha_n_val + "," + harsh_hb_n_val + "," + harsh_cornering_n_val + "," + harsh_score_val + ","
                                      + age_criteria1_val + "," + age_criteria2_val + "," + age_criteria3_val + "," + age_criteria4_val + "," + age_score + ","
                                      + gender_male_val + "," + gender_female_val + "," + gender_other_val + "," + gender_score + "," + car_white_val + ","
                                      + car_lightother_val + "," + car_metallic_light_val + "," + car_red_val + "," + car_metallic_dark_val + "," + car_darkother_val
                                      + "," + car_black_val + "," + car_color_score + "," + call_d_val + "," + call_n_val + "," + calling_score_val + "," + weather_d_val + "," + weather_n_val + "," + weather_score_val + ");");
                            InsertQuereies.Add(strSqlQry.ToString());
                            strSqlQry = null;
                        }
                        #endregion
                    }
                    #endregion

                    //push data in database
                    ConsoleLog("execute InsertQuereies.");
                    foreach (string qry in InsertQuereies)
                    {
                        objDAL.ExecuteSQL(qry);
                    }

                    ConsoleLog("calculate overspeedscore ,harsh_score,final_score");
                    #region  calculate overspeedscore ,harsh_score,final_score
                    dtPercentile = objDAL.FetchRecords(@"select deviceno, recorddate, overspeedlow_d, overspeed10_d, overspeed20_d, overspeed30_d, overspeedlow_n, overspeed10_n, overspeed20_n,
                                overspeed30_n, overspeedwt_avg, overspeedscore, harsh_ha_d, harsh_hb_d, harsh_cornering_d,harsh_ha_n, harsh_hb_n, harsh_cornering_n, harsh_score, age_criteria1, age_criteria2, age_criteria3, age_criteria4, age_score, gender_male, gender_female, gender_other, gender_score, car_white, car_lightother, car_metallic_light, car_red, car_metallic_dark, car_darkother, car_black, car_score,call_d,call_n,calling_score,weather_d,weather_n,weather_score  from dr_daily_trip_parameter_wise_rating where trip_id=" + trip_id + " and to_char(recorddate, 'YYYY-MM-DD') ='" + CommonUtils.GetDateFromatyyyy_MM_dd(_fromdate) + "'");

                    //                    List<double> overspeedwt_avglist = new List<double>();
                    //                    double[] overspeedwt_avgArr;

                    //                    List<double> harsh_ha_d_list = new List<double>();
                    //                    double[] harsh_ha_d_Arr;

                    //                    List<double> harsh_hb_d_list = new List<double>();
                    //                    double[] harsh_hb_d_Arr;

                    //                    List<double> harsh_cornering_d_list = new List<double>();
                    //                    double[] harsh_cornering_d_Arr;

                    //                    List<double> harsh_ha_n_list = new List<double>();
                    //                    double[] harsh_ha_n_Arr;

                    //                    List<double> harsh_hb_n_list = new List<double>();
                    //                    double[] harsh_hb_n_Arr;

                    //                    List<double> harsh_cornering_n_list = new List<double>();
                    //                    double[] harsh_cornering_n_Arr;

                    //                    List<double> call_d_list = new List<double>();
                    //                    double[] call_d_Arr;

                    //                    List<double> call_n_list = new List<double>();
                    //                    double[] call_n_Arr;

                    //                    List<double> weather_d_list = new List<double>();
                    //                    double[] weather_d_Arr;

                    //                    List<double> weather_n_list = new List<double>();
                    //                    double[] weather_n_Arr;

                    //                    foreach (DataRow dataPercentileRow in dtPercentile.Rows)
                    //                    {
                    //                        overspeedwt_avglist.Add(CommonUtils.ConvertToDouble(dataPercentileRow["overspeedwt_avg"]));
                    //                        harsh_ha_d_list.Add(CommonUtils.ConvertToDouble(dataPercentileRow["harsh_ha_d"]));
                    //                        harsh_hb_d_list.Add(CommonUtils.ConvertToDouble(dataPercentileRow["harsh_hb_d"]));
                    //                        harsh_cornering_d_list.Add(CommonUtils.ConvertToDouble(dataPercentileRow["harsh_cornering_d"]));
                    //                        harsh_ha_n_list.Add(CommonUtils.ConvertToDouble(dataPercentileRow["harsh_ha_n"]));
                    //                        harsh_hb_n_list.Add(CommonUtils.ConvertToDouble(dataPercentileRow["harsh_hb_n"]));
                    //                        harsh_cornering_n_list.Add(CommonUtils.ConvertToDouble(dataPercentileRow["harsh_cornering_n"]));

                    //                        call_d_list.Add(CommonUtils.ConvertToDouble(dataPercentileRow["call_d"]));
                    //                        call_n_list.Add(CommonUtils.ConvertToDouble(dataPercentileRow["call_n"]));
                    //                        weather_d_list.Add(CommonUtils.ConvertToDouble(dataPercentileRow["weather_d"]));
                    //                        weather_n_list.Add(CommonUtils.ConvertToDouble(dataPercentileRow["weather_n"]));
                    //                    }

                    //overspeedwt_avgArr = overspeedwt_avglist.ToArray();
                    //overspeed_percentile = GetPercentile(overspeedwt_avgArr, overspeed_percentile);
                    //if (overspeed_percentile == 0)
                    //{
                    overspeed_percentile = daily_overspeed_percentile;
                    if (overspeed_percentile == 0)
                    {
                        overspeed_percentile = overspeed_per_def;
                    }
                    //}


                    //harsh_ha_d_Arr = harsh_ha_d_list.ToArray();
                    //harsh_ha_d_percentile = GetPercentile(harsh_ha_d_Arr, harsh_ha_d_percentile);
                    //if (harsh_ha_d_percentile == 0)
                    //{
                    harsh_ha_d_percentile = daily_ha_d_percentile;
                    if (harsh_ha_d_percentile == 0)
                    {
                        harsh_ha_d_percentile = ha_d_per_def;
                    }
                    //}

                    //harsh_hb_d_Arr = harsh_hb_d_list.ToArray();
                    //harsh_hb_d_percentile = GetPercentile(harsh_hb_d_Arr, harsh_hb_d_percentile);
                    //if (harsh_hb_d_percentile == 0)
                    //{
                    harsh_hb_d_percentile = daily_hb_d_percentile;
                    if (harsh_hb_d_percentile == 0)
                    {
                        harsh_hb_d_percentile = hb_d_per_def;
                    }
                    //}

                    //harsh_cornering_d_Arr = harsh_cornering_d_list.ToArray();
                    //harsh_cornering_d_percentile = GetPercentile(harsh_cornering_d_Arr, harsh_cornering_d_percentile);
                    //if (harsh_cornering_d_percentile == 0)
                    //{
                    harsh_cornering_d_percentile = daily_hc_d_percentile;
                    if (harsh_cornering_d_percentile == 0)
                    {
                        harsh_cornering_d_percentile = hc_d_per_def;
                    }
                    //}

                    //harsh_ha_n_Arr = harsh_ha_n_list.ToArray();
                    //harsh_ha_n_percentile = GetPercentile(harsh_ha_n_Arr, harsh_ha_n_percentile);
                    //if (harsh_ha_n_percentile == 0)
                    //{
                    harsh_ha_n_percentile = daily_ha_n_percentile;
                    if (harsh_ha_n_percentile == 0)
                    {
                        harsh_ha_n_percentile = ha_n_per_def;
                    }
                    //}

                    //harsh_hb_n_Arr = harsh_hb_n_list.ToArray();
                    //harsh_hb_n_percentile = GetPercentile(harsh_hb_n_Arr, harsh_hb_n_percentile);
                    //if (harsh_hb_n_percentile == 0)
                    //{
                    harsh_hb_n_percentile = daily_hb_n_percentile;
                    if (harsh_hb_n_percentile == 0)
                    {
                        harsh_hb_n_percentile = hb_n_per_def;
                    }
                    //}

                    //harsh_cornering_n_Arr = harsh_cornering_n_list.ToArray();
                    //harsh_cornering_n_percentile = GetPercentile(harsh_cornering_n_Arr, harsh_cornering_n_percentile);
                    //if (harsh_cornering_n_percentile == 0)
                    //{
                    harsh_cornering_n_percentile = daily_hc_n_percentile;
                    if (harsh_cornering_n_percentile == 0)
                    {
                        harsh_cornering_n_percentile = hc_n_per_def;
                    }
                    //}

                    //call_d_Arr = call_d_list.ToArray();
                    //call_d_percentile = GetPercentile(call_d_Arr, call_d_percentile);
                    //if (call_d_percentile == 0)
                    //{
                    call_d_percentile = daily_call_d_percentile;
                    if (call_d_percentile == 0)
                    {
                        call_d_percentile = call_d_per_def;
                    }
                    //}

                    //call_n_Arr = call_n_list.ToArray();
                    //call_n_percentile = GetPercentile(call_n_Arr, call_n_percentile);
                    //if (call_n_percentile == 0)
                    //{
                    call_n_percentile = daily_call_n_percentile;
                    if (call_n_percentile == 0)
                    {
                        call_n_percentile = call_n_per_def;
                    }
                    //}

                    //weather_d_Arr = weather_d_list.ToArray();
                    //weather_d_percentile = GetPercentile(weather_d_Arr, weather_d_percentile);
                    //if (weather_d_percentile == 0)
                    //{
                    weather_d_percentile = daily_weather_d_percentile;
                    if (weather_d_percentile == 0)
                    {
                        weather_d_percentile = weather_d_per_def;
                    }
                    //}

                    //weather_n_Arr = weather_n_list.ToArray();
                    //weather_n_percentile = GetPercentile(weather_n_Arr, weather_n_percentile);
                    //if (weather_n_percentile == 0)
                    //{
                    weather_n_percentile = daily_weather_n_percentile;
                    if (weather_n_percentile == 0)
                    {
                        weather_n_percentile = weather_n_per_def;
                    }
                    //}


                    double final_score, overspeedwt_avg, overspeedscore, harsh_score, car_scoredb, gender_scoredb, age_scoredb, harsh_ha_d, harsh_hb_d, harsh_cornering_d, harsh_ha_n, harsh_hb_n, harsh_cornering_n, call_d, call_n, weather_d, weather_n, calling_score, weather_score;

                    InsertQuereies = new List<string>();

                    ////                    #region prepare Insert  query for add dr_daily_percentile and add in to InsertQuereies list
                    ////                    strSqlQry = new StringBuilder();

                    ////                    strSqlQry.Append(@" INSERT INTO dr_daily_percentile (o_id, recorddate, overspeed_percentile, ha_d_percentile, hb_d_percentile, hc_d_percentile, ha_n_percentile, hb_n_percentile, hc_n_percentile)
                    ////                                  VALUES  ( " + org_id + ",'" + CommonUtils.GetDateFromatyyyy_MM_dd(_privousfromdate) + "'," + overspeed_percentile + "," + harsh_ha_d_percentile + "," +
                    ////                                              harsh_hb_d_percentile + "," + harsh_cornering_d_percentile + "," + harsh_ha_n_percentile + "," + harsh_hb_n_percentile + "," + harsh_cornering_n_percentile + ");");
                    ////                    InsertQuereies.Add(strSqlQry.ToString());
                    ////                    strSqlQry = null;
                    ////                    #endregion

                    foreach (DataRow dataPercentileRow in dtPercentile.Rows)
                    {
                        deviceno = CommonUtils.ConvertToInt64(dataPercentileRow["deviceno"]);

                        overspeedwt_avg = CommonUtils.ConvertToDouble(dataPercentileRow["overspeedwt_avg"]);

                        overspeedscore = 100 - ((overspeedwt_avg * 100) / overspeed_percentile);

                        harsh_ha_d = CommonUtils.ConvertToDouble(dataPercentileRow["harsh_ha_d"]);
                        harsh_hb_d = CommonUtils.ConvertToDouble(dataPercentileRow["harsh_hb_d"]);
                        harsh_cornering_d = CommonUtils.ConvertToDouble(dataPercentileRow["harsh_cornering_d"]);
                        harsh_ha_n = CommonUtils.ConvertToDouble(dataPercentileRow["harsh_ha_n"]);
                        harsh_hb_n = CommonUtils.ConvertToDouble(dataPercentileRow["harsh_hb_n"]);
                        harsh_cornering_n = CommonUtils.ConvertToDouble(dataPercentileRow["harsh_cornering_n"]);

                        car_scoredb = CommonUtils.ConvertToDouble(dataPercentileRow["car_score"]);
                        gender_scoredb = CommonUtils.ConvertToDouble(dataPercentileRow["gender_score"]);
                        age_scoredb = CommonUtils.ConvertToDouble(dataPercentileRow["age_score"]);



                        harsh_score = 100 - ((((harsh_ha_d * 100) / harsh_ha_d_percentile) +
                            ((harsh_hb_d * 100) / harsh_hb_d_percentile) +
                            ((harsh_cornering_d * 100) / harsh_cornering_d_percentile) +
                            ((harsh_ha_n * 100) / harsh_ha_n_percentile) +
                            ((harsh_hb_n * 100) / harsh_hb_n_percentile) +
                            ((harsh_cornering_n * 100) / harsh_cornering_n_percentile)) / 6);

                        if (overspeedscore < 0 || Double.IsNaN(overspeedscore))
                        {
                            overspeedscore = 0;
                        }
                        if (harsh_score < 0 || Double.IsNaN(harsh_score))
                        {
                            harsh_score = 0;
                        }


                        call_d = CommonUtils.ConvertToDouble(dataPercentileRow["call_d"]);
                        call_n = CommonUtils.ConvertToDouble(dataPercentileRow["call_n"]);
                        calling_score = 100 - ((((call_d * 100) / call_d_percentile) + ((call_n * 100) / call_n_percentile)) / 2);
                        if (calling_score < 0 || Double.IsNaN(calling_score))
                        {
                            calling_score = 0;
                        }

                        weather_d = CommonUtils.ConvertToDouble(dataPercentileRow["weather_d"]);
                        weather_n = CommonUtils.ConvertToDouble(dataPercentileRow["weather_n"]);
                        weather_score = 100 - ((((weather_d * 100) / weather_d_percentile) + ((weather_n * 100) / weather_n_percentile)) / 2);
                        if (weather_score < 0 || Double.IsNaN(weather_score))
                        {
                            weather_score = 0;
                        }

                        final_score = ((overspeedscore * overspeed_overall_wt) +
                            (harsh_score * harsh_overall_wt) +
                            (age_scoredb * age_overall_wt) +
                            (gender_scoredb * gender_overall_wt) +
                            (car_scoredb * car_overall_wt) + (weather_score * weather_wt) + (calling_score * call_wt)) / (overspeed_overall_wt + harsh_overall_wt + age_overall_wt + gender_overall_wt + car_overall_wt + weather_wt + call_wt);

                        #region prepare update query for update over speeds core and add in to InsertQuereies list
                        strSqlQry = new StringBuilder();
                        strSqlQry.Append(@"Update dr_daily_trip_parameter_wise_rating set overspeedscore =" + overspeedscore + " ,harsh_score=" + harsh_score + ",calling_score=" + calling_score + ",weather_score=" + weather_score + ",final_score=" + final_score + " where trip_id=" + trip_id + " and deviceno=" + deviceno
                            + " and recorddate='" + CommonUtils.GetDateFromatyyyy_MM_dd(_fromdate) + "';");
                        InsertQuereies.Add(strSqlQry.ToString());
                        strSqlQry = null;
                        #endregion
                    }

                nextday:

                    ConsoleLog("execute update score Quereies");
                    //update over speed score data in database
                    foreach (string qry in InsertQuereies)
                    {
                        objDAL.ExecuteSQL(qry);
                    }
                    #endregion
                }

                #region  calculate and update dr_overall_rating
                //DataTable dtTripOverallScore;

                //dtTripOverallScore = GetTripOverallScore(trip_id);
                //foreach (DataRow dataDeviceRow in dtTripOverallScore.Rows)
                //{
                //    deviceno = CommonUtils.ConvertToInt64(dataDeviceRow["deviceno"]);
                //    double distance = CommonUtils.ConvertToDouble(dataDeviceRow["distance"]);
                //    double final_score = CommonUtils.ConvertToDouble(dataDeviceRow["final_score"]);
                //    strSqlQry = new StringBuilder();
                //    strSqlQry.Append(@"INSERT INTO dr_overall_trip_rating  (trip_id,deviceno, distance, rating) VALUES (" + trip_id + "," + deviceno + "," + distance + "," + final_score + ");");
                //    objDAL.ExecuteSQL(strSqlQry.ToString());
                //    strSqlQry = null;
                //}

                strSqlQry = new StringBuilder();
                strSqlQry.Append(@"INSERT INTO dr_overall_trip_rating (trip_id,deviceno, distance,distance_d, distance_n,
                    overspeedlow_d, overspeed10_d, overspeed20_d, overspeed30_d, 
                    overspeedlow_n, overspeed10_n, overspeed20_n, overspeed30_n, 
                    overspeedscore, harsh_ha_d,harsh_hb_d,harsh_cornering_d, 
                    harsh_ha_n,harsh_hb_n,harsh_cornering_n,harsh_score,age_score, 
                    gender_score,car_score,weather_score,calling_score,rating,call_d,call_n,weather_d,weather_n
                    )   SELECT trip_id,deviceno, sum(distance),sum(distance_d), sum(distance_n),
                    sum(overspeedlow_d), sum(overspeed10_d), sum(overspeed20_d), sum(overspeed30_d), 
                    sum(overspeedlow_n), sum(overspeed10_n), sum(overspeed20_n), sum(overspeed30_n), 
                    avg(overspeedscore), sum(harsh_ha_d), sum(harsh_hb_d), sum(harsh_cornering_d), 
                    sum(harsh_ha_n), sum(harsh_hb_n), sum(harsh_cornering_n), avg(harsh_score), avg(age_score), 
					avg(gender_score),avg(car_score),avg(weather_score), 
                    avg(calling_score), avg(final_score),sum(call_d),sum(call_n),sum(weather_d),sum(weather_n)
                    FROM dr_daily_trip_parameter_wise_rating where trip_id=" + trip_id + " group by trip_id,deviceno;");
                objDAL.ExecuteSQL(strSqlQry.ToString());
                strSqlQry = null;

                #endregion


                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                // strSqlQry2 = null;

            }
        }

        public DataSet GetAllDevicesDistanceData(Int64 deviceno, DateTime? FromDate, DateTime? ToDate = null, DateTime? _dayFromDate = null,
                         DateTime? _dayToDate = null, DateTime? _nightFromDate = null, DateTime? _nightToDate = null, DateTime? _nightFromDate2 = null, DateTime? _nightToDate2 = null)
        {

            //day

            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,sum(coalesce(distance,0))/1000 as distance
            FROM  iot_hub where deviceno=" + deviceno + " and   (recorddatetime between '" + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_dayFromDate) + "' and '"
            + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_dayToDate) + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "distanceDay";

            //night

            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,sum(coalesce(distance,0))/1000 as distance
            FROM  iot_hub where deviceno=" + deviceno + " and  ((recorddatetime between '" + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightFromDate) + "' and '"
            + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightToDate) + "') or (recorddatetime between '" + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightFromDate2) + "' and '"
            + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightToDate2) + "')) group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "distanceNight";



            return dsData;
        }

        public DataTable GetDevicesSpeedCriteriaDataDay(Int64 deviceno, string pra_criteria, DateTime? FromDate, DateTime? ToDate)
        {
            //  dtData = new DataTable();
            if (pra_criteria != "")
            {
                if (pra_criteria.Contains("-"))
                {
                    String[] substrings = pra_criteria.Split('-');
                    string r1 = substrings[0].ToString();
                    string r2 = substrings[1].ToString();

                    strSqlQry = new StringBuilder();
                    strSqlQry.Append(@"SELECT deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as CriteriaVal FROM iot_hub where  deviceno=" + deviceno + " and  (recorddatetime between '" + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(FromDate) + "' and '"
                    + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(ToDate) + "') and (speed between " + CommonUtils.ConvertToDecimal(r1) + " and " + CommonUtils.ConvertToDecimal(r2) + ") group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
                    dtData = objDAL.FetchRecords(strSqlQry.ToString());
                    strSqlQry = null;
                }

            }
            return dtData;
        }

        public DataTable GetDevicesSpeedCriteriaDataNight(Int64 deviceno, string pra_criteria, DateTime? FromDate, DateTime? ToDate, DateTime? FromDate2, DateTime? ToDate2)
        {
            //  dtData = new DataTable();
            if (pra_criteria != "")
            {
                if (pra_criteria.Contains("-"))
                {
                    String[] substrings = pra_criteria.Split('-');
                    string r1 = substrings[0].ToString();
                    string r2 = substrings[1].ToString();

                    strSqlQry = new StringBuilder();
                    strSqlQry.Append(@"SELECT deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as CriteriaVal FROM iot_hub where  deviceno=" + deviceno + " and  ((recorddatetime between '" + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(FromDate) + "' and '"
                    + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(ToDate) + "') or (recorddatetime between '" + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(FromDate2) + "' and '"
                    + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(ToDate2) + "')) and (speed between " + CommonUtils.ConvertToDecimal(r1) + " and " + CommonUtils.ConvertToDecimal(r2) + ") group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
                    dtData = objDAL.FetchRecords(strSqlQry.ToString());
                    strSqlQry = null;
                }

            }
            return dtData;
        }

        public DataSet GetAllDevicesHAHBCorneringData(Int64 deviceno, DateTime? FromDate, DateTime? ToDate = null, DateTime? _dayFromDate = null,
                          DateTime? _dayToDate = null, DateTime? _nightFromDate = null, DateTime? _nightToDate = null, DateTime? _nightFromDate2 = null, DateTime? _nightToDate2 = null)
        {
            //            //Accross Day Night 
            //            dsData = new DataSet();
            //            strSqlQry = new StringBuilder();
            //            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as hardbraking
            //            FROM  iot_hub where coalesce(hardbraking,0)=1 and  (recorddatetime between '" + string.Format("{0:yyyy-MM-dd HH:mm:ss}", FromDate).ToString().Replace(".", ":") + "' and '"
            //            + string.Format("{0:yyyy-MM-dd HH:mm:ss}", ToDate).ToString().Replace(".", ":") + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            //            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            //            strSqlQry = null;
            //            dsData.Tables.Add(dtData);
            //            dsData.Tables[dsData.Tables.Count - 1].TableName = "hardbraking";

            //day

            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as hardbraking
            FROM  iot_hub where  deviceno=" + deviceno + " and  coalesce(hardbraking,0)=1 and  (recorddatetime between '" + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_dayFromDate) + "' and '"
            + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_dayToDate) + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "hardbrakingDay";

            //night

            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as hardbraking
            FROM  iot_hub where  deviceno=" + deviceno + " and  coalesce(hardbraking,0)=1 and  ((recorddatetime between '" + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightFromDate) + "' and '"
            + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightToDate) + "') or (recorddatetime between '" + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightFromDate2) + "' and '"
            + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightToDate2) + "')) group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "hardbrakingNight";

            //--------------------------------------------------------

            //            //Accross Day Night 
            //            strSqlQry = new StringBuilder();
            //            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as hardacceleration
            //            FROM  iot_hub where  deviceno=" + deviceno + " and  coalesce(hardacceleration,0)=1 and  (recorddatetime between '" + string.Format("{0:yyyy-MM-dd HH:mm:ss}", FromDate).ToString().Replace(".", ":") + "' and '"
            //            + string.Format("{0:yyyy-MM-dd HH:mm:ss}", ToDate).ToString().Replace(".", ":") + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            //            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            //            strSqlQry = null;
            //            dsData.Tables.Add(dtData);
            //            dsData.Tables[dsData.Tables.Count - 1].TableName = "hardacceleration";

            //Day 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as hardacceleration
            FROM  iot_hub where  deviceno=" + deviceno + " and  coalesce(hardacceleration,0)=1 and  (recorddatetime between '" + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_dayFromDate) + "' and '"
            + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_dayToDate) + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "hardaccelerationDay";


            //Night 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as hardacceleration
            FROM  iot_hub where  deviceno=" + deviceno + " and  coalesce(hardacceleration,0)=1 and  ((recorddatetime between '" + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightFromDate) + "' and '"
            + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightToDate) + "') or (recorddatetime between '" + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightFromDate2) + "' and '"
            + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightToDate2) + "')) group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "hardaccelerationNight";

            //--------------------------------------------------------


            //            //Accross Day Night 
            //            strSqlQry = new StringBuilder();
            //            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as cornerpacket
            //            FROM  iot_hub where  deviceno=" + deviceno + " and  coalesce(cornerpacket,0)=1 and  (recorddatetime between '" + string.Format("{0:yyyy-MM-dd HH:mm:ss}", FromDate).ToString().Replace(".", ":") + "' and '"
            //            + string.Format("{0:yyyy-MM-dd HH:mm:ss}", ToDate).ToString().Replace(".", ":") + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            //            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            //            strSqlQry = null;
            //            dsData.Tables.Add(dtData);
            //            dsData.Tables[dsData.Tables.Count - 1].TableName = "cornerpacket";

            //Day 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as cornerpacket
            FROM  iot_hub where  deviceno=" + deviceno + " and  coalesce(cornerpacket,0)=1 and  (recorddatetime between '" + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_dayFromDate) + "' and '"
            + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_dayToDate) + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "cornerpacketDay";


            //Night 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as cornerpacket
            FROM  iot_hub where  deviceno=" + deviceno + " and  coalesce(cornerpacket,0)=1 and  ((recorddatetime between '" + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightFromDate) + "' and '"
            + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightToDate) + "') or (recorddatetime between '" + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightFromDate2) + "' and '"
            + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightToDate2) + "')) group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "cornerpacketNight";

            //--------------------------------------------------------

            //--------------------------------------------------------

            //Day 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as weather
            FROM  iot_hub where  deviceno=" + deviceno + " and  coalesce(weather,0)=1 and  (recorddatetime between '" + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_dayFromDate) + "' and '"
            + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_dayToDate) + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "weatherDay";


            //Night 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as weather
            FROM  iot_hub where  deviceno=" + deviceno + " and  coalesce(weather,0)=1 and  ((recorddatetime between '" + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightFromDate) + "' and '"
            + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightToDate) + "') or (recorddatetime between '" + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightFromDate2) + "' and '"
            + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightToDate2) + "')) group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "weatherNight";

            //--------------------------------------------------------

            //--------------------------------------------------------

            //Day 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as mcall
            FROM  iot_hub where  deviceno=" + deviceno + " and  coalesce(mcall,0)=1 and  (recorddatetime between '" + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_dayFromDate) + "' and '"
            + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_dayToDate) + "') group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "callDay";


            //Night 
            strSqlQry = new StringBuilder();
            strSqlQry.Append(@"SELECT  deviceno,to_char(recorddatetime, 'DD-MM-YYYY') as rd,coalesce(count(*),0) as mcall
            FROM  iot_hub where  deviceno=" + deviceno + " and  coalesce(weather,0)=1 and  ((recorddatetime between '" + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightFromDate) + "' and '"
            + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightToDate) + "') or (recorddatetime between '" + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightFromDate2) + "' and '"
            + CommonUtils.GetDateFromatyyyy_MM_ddHHmmss(_nightToDate2) + "')) group by deviceno,to_char (recorddatetime, 'DD-MM-YYYY');");
            dtData = objDAL.FetchRecords(strSqlQry.ToString());
            strSqlQry = null;
            dsData.Tables.Add(dtData);
            dsData.Tables[dsData.Tables.Count - 1].TableName = "callNight";

            //--------------------------------------------------------

            return dsData;


        }

        public DataSet GetAllDevicesSpeedCriteriaData(Int64 deviceno, DateTime? FromDate, DateTime? ToDate = null, DateTime? _dayFromDate = null,
                         DateTime? _dayToDate = null, DateTime? _nightFromDate = null, DateTime? _nightToDate = null, DateTime? _nightFromDate2 = null, DateTime? _nightToDate2 = null, string overspeedlow = "", string overspeed10 = "", string overspeed20 = "", string overspeed30 = "")
        {

            dsData = new DataSet();
            if (overspeedlow != "")
            {
                dtData = GetDevicesSpeedCriteriaDataDay(deviceno, overspeedlow, _dayFromDate, _dayToDate);
                strSqlQry = null;
                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "overspeedlow_day";

                dtData = GetDevicesSpeedCriteriaDataNight(deviceno, overspeedlow, _nightFromDate, _nightToDate, _nightFromDate2, _nightToDate2);
                strSqlQry = null;
                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "overspeedlow_night";
            }

            if (overspeed10 != "")
            {
                dtData = GetDevicesSpeedCriteriaDataDay(deviceno, overspeed10, _dayFromDate, _dayToDate);
                strSqlQry = null;
                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "overspeed10_day";

                dtData = GetDevicesSpeedCriteriaDataNight(deviceno, overspeed10, _nightFromDate, _nightToDate, _nightFromDate2, _nightToDate2);
                strSqlQry = null;
                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "overspeed10_night";
            }

            if (overspeed20 != "")
            {
                dtData = GetDevicesSpeedCriteriaDataDay(deviceno, overspeed20, _dayFromDate, _dayToDate);
                strSqlQry = null;
                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "overspeed20_day";

                dtData = GetDevicesSpeedCriteriaDataNight(deviceno, overspeed20, _nightFromDate, _nightToDate, _nightFromDate2, _nightToDate2);
                strSqlQry = null;
                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "overspeed20_night";
            }

            if (overspeed30 != "")
            {
                dtData = GetDevicesSpeedCriteriaDataDay(deviceno, overspeed30, _dayFromDate, _dayToDate);
                strSqlQry = null;
                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "overspeed30_day";

                dtData = GetDevicesSpeedCriteriaDataNight(deviceno, overspeed30, _nightFromDate, _nightToDate, _nightFromDate2, _nightToDate2);
                strSqlQry = null;
                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "overspeed30_night";
            }

            return dsData;
        }

        public decimal GetParameterCateriaVal(String filter, string tableNM, string colNM, DataSet ds)
        {
            decimal result = 0;
            DataRow[] dr;
            dr = ds.Tables[tableNM].Select(filter);
            foreach (DataRow dtrow in dr)
            {
                if (dtrow[colNM] != null)
                {
                    result = CommonUtils.ConvertToDecimal(dtrow[colNM]);
                }
            }
            dr = null;

            return result;

        }

        public decimal GetAgeScore(string age_criteria, decimal age_criteria_wt, int age, out bool age_criteria_val)
        {
            if (age_criteria != "")
            {
                if (age_criteria.Contains("-"))
                {
                    String[] substrings = age_criteria.Split('-');
                    string r1 = substrings[0].ToString();
                    string r2 = substrings[1].ToString();
                    if (age >= Convert.ToInt32(r1) && age <= Convert.ToInt32(r2))
                    {
                        age_criteria_val = true;
                        return 100 * age_criteria_wt;
                    }
                }
            }
            age_criteria_val = false;
            return 0;
        }

        public double GetPercentile(double[] sequence, double excelPercentile)
        {
            try
            {
                if (sequence.Length > 0)
                {
                    Array.Sort(sequence);
                    int N = sequence.Length;
                    double n = (N - 1) * excelPercentile + 1;
                    // Another method: double n = (N + 1) * excelPercentile;
                    if (n == 1d) return sequence[0];
                    else if (n == N) return sequence[N - 1];
                    else
                    {
                        int k = (int)n;
                        double d = n - k;
                        return sequence[k - 1] + d * (sequence[k] - sequence[k - 1]);
                    }
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ConsoleLog(string msg)
        {
            Console.WriteLine(msg);
        }

        public void ConsoleLogwithDatetime(string msg)
        {
            Console.WriteLine(msg + DateTime.Now.ToString());
        }

        public int SyncTripLogData(Int64 trip_id, Decimal m_distance, Int64 deviceno, string strTSdate, string strTEdate)
        {
            try
            {
                ////
                string filePath = System.Configuration.ConfigurationManager.AppSettings["TripLogPath"].ToString();
                if (filePath == "")
                {
                    ConsoleLogwithDatetime("Trip Log file sync desiable");
                    return 3; //Trip Log file sync desiable
                }
                string Apiurl = System.Configuration.ConfigurationManager.AppSettings["Apiurl"].ToString();
                string ApiRequestTimeout = System.Configuration.ConfigurationManager.AppSettings["ApiRequestTimeout"].ToString();

                #region get distance
                decimal dist_diff = 0;
                strSqlQry = new StringBuilder();
                strSqlQry.Append(@"SELECT  round(coalesce(sum(distance),0)/1000,1) as distance FROM  iot_hub where deviceno=" + deviceno + "  and  (recorddatetime between '" + strTSdate + "' and '" + strTEdate + "');");
                string s_distance = objDAL.FetchSingleRecord(strSqlQry.ToString());
                strSqlQry = null;
                if (Convert.ToDecimal(s_distance) > m_distance)
                {
                    dist_diff = Convert.ToDecimal(s_distance) - m_distance;
                }
                else
                {
                    dist_diff = m_distance - Convert.ToDecimal(s_distance);
                }
                #endregion

                if (dist_diff <= 1 && Convert.ToDecimal(s_distance)> 0)  //if distance diff more than 1 then sync file data
                {
                    ConsoleLogwithDatetime("not required to sync server recived data and mobile data match");
                    return 2;  //-not required to sync server recived data and mobile data match
                }

                filePath = filePath + trip_id;

                #region get data from log file
                if (Directory.Exists(filePath))
                {
                    string[] fileEntries = Directory.GetFiles(filePath);
                    foreach (string fileName in fileEntries)
                    {
                        if (System.IO.File.Exists(fileName))
                        {
                            using (System.IO.StreamReader sr = System.IO.File.OpenText(fileName))
                            {

                                ConsoleLogwithDatetime("Request to  server " + fileName);

                                String input = sr.ReadToEnd();

                                string json;

                                if (input.LastIndexOf(',') < input.LastIndexOf(']'))
                                {
                                    json = "{\"data\":" + input.ToString().Remove(input.LastIndexOf(','), 1) + "}";
                                }
                                else
                                {
                                    json = "{\"data\":" + input.ToString().Remove(input.LastIndexOf(','), 1) + "]}";
                                }
                                // ConsoleLog(json);

                                #region httpWebRequest

                                var httpWebRequest = (HttpWebRequest)WebRequest.Create(Apiurl);

                                // Set the  'Timeout' property of the HttpWebRequest to 10 min.

                                httpWebRequest.Timeout = 600000;
                                if (ApiRequestTimeout != "")
                                {
                                    httpWebRequest.Timeout = Convert.ToInt32(ApiRequestTimeout);
                                }

                                httpWebRequest.ContentType = "application/json";
                                httpWebRequest.Method = "POST";

                                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                                {
                                    streamWriter.Write(json);
                                    streamWriter.Flush();
                                    streamWriter.Close();
                                }

                                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                                {
                                    var result = streamReader.ReadToEnd();

                                    ConsoleLogwithDatetime("Response" + result);

                                    if (result.ToString().Contains("200") && result.ToString().Contains("ok"))
                                    {
                                        ConsoleLogwithDatetime("successfully sync log data to server");
                                        return 1; //successfully sync log data to server
                                    }
                                    else
                                    {
                                        ConsoleLogwithDatetime("fail to sync error ");
                                        return 0; //fail to sync error
                                    }

                                }

                                #endregion

                            }

                        }

                    }
                }
                #endregion

                ConsoleLogwithDatetime("fail to sync error ");
                return 0; //fail to sync error

            }
            catch (Exception ex)
            {
                ConsoleLogwithDatetime("fail to sync error ");
                return 0; //fail to sync error

            }
        }

    }
}
