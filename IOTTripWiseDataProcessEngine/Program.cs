﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace IOTTripWiseDataProcessEngine
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                while (true)
                {
                    try
                    {

                        Console.WriteLine("start Process  " + DateTime.Now.ToString());
                        using (DailyDataProcess DailyDataProcess = new DailyDataProcess())
                        {
                            DailyDataProcess.TripDataProcessEngine();
                        }
                        Console.WriteLine("end Process  " + DateTime.Now.ToString());

                        //System.Threading.Thread.Sleep(600000);
                        System.Threading.Thread.Sleep(60000);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("process error: " + ex.ToString());
                    }
                    finally
                    {
                        Console.WriteLine("finish");
                    //    Console.ReadLine();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("process error at while loop: " + ex.ToString());
                Console.ReadLine();
            }

        }
    }
}
